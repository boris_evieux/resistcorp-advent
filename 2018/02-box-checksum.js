const fs = require( 'fs' )

function checkLineP1(line, state){
	const check = {};
	for(let char of line){
		if(!check[char])
			check[char] = 1;
		else
			check[char]++;
	}
	let hasTwo = false,
		hasThree = false;
	for(let char in check){
		if(check[char] === 2)
			hasTwo = true;
		if(check[char] === 3)
			hasThree = true;
	}
	if(hasTwo)
		state.twos ++
	if(hasThree)
		state.threes ++
}
function checkLinesP2(line1, line2){
	let ret = "",
		common = "",
		different = "",
		differences = 0,
		lastDiffIdx = 0;
	if(line1.length !== line2.length)
		return "";//useless, but hey!

	for(let i = 0; i < line1.length; ++i){
		if(line1[i] === line2[i]){
			ret += line1[i];
			common += line1[i];
			different += "_";
		}else{
			differences ++
			lastDiffIdx = i
			common += "X";
			different += "^";
		}
	}
	const doLog = !(differences <= 2)
	if(differences === 1){
		console.log("these lines DO MATCH :")
	}else{
		ret = ""
		console.assert(doLog, "these lines DON't MATCH with", differences," diffferences :")
		console.assert(doLog, "    " + common)
	}
	console.assert(doLog, "    " + line1)
	console.assert(doLog, "    " + line2)
	console.assert(doLog, "    " + different)
	return ret;
}
function main(){
	const 	data = fs.readFileSync('02-box-checksum.txt').toString().split('\n'),
			state = {
				twos : 0,
				threes: 0
			};
	for( let line of data ){
		checkLineP1(line, state);
	}
	console.log("=======Phase 1========")
	console.log(`checked ${data.length} lines`)
	console.log("got state :")
	console.dir(state)
	console.log("result for phase 1 is :", state.twos * state.threes)
	console.log("=======Phase 2========")
	for (let i = 0; i < data.length; i++) {
		let line1 = data[i]
		// O( N² / 2 - N ) ;)
		for (let j = i+1; j < data.length; j++) {
			let line2 = data[j]
			let result = checkLinesP2(line1, line2)
			if(result){
				console.log("result for phase 2 is :", result)
				return true;
			}
		}
	}
	return false
}

if( main() )
	console.log( "finished with result" );
else
	console.log( "finished without result" );