const 	fs = require( 'fs' ),
		//File was sorted by hand (thanks sublime :))
		data = fs.readFileSync('04-Guards.txt').toString().split('\r\n'),
		lineRegexp = /\[(\d\d\d\d-\d\d-\d\d \d\d:\d\d)\] (.*)/,
		GuardProto = {
			id: -1,
			minuteCounter : 0,
			minutes : []//only for readability, this array is going to be replaced
		};
//functions
function createGuard(id){
	const ret = Object.create(GuardProto);
	ret.id = id;
	ret.minutes = createMinuteCounter();
	return ret;
}
function createMinuteCounter(){
	const ret = new Array(60);
	ret.fill(0);
	return ret;
}
//comparator
const guardSorter = (a, b) => a.minuteCounter - b.minuteCounter;
function main(){
	const 	guards = {};
	let guardArray = [];
	let counter = 0,
		fellAsleep = null,
		currentGuard = null;
	for(let line of data){
		counter++;
		let match = lineRegexp.exec(line),
			date = new Date(Date.parse(match[1])),
			event = match[2];

		switch(event){
			case 'falls asleep':
				if(!currentGuard)
					console.error("error, no active guard at line", counter);
				fellAsleep = date.getMinutes();
			break;
			case 'wakes up':
				if(!currentGuard)
					console.error("error, no active guard at line", counter)
				if(fellAsleep === null)
					console.error("error, active not asleep ")
				let time = date.getMinutes();
				console.log("guard", currentGuard.id, "was asleep from", time, "to", fellAsleep)
				if(time < fellAsleep) time += 60;
				for(let i = fellAsleep; i < time; ++i){
					++ currentGuard.minutes[i % 60];
					++ currentGuard.minuteCounter;
				}
				felleAsleep = null;
			break;
			default:
				let guardId = parseInt(event.substr(7), 10),
					guard = guards[guardId];
				if(!guard){
					guard = guards[guardId] = createGuard(guardId);
					guardArray.push(guard)
				}
				currentGuard = guard;
		}
	}
	guardArray = guardArray.sort(guardSorter);
	const 	bestGuard = guardArray.pop(),
			minutes  = bestGuard.minutes;
	let bestMin = -1,
		best = 0,
		bestGuardID = bestGuard.id;
	console.log("guard", bestGuard.id, "is our best bet : ", minutes.join(", "));
	for(let min = 0; min < 60; ++min){
		const times = minutes[min]
		if(times >= best){
			best = times;
			bestMin = min;
		}
	}
	console.log("best minute seems to be", bestMin, "(", best,")");
	console.log("answer for phase 1:", bestMin * bestGuardID);
	for(let guard of guardArray){
		console.log("guard", guard.id, guard.minutes.join(", "));
		for(let min = 0; min < 60; ++min){
			const times = guard.minutes[min]
			if(times >= best){
				best = times;
				bestMin = min;
				bestGuardID = guard.id;
			}
		}
	}
	console.log("but in fact, guard", bestGuardID, "was asleep", best, "times on minute", bestMin);
	console.log("answer for phase 1:", bestMin * bestGuardID);
}
main();