package main

import "fmt"
const input int = 409551
// const input int = 92510
func main() {
	fmt.Println("begin !")
	theTable := make([]int, 2, 2000000000)
	comparison := slicedInput()
	elf1:=0
	elf2:=1
	theTable[elf1] = 3
	theTable[elf2] = 7

	doneA, doneB := false, false
	for ; !doneA || !doneB; {
		score1 := theTable[elf1]
		score2 := theTable[elf2]
		newScore := score1 + score2
		if newScore >=10 {
			theTable = append(theTable, newScore / 10)
			if !doneB && compare(comparison, theTable) {			
				fmt.Println("answer for phase 2")
				fmt.Println(len(theTable) - len(comparison))
				fmt.Printf("%v", theTable[len(theTable)-10:])
				fmt.Println()

				doneB = true
			}
			theTable = append(theTable, newScore % 10)
		}else{
			theTable = append(theTable, newScore)
		}
		elf1 = (elf1 + score1 + 1) % len(theTable)
		elf2 = (elf2 + score2 + 1) % len(theTable)
		// printTurn(theTable, elf1, elf2)
		if(len(theTable) == input +10){
			fmt.Println("answer for phase 1")
			for i := 0; i <10; i++ {
				fmt.Printf("%d", theTable[input+i])
			}
			fmt.Println()
			doneA = true
		}
		if len(theTable) > 2000000000 {
			fmt.Println("TIME OUT")
			doneB = true
		}
		if !doneB && compare(comparison, theTable) {			
			fmt.Println("answer for phase 2")
			fmt.Println(len(theTable) - len(comparison))
			fmt.Printf("%v", theTable[len(theTable)-10:])
			fmt.Println()

			doneB = true
		}
	}
	fmt.Println("done")
}
func compare(comp, src []int) bool{
	lenA, lenB := len(comp), len(src)
	if lenA > lenB{
		return false
	}
	compB := src[lenB-lenA:lenB]
	for i, v := range(compB) {
		if comp[i] != v {
			return false
		}
	}
	return true
}
func slicedInput()[]int{
	ret := make([]int, 0)
	for val := input; val > 0; {
		ret = append([]int{val % 10}, ret...)
		val /= 10
	}
	fmt.Println("sliced input :")
	fmt.Println(ret)
	return ret;
}
func printTurn(data []int, elf1, elf2 int) {
	str := ""
	for i, score := range(data) {
		if i == elf1 {
			str += fmt.Sprintf("[%d]", score)
		}else if i == elf2{
			str += fmt.Sprintf("(%d)", score)
		}else{
			str += fmt.Sprintf(" %d ", score)
		}
	}
	fmt.Println(str)
}