import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ElvesAndFabric {
	static final Pattern CLAIM_PATTERN = Pattern.compile("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)");
	static class Claim{
		final int id;
		final int top;
		final int left;
		final int width;
		final int height;
		public boolean conflicted;
		Claim(String line){
			Matcher matcher = CLAIM_PATTERN.matcher(line);
			boolean find = matcher.find();
			id = Integer.valueOf(matcher.group(1));
			top = Integer.valueOf(matcher.group(3));
			left = Integer.valueOf(matcher.group(2));
			width = Integer.valueOf(matcher.group(4));
			height = Integer.valueOf(matcher.group(5));
		}
		public int bottom() {
			return top + height;
		}
		public int right() {
			return left + width;
		}
	}
	enum SquareInchState{FREE, CLAIMED, CONFLICTED}
	static class Cloth{
		final AtomicInteger conflicteds = new AtomicInteger(0);
		final SquareInch[][] fabric;
		Cloth(int width, int height){
			fabric = new SquareInch[width][height];
			for(int col = 0; col < width; ++col)
				for(int line = 0; line < height; ++line)
					fabric[col][line] = new SquareInch();
				
		}
		void claim(Claim claim){
			for(int x = claim.left; x < claim.right(); ++x){
				for(int y = claim.top; y < claim.bottom(); ++y){
					if(fabric[x][y].claim(claim))
						conflicteds.incrementAndGet();
				}
			}
		}
	}	
	static class SquareInch{
		SquareInchState state = SquareInchState.FREE;
		private Claim m_firstClaim;
		/**
		 * @return true if this claim creates a new conflict
		 */
		synchronized boolean claim(Claim claim){
			switch(state){
			case FREE:
				m_firstClaim = claim;
				state = SquareInchState.CLAIMED;
				break;
			case CLAIMED:
				state = SquareInchState.CONFLICTED;
				m_firstClaim.conflicted = true;
				claim.conflicted = true;
				return true;
			case CONFLICTED:
				claim.conflicted = true;
				break;
			default:
			}
			return false;
		}
	}
	public static void main(String[] args) {
		List<Claim> claims = new ArrayList<>(1233);
		int maxX = 0,
			maxY = 0;
		try(FileReader reader = new FileReader("03-ElvesAndFabric.txt")){
			Scanner sc = new Scanner(reader);
			while(sc.hasNextLine()){
				Claim claim = new Claim(sc.nextLine());
				claims.add(claim);
				if(claim.bottom() > maxY)
					maxY = claim.bottom();
				if(claim.right() > maxX)
					maxX = claim.right();
			}
		} catch (FileNotFoundException e) {
			System.err.println("didn't find file");
		} catch (IOException e) {
			System.err.println("error closing file");
		}
		
		Cloth cloth = new Cloth(maxX, maxY);
		long count = //not used
			claims
				.parallelStream()///do the claiming in parallel
					.peek(cloth::claim)//claim
						.count();//we just close the stream to commit all the changes
		List<Integer> freeIDs =
			claims.parallelStream()
				.filter(claim -> !claim.conflicted)
					.map(claim -> claim.id)//get ids
						.collect(Collectors.toList());//problem specifically tells us there's only one 
	
		System.out.println("the total number of conflicted zones was :");
		System.out.println(cloth.conflicteds.get());
		System.out.println("claim not conflicted :");
		freeIDs.forEach(System.out::println);
	}

}
