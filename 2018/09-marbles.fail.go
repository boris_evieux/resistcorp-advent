package main

import "fmt"
//input : 441 players; last marble is worth 71032 points

const numPlayers int = 441
const lastMarble int = 71032
// const numPlayers int = 9
// const lastMarble int = 50

func main() {
	circle := append(make([]int, 0, lastMarble+1), 0)
	scores := [numPlayers]int{}
	currentMarbleI := 1
	fmt.Println("[-] ( 0)")
	// printCircle(circle, 0, 1)
	c := make(chan int)
	go func(){
		for marble := 1; marble <= lastMarble; marble++ {
			player := ( marble -1) % numPlayers
			is23   := ( marble % 23 ) == 0
			if is23 {
				scores[player] += marble
				toRemove := currentMarbleI - 7
				newCircle, points := remove(circle, toRemove)
				circle = newCircle
				scores[player] += points
				currentMarbleI = toRemove
			}else{
				insertPoint := currentMarbleI + 2
				newCircle, newI := insert(circle, insertPoint, marble)
				currentMarbleI = newI
				circle = newCircle
			}
			if marble < 50{
				printCircle(circle, player, currentMarbleI)
			}
		} 
		hiscore := -1
		for _, score := range(scores) {
			if score > hiscore {
				hiscore = score
			}
		}
		fmt.Printf("%v\n", scores)
		c<-hiscore
	}()
	fmt.Printf("hiscore : %d\n", <-c)
}
func printCircle(circle []int, player int, currentMarbleI int) {
	fmt.Printf("[%2d]", player+1)
	for i, val := range(circle){
		if i == currentMarbleI {
			fmt.Printf(" (%2d)", val)
		}else{
			fmt.Printf("  %2d ", val)
		}
	} 
	fmt.Printf("\n")
}
func insert(circle []int, index, value int) ([]int, int) {
	for ; index < 0; index += len(circle) {}
	for ; index >= len(circle)+1; index -= len(circle){}

	ret := append(circle, 0)
	for i := len(circle); i > index; i-- {
		ret[i] = circle[i-1]
	}
	ret[index] = value
	return ret, index
}

func remove(circle []int, index int) ([]int, int) {
	for ; index < 0; index += len(circle){}
	for ; index >= len(circle); index -= len(circle){}


	val := circle[index]
	for i := index; i < len(circle) -1; i++ {
		circle[i] = circle[i+1]
	}
	ret := circle[:len(circle)-1]
	return ret, val
}