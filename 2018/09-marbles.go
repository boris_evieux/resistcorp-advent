package main

import "fmt"
import "container/ring"
//input : 441 players; last marble is worth 71032 points

const NUM_PLAYERS int = 441
const LAST_MARBLE int = 71032
// const NUM_PLAYERS int = 13
// const LAST_MARBLE int = 7999

func main() {
	c := make(chan int, 2)
	go play(NUM_PLAYERS, LAST_MARBLE, c)
	go play(NUM_PLAYERS, LAST_MARBLE * 100, c)

	fmt.Printf("hiscore phase 1: %d\n", <-c)
	fmt.Printf("hiscore phase 2: %d\n", <-c)
}
func play(numPlayers, lastMarble int, c chan int){
	circle := ring.New(1)
	circle.Value = 0
	scores := make([]int, numPlayers)
	for marble := 1; marble <= lastMarble; marble++ {
		player := ( marble -1) % numPlayers
		is23   := ( marble % 23 ) == 0
		if is23 {
			circle = circle.Move(-8)
			removed := circle.Unlink(1)
			scores[player] += marble + removed.Value.(int)
			circle = circle.Next()
		}else{
			newRing := ring.New(1)
			newRing.Value = marble
			circle.Next().Link(newRing)
			circle = newRing
		}
	} 
	hiscore := -1
	for _, score := range(scores) {
		if score > hiscore {
			hiscore = score
		}
	}
	fmt.Printf("%v\n", scores)
	c<-hiscore
}
func printCircle(circle *ring.Ring){
	circle.Do(func( v interface{}){fmt.Printf(" %v", v)} )
	fmt.Println()
}