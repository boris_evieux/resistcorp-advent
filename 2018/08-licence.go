package main

import "io/ioutil"
import "strings"
import "strconv"
import "fmt"

type node struct{
	children []*node
	meta []int
	value int
}
func readTree(ints []int) (*node, int){
	index := 0
	return readSubTree(ints, &index, 0)
}

//returns the node, and sum of meta
func readSubTree(ints []int, index *int, depth int) (*node, int){
	totalMetadata := 0
	numChildren := ints[*index]
	(*index)++
	numMeta := ints[*index]
	(*index)++
	ret := node{make([]*node, numChildren), make([]int, numMeta), 0}

	fmt.Printf(" %s %d %d \n", strings.Repeat("> ", depth), numChildren, numMeta)
	for i := 0; i < numChildren; i++ {
		//index is a pointer
		child, meta := readSubTree(ints, index, depth + 1)
		totalMetadata += meta
		ret.children[i] = child
	}

	for i := 0; i < numMeta; i++ {
		meta := ints[*index]
		(*index)++
		ret.meta[i] = meta
		totalMetadata += meta
	}
	ret.calcValue()
	fmt.Printf(" %s %d %v \n", strings.Repeat("< ", depth), totalMetadata, ret)

	return &ret, totalMetadata
}

func (n *node) calcValue(){
	val := 0
	if len(n.children) == 0 {
		for _, meta := range(n.meta){
			val += meta
		}
	}else{
		for _, meta := range(n.meta){
			if(meta > 0 && meta <= len(n.children)){
				val += n.children[meta-1].value
			}
		}
	}
	n.value = val
}

func main(){
	ints, ok := readFile()
	if !ok {
		panic("error reading file")
	}
	node, totalMetadata := readTree(ints);
	fmt.Printf("=============== phase 1 ==============\n")
	fmt.Printf("total metadata : %d\n", totalMetadata)
	fmt.Printf("===============         ==============\n")

	fmt.Printf("=============== phase 2 ==============\n")
	fmt.Printf("root value : %d\n", node.value)
	fmt.Printf("===============         ==============\n")
}
func readFile() ([]int, bool) {
	bytes, err := ioutil.ReadFile("08-licence.txt")
	if err != nil{
		fmt.Printf("error reading file : %v", err)
		return nil, false
	}
	strs := strings.Split(string(bytes), " ")
	ret := make([]int, len(strs))

	for i, s := range(strs){
		ret[i], err = strconv.Atoi(s)
		if err != nil {
			fmt.Printf("can't parse %s\n", s)
			return nil, false
		}
	}
	return ret, true
}
