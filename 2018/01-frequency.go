package main

import "os"
import "fmt"
import "bufio"
import "strconv"

type frequencyOp struct{
	isAdd bool
	value int
}

func main() {
	const phase int = 2;
	list, ok := readFile();
	if(!ok){
		panic("error reading file")
	}

	freq := 0


	if phase == 1 {
		for _, op := range(list){
			apply(op, &freq)
			//fmt.Printf(" : %d", freq)
		}
		fmt.Printf("ending freq : %d", freq)
	}else if phase == 2{
		theMap := make(map[int]bool)
		index  := 0
		length := len(list)
		for wasHere := false; !wasHere; index++{
			op := list[index%length]
			apply(op, &freq)
			_, present := theMap[freq]
			wasHere = present
			theMap[freq] = true
		}
		fmt.Printf("first repeated freq : %d after %d", freq, index)
	}
}
func apply(op frequencyOp, freq *int){
 	// fmt.Printf("applying %v to %d", op, *freq)
	if op.isAdd{
		*freq += op.value
	}else{
		*freq -= op.value
	}
	// fmt.Printf(" ... result %d\n", *freq)
}
func readFile() ([]frequencyOp, bool) {
	file, err := os.Open("01-frequency.txt")
	if err != nil {
		fmt.Println(err)
		return nil, false
	}
	defer file.Close()

	ret := make([]frequencyOp, 0, 2000)//meh

	scanner := bufio.NewScanner(file)
	line := 0

	for scanner.Scan() {
		line ++
		str := scanner.Text()
		sig := str[0]
		num, err := strconv.Atoi(str[1:])
		if err != nil {
			fmt.Printf("PANIC! couldn't parse num at line %d", sig, line)
			fmt.Println(err)
			return nil, false
		}
		if sig == '+' || sig == '-' {
			op := frequencyOp{sig == '+', num}
			//fmt.Printf("operation : %v\n", op)
			ret = append(ret, op)
		}else{
			fmt.Printf("PANIC! not a known op : %v at line %d", sig, line)
			return nil, false
		}
	}
	fmt.Printf("read file. we have %d ops.\n", len(ret))
	return ret, true
}
