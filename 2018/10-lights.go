package main

import "io/ioutil"
import "strings"
import "strconv"
import "regexp"
import "fmt"
import "os"
import "os/exec"
import "bufio"

type v2 struct{
	x, y int
}
type pointLight struct{
	pos v2
	vel v2
}
type boundingBox struct{
	min v2
	ext v2
}
func (pl *pointLight) play(frame int) v2{
	return v2{ pl.pos.x + frame * pl.vel.x, pl.pos.y + frame * pl.vel.y}
}
func (pt v2) remove(pt2 v2) v2{
	return v2{pt.x - pt2.x, pt.y - pt2.y}
}
func (bb *boundingBox) contains(pt v2) bool{
	if pt.x < bb.min.x || pt.y < bb.min.y {
		return false
	}
	if pt.x >= bb.min.x + bb.ext.x || pt.y >= bb.min.y + bb.ext.y {
		return false
	}
	return true
}
func readV2(s string) (v2, bool){
	strs := strings.Split(s, ", ")
	x, eX := strconv.Atoi(strings.Trim(strs[0], " "))
	y, eY := strconv.Atoi(strings.Trim(strs[1], " "))
	if eX != nil || eY != nil {
		return v2{}, false
	}
	return v2{x, y}, true
}
func main(){
	points, ok := readFile()
	if !ok {
		panic("error reading file")
	}
	fmt.Printf("%d lines\n", len(points))
	fmt.Printf("%v \n", points[0:5])
    frame:=10000
    lastBB := boundingBox{v2{}, v2{100000000,1000000000}}
    smaller := true
	for ; smaller; frame++ {
        //playFrame(points)
        //readIn()
        bb := computeBB(points, frame)
      	smaller = bb.ext.x < lastBB.ext.x || bb.ext.y < lastBB.ext.y
		fmt.Printf("bb %v at frame %d\n", bb, frame)
        lastBB = bb
	}
	bb := lastBB
	bb.min.x -= 10
	bb.min.y -= 10
	bb.ext.x += 20
	bb.ext.y += 20
	//frame -= 10
	for cmd, prevcmd := 'n', 'n'; cmd != 'x'; cmd = readIn(){
		if cmd == '\n' {
			cmd = prevcmd
		}
		switch cmd {
		case 'n' :
			frame++
		break;
		case 'p' :
			frame--
		break;
		case '+' :
			bb.ext.x++
			bb.ext.y++
		break;
		case '-' :
			bb.ext.x--
			bb.ext.y--
		break;
		}
		prevcmd = cmd
		playFrame(points, bb, frame)
		fmt.Printf("displaying bb : %v at frame %d\n", bb, frame)
	}
}
func computeBB(points []pointLight, frame int) boundingBox{
	min := v2{50000,50000}
	max := v2{-50000,-50000}
	for _, pt := range(points) {
		pos := pt.play(frame)
		if(pos.x<min.x){ min.x = pos.x }
		if(pos.y<min.y){ min.y = pos.y }
		if(pos.x>max.x){ max.x = pos.x }
		if(pos.y>max.y){ max.y = pos.y }
	}
	ext := v2{max.x-min.x, max.y-min.y}
	return boundingBox{min, ext}
}
func playFrame(points []pointLight, bb boundingBox, frame int){
		cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested 
        cmd.Stdout = os.Stdout
        cmd.Run()
        out := make([][]byte, bb.ext.y)
        for i, _ := range(out) {
        	out[i] = []byte(strings.Repeat(".", bb.ext.x))
        }
        for _, pt := range(points) {
        	pos := pt.play(frame).remove(bb.min)
        	if pos.x < bb.ext.x && pos.y < bb.ext.y && pos.x >=0 && pos.y >= 0{
        		defer func(){
        			if r:= recover(); r != nil {
        				fmt.Println("recovered from error : ")
        				fmt.Printf("pos %v don't fit in %v\n", pos, bb)
        			}
        		}()
	       		out[pos.y][pos.x] = byte('#')
        	}
        }
        for _, str := range(out) {
        	fmt.Println(string(str))
        }
}
func readIn() rune{
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter text: ")
	text, _, _ := reader.ReadRune()
	return text
}
func readFile() ([]pointLight, bool) {
	reg := regexp.MustCompile("position=<(.*)> velocity=<(.*)>")
	bytes, err := ioutil.ReadFile("10-lights.txt")
	if err != nil{
		fmt.Printf("error reading file : %v", err)
		return nil, false
	}
	strs := strings.Split(string(bytes), "\n")
	ret := make([]pointLight, len(strs))

	for i, s := range(strs){
		subs := reg.FindAllStringSubmatch(s, -1)
		pos, okPos := readV2(subs[0][1])
		vel, okVel := readV2(subs[0][2])
		if !okPos || !okVel {
			fmt.Printf("error reading line : %v %v\n", okVel, okPos)
			return nil, false
		}
		ret[i] = pointLight{pos, vel}
		//ret[i].play(10200)
	}
	return ret, true
}
