package main

import "fmt"
import "runtime"
import "strings"
import "io/ioutil"

func main() {
	state, rules, ok := readFile()
	if !ok {
		panic("input couldn't be read !")
	}
	stateLength := len(state)
	lastScore := -1
	currentDecal := 0
	fmt.Printf(" - : %150s %d\n", state + strings.Repeat(" ", (150-stateLength +currentDecal)/2), currentDecal)
	fmt.Println()
	gens := 0

	for score := 0; score != lastScore && gens < 500; {
		gens++
		newState, decal := advance(state, rules)
		currentDecal += decal
		lastScore = score
		score = estimate(newState, currentDecal)
		if newState == "." + state {
			increment := score - lastScore
			fmt.Println("from here on, we are drifting to the left")
			fmt.Printf("incrementing the score by %d every time\n", increment)
			remainingGens := 50000000000 - gens//fiftyBillion!
			fmt.Printf("the end score would be %d\n", score + remainingGens * increment)
			panic("done!")
		}else{
			state = newState
			// indentN := 50 + currentDecal
			// if indentN < 0 {
				// indentN = 0
			// }
			// indent := strings.Repeat(" ", indentN)
			fmt.Printf("%2d : %s %d %d %d\n", gens, /*indent +*/ state, currentDecal, score, lastScore)
			if gens % 10 == 0 {
				runtime.Gosched()
			}
		}
	}
	fmt.Println()
	fmt.Printf("%2d : %s\n", gens, state)
	fmt.Printf("after %d gens, %d\n", gens, lastScore)
}
func advance(state string, rules map[string]bool) (string, int){
	internalState := "..." + state + "..."
	length := len(state)
	output := make([]byte, length+2)

	for i := 0; i < length+2; i++ {
		input := internalState[i:i+5]
		var value rune
		ruleResult, _ := rules[input]
		if ruleResult {
			value = '#'
		}else{
			value = '.'
		}
		output[i] = byte(value)
	}

	decal := -1
	start := 0
	end := len(output)
	if output[0] != byte('#') {
		decal = 0
		start = 1
	}
	if output[len(output)-1] != byte('#') {
		end = len(output)-1
	}
	return string(output[start:end]), decal
}
func estimate(state string, start int) int{
	ret := 0
	for i, val := range(state) {
		if(val == '#'){
			ret += i + start
		}
	}
	return ret
}
func readFile() (string, map[string]bool, bool) {
	bytes, err := ioutil.ReadFile("12-plants.txt")
	if err != nil{
		fmt.Printf("error reading file : %v", err)
		return "", nil, false
	}
	lines := strings.Split(string(bytes), "\r\n")
	state := strings.Split(lines[0], "initial state: ")[1]

	rules := make(map[string]bool)

	for i := 2; i < len(lines); i++ {
		line := strings.Split(lines[i], " => ")
		state, result := line[0], line[1]
		rules[state] = result == "#"
	}
	return state, rules, true
}