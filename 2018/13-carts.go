package main

import "fmt"
import "strings"
import "sort"
import "io/ioutil"
import "os"
import "os/exec"
import "bufio"

func main() {
	tracks, carts, ok := readFile()
	if !ok {
		panic("can't read file")
	}
	// fmt.Printf("%d lines\n", len(tracks))
	// for _, track := range(tracks) {
	// 	fmt.Println(track)
	// }
	// for _, cart := range(carts) {
	// 	fmt.Printf("cart : %v\n", cart)
	// }

	turn := 0
	for ; len(carts) > 1; turn++ {
		// cls()
		// print(tracks, carts)
		// fmt.Printf("turn %d : carts are %v\n", turn, carts)
		collisions := make([]int, 0, len(carts))
		sort.Sort(carts)
		for i, cart := range(carts) {
			move(cart, tracks)
			for j, otherCart := range(carts) {
				if i != j && otherCart.x == cart.x && otherCart.y == cart.y {
					fmt.Printf("carts %v and %v collided at turn %d\n", cart, otherCart, turn)
					collisions = append(collisions, i, j)
				}
			}
		}
		sort.Sort(sort.Reverse(sort.IntSlice(collisions)))
		for _, index := range(collisions) {
			carts = remove(carts, index)
		}
		if len(carts) ==1 {
			fmt.Printf("LAST CART STANDING is %v\n", carts[0])
		}
	}
	fmt.Printf("DONE after %d moves\n", turn)
}
type direction int
const (
	UP    direction = 0
	LEFT  direction = 1
	DOWN  direction = 2
	RIGHT direction = 3
)
type cart struct{
	x, y int
	dir direction
	numturns int64
}
func remove (slice carts, i int)carts{
	return append(slice[:i], slice[i+1:]...)
}
func (dir direction) String() string{
	names := [...]string{"UP","LEFT","DOWN","RIGHT"}
	return names[dir]
}

//sorting for play order
func (c carts) Len() int {
	return len(c)
}
func (c carts) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}
func (c carts) Less(i, j int) bool{
	I, J := c[i], c[j]
	if I.y == J.y {
		return I.x < J.x
	}
	return I.y < J.y
}

func (cart *cart) String() string{
	return fmt.Sprintf("cart(%d,%d) moving %s", cart.x, cart.y, cart.dir)
}
type carts []*cart
func makeTurn(cart *cart, rightGoesUp bool){
	newDir := cart.dir
	switch cart.dir {
	case UP:
		if rightGoesUp {
			newDir = RIGHT
		}else{
			newDir = LEFT
		}
	case DOWN:
		if rightGoesUp {
			newDir = LEFT
		}else{
			newDir = RIGHT
		}
	case LEFT:
		if rightGoesUp {
			newDir = DOWN
		}else{
			newDir = UP
		}
	case RIGHT:
		if rightGoesUp {
			newDir = UP
		}else{
			newDir = DOWN
		}
	}
	cart.dir = newDir
}
func decideNextDir(cart *cart){
	dirchanges := [...]int{+1,0,-1}
	newDir := int(cart.dir) + dirchanges[cart.numturns % int64(len(dirchanges))]

	(*cart).dir = direction((4 + newDir) % 4)
	(*cart).numturns++
	//fmt.Printf("cart %v just turned\n", cart)
}
func move(cart *cart, tracks []string ){
	// fmt.Printf("cart %v moving\n", cart)
	switch cart.dir {
	case UP:
		(*cart).y --;
	case DOWN:
		(*cart).y ++;
	case LEFT:
		(*cart).x --;
	case RIGHT:
		(*cart).x ++;
	}
	// fmt.Printf(" >> %v \n", cart)
	switch tracks[cart.y][cart.x] {
	case '+' :
		decideNextDir(cart)
	case '/' :
		makeTurn(cart, true)
	case '\\' :
		makeTurn(cart, false)
	}
}
func print(tracks []string, carts carts) {
	cartRunes := make([][]rune, len(tracks))
	dirs := [...]rune{'^', '<', 'v', '>'}
	for i, _ := range(cartRunes) {
		cartRunes[i] = []rune(tracks[i])
	}
	for _, cart := range(carts) {
		cartRunes[cart.y][cart.x] = dirs[cart.dir]
	}
	for _, line := range(cartRunes) {
		fmt.Println(string(line))
	}
}
func readIn() rune{
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter text: ")
	text, _, _ := reader.ReadRune()
	return text
}
func cls() {
	cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested 
  cmd.Stdout = os.Stdout
  cmd.Run()
}
func readFile() ([]string, carts, bool) {
	bytes, err := ioutil.ReadFile("13-carts.txt")
	// bytes, err := ioutil.ReadFile("13-carts.example.txt")
	if err != nil{
		fmt.Printf("error reading file : %v", err)
		return nil, nil, false
	}
	mapIn := strings.Split(string(bytes), "\r\n")
	mapOut := strings.Split(string(bytes), "\r\n")
	carts := make(carts, 0, 50)
	for y, line  := range(mapIn) {
		//update the map with tracks only
		lineOut  := strings.Replace(line, ">", "-", -1)
		lineOut   = strings.Replace(lineOut, "<", "-", -1)
		lineOut   = strings.Replace(lineOut, "^", "|", -1)
		mapOut[y] = strings.Replace(lineOut, "v", "|", -1)

		// create carts
		for x, char := range(line) {
			switch char {
			case '^' :
				carts = append(carts, &(cart{x, y, UP, 0}))
			case '<' :
				carts = append(carts, &(cart{x, y, LEFT, 0}))
			case 'v' :
				carts = append(carts, &(cart{x, y, DOWN, 0}))
			case '>' :
				carts = append(carts, &(cart{x, y, RIGHT, 0}))
			default  :
			}
		}
	}
	return mapOut, carts, true
}