const 	fs 		= require('fs'),
		path 	= require('path'),
		regex	= /Step (\w) must be finished before step (\w) can begin./,
		data 	= fs.readFileSync(path.basename(__filename).replace(/\.js/, ".txt")).toString().split('\r\n')

const 	charCode = (str, index)=>str.charCodeAt(index);
		alphaStepSort = (stepA, stepB) => charCode(stepA.name, 0) - charCode(stepB.name, 0),
		timeOfEndSort = (stepA, stepB) => stepA.endTime - stepB.endTime;
function main(){
	const 	stepsP1 = {},
			stepsP2 = {};
	function createStep(letter, index){
		stepsP1[letter] = {
			name : letter,
			unlocks : [],
			lockedBy : []
		};
		stepsP2[letter] = {
			name : letter,
			unlocks : [],
			lockedBy : [],
			endTime : 10000000,
			duration : 61+index
		};
		return {
			name : letter,
			unlocks : [],
			lockedBy : []
		};
	}
	[..."ABCDEFGHIJKLMNOPQRSTUVWXYZ"].forEach(createStep);

	const constraints = data.map(line=>regex.exec(line));
	for(let constraint of constraints){
		let [_, before, after] = constraint;
		stepsP1[before].unlocks.push(after);
		stepsP1[after].lockedBy.push(before);
		stepsP2[before].unlocks.push(after);
		stepsP2[after].lockedBy.push(before);
	}
	doPhase1(stepsP1)
	doPhase2(stepsP2)
}
function doPhase2(steps){
	const WORKERS = 5;
	let freeSteps = computeFree(steps),
		currentSteps = [],
		answer = "", currentTime = 0;
	do{
		let endedSteps = []
		for(let i = currentSteps.length -1; i >=0; --i){
			let theStep = currentSteps[i];

			if(theStep.endTime <= currentTime){
				const letter = theStep.name;
				console.log("ending step", theStep.name, "at time", currentTime);
				answer += theStep.name;
				currentSteps.splice(i, 1);
				for(let next of theStep.unlocks){
					let nextStep = steps[next];
					//console.log("   > step :", next, "is locked by", nextStep.lockedBy);
					if(nextStep.lockedBy.length === 1 && nextStep.lockedBy[0] === letter){
						//console.log("  >> freeing step :", nextStep);
						freeSteps.push(nextStep);
					}else{
						nextStep.lockedBy = nextStep.lockedBy.filter( x => x !== letter)
					}
				}
			}
		}
		freeSteps = freeSteps.sort(alphaStepSort);
		while(freeSteps.length && currentSteps.length < WORKERS){
			let step = freeSteps.shift();
			console.log("starting step", step.name, "at time", currentTime);
			step.endTime = currentTime + step.duration;
			currentSteps.push(step);
			console.log("will end at", step.endTime);
		}
		currentSteps = currentSteps.sort(timeOfEndSort);
		if(currentSteps.length)
			currentTime = currentSteps[0].endTime;
		console.log("ready steps :", freeSteps);
		console.log("current steps :", currentSteps);
	}while(freeSteps.length || currentSteps.length)
	console.log("===============Phase2=============");
	console.log("time :", currentTime);
	console.log("order :", answer);
	console.log("===============      =============");
	console.log("");
}
function doPhase1(steps){
	let freeSteps = computeFree(steps);
	let answer = "";
	do{
		freeSteps = freeSteps.sort(alphaStepSort);
		console.log("free steps", freeSteps.map(s=>s.name));
		let first = freeSteps.shift(),
			letter = first.name;

		answer += letter;

		//console.log("doing step :", letter, "unlocks", first.unlocks);
		for(let next of first.unlocks){
			let nextStep = steps[next];
			//console.log("   > step :", next, "is locked by", nextStep.lockedBy);
			if(nextStep.lockedBy.length === 1 && nextStep.lockedBy[0] === letter){
				//console.log("  >> freeing step :", nextStep);
				freeSteps.push(nextStep);
			}else{
				nextStep.lockedBy = nextStep.lockedBy.filter( x => x !== letter)
			}
		}
	}while(freeSteps.length)
	console.log("===============Phase1=============");
	console.log("order :", answer);
	console.log("===============      =============");
	console.log("");
}
function computeFree(steps){
	let freeSteps = [];
	for ( let stepL in steps ){
		let step = steps[stepL];
		//console.log("step", step.name, step.lockedBy, step.lockedBy.length)
		if(step.lockedBy.length === 0)
			freeSteps.push(step);
	}
	return freeSteps;
}
main();