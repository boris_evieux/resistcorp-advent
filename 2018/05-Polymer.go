package main

import "fmt"
import "io/ioutil"

type frequencyOp struct{
	isAdd bool
	value int
}

// 'a' - 'A' 
const UPPERCASE_DIFF int = int('a') - int('A')

type result struct {
	removed rune
	length int
	// result string
}

func main() {
	const phase int = 2;
	bytes, ok := readFile();
	if(!ok){
		panic("error reading file")
	}
	c := make(chan result, 27)

	go func(){
		c <- fullyReact(' ', &bytes)
	}()
	results := make([]result, 0, 27)
	best := ' '
	bestL := len(bytes)
	for r := int('A'); r <= int('Z'); r++{
		r:=rune(r)
		fmt.Printf("launching work for char %c\n", r)
		go func(){
			c <- fullyReact(r, &bytes)
		}()
	}
	e := make(chan bool)
	go func(){
		for i:=0; i < 27; i++{
			res := <- c
			fmt.Printf("got result %v\n", res)
			if res.length < bestL{
				best = res.removed
				bestL = res.length
			}
			results = append(results, res)
		}
		fmt.Println()
		fmt.Printf("best result was %c : %d", best, bestL)
		fmt.Println()
		e <- true
	}()
	fmt.Println("all goroutines sent")
	<-e
	fmt.Println("all goroutines ended")
}

func fullyReact(toRemove rune, bytes *[]byte) result{
	upperCaseRemove := byte(toRemove)
	lowerCaseRemove := byte(toRemove) + byte(UPPERCASE_DIFF)

	startLength := len(*bytes)
	theSwapBytes := make([]byte, startLength)
	bytesSwapDest := &theSwapBytes
	theSwapBytes = make([]byte, startLength)
	bytesSwapSource := &theSwapBytes
	length := 0
	for i := 0; i < startLength; i++ {
		currByte := (*bytes)[i]
		if currByte != upperCaseRemove && currByte != lowerCaseRemove{
			(*bytesSwapSource)[length] = currByte
			length ++
		}else{

		}
	}

	loops := 0
	for modified := true; modified; {
		modified = false
		newLength := 0
		for i :=0; i < length; i++ {
			currByte := (*bytesSwapSource)[i]
			// if currByte != upperCaseRemove && currByte != lowerCaseRemove{
				nextByte := byte(0x0)
				if i < length -1{
					nextByte = (*bytesSwapSource)[i+1]
				}
				diff := int(currByte) - int(nextByte)
				if diff == UPPERCASE_DIFF || diff == -UPPERCASE_DIFF {
					//remove these
					i++
					modified = true;
					// fmt.Printf("%d %c%c, ",i , rune(currByte), rune(nextByte))
				}else{
					(*bytesSwapDest)[newLength] = (*bytesSwapSource)[i]
					newLength++
				}
			// }
		}
		//swap
		temp := bytesSwapDest
		bytesSwapDest = bytesSwapSource
		bytesSwapSource = temp

		length = newLength
		loops++
	}
	fmt.Printf("after %d loops, returning result for %c : %d\n", loops, toRemove, length)
	return result{toRemove, length}
}

func readFile() ([]byte, bool) {
	bytes, err := ioutil.ReadFile("05-Polymer.txt")
	if err != nil{
		fmt.Printf("error reading file : %v", err)
		return bytes, false
	}
	return bytes, true
}
