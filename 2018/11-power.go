package main
import "fmt"

const INPUT int = 4455

func powerLevelCell(x int, y int, serial int) int{
	id := x + 10
	val := y * ( id )
	val += serial
	val *= id
	val = (val / 100) % 10 - 5
	return val
}
func powerLevelGridTest(x int, y int, size int, serial int) int{
	val := 0
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			val += powerLevelCell(x+i, y+j, serial)
		}	
	}
	return val
}
func powerLevelGridReal(x int, y int, size int, grid *[301][301]int) int{
	val := 0
	for i := 0; i < size; i++ {
		for j := 0; j < size; j++ {
			val += (*grid)[x+i][y+j]
		}	
	}
	return val
}
type v2 struct{
	x, y int
}
type square struct{
	x, y, size int
}
func fillGrid(serial int, grid *[301][301]int){
	for i := 1; i < 301; i++ {
		(*grid)[i] = [301]int{}
		for j := 1; j < 301; j++ {
			(*grid)[i][j] = powerLevelCell(i, j, serial)
		}	
	}
}
func main() {
	var realGrid [301][301]int
	fillGrid(INPUT, &realGrid)
	fmt.Printf("for cell 3,5 power level is %d\n", powerLevelCell(3,5, 8))
	fmt.Printf("for cell 122,79 power level is %d\n", powerLevelCell(122,79, 57))
	fmt.Printf("for cell 217,196 power level is %d\n", powerLevelCell(217,196, 39))
	fmt.Printf("for cell 101,153 power level is %d\n", powerLevelCell(101,153, 71))

	fmt.Printf("for cell 33,45 3x3 level is level is %d\n", powerLevelGridTest(33,45, 3, 18))
	fmt.Printf("for cell 21,61 3x3 level is level is %d\n", powerLevelGridTest(21,61, 3, 42))

	best := v2{0,0}
	bestPL := -15000000
	for x := 1; x < 299; x++ {
		for y := 1; y < 299; y++ {
			val := powerLevelGridTest(x, y, 3, 42)
			// fmt.Printf("%d ", val)
			if bestPL < val {
				best = v2{x, y}
				bestPL = val
			}
		}	
	}
	fmt.Printf("Test data is level is %d for 3x3 grid at %v\n", bestPL, best)

	best = v2{0,0}
	bestPL = -15000000
	for x := 1; x < 299; x++ {
		for y := 1; y < 299; y++ {
			val := powerLevelGridReal(x, y, 3, &realGrid)
			// fmt.Printf("%d ", val)
			if bestPL < val {
				best = v2{x, y}
				bestPL = val
			}
		}	
	}
	fmt.Println("============Phase 1==========================")
	fmt.Printf("best is level is %d for 3x3 grid at %v\n", bestPL, best)
	fmt.Println("============Phase 2==========================")
	var testGrid [301][301]int
	fillGrid(18, &testGrid)
	bestSquare := square{0,0,1}
	for x := 1; x < 299; x++ {
		for y := 1; y < 299; y++ {
			maxSizeX := 300 - x
			maxSizeY := 300 - y
			for size := 1; size < maxSizeX && size < maxSizeY; size++ {
				val := powerLevelGridReal(x, y, size, &realGrid)
				// fmt.Printf("%d ", val)
				if bestPL < val {
					bestSquare = square{x, y, size}
					bestPL = val
				}
			}	 
		}	 
	}
	fmt.Printf("Test data 2 %d for 3x3 grid at %v\n", bestPL, bestSquare)

}