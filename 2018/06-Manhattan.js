const 	fs = require( 'fs' ),
		intParser = x => parseInt(x, 10),
		data = fs.readFileSync('06-Manhattan.txt').toString().split('\r\n');

function main(){
	const 	areas = [];
	let minX = 10000000,
		maxX = 0			
		minY = 10000000,
		maxY = 0,
		safeNum = 0;
	function checkBorders(x, y){
		if ( x > maxX) maxX = x;
		if ( x < minX) minX = x;
		if ( y > maxY) maxY = y;
		if ( y < minY) minY = y;
	};
	for( let line of data ){
		let [x, y] = line.split(", ").map(intParser);

		checkBorders(x, y);
		areas.push({x, y, infinite: false, surface:0});
	}
	function isBorder(x, y){
		return x == minX || x == maxX || y == minY || y == maxY;
	}
	for(let x = minX; x < maxX; ++x){
		for(let y = minY; y < maxY; ++y){
			const border = isBorder(x, y);
			let minDist = 100000000,
				closest = null,
				sum = 0;
			for(let area of areas){
				const dist = manhattanDistance(x, y, area.x, area.y);
				if(minDist > dist){
					minDist = dist;
					closest = area;
				}
				sum += dist;
			}
			closest.surface++;
			if(border){
				console.log("  region at", closest, "is closest to border", x, y, "... so it is infinite")
				closest.infinite = true;
			}
			if(sum < 10000)
				safeNum ++;
		}
	}
	let biggest = {surface : -1};
	for(let area of areas){
		if(!area.infinite &&  area.surface > biggest.surface){
			biggest = area;
		}
	}
	console.log("here is the biggest finite area :", biggest)
	console.log("there are", safeNum, "points that are within 1000 total of all locations")
}
function manhattanDistance(x, y, cX, cY){
	return Math.abs(x-cX) + Math.abs(y-cY);
}
main()