package main

import "fmt"
import "strings"
import "io/ioutil"
import "bufio"
import "sort"
import "os/exec"
import "os"

type pos struct{
	posx,posy int
}
type actual_character struct{
	pos
	isElf_  bool
	health int
	powerPts		 int
}

func(p pos) x() int{
	return p.posx
}
func(p pos) y() int{
	return p.posy
}
func(p pos) left() position{
	return pos{p.posx - 1, p.posy}
}
func(p pos) right() position{
	return pos{p.posx + 1, p.posy}
}
func(p pos) up() position{
	return pos{p.posx, p.posy - 1}
}
func(p pos) down() position{
	return pos{p.posx, p.posy + 1}
}
func(c *actual_character) HP() int{
	return c.health
}
func(c *actual_character) power() int{
	return c.powerPts
}
func(c *actual_character) Hit(power int){
	c.health -= power
}
func(c *actual_character) moveTo(dest position){
	//fmt.Printf("%v at (%d %d) moves to (%d %d)\n", c, c.x(), c.y(), dest.x(), dest.y())
	c.pos = pos{dest.x(), dest.y()}
}
func(c *actual_character) typeStr() string{
	if c.isElf_ {
		return "E"
	}
	return "G"
}
func(c *actual_character) isElf() bool{
	return c.isElf_
}
func(c *actual_character) isAlive() bool{
	return c.health > 0
}
type position interface{
	x() int
	y() int
	left() position
	right() position
	up() position
	down() position
}
type character interface{
	position
	HP() int
	isElf() bool
	typeStr() string
	isAlive() bool
	String() string
	Hit(power int)
	moveTo(pos position)
	power() int
}
type tile struct{
	pos
	isPassable bool
	occupant character
}
type state struct{
	themap fieldmap
	chars characters 
	gen int
	mapSize int
}

type tiles []*tile
type fieldmap []tiles
type characters []character
type positions []position
const CHARACTER_HEALTH int = 200
const CHARACTER_HIT int = 3
func main() {
	state, ok := readFile()
	if !ok {
		panic("error reading file")
	}
	fmt.Println(*state)
	input := readIn()
	for cont := true ; cont && input != 'x';{
		cont, input = advance(state, input)
	}
	fmt.Println(*state)
	fmt.Printf("ended after %d turns\n", state.gen)
}
func makeChar(x,y int, isElf bool) character {
	health := CHARACTER_HEALTH
	hit := CHARACTER_HIT
	ret := actual_character{pos{x, y}, isElf, health, hit}
	return &ret
}
func (themap fieldmap) at(p position) *tile{
	x, y := p.x(), p.y()
	if y < 0 || y >= len(themap) {
		return &tile{pos{p.x(), p.y()},false,nil}
	}
	line := themap[y]
	if x < 0 || x >= len(line) {
		return &tile{pos{p.x(), p.y()},false,nil}
	}
	return line[x]
}
func advance(st *state, input rune) (bool, rune){
	sort.Sort(st.chars)
	for _, c := range(st.chars){
		if input == '.' {
			fmt.Printf("last input: %v, current turn %d\n", lastIn, st.gen)
			fmt.Printf("%v", *st)
			fmt.Println()
			input = readIn()
		}
		if input == 'x' {
			return false, '.'
		}
		cls()
		if !c.isAlive() {
			continue
		}
		targets := make([]character, 0, len(st.chars))
		inRange := make(tiles, 0, len(targets) * 4)
		var currentTarget character = nil
		for _,t := range(st.chars){
			// fmt.Printf("considering target %v", t)
			if t.isAlive() && t.isElf() != c.isElf() {
				targets = append(targets, t)
				positions := [...]position{
					t.up(),
					t.left(),
					t.right(),
					t.down(),
				}
				for _,position := range(positions){
					tile := st.themap.at(position)
					if tile.isPassable && ( tile.occupant == nil || tile.occupant == c ) {
						inRange = append(inRange, tile)
						if tile.x() == c.x() && tile.y() == c.y() {
							if currentTarget == nil || t.HP() < currentTarget.HP(){
								currentTarget = t
							}
						}
					}
				}
			}
		}
		if len(targets) == 0 {
			// fmt.Printf("no target for %v\n", c)
			return false, 'x'
		}
		if currentTarget == nil {
			closestTile := move(c, inRange, st)
			if closestTile == nil {
				continue
			}
			positions := [...]position{
				closestTile.down(),
				closestTile.right(),
				closestTile.left(),
				closestTile.up(),
			}
			for _, p := range(positions){
				t := st.themap.at(p)
				if t.occupant != nil && t.occupant.isElf() != c.isElf() {
					if currentTarget == nil || t.occupant.HP() < currentTarget.HP(){
						currentTarget = t.occupant
					}
				}
			}

		}
		if currentTarget != nil {
			if attack(c, currentTarget, st) && input == 'd' {
				input = '.'
			}
		}
		if input == 's' {
			input = '.'
		}

	}
	st.gen ++

	if input == 't'{
		return true, '.'
	}
	return true, input
}
func move(c character, goals tiles, st *state) *tile {
	// fmt.Printf("%v trying to move\n", c)
	prev := st.themap.at(c)
	reached := make(map[*tile]tiles, st.mapSize)
	mostRecentRing := make(map[*tile]tiles, st.mapSize)
	mostRecentRing[prev] = make(tiles, 0)

	for ; len(mostRecentRing) != 0; {
		// fmt.Printf(" ring\n")
		newRing := make(map[*tile]tiles, len(mostRecentRing))
		for t, road := range(mostRecentRing){
			reached[t] = road
		}
		for t, road := range(mostRecentRing){
			// fmt.Printf(" source %d %d\n", t.x(), t.y())

			var bestDest *tile = nil
			var firstStep *tile = nil
			positions := [...]position{
				t.up(),
				t.left(),
				t.right(),
				t.down(),
			}
			for _, p := range(positions){
				tile := st.themap.at(p)
				if ! tile.isPassable || tile.occupant != nil {
					// fmt.Printf(" target impassable %v\n", p)
					continue
				}
				// fmt.Printf(" target %d %d\n", p.x(), p.y())
				if goals.contains(*tile) && ( bestDest == nil || Less(tile, bestDest)) {
					// fmt.Printf(" target %v %v %v\n", tile, t, road)
					bestDest = tile
					if len(road) == 0 {
						firstStep = tile
					}else{
						firstStep = road[0]
					}
				}
				val, has := reached[tile]
				if ! has || (len(road) > 0 && (len(road) == 0 || Less(road[0], val[0]))) {
					newRoad := make(tiles, len(road) +1)
					copy(newRoad, road)
					newRing[tile] = append(newRoad, tile)
				}
			}
			if firstStep != nil {
				c.moveTo(firstStep)
				prev.occupant = nil
				firstStep.occupant = c
				return firstStep
			}
			mostRecentRing = newRing
		}
	}
	// fmt.Printf("%v no answer\n", c)
	return nil
}
func attack(attacker, defender character, st *state) bool{
	defender.Hit(attacker.power())
	if !defender.isAlive(){
		st.themap.at(defender).occupant = nil
		return true
	}
	return false
}
func (tiles tiles) contains(tile tile) bool {
	for _, t := range(tiles) {
		if t.x() == tile.x() && t.y() == tile.y(){
			return true
		}
	}
	return false
}
func readFile() (*state, bool) {
	bytes, err := ioutil.ReadFile("15-combat.example.txt")
	// bytes, err := ioutil.ReadFile("15-combat.txt")
	if err != nil{
		fmt.Printf("error reading file : %v", err)
		return nil, false
	}
	lines := strings.Split(string(bytes), "\r\n")
	mapOut := make(fieldmap, len(lines))
	chars := make(characters, 0, 150)

	for y, line := range(lines) {
		mapOut[y] = make(tiles, len(line))
		mapLine := mapOut[y]
		for x, char := range(line ){
			isPassable := char != '#'
			mapLine[x] = &tile{pos{x, y}, isPassable, nil}
			if isPassable {
				if char != '.' {
					mapLine[x].occupant = makeChar(x, y, char == 'E')
					chars = append(chars, mapLine[x].occupant)
				}
			}
		}
	}
	mapSize := len(lines) * len(lines[0])
	return &(state{mapOut, chars, 0, mapSize}), true
}
func cls() {
	cmd := exec.Command("cmd", "/c", "cls") //Windows example, its tested 
  cmd.Stdout = os.Stdout
  cmd.Run()
}
var lastIn rune = 't'
func readIn() rune{
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("command ? [s]tep, [t]urn, next [d]eath, e[x]it ?: ")
	text, length, err := reader.ReadRune()
	if text == '\r' {
		text = lastIn
	}
	if err != nil || ! isInput(text) {
		fmt.Printf("you just entered \n'%c'\n (%d %d)",text, byte(text), length)
		text = lastIn
	}

	lastIn = text
	return text
}
func isInput(text rune) bool{
	switch{
	case text == 'x' :
		return true
	case text == 't' :
		return true
	case text == 'd' :
		return true
	case text == 's' :
		return true
	}
	return false
}
//sorting for play order
func (c positions) Len() int {
	return len(c)
}
func (c positions) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}
func (c positions) Less(i, j int) bool{
	I, J := c[i], c[j]
	return Less(I,J)
}
func (c characters) Len() int {
	return len(c)
}
func (c characters) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}
func (c characters) Less(i, j int) bool{
	I, J := c[i], c[j]
	return Less(I,J)
}
func (c tiles) Len() int {
	return len(c)
}
func (c tiles) Swap(i, j int) {
	c[i], c[j] = c[j], c[i]
}
func (c tiles) Less(i, j int) bool{
	I, J := c[i], c[j]
	return Less(I,J)
}
func Less(a, b position) bool{
	if a.y() == b.y() {
		return a.x() < b.x()
	}
	return a.y() < b.y()
}
func(st state) String() string {
	strs := make([]string, len(st.themap))
	score := 0
	for _, c := range(st.chars) {
		if c.isAlive(){
			score +=  c.HP()
		}
	}
	for i, l := range(st.themap) {
		strs[i] = l.String()
	}
	return strings.Join(strs, "\n") + fmt.Sprintf("\n\n value : %dx%d = %d", st.gen, score, st.gen * score) 
}
func(c actual_character) String() string {
	return fmt.Sprintf("(%s %d)", c.typeStr(), c.HP())
}
func(line tiles) String() string {
	baseLine := make([]rune, len(line))
	charStates := "  "
	for i, tile := range(line) {
		char := '.'
		if ! tile.isPassable {
			char = '#'
		}else if tile.occupant != nil {
			if tile.occupant.isElf() {
				char = 'E'
			}else{
				char = 'G'
			}
			charStates += tile.occupant.String()
		}
		baseLine[i] = char
	}
	return string(baseLine) + charStates
}
