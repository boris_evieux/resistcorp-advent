use std::sync::mpsc::channel;
use std::collections::HashMap;
use cursive::views::{TextView, Dialog};
use cursive::CbSink;
use crate::util::*;
use crate::days::day02::*;


pub fn run(cs: CbSink) ->(i64, i64) {
	let v = read_input("./input/day11_robot.txt");
	let (map1, _, _) = run_bot(copy(&v), false);
	let (map2, min, max) = run_bot(copy(&v), true);

	let size = max.sub(min).add(Point{x: 1, y: 1});

	println!("{:?} {:?} {:?}", min, max, size);
	let mut drawing = vec![(0..size.x).map(|_| ' ').collect::<Vec<char>>(); size.y as usize];
	for (key, val) in map2.iter(){
		drawing[key.y as usize][key.x as usize] = match val {
			1 => '█',
			_ => ' ',
		}
	}
	let mut res_str = String::with_capacity((6 + size.y + size.x * size.y) as usize);
	println!("actual result for day 8");
	res_str.push_str("day11\n");
	for l in drawing.iter() {
		let mut s = String::with_capacity(size.x as usize);
		for c in l { s.push(*c) }
		res_str.push_str(&s);
		println!("{}", s);
		
		res_str.push('\n');
	}
	match cs.send(Box::new(|c|c.add_layer(Dialog::around(TextView::new(res_str))))){
		Err(_) => println!("error showing result for day 08"),
		Ok(_) => ()
	}
	(map1.len() as i64,-99)
}
fn run_bot(v: Vec<i64>, start_painted: bool)-> (HashMap<Point, i64>, Point, Point){
	let (bot_sends, i_receive) = channel();
	let (handle, i_send) = IntComputer::run_code_async(v, bot_sends, "Painterbot".to_string(), false);
	let mut bot_pos = Point{x: 0, y: 0};
	let mut map:HashMap<Point, i64> = HashMap::new();

	let mut min_pos = Point{x:0, y:0};
	let mut max_pos = Point{x:0, y:0};

	let mut dir = 0i32;

	if start_painted {
		map.insert(bot_pos, 1);
	}
	loop {

		if max_pos.x < bot_pos.x {
			max_pos.x = bot_pos.x;
		}
		if min_pos.x > bot_pos.x {
			min_pos.x = bot_pos.x;
		}
		if max_pos.y < bot_pos.y {
			max_pos.y = bot_pos.y;
		}
		if min_pos.y > bot_pos.y {
			min_pos.y = bot_pos.y;
		}
		i_send.send(match map.get(&bot_pos){
			None => 0,
			Some(c) => *c,
		}).expect("painterbot died");
		let color = i_receive.recv();
		if color.is_err() {
			break;
		}
		let turn = i_receive.recv().unwrap();
		let color = color.unwrap();
		if map.contains_key(&bot_pos) || color == 1 {
			map.insert(bot_pos, color);
		}
		dir = match turn {
			0 => dir - 1,
			1 => dir + 1,
			x => panic!("value {:?} shouldn't happen", x)
		};
		if dir < 0 { dir += DIRS.len() as i32}

		bot_pos = bot_pos.add(DIRS[(dir) as usize % DIRS.len()]);
	}
	handle.join().expect("couldn't shutdown painterbot");
	(map, min_pos, max_pos)
}