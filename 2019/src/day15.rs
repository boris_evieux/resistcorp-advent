use cursive::CbSink;
use cursive::views::{TextView, Dialog};
use std::collections::HashMap;
use cursive::view::Identifiable;
use std::sync::mpsc::channel;
use std::thread;
use std::time::Duration;
use crate::days::day02::*;
use crate::util::*;

pub fn run(cbs:CbSink) ->(i64, i64){
	let v = read_input("./input/day15_alone_in_the_dark.txt");
	let (mut map, (ox_pos, ret1)) = find_panel(copy(&v));
	let sc = make_screen(&map, ox_pos);
	match cbs.send(
			Box::new(move |c|{
				let tv = TextView::new(sc).with_id("maze");
				c.add_layer(Dialog::around(tv));
			})
		){
		Err(_) => println!("error showing result for day 13"),
		Ok(_) => ()
	}
	let ret2 = flood_fill(&mut map, ox_pos, &Some(cbs));

	(ret1, ret2)
}

fn flood_fill(mind_map : &mut HashMap<Point, (i64, bool)>, pos : Point, cbs: &Option<CbSink>) -> i64{
	let mut front = vec![pos];
	let mut steps = -1;

	while front.len() > 0 {
		let mut new_front = vec![];
		for point in front{
			for dir in DIRS.into_iter() {
				let address = point.add(*dir);

				match mind_map.get(&address) {
					Some((-2, _)) => (),//already flooded
					Some((-1, _)) => (),//wall
					Some((_, _)) => new_front.push(address),//to flood
					None =>(),//unreachable
				};
			}
			mind_map.insert(point, (-2, false));
		}
		front = new_front;
		// if steps%100 == 99 {
			thread::sleep(Duration::from_millis(20));
		// }
		if steps % 10 == 0 {
			match cbs {
				Some(cbs) => {
						let screen = make_screen(&mind_map, ZERO);
						cbs.send(Box::new(move |c|{
							c.call_on_id("maze", |view: &mut TextView| {
					      view.set_content(screen);
					  	});
						})).unwrap();
					},
				None => ()
			}
		}
		steps += 1;
	}

	steps
}

fn find_panel(code : Vec<i64>) -> (HashMap<Point, (i64, bool)>, (Point, i64)){
	let (bot_sends, i_receive) = channel();
	let (handle, i_send) = IntComputer::run_code_async(code, bot_sends, "repair_bot".to_string(), false);
		thread::sleep(Duration::from_millis(1));

	let mut ox_pos = (ZERO, 0);
	let mut mind_map = HashMap::new();
	let mut bot_pos = Point{x: 0, y: 0};
	let mut start_dist = 0;
	mind_map.insert(bot_pos, (start_dist, false));
	let mut runs = 0;

	loop {
		let (next_dir, next_pos, next_dist, blocked) = meditate(&mind_map, bot_pos, start_dist, 1);
		// println!("sending command {:?}", next_dir);

		if blocked {
			if next_pos == ZERO {
				//we are finished : went all the way back to the start, and explored every corridor
				drop(i_send);
				match handle.join() {
					Err(reason)=>println!("bot didn't shutdown {:?}", reason),
					_ =>()
				}
				println!("{}", make_screen(&mind_map, bot_pos));
				break (mind_map, ox_pos);
			}
			mind_map.insert(bot_pos, (next_dist, true));
		}
		// println!("next : {:?} {}", next_pos, next_dist);
		i_send.send(next_dir).expect("bot refused command");

		let result =loop{
			let debug:Vec<i64> = i_receive.try_iter().collect();
			if debug.len() > 0 {
				break debug[debug.len()-1];
			}
			thread::sleep(Duration::from_millis(1));

		};
		match result {
			0 => {
				// println!("wall at position {:?}", next_pos);
				mind_map.insert(next_pos, (-1, true));
			},
			1 => {
				// println!("moved to position {:?}", next_pos);
				bot_pos = next_pos;
				start_dist = next_dist.abs();
				mind_map.insert(bot_pos, (start_dist, false));
			},
			2 => {
				bot_pos = next_pos;
				ox_pos = (next_pos, start_dist+1);
				start_dist = next_dist.abs();
				mind_map.insert(bot_pos, (start_dist, false));
				println!("{}", make_screen(&mind_map, bot_pos));
				// break start_dist;
			},
			_ => panic!("bot answered unknown code"),
		}

		runs += 1;
	}
}
const DIRS :[Point; 4] = [
    Point{x:  0, y: -1},
    Point{x:  0, y:  1},
    Point{x: -1, y:  0},
    Point{x:  1, y:  0}
];
fn make_screen(mind_map: &HashMap<Point, (i64, bool)>, bot_pos:Point ) -> String {
	let mut min = ZERO;
	let mut max = ZERO;
	for (key, _val) in mind_map {
		if key.x + 3 > max.x { max.x = key.x + 3 }
		if key.x - 2 < min.x { min.x = key.x - 2 }
		if key.y + 3 > max.y { max.y = key.y + 3 }
		if key.y - 2 < min.y { min.y = key.y - 2 }
	}
	let mut ret = String::new();
	for y in min.y..max.y{
		for x in min.x..max.x {

			let pos = Point{x, y};
			
				if pos == bot_pos {
					ret.push_str("XX");
				}else{
					match mind_map.get(&pos){
						None => ret.push_str("  "),
						Some((-1, _)) => ret.push_str("██"),
						Some((-2, _)) => ret.push_str("░░"),
						Some((_, true)) => ret.push_str(".."),
						Some((_, _)) => ret.push_str("++"),
						// Some(_)	=> ret.push_str("..")
					}
				};
		}
		ret.push_str("\n");
	}
	ret
}

const DIRNAMES : [&str; 5] = [
	"NOWHERE",
	"NORTH",
	"SOUTH",
	"WEST",
	"EAST"
];
fn meditate(mind_map: &HashMap<Point, (i64, bool)>, bot_pos:Point, dist_current: i64, num_backtrack: usize ) -> ( i64, Point, i64, bool ) {
	let mut best = (std::i64::MIN, 1, Point{x: 1_000_000, y: 1_000_000}, false);
	let mut paths = 4;
	let mut indent =String::new();
	for _i in num_backtrack..2 {
		indent.push('>');
	}
	for i in 0..DIRS.len() {
		let dir = DIRNAMES[i+1];

		let next_pos = bot_pos.add(DIRS[i]);
		match mind_map.get(&next_pos) {
			Some((-1, _)) => {
				// println!("{}Wall {:?}", indent, dir);
				paths -= 1;
			},
			Some((_, true)) => {
				paths -= 1;
			},
			Some((dist, _)) => {
				// println!("{}Known {:?} : {:?}", indent, dir, next_pos);
				if !best.3 && *dist > best.0 {
					// println!("{}maybe we'll go", indent);
					best = (dist.abs(), (i+1) as i64, next_pos, false);
				}
				// if num_backtrack > 0 && *dist < dist_current {
				// 	let backtrack = meditate(&mind_map, next_pos, dist.abs(), num_backtrack-1);
				// 	let best_dist = best.2.norm1();
				// 	let new_dist = backtrack.1.norm1();
				// 	if backtrack.3 == 1 && (new_dist <= best_dist || best.3 == 0 ) {
				// 		// println!("{}backtrack toward {:?} : {}", indent, backtrack.1, dir);
				// 		best = (dist.abs(), (i+1) as i64, next_pos, 2);
				// 	}
				// }
			},
			None =>{
				let dist = next_pos.norm1();
				let best_dist = best.2.norm1();
				if !best.3 || dist < best_dist {
					// println!("{}into the unknown {:?} : {} {}", indent, next_pos, dir, dist_current);
					best = (
						dist_current +1,
						(i+1) as i64,
						next_pos,
						true);
				}
				// return ( (i+1) as i64, next_pos, dist+1)
			},
		}
	}
	(best.1, best.2, best.0, paths == 1)
}
#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_d15_2() {
		let mut mind_map = HashMap::new();
		let input = [
			" ##   ",
			"#..## ",
			"#.#..#",
			"#.O.# ",
			" ###  "
 		];
 		for (i, line) in input.iter().enumerate() {
 			let y = i as i32;
 			for (j, c) in line.chars().enumerate() {
 				let x = j as i32;
 				match c{
 					'#' => mind_map.insert(Point{x, y}, (-1, false)),
 					'.' => mind_map.insert(Point{x, y}, (42, false)),
 					 _ 	=> None,
 				};
 				
 			}
 		}

		assert_eq!(flood_fill(&mut mind_map, Point{x:2, y:3}, &None), 4);
	}
}

