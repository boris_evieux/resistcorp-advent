use std::sync::mpsc::channel;
use cursive::CbSink;
use cursive::Cursive;
use std::time::{Instant};
use std::thread;


mod day01;mod day02;mod day03;mod day04;mod day05;mod day06;mod day07;mod day08;mod day09;mod day10;

mod day11;
mod day12;
mod day13;
mod day14;
mod day15;
mod day16;
// mod day17;
mod util;
type Runner = fn(CbSink)->(i64, i64);

fn main() {
	let (s, r) = channel();

	let event_loop = thread::spawn(move ||{
		let mut c = Cursive::default();
		c.add_global_callback('q', |s| s.quit());
		s.send(c.cb_sink().clone()).expect("");
		c.run();
	});
	let master_sink = r.recv().expect("cursive win didn't start");

	let days = vec![
	/*
		crate::day01::run as Runner,
		crate::day02::run as Runner,
		crate::day03::run as Runner,
		crate::day04::run as Runner,
		crate::day05::run as Runner,
		crate::day06::run as Runner,
		crate::day07::run as Runner,
		crate::day08::run as Runner,
		crate::day09::run as Runner,
		crate::day10::run as Runner,
		crate::day11::run as Runner,
		crate::day12::run as Runner,*/
		crate::day13::run as Runner,/*
		crate::day14::run as Runner,
		crate::day15::run as Runner,
		crate::day16::run as Runner,
		crate::day17::run as Runner,
	// */
	];

	let start = Instant::now();
	let mut results = vec![];
	{
		let mut thds = vec![];
		let mut i = 1;
		for run in &days {
			let sink = master_sink.clone();
			let run = run.clone();
			let handle = thread::Builder::new()
                              .name(format!("day{:02}", i).into())
                              .spawn(
        move ||{
					let now = Instant::now();
				  let (s1, s2) = run(sink);
				  (s1, s2, now.elapsed())
			});
			thds.push(handle.unwrap());
			i += 1;
		}
		let mut i = 1;
		for handle in thds{
			let r = match handle.join() {
				Ok((a, b, c)) => (a, b, c).clone(),
				Err(_) => (-1, -1, Instant::now().elapsed())
			};
			results.push(r);
			let (s1, s2, duration) = r;
		  println!("day{:02}", i);
		  println!(" STEP 1 : {:?}", s1);
		  println!(" STEP 2 : {:?}", s2);
		  println!("  run 2 steps in {:?}", duration);
			i += 1;
		}
	}
	for (i, (s1, s2, duration)) in results.iter().enumerate(){
		  println!("day{:02}", i+1);
		  println!(" STEP 1 : {:?}", s1);
		  println!(" STEP 2 : {:?}", s2);
		  println!("  run 2 steps in {:?}", duration);
	}

	println!();
	println!("all days done.");
	println!(" run {} days in {:?}", days.len(), start.elapsed());
	println!(" note : results that are -99 are graphic. search the trace and read them.");

	event_loop.join();
}
