use std::ops::Range;

const INPUT_RANGE:Range<i32> = 206_938..679_128;

pub fn run<T>(_c: T) -> (i64, i64) {
    let mut answer1 = 0;
    let mut answer2 = 0;

    for value in INPUT_RANGE {
        let (a1, a2) = meets_criteria(value);
        if a1 {
            answer1 += 1;
        }
        if a2 {
            answer2 += 1;
        }
    }

    (answer1, answer2)
}

fn meets_criteria(value:i32) -> (bool, bool) {
    let mut has_repeating = false;
    let mut all_increasing = true;
    let mut rep_twice_pattern = 0x0;
    let mut prev = -1;
    let mut current_run = 1;

    // println!("test for value {:?}", value);
    for order in 1..7 {
        let pow = 10i32.pow(6 - order);
        let digit = (value / pow) % 10;
        // println!("digit at order {:?}({:?}) is {:?}", order, pow, digit);
        if digit < prev{
            all_increasing = false;
        }
        if digit == prev{
            has_repeating = true;
            current_run += 1;
        }else{
            current_run = 1;
        }

        let pattern = 1 << digit;
        if current_run == 2 {
            rep_twice_pattern |= pattern;
        }else{
            rep_twice_pattern &= !pattern;
        }
        prev = digit;
    }
    let a1 = has_repeating && all_increasing;
    let a2 = a1 && (rep_twice_pattern != 0);
    (a1, a2)
}

#[cfg(test)]
mod test{
    use super::*;
    #[test]
    fn test1(){
        assert!(meets_criteria(111111).0)
    }
    #[test]
    fn test2(){
        assert!(!meets_criteria(223450).0)
    }
    #[test]
    fn test3(){
        assert!(!meets_criteria(123789).0)
    }
    #[test]
    fn test4(){
        assert!(meets_criteria(112233).1)
    }
    #[test]
    fn test5(){
        assert!(!meets_criteria(123444).1)
    }
    #[test]
    fn test6(){
        assert!(meets_criteria(111122).1)
    }
}
