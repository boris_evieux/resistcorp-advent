use crate::util::*;
use crate::days::day02::*;


pub fn run<T>(_c: T)-> (i64, i64){
    let v = read_input("./input/day09_BOOST.txt");

    let test_ret = run_code(copy(&v), [1].to_vec(), false);
    if test_ret.1.len() > 1{
        println!("{:?}", test_ret.1);
    }
    let boost_ret = run_code(copy(&v), [2].to_vec(), false);

    (test_ret.1[0] , boost_ret.1[0])
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1(){
        let mut v: Vec<i64> = [109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99].to_vec();
        v = run_code(v, vec![], false).2;
       internal_assert(v, &[109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99]);
    }
    #[test]
    fn test2(){
        let mut v: Vec<i64> = [1102,34915192,34915192,7,4,7,99,0].to_vec();
        v = run_code(v, vec![], false).1;
        assert!(v[0] >= 1e15 as i64 && v[0] < 1e16 as i64 );
    }
    #[test]
    fn test3(){
        let mut v: Vec<i64> = [104,1125899906842624,99].to_vec();
        v = run_code(v, vec![], false).1;
        assert_eq!(v[0], 1125899906842624 );
    }

}