use std::io::{BufReader, BufRead};
use core::fmt::{Debug, Formatter};
use std::iter::FromIterator;
use std::path::Path;
use std::fs::File;


pub trait DayRunner{
    fn run(&self, cb: &dyn Fn(&str));
}

struct DayRunnerOnce{
    filepath : String
}
impl DayRunnerOnce{
    pub fn new(path : &str) -> Self
    {
        Self{
            filepath : path.to_string()
        }
    }
}
impl DayRunner for DayRunnerOnce{
    fn run(&self, cb: &dyn Fn(&str)){
        read_data(&self.filepath, cb)
    }
}
struct DayRunnerMulti<'a>{
    data : String,
    lines : Vec<&'a str>
}
impl DayRunnerMulti<'_>{
    pub fn new(path : &str, iter : u16) -> Self
    {
        let mut data = String::with_capacity(1_000_000);
        let lines = file_to_lines(&mut data, path);
        Self{data, lines}
    }
}
impl DayRunner for DayRunnerMulti<'_>{
    fn run(&self, cb: &dyn Fn(&str)){
        for line in &self.lines {
            cb(line);
        }
    }
}

pub fn read_data<F>(filepath: &str, mut cb: F ) where F: Fn(&str) {
    let path = Path::new(filepath);
    let display = path.display();

    // Open the path in read-only mode, returns `io::Result<File>`
    let file = match File::open(&path){
        Err(why) => panic!("couldn't read {} : {}", display, why),
        Ok(file) => file,
    };
    let read = BufReader::new(file);

    read.lines().for_each(move |line| cb(&(line.unwrap())[..]));
}

pub fn file_to_lines<'a>(s: &'a mut String, path:&'_ str) -> Vec<&'a str> {
    let mut v:Vec<usize> = vec![];
    read_data(path, |line|{
            let i = line.len();
            s.push_str(line);
            v.push(i)
    });

    let mut pos = 0;
    let mut strs = vec![];
    for i in v {
        strs.push(s[pos..pos+i].into());
        pos += i;
    }
    strs
}


pub fn parse_value(line: &str) -> i64{
	line.parse().unwrap()
}

pub fn copy<T>(v: &[T]) -> Vec<T> where T:Clone{
    Vec::from_iter(v.iter().cloned())
}

#[derive(Eq, PartialEq, Hash, Clone, Copy)]
pub struct Point {
    pub x : i32,
    pub y : i32,
}

impl Point{
    pub fn normalize(self) -> Self{
        let mc = gcd(self.x, self.y).abs();
        Point{
            x: self.x / mc,
            y: self.y / mc,
        }
    }

    pub fn add(self, other:Point) -> Self{
        Point{
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }

    pub fn sub(self, other:Point) -> Self{
        Point{
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
    pub fn norm1(self) -> i32{
        self.x.abs() + self.y.abs()
    }
}

pub const DIRS :[Point; 4] = [
    Point{x:  0, y: -1},
    Point{x:  1, y:  0},
    Point{x:  0, y:  1},
    Point{x: -1, y:  0}
];
pub const ZERO: Point = Point{x:0,y:0};

fn gcd(x: i32, y: i32) -> i32 {
    let mut x = x;
    let mut y = y;
    while y != 0 {
        let t = y;
        y = x % y;
        x = t;
    }
    x
}

impl Debug for Point{
    fn fmt(&self, f:&mut Formatter )->Result<(), core::fmt::Error>{
        write!(f, "[ {}, {} ]", self.x, self.y)
    }
}
