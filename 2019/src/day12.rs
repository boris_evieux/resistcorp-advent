use std::io::{BufReader, BufRead};
use std::cmp::Ordering::*;
use std::error::Error;
use num::integer::lcm;
use std::path::Path;
use std::fs::File;


pub fn run<T>(_c: T) -> (i64, i64){
	let path = Path::new("./input/day12_moons.txt");
  let display = path.display();

  // Open the path in read-only mode, returns `io::Result<File>`
  let file = match File::open(&path){
      Err(why) => panic!("couldn't read {} : {}", display, why.description()),
      Ok(file) => file,
  };
	let read = BufReader::new(file);
	let lines: Vec<Moon> = read.lines().map(|l|Moon::from_str(&l.unwrap())).collect();
	let mut moons = [ZERO_MOON; 4];
	for i in 0..4{
		moons[i] = lines[i];
	}

	let end_moons = run_sim(moons, 1000);
  (
     total_energy(end_moons),
     find_repeat(moons)
  )
}

/** finds a repeat patter in one dimension */
fn find_orbit(moons:[Moon; 4], index:usize) -> i64{
	let mut moons = moons;
	for ret in 1..1_000_000 {
		moons = run_step(moons);

		if (0..4).all(|i|moons[i].vel.get(index) == 0) {
			return ret * 2;
		}
	}
	panic!("couldn't find stable orbit in 1 000 000 iterations !");
}

fn find_repeat(moons:[Moon; 4]) -> i64{
	let mut total = 1;
	for i in 0..3 {
		let orbit = find_orbit(moons, i);
		total = lcm(total, orbit);
	}
	total
}

fn run_sim(moons:[Moon; 4], steps: i64) ->[Moon; 4]{
	let mut moons = moons;
	for _ in 0..steps {
		moons = run_step(moons);
	}

	moons
}
fn run_step(moons:[Moon; 4]) -> [Moon; 4]{
	let mut moons = moons;
	for i in 0..4 {
		for j in 0..i {
			let (v1, v2) = update_velocities(&moons[i], &moons[j]);
			moons[i].vel = v1;
			moons[j].vel = v2;
		}
	}

	for m in &mut moons {
		m.update_pos();
	}
	moons
}
fn compare_vals(m1: i64, m2: i64) -> i64 {
	match m1.cmp(&m2) {
		Greater => -1,
		Equal => 0,
		Less => 1,
	}
}

fn total_energy(system: [Moon; 4]) -> i64 {
	let mut ret = 0;
	for moon in system.iter() {
		let pot_energy = moon.pos.norm1();
		let kin_energy = moon.vel.norm1();
		ret += pot_energy * kin_energy;
	}
	ret
}

fn update_velocities(m1: &Moon, m2: &Moon) -> (Point3, Point3) {
	let mut vel1 = ZERO;

	vel1.x = compare_vals(m1.pos.x, m2.pos.x);
	vel1.y = compare_vals(m1.pos.y, m2.pos.y);
	vel1.z = compare_vals(m1.pos.z, m2.pos.z);

	let vel2 = ZERO.sub(vel1);

	(vel1.add(m1.vel),
		vel2.add(m2.vel))
}

fn make_system(lines:Vec<&str>) -> [Moon; 4] {
		let mut moons = [ZERO_MOON; 4];
		let mut i = 0;
		for s in lines {
			moons[i] = Moon::from_str(s);
			i += 1;
		}
		moons
}

#[derive(Eq, PartialEq, Hash, Debug, Clone, Copy)]
struct Point3{
	x : i64,
	y : i64,
	z : i64
}

#[derive(Eq, PartialEq, Hash, Debug, Clone, Copy)]
struct Moon {
	pos  : Point3,
	vel  : Point3,
}

impl Point3 {
	fn add(self, other : Point3) -> Point3 {
		Point3{
			x : self.x + other.x,
			y : self.y + other.y,
			z : self.z + other.z
		}
	}
	fn sub(self, other : Point3) -> Point3 {
		Point3{
			x : self.x - other.x,
			y : self.y - other.y,
			z : self.z - other.z
		}
	}
	fn get(&self, i:usize)->i64{
		match i {
			0 => self.x,
			1 => self.y,
			2 => self.z,
			_ => panic!("can't get comp {}", i),
		}
	}
	fn norm1(self) -> i64 {
		self.x.abs() + self.y.abs() + self.z.abs()
	}
}

const ZERO : Point3 = Point3{x : 0, y : 0, z : 0};
const ZERO_MOON : Moon = Moon{pos:ZERO, vel: ZERO};
impl Moon {
	fn from_str(s: &str) -> Moon{
		let act = s.replace("<", "").replace(">", "");
		let split : Vec<&str> = act.split(",").collect();

		let x = split[0].replace("x=", "").parse::<i64>().unwrap();
		let y = split[1].replace(" y=", "").parse::<i64>().unwrap();
		let z = split[2].replace(" z=", "").parse::<i64>().unwrap();

		Moon{
			pos : Point3{x, y, z},
			vel : ZERO
		}
	}

	fn update_pos(&mut self){
		self.pos = self.pos.add(self.vel);
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test1() {
		let lines = [
			"<x=-1, y=0, z=2>",
			"<x=2, y=-10, z=-7>",
			"<x=4, y=-8, z=8>",
			"<x=3, y=5, z=-1>"
		].to_vec();
		let moons = make_system(lines);
		let end_moons = run_sim(moons, 10);

		assert_eq!(total_energy(end_moons), 179);
	}
	#[test]
	fn test1_2() {
		let lines = [
			"<x=-1, y=0, z=2>",
			"<x=2, y=-10, z=-7>",
			"<x=4, y=-8, z=8>",
			"<x=3, y=5, z=-1>"
		].to_vec();
		let moons = make_system(lines);
		let end_moons = find_repeat(moons);

		assert_eq!(end_moons, 2772);
	}
	#[test]
	fn test2() {
		let lines = [
			"<x=-8, y=-10, z=0>",
			"<x=5, y=5, z=10>",
			"<x=2, y=-7, z=3>",
			"<x=9, y=-8, z=-3>"
		].to_vec();
		let moons = make_system(lines);
		let end_moons = run_sim(moons, 100);

		assert_eq!(total_energy(end_moons), 1940);
	}
	#[test]
	fn test2_2() {
		let lines = [
			"<x=-8, y=-10, z=0>",
			"<x=5, y=5, z=10>",
			"<x=2, y=-7, z=3>",
			"<x=9, y=-8, z=-3>"
		].to_vec();
		let moons = make_system(lines);

		let end_moons = find_repeat(moons);

		assert_eq!(end_moons, 4686774924);
	}
}