use crate::util::file_to_lines;
use std::fmt::Formatter;
use std::time::Instant;
use core::fmt::Error;
use std::fmt::Debug;


pub fn run<T>(_c: T)-> (i64, i64){
	let path = "./input/day14_ore.txt";
	let mut master = String::new();
 	let lines = file_to_lines(&mut master, path);
  min_max(lines,1_000_000_000_000)
  // (0,0)
}

fn min_max<'a>(source: std::vec::Vec<&'a str>, hold: i64) -> (i64, i64) {
	let reactions = parse_reactions(&source);
	// let reactions = calculate_recipe_prices(reactions);

	// println!("{:?}", reactions);
	let start = Instant::now();
	let min = min_ore_for_fuel(&reactions);
	println!("time taken for step 1 {:?}", start.elapsed());
	let max = max_output(&reactions, min, hold);

	(min, max)
}

fn min_ore_for_fuel(dict : &Vec<Reaction>) -> i64 {
	find_best_ore_price_for(dict, vec![ReactPair("FUEL".to_string(), 1)])
}

fn max_output(dict : &Vec<Reaction>, price_for_one: i64, hold: i64) -> i64{
	let mut low_estimate = hold/price_for_one;//we are sure this works
	let low_price = find_best_ore_price_for(dict, vec![ReactPair("FUEL".to_string(), low_estimate)]);
	let mut medium_price = (low_price as f64) / (low_estimate as f64);

	let mut high_estimate = (hold as f64 / medium_price).ceil() as i64;

	let mut high_price = find_best_ore_price_for(dict, vec![ReactPair("FUEL".to_string(), high_estimate)]);
	let mut iterations = 0;
	while low_estimate + 1 < high_price {
		let remaining = hold - high_price;
		medium_price = (high_price as f64) / (high_estimate as f64);

		if remaining == 0{
			low_estimate = high_estimate;
			break;
		}
		if high_price < hold {
			low_estimate = high_estimate;
			high_estimate = (hold as f64 / medium_price).ceil() as i64;
		}else{
			high_estimate = (hold as f64 / medium_price).floor() as i64;
		}
		high_price = find_best_ore_price_for(dict, vec![ReactPair("FUEL".to_string(), high_estimate)]);
		iterations += 1;
		if iterations > 10 {
			break;
		}
	}
	low_estimate
}

fn find_best_ore_price_for(dict : &Vec<Reaction>, wanted:Vec<ReactPair>) -> i64{
	let mut ret = compact(&wanted);
	let mut changed = true;
	while changed {
		changed = false;
		for i in 0..ret.len() {
			let want = &ret[i].copied();
			let ReactPair(wanted_name, wanted_num) = want;
			let mut reactions = vec![];
			for recipe in dict {
				if recipe.result.0 == *wanted_name {
					reactions.push(recipe);
				}
			}
			if reactions.len() == 1 {
				let mut iter = wanted_num / reactions[0].result.1;
				let mut remainder = wanted_num % reactions[0].result.1;
				if want.1 < 0 {
					if iter == 0 {
						continue;
					}
					// println!("entering negative territory{:?}, {}, {}", want, iter, remainder);
				}
				// println!("only one way to create {:?} : {} x {:?}", want, iter, reactions[0]);
				if remainder > 0 {
					remainder = reactions[0].result.1 - remainder;
					// println!("and {} will remain", remainder);
					iter += 1;

				}
				for src in &reactions[0].sources {
					let mut n = src.copied();
					n.1 *= iter;
					ret.push(n);
				}
				if remainder != 0 {
					let n = ReactPair(
						wanted_name.to_string(),
						- remainder
					);
					ret.push(n);
				}
				ret.swap_remove(i);
				changed = true;
			}
		}
		ret = compact(&ret);
	}
	// println!("the request {:?} has been reduced to {:?}", &wanted, &ret);

	find_best_ore_price(dict, ret)
}
fn find_best_ore_price(dict : &Vec<Reaction>, wanted:Vec<ReactPair>) -> i64 {
	if wanted.iter().all(|w| w.0 == "ORE" || w.1 <= 0) {
		wanted.iter().filter(|pair| pair.1 > 0).fold(0, |acc:i64, w:&ReactPair| acc + (w.1 as i64))
	}else{

		let mut candidates = vec![];
		for react in dict {
			let ReactPair(provided_name, provided_num) = &react.result;
			for i in 0..wanted.len() {
				let want = &wanted[i];
				let ReactPair(wanted_name, wanted_num) = want;
				if wanted_name == provided_name {
					let mut iterations = wanted_num / provided_num;
					if wanted_num % provided_num > 0 {
						iterations += 1;
					}
					let mut new_vec = Vec::with_capacity(react.sources.len() + wanted.len() -1);
					for j in 0..wanted.len() {
						if i != j {
							new_vec.push(wanted[j].copied())
						}
					}
					let mut ore = 0;
					for s in &react.sources {

						if s.0 != "ORE" {
							let mut n = s.copied();
							n.1 *= iterations;
							new_vec.push(n);
						} else {
							ore += (s.1 * iterations) as i64;
						}
					}
					candidates.push((compact(&new_vec), ore));
				}
			}
		}
		let mut best = std::i64::MAX;
		for (cnd, ore) in candidates {
			let val = ore + find_best_ore_price_for(dict, cnd);
			if val < best {
				best = val;
			}
		}

		best
	}
}
// transforms all reactions
// fn reduce(wants: Vec<ReactPair>, dict: &Vec<Reaction>) -> Vec<ReactPair> {
// }
fn compact(wants: &Vec<ReactPair>) -> Vec<ReactPair> {
	// println!("compacting {:?}",wants );
	let mut ret:Vec<ReactPair> = Vec::with_capacity(wants.len());
	for i in 0..wants.len() {
		let mut is_new = true;
		for j in 0..ret.len() {
			if wants[i].0 == ret[j].0 {
				ret[j].1 += wants[i].1;
				is_new = false;
			}
		}
		if is_new {
			ret.push(ReactPair(String::from(&wants[i].0),wants[i].1));
		}
	}
	// println!(" >> result{:?}", ret );
	ret.sort_by_key(|a| - a.1);
	ret.into_iter().filter(|pair| pair.1 != 0).collect()
}
fn parse_reactions(source: &[&str]) -> Vec<Reaction> {
	let mut ret = Vec::with_capacity(source.len());
	for line in source {
		ret.push(Reaction::parse(line));
	}
	ret
}

#[derive( Clone )]
struct ReactPair(String, i64); 


struct Reaction {
	result : ReactPair,
	sources : Vec<ReactPair>
}

impl Reaction {
	fn parse(line : &str) -> Self {
		let split:Vec<&str> = line.split(" => ").collect();
		let result = ReactPair::parse(split[1]);
		let mut sources = vec![];
		for source in split[0].split(", "){
			sources.push(ReactPair::parse(source));
		}
		Reaction{
			result,
			sources: sources.to_vec()
		}
	}
}
impl ReactPair {
	fn parse(from : &str) -> Self {
		let split:Vec<&str> = from.split(" ").collect();
		ReactPair (
			String::from(split[1]),
			split[0].parse().unwrap()
		)
	}
	fn copied(&self) -> Self {
		ReactPair(String::from(&self.0), self.1)
	}
}
impl Debug for ReactPair {
	fn fmt(&self, f:&mut Formatter )->Result<(), Error>{
		write!(f, "{}x{}", self.0, self.1 )
	}
}
impl Debug for Reaction {
	fn fmt(&self, f:&mut Formatter )->Result<(), Error> {
		let sources = self.sources.iter();
		let sources = sources
					.map(
						|pair| String::from(format!("{:?}", pair)))
					.collect::<Vec<String>>();
		let sources = sources.join(" + ");
		write!(f, "{} :: {:?} ", sources, self.result )
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	pub fn test0() {
		let data = [
			"10 ORE => 10 A",
			"1 ORE => 1 B",
			"7 A, 1 B => 1 C",
			"7 A, 1 C => 1 D",
			"7 A, 1 D => 1 E",
			"7 A, 1 E => 1 FUEL"
			];
		let res = min_ore_for_fuel(&parse_reactions(&data.to_vec()));
		assert_eq!(res, 31);
		let data = [
			"9 ORE => 2 A",
			"8 ORE => 3 B",
			"7 ORE => 5 C",
			"3 A, 4 B => 1 AB",
			"5 B, 7 C => 1 BC",
			"4 C, 1 A => 1 CA",
			"2 AB, 3 BC, 4 CA => 1 FUEL"
		];
		let res = min_ore_for_fuel(&parse_reactions(&data.to_vec()));
		assert_eq!(res, 165);
	}
	#[test]
	fn test1() {
		let data = [
			"157 ORE => 5 NZVS",
			"165 ORE => 6 DCFZ",
			"44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL",
			"12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ",
			"179 ORE => 7 PSHF",
			"177 ORE => 5 HKGWZ",
			"7 DCFZ, 7 PSHF => 2 XJWVT",
			"165 ORE => 2 GPVTF",
			"3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT"
		];
		let res = min_max(data.to_vec(), 1_000_000_000_000);
		assert_eq!(res.0, 13312);
		assert_eq!(res.1, 82892753);
	}
	#[test]
	fn test2() {
		let data = [
			"2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG",
			"17 NVRVD, 3 JNWZP => 8 VPVL",
			"53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL",
			"22 VJHF, 37 MNCFX => 5 FWMGM",
			"139 ORE => 4 NVRVD",
			"144 ORE => 7 JNWZP",
			"5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC",
			"5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV",
			"145 ORE => 6 MNCFX",
			"1 NVRVD => 8 CXFTF",
			"1 VJHF, 6 MNCFX => 4 RFSQX",
			"176 ORE => 6 VJHF"
		];
		let res = min_max(data.to_vec(), 1_000_000_000_000);
		assert_eq!(res.0, 180697);
		assert_eq!(res.1, 5586022);
	}
	#[test]
	fn test3() {
		let data = [
			"171 ORE => 8 CNZTR",
			"7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL",
			"114 ORE => 4 BHXH",
			"14 VRPVC => 6 BMBT",
			"6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL",
			"6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT",
			"15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW",
			"13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW",
			"5 BMBT => 4 WPTQ",
			"189 ORE => 9 KTJDG",
			"1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP",
			"12 VRPVC, 27 CNZTR => 2 XDBXC",
			"15 KTJDG, 12 BHXH => 5 XCVML",
			"3 BHXH, 2 VRPVC => 7 MZWV",
			"121 ORE => 7 VRPVC",
			"7 XCVML => 6 RJRHP",
			"5 BHXH, 4 VRPVC => 5 LTCX"
		];
		let res = min_max(data.to_vec(), 1_000_000_000_000);
		assert_eq!(res.0, 2210736);
		assert_eq!(res.1, 460664);
	}
}
