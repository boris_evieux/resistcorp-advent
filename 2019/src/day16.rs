use crate::util::read_data;
use cursive::CbSink;

pub fn run(_cbs:CbSink) ->(i64, i64) {
	let mut input_line = String::new();
	read_data("./input/day16_fft.txt", |line| input_line.push_str(line) );
	let ret1 = fft(&input_line, 100);
	let ret2 = fft_10k(&input_line, 100);
	(ret1,ret2)
}

const PATTERN : [i64; 4] = [0,1,0,-1];

fn fft_10k(input: &str, phases: usize) -> i64 {
	let adress = input[..7].parse::<usize>().unwrap();
	let len = input.len() * 10_000;
	assert!(adress > len /2);// pattern at the end of input

	let mut chars : Vec<i64> = input.chars().map(|c| c.to_string().parse::<i64>().unwrap()).collect();
	let size = chars.len();

	let mut repeated = Vec::with_capacity(len-adress);
	for i in adress..len {
		repeated.push(chars[i % size]);
	}
	let seed = *chars.last().unwrap();
	for _phase in 0..phases {
		// println!(">>>{:?}...{:?}", &repeated[..8], &repeated[repeated.len()-8..]);
		let mut output = vec![0;repeated.len()];
		let mut value = seed;
		for col in (adress..len).rev() {
			value = (value + repeated[col - adress]) % 10;
			// println!("   {:?} > {:?}", value, output[col - adress]);
			output[col - adress] = value;
		}
		repeated = output;
		// println!("<<<{:?}...{:?}", &repeated[..8], &repeated[repeated.len()-8..]);
	}
	repeated[..8].into_iter().fold(0, |acc, val| 10 * acc + val)
}
fn fft(input: &str, phases: usize) -> i64 {
	fft_str(input, phases)[..8].parse().unwrap() 

}
fn fft_str(input: &str, phases: usize) -> String {

	let mut chars : Vec<i64> = input.chars().map(|c| c.to_string().parse::<i64>().unwrap()).collect();
	let size = chars.len();

	for _phase in 0..phases {
		let mut output = Vec::with_capacity(size);
		for col in 0..size {
			let mut value = 0;
			for (row, &ch_row) in chars.iter().enumerate(){
				let mult_in_pattern = mult_in_pattern(col, row);
				value += mult_in_pattern * ch_row;
			}
			output.push(value.abs() % 10);
		}
		chars = output;
	}
	chars.into_iter().map(|d| d.to_string()).collect()
}
fn mult_in_pattern(col: usize, row: usize) -> i64 {
	let repeat = col+1;
	let idx = (1+row) / repeat;
	return PATTERN[idx%4];
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn test_d16_0() {
		for i in 0..20 {
			println!("{:3?}\t\t{:3?}\t\t{:3?}", mult_in_pattern(0, i), mult_in_pattern(1, i), mult_in_pattern(2, i));
		}
		assert_eq!(mult_in_pattern(1, 0), 0);
		assert_eq!(mult_in_pattern(1, 1), 1);
		assert_eq!(mult_in_pattern(1, 2), 1);
		assert_eq!(mult_in_pattern(1, 3), 0);
		assert_eq!(mult_in_pattern(1, 4), 0);
		assert_eq!(mult_in_pattern(1, 5), -1);
		assert_eq!(mult_in_pattern(1, 6), -1);
		assert_eq!(mult_in_pattern(1, 7), 0);
		
		assert_eq!(mult_in_pattern(2, 0), 0);
		assert_eq!(mult_in_pattern(2, 1), 0);
		assert_eq!(mult_in_pattern(2, 2), 1);
		assert_eq!(mult_in_pattern(2, 3), 1);
		assert_eq!(mult_in_pattern(2, 4), 1);
		assert_eq!(mult_in_pattern(2, 5), 0);
		assert_eq!(mult_in_pattern(2, 6), 0);
		assert_eq!(mult_in_pattern(2, 7), 0);
		assert_eq!(mult_in_pattern(2, 8), -1);
		assert_eq!(mult_in_pattern(2, 9), -1);
	}

	#[test]
	fn test_d16_1() {
		let input = "12345678";
		assert_eq!(fft(input, 4), 01029498);
	}

	#[test]
	fn test_d16_2() {
		let input = "80871224585914546619083218645595";
		assert_eq!(fft(input, 100), 24176176);
	}

	#[test]
	fn test_d16_3() {
		let input = "19617804207202209144916044189917";
		assert_eq!(fft(input, 100), 73745418);
	}

	#[test]
	fn test_d16_4() {
		let input = "69317163492948606335995924319873";
		assert_eq!(fft(input, 100), 52432133);
	}

	#[test]
	fn test_d16_5() {
		let input = "03036732577212944063491565474664";

		assert_eq!(fft_10k(input, 100), 84462026);
	}

	#[test]
	fn test_d16_6() {
		let input = "02935109699940807407585447034323";
		assert_eq!(fft_10k(input, 100), 78725270);
	}

	#[test]
	fn test_d16_7() {
		let input = "03081770884921959731165446850517";
		assert_eq!(fft_10k(input, 100), 53553731);
	}
}