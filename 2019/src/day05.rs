use crate::util::copy;
use crate::days::day02::*;

pub fn run<T>(_c: T) -> (i64, i64){
	    let v = read_input("./input/day05_return-to-intcode.txt");
	    let(_, o1, _) = run_code(copy(&v), vec![1], false);
	    let diag1 = *o1.last().expect("program didn't output anything");
	    let(_, o2, _) = run_code(copy(&v), vec![5], false);
	    let diag2 = *o2.last().expect("program didn't output anything");
	    (diag1, diag2)
}

#[cfg(test)]
mod tests {
	use crate::util::copy;
use crate::days::day02::*;
  // use super::*;
    #[test]
    fn test1(){
        let v: Vec<i64> = [1002,4,3,4,33].to_vec();
        let (_, _, a) = run_code(v, vec![0], false);
        assert_eq!(a[0..5], [1002,4,3,4,99]);
    }
    //step 2
    #[test]
    fn test_comp1(){
        let v: Vec<i64> = [3,9,8,9,10,9,4,9,99,-1,8].to_vec();
        let (_, o, _) = run_code(copy(&v), vec![8], false);
        assert_eq!(*o.last().unwrap(), 1);
        let (_, o, _) = run_code(copy(&v), vec![17], false);
        assert_eq!(*o.last().unwrap(), 0);
        let (_, o, _) = run_code(copy(&v), vec![558], false);
        assert_eq!(*o.last().unwrap(), 0);

    }
    #[test]
    fn test_comp2(){
        let v: Vec<i64> = [3,9,7,9,10,9,4,9,99,-1,8].to_vec();
        let (_, o, _) = run_code(copy(&v), vec![5], false);
        assert_eq!(*o.last().unwrap(), 1);
        let (_, o, _) = run_code(copy(&v), vec![8], false);
        assert_eq!(*o.last().unwrap(), 0);
        let (_, o, _) = run_code(copy(&v), vec![15], false);
        assert_eq!(*o.last().unwrap(), 0);
        let (_, o, _) = run_code(copy(&v), vec![1], false);
        assert_eq!(*o.last().unwrap(), 1);

    }
    #[test]
    fn test_comp3(){
        let v: Vec<i64> = [3,3,1108,-1,8,3,4,3,99].to_vec();
        let (_, o, _) = run_code(copy(&v), vec![8], false);
        assert_eq!(*o.last().unwrap(), 1);
        let (_, o, _) = run_code(copy(&v), vec![17], false);
        assert_eq!(*o.last().unwrap(), 0);
        let (_, o, _) = run_code(copy(&v), vec![558], false);
        assert_eq!(*o.last().unwrap(), 0);

    }
    #[test]
    fn test_comp4(){
        let v: Vec<i64> = [3,3,1107,-1,8,3,4,3,99].to_vec();
        let (_, o, _) = run_code(copy(&v), vec![5], false);
        assert_eq!(*o.last().unwrap(), 1);
        let (_, o, _) = run_code(copy(&v), vec![8], false);
        assert_eq!(*o.last().unwrap(), 0);
        let (_, o, _) = run_code(copy(&v), vec![15], false);
        assert_eq!(*o.last().unwrap(), 0);
        let (_, o, _) = run_code(copy(&v), vec![1], false);
        assert_eq!(*o.last().unwrap(), 1);
    }

    #[test]
    fn test_jump1(){
        let v: Vec<i64> = [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9].to_vec();
        let (_, o, _) = run_code(copy(&v), vec![0], false);
        assert_eq!(*o.last().unwrap(), 0);
        let (_, o, _) = run_code(copy(&v), vec![5], false);
        println!("{:?}", o);
        assert_eq!(*o.last().unwrap(), 1);

    }

    #[test]
    fn test_jump2(){
        let v: Vec<i64> = [3,3,1105,-1,9,1101,0,0,12,4,12,99,1].to_vec();
        let (_, o, _) = run_code(copy(&v), vec![0], false);
        assert_eq!(*o.last().unwrap(), 0);
        let (_, o, _) = run_code(copy(&v), vec![5], false);
        assert_eq!(*o.last().unwrap(), 1);

    }

    #[test]
    fn test_real_life(){
        let v: Vec<i64> = [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
														1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
														999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99].to_vec();
        let (_, o, _) = run_code(copy(&v), vec![0], false);
        assert_eq!(*o.last().unwrap(), 999);
        let (_, o, _) = run_code(copy(&v), vec![8], false);
        assert_eq!(*o.last().unwrap(), 1000);
        let (_, o, _) = run_code(copy(&v), vec![1546], false);
        assert_eq!(*o.last().unwrap(), 1001);
    }
}