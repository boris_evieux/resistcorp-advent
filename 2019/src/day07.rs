use std::sync::mpsc::channel;
use std::ops::Range;
use crate::util::copy;
use crate::days::day02::*;

pub fn run<T>(_c: T) -> (i64, i64){
    let v = read_input("./input/day07_amps.txt");
    let(thrust1, _) = run_amps(&v);
    let(thrust2, _) = run_amps_feedback(&v);
    (thrust1, thrust2)
}

fn run_amps_feedback(v: &[i64])-> (i64, [i64; 5]){
    let mut best = (0, [0;5]);
    for seq in 0..10i64.pow(5) {
       let (ok, inputs) = gen_codes(seq, 5..10);
        if ok {
            let mut handles = vec![];
            let (mut prev_send, my_rcv) = channel();
            for i in (0..5).rev() {
                let internal_copy = copy(&v);
                let (handle, new_send) = IntComputer::run_code_async(internal_copy, prev_send.clone(), format!("amp{:?}", i), false);
                handles.push(handle);
                new_send.send(inputs[i] as i64).expect(&format!("couldn't configure amp {}.", i));
                prev_send = new_send;
                // println!("started amp{} with {}",i, inputs[i]);
            }
            let my_send = prev_send;
            my_send.send(0).expect("whole program couldn't even start");
            // println!("started program ok");

            let mut result = -1;
            for output in &my_rcv {
                let val = output;

                match my_send.send(val){
                    Err(_) => {
                        // println!("received final {:?}", val);
                        result = val;
                        break;
                    },
                    Ok(_) => {
                        // println!("sent through {:?}", val);
                    }
                }
            } 
            drop(my_send);
            drop(my_rcv);

            for handle in handles{
                handle.join().expect("one thread or the other panicked !");
            }
            if result > best.0 {
                let mut arr = [0; 5];
                for i in 0..5 {
                    arr[i] = inputs[i] as i64;
                }
                best = (result, arr);
            }
        }
    }
    best
}

fn run_amps(v: &[i64])-> (i64, [i64; 5]){
    let mut best = (0, [0;5]);
    for seq in 0..10i64.pow(5) {
        let (ok, inputs) = gen_codes(seq, 0..5);
        if ok {
            let mut pass = vec![0, 0];
            let mut result = 0;
            for i in 0..5 {
                pass[0] = inputs[i] as i64;
                let (_, output, _) = run_code(copy(&v), copy(&pass), false);
                pass.drain(1..);
                result = output[0];
                pass.push(result);
            }
            if result > best.0 {
                let mut arr = [0; 5];
                for i in 0..5 {
                    arr[i] = inputs[i] as i64;
                }
                best = (pass[1], arr);
            }
        }
    }
    best
}

fn gen_codes(input:i64, rng: Range<usize>) -> (bool, [usize; 5]) {
    let mut ret = [5;5];
    for i in 0usize..5 {
        let d = digit_at(input, 4-i as u32) as usize;
        if !rng.contains(&d) {
            return (false, ret);
        }
        for j in 0..i {
            if ret[j] == d {
                return (false, ret);
            }
        }
        ret[i] = d;
    }
    (ret[0] != ret[1], ret)
}
pub fn test4(){
    let v: Vec<i64> = [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5].to_vec();
    let (t_sig, seq) = run_amps_feedback(&v);
    assert_eq!(t_sig, 139629729);
    assert_eq!(seq, [9,8,7,6,5]);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1(){
        let v: Vec<i64> = [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0].to_vec();
        let (t_sig, seq) = run_amps(&v);
        assert_eq!(t_sig, 43210);
        assert_eq!(seq, [4,3,2,1,0]);
    }

    #[test]
    fn test2(){
        let v: Vec<i64> = [3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0].to_vec();
        let (t_sig, seq) = run_amps(&v);
        assert_eq!(t_sig, 54321);
        assert_eq!(seq, [0,1,2,3,4]);
    }

    #[test]
    fn test3(){
        let v: Vec<i64> = [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0].to_vec();
        let (t_sig, seq) = run_amps(&v);
        assert_eq!(t_sig, 65210);
        assert_eq!(seq, [1,0,4,3,2]);
    }

    #[test]
    fn test4(){
        let v: Vec<i64> = [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5].to_vec();
        let (t_sig, seq) = run_amps_feedback(&v);
        assert_eq!(t_sig, 139629729);
        assert_eq!(seq, [9,8,7,6,5]);
    }

    #[test]
    fn test5(){
        let v: Vec<i64> = [3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10].to_vec();
        let (t_sig, seq) = run_amps_feedback(&v);
        assert_eq!(t_sig, 18216);
        assert_eq!(seq, [9,7,8,5,6]);
    }
}