use std::cmp::min;
use std::cmp::max;
use crate::util::*;

type DirFn = dyn Fn(&mut i32, &mut i32, i32);

#[derive(Copy,Clone)]
struct Point{
	x: i32,
	y: i32
}


#[derive(Copy,Clone)]
struct MoveInstruction {
	m_num: i32,
	m_dir: &'static DirFn
} 


impl MoveInstruction{
	fn new(instruction: &str) -> Self {
		let num = instruction[1..].parse::<i32>().unwrap();
		let dir:&'static DirFn = match instruction.chars().nth(0).unwrap(){
			'U'=> &U,
			'D'=> &D,
			'L'=> &L,
			'R'=> &R,
			_=>panic!("unrecognized instruction {:?}", instruction),
		};

		MoveInstruction {m_num : num, m_dir : dir}
	}
	fn change(self, x: &mut i32, y: &mut i32){
		(self.m_dir)(x, y, self.m_num);
	}
	fn iterate_w1(self, x:i32, y:i32, v: &mut Vec<i32>, size_x:i32, starting_steps: i32) -> i32 {
		let mut in_x = x;
		let mut in_y = y;

		for i in 0..self.m_num {
			set(v, in_x, in_y, size_x, i + starting_steps);
			(self.m_dir)(&mut in_x, &mut in_y, 1);
		}

		self.m_num + starting_steps
	}
	fn iterate_w2(self, start: Point, v: &[i32], size_x:i32, closest_point: &mut Point, best_steps: &mut i32, center: Point, starting_steps: i32) -> i32 {
		let mut in_x = start.x;
		let mut in_y = start.y;

		for i in 0..self.m_num {
			let steps_w1 = get_steps(v, in_x, in_y, size_x);
			if steps_w1 > 0 {
				let pt = Point{x : in_x, y : in_y};
				if dist(pt, center) < dist(*closest_point, center) {
					*closest_point = pt;
				}
				let steps = starting_steps + i + steps_w1;
				if steps < *best_steps {
					*best_steps = steps;
				}
			}
			(self.m_dir)(&mut in_x, &mut in_y, 1);
		}

		self.m_num + starting_steps
	}
}
#[allow(non_snake_case)]
fn U(_x:&mut i32, y:&mut i32, num: i32){
	*y -= num;
}

#[allow(non_snake_case)]
fn D(_x:&mut i32, y:&mut i32, num: i32){
	*y += num;
}

#[allow(non_snake_case)]
fn L(x:&mut i32, _y:&mut i32, num: i32){
	*x -= num;
}

#[allow(non_snake_case)]
fn R(x:&mut i32, _y:&mut i32, num: i32){
	*x += num;
}


pub fn run<T>(_c: T) -> (i64, i64) {

		let mut v1 = vec![];
		let mut v2 = vec![];
		let mut second_line = false;
    read_data("./input/day03_wires.txt", |line|{
    	if second_line {
    		line.split(',').for_each(|instruction| v2.push(MoveInstruction::new(instruction)));
    	} else {
    		line.split(',').for_each(|instruction| v1.push(MoveInstruction::new(instruction)));
    	};
    	second_line = true;
    });
    run_wires(v1, v2)
}

fn run_wires(v1:Vec<MoveInstruction>, v2:Vec<MoveInstruction>) -> (i64, i64) {
		let mut min_x : i32 = 0;
		let mut min_y : i32 = 0;
		let mut max_x : i32 = 0;
		let mut max_y : i32 = 0;
		let mut    x : i32 = 0;
		let mut    y : i32 = 0;
		for instruction in v1.iter() {
			instruction.change(&mut x, &mut y);
			min_x = min(min_x, x);
			min_y = min(min_y, y);
			max_x = max(max_x, x);
			max_y = max(max_y, y);
		}

		x = 0;
		y = 0;
		for instruction in v2.iter() {
			instruction.change(&mut x, &mut y);
			min_x = min(min_x, x);
			min_y = min(min_y, y);
			max_x = max(max_x, x);
			max_y = max(max_y, y);
		}
		println!("Calculated bounds : [{:?} {:?}]->[{:?} {:?}]", min_x, min_y, max_x, max_y);
		let size_x = 1 + max_x - min_x;
		let size_y = 1 + max_y - min_y;

		println!("grid size is : {:?}x{:?}", size_x, size_y);
		let start = Point{x: -min_x, y:-min_y};
		let mut closest_point = Point{x: start.x + size_x, y: start.y + size_y};

		x = start.x;
		y = start.y;
		println!("starting node is at : [{:?} {:?}]", x, y);
		let mut bits : Vec<i32> = vec![0; (size_y * size_x) as usize];
		let mut steps = 0;
		for instruction in v1.iter() {
			steps = instruction.iterate_w1(x, y, &mut bits, size_x, steps);
			instruction.change(&mut x, &mut y);
		}
		x = start.x;
		y = start.y;
		steps = 0;

		let mut best_steps = std::i32::MAX;

		set(&mut bits, start.x, start.y, size_x, 0);
		for instruction in v2.iter() {
			steps = instruction.iterate_w2(Point{x, y}, &bits, size_x, &mut closest_point, &mut best_steps, start, steps);
			instruction.change(&mut x, &mut y);
		};

		(dist(closest_point, start) as i64, best_steps as i64)
}

fn get_steps(v:&[i32], x:i32, y:i32, width:i32) -> i32{
	let idx = (y * width + x) as usize;
	v[idx]
}

fn set(v:&mut std::vec::Vec<i32>, x:i32, y:i32, width:i32, val:i32){
	let idx = (y * width + x) as usize;
	v[idx] = val;
}

fn dist(a:Point, b:Point) -> i32 {
	(a.x - b.x).abs() + (a.y - b.y).abs()
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test1(){
    	let w1 = vec!["R8","U5","L5","D3"];
    	let w2 = vec!["U7","R6","D4","L4"];
    	let (dist, steps) = run_test(w1, w2);
    	assert_eq!(dist, 6);
    	assert_eq!(steps, 30);
    }
    #[test]
    fn test2(){

    	let w1 = vec!["R75","D30","R83","U83","L12","D49","R71","U7","L72"];
    	let w2 = vec!["U62","R66","U55","R34","D71","R55","D58","R83"];
    	let (dist, steps) = run_test(w1, w2);
    	assert_eq!(dist, 159);
    	assert_eq!(steps, 610);
    }
    #[test]
    fn test3(){
    	let w1 = vec!["R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"];
    	let w2 = vec!["U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"];
    	let (dist, steps) = run_test(w1, w2);
    	assert_eq!(dist, 135);
    	assert_eq!(steps, 410);
    }
    fn run_test(w1:Vec<&str>, w2:Vec<&str>) -> (i64, i64){
    	let v1 = w1.iter().map(|inst| MoveInstruction::new(inst.to_owned())).collect();
    	let v2 = w2.iter().map(|inst| MoveInstruction::new(inst.to_owned())).collect();
    	run_wires(v1, v2)
    }
}
