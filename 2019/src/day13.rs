
use cursive::CbSink;
use std::sync::mpsc::channel;
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};
use crate::util::*;
use crate::days::day02::*;
use std::thread;
use std::time::{Duration, Instant};
use cursive::views::{TextView, Dialog};
use cursive::view::Identifiable;
use cursive::event;

use rand::Rng;

use std::collections::HashMap;

const CHEAT : bool = false;
const SCORE_COORD : Point = Point{x: -1, y:0};
pub fn run(cs: CbSink) ->(i64, i64) {
	let v = read_input("./input/day13_arcade.txt");
	let (map1, size) = run_game(copy(&v));
	let count_blocks = count_tiles(&map1, 2);
	let cheat = Arc::new(AtomicBool::new(CHEAT));
	let cheat_clone = cheat.clone();

	cs.send(Box::new(|siv|{
		siv.add_global_callback('c', move |_| {
			let val = cheat_clone.load(Ordering::Relaxed);
			cheat_clone.store(!val, Ordering::Relaxed);
		});
	})).unwrap();
	let final_score = loop{
		cs.send(Box::new(|siv|{
			siv.clear_global_callbacks(event::Key::Up);
			siv.clear_global_callbacks(event::Key::Left);
			siv.clear_global_callbacks(event::Key::Right);
		})).unwrap();

		let mut v2 = copy(&v);
		v2[0] = 2;//insert quarters
		if cheat.load(Ordering::Relaxed) {
			for i in 1652..1695 {
				v2[i] = 1; //cheat by adding a wall below us
			}
		}

		let (final_score, won) = play_game(v2, size, cs.clone(), cheat.load(Ordering::Relaxed));
		println!("{:?}", (final_score, won));
		if won {
			break final_score
		}
	};
	(count_blocks, final_score)
}
fn count_tiles(map:&HashMap<Point, i64>, tile_type: i64) -> i64{
	map.iter().fold(0, | acc, (_, val) | if *val == tile_type {acc + 1 } else {acc})
}
fn play_game(v: Vec<i64>, screen_size: Point, cbs : CbSink, cheat : bool)-> (i64, bool) {
	let (cab_send, recv_here) = channel();
	let (handle, send_input) = IntComputer::run_code_async(v, cab_send, "cabinet".to_string(), false);

	let mut rng = rand::thread_rng();
	let sc = make_screen(&HashMap::new(), screen_size, 0);
	match cbs.send(
			Box::new(move |c|{
				let tv = TextView::new(sc).with_id("cabinet");
				c.add_layer(Dialog::around(tv));
			})
		){
		Err(_) => println!("error showing result for day 13"),
		Ok(_) => ()
	}

	let mut score = 0;

	let send1 = send_input.clone();
	let send2 = send_input.clone();
	let send3 = send_input.clone();

	let dead = Arc::new(AtomicBool::new(false));
	let dead_clone = dead.clone();

	cbs.send(Box::new(move |siv|{
		let dc = dead_clone.clone();
		siv.add_global_callback(event::Key::Left, move |_| {
			match send1.send(-1) {
				Err(_e) => dc.store(true, Ordering::Relaxed),
				Ok(())=>()
			}});
		let dc = dead_clone.clone();
		siv.add_global_callback(event::Key::Up, move |_| {
			match send2.send(0) {
				Err(_e) => dc.store(true, Ordering::Relaxed),
				Ok(())=>()
			}});
		let dc = dead_clone.clone();
		siv.add_global_callback(event::Key::Right, move |_| {
			match send3.send(1) {
				Err(_e) => dc.store(true, Ordering::Relaxed),
				Ok(())=>()
			}});
	})).expect("events not setup!");
	let map = &mut HashMap::new();
	let mut screen_data = vec![];
	let mut now = Instant::now();
	while count_tiles(map, 2) == 0 {
		screen_data.push(recv_here.recv().unwrap());
		screen_data.push(recv_here.recv().unwrap());
		screen_data.push(recv_here.recv().unwrap());
		screen_data = update_screen(screen_data, map, &mut score);
	}
	let result = loop {
		screen_data.extend(recv_here.try_iter());
		screen_data = update_screen(screen_data, map, &mut score);

		if cheat {
			if send_input.send(rng.gen_range(-1, 2)).is_err(){
				break count_tiles(map, 2) == 0;
			}
			//thread::sleep(Duration::from_millis(1));
		} else {
			thread::sleep(Duration::from_millis(100));
		}
		if now.elapsed() > Duration::from_millis(50) {
			now = Instant::now();
			let screen = make_screen(&map, screen_size, score);
			if cbs.send(Box::new(move |c|{
				c.call_on_id("cabinet", |view: &mut TextView| {
	        view.set_content(screen);
	    	});
			})).is_err() ||dead.load(Ordering::Relaxed) {
				break dead.load(Ordering::Relaxed) && count_tiles(map, 2) == 0;
			}
		}
	};
	println!("score on out {:?}", score);
	screen_data.extend(recv_here.iter());
	println!("still {:?} to read", screen_data);
	update_screen(screen_data, map, &mut score);
	let screen = make_screen(&map, screen_size, score);
	cbs.send(Box::new(move |c|{
		c.call_on_id("cabinet", |view: &mut TextView| {
      view.set_content(screen);
  	});
  })).unwrap();
	println!("score after last hurrah {:?}", score);

	handle.join().unwrap();
	cbs.send(Box::new(move |siv|{
		siv.pop_layer();
	}));
	(score, result)
}
fn make_screen(pixels: &HashMap<Point, i64>, size: Point, score : i64)-> String {
	let mut ret = String::with_capacity( ( ( size.x +1 ) * (size.y + 1)) as usize );
	ret.push_str(&format!("score : {}\n", score));
	for y in 0..size.y {
		for x in 0..size.x {
			let ch = match pixels.get(&Point{x, y}) {
				Some(t) => match t {
					0 =>  ' ',
					1 =>  '█',
					2 =>  '▄',
					3 =>  '=',
					4 =>  '*',
					i => panic!("unknown type {:?}", i)
				},
				None => ' '
			};
			ret.push(ch);
		}
		ret.push('\n');
	}
	ret
}
fn run_game(v: Vec<i64>)-> (HashMap<Point, i64>, Point){
	let (_, output, _) = run_code(v, vec![], false);
	let (map, max_coord, _) = read_screen(output);
	(map, max_coord)
}
fn read_screen(data: Vec<i64>) -> (HashMap<Point, i64>, Point, i64) {
	let mut map:HashMap<Point, i64> = HashMap::new();

	let mut max_coord = Point{x: 1, y:1};
	let mut i = 0;
	let mut score = -1;
	loop {
		if i >= data.len() {
			break (map, max_coord, score);
		}
		let x = data[i+0] as i32;
		let y = data[i+1] as i32;
		let t = data[i+2] as i64;

		let coord = Point{x, y};
		if coord == SCORE_COORD {
			score = t;
		}else {
			map.insert(coord, t);
		}

		if max_coord.x <= x {
			max_coord.x = x+1;
		}
		if max_coord.y <= y {
			max_coord.y = y+1;
		}
		i+=3
	}
}
fn update_screen<'a>(data: Vec<i64>, pixels: &'a mut HashMap<Point, i64>, score: &'a mut i64) -> Vec<i64> {
	let mut i = 0;
	loop {
		if i+3 > data.len() {
			break;
		}
		let x = data[i+0] as i32;
		let y = data[i+1] as i32;
		let t = data[i+2] as i64;

		let coord = Point{x, y};
		if coord == SCORE_COORD {
			*score = t;
		}else {
			pixels.insert(coord, t);
		}
		i+=3
	}
	data[i..].to_vec()
}