use crate::util::file_to_lines;
use std::collections::HashMap;

pub fn run<'a, T>(_c: T)-> (i64, i64){
	let path = "./input/day06_orbits.txt";
	let mut master = String::new();
	let strs = file_to_lines(&mut master, path);
	let g = construct_ograph(&strs);
	(
		g.count_all() as i64,
		g.find_way("YOU", "SAN") as i64
	)
}

struct Graph<'a>{
	nodes:Vec<Box<Orbit<'a>>>,
	indices:HashMap<&'a str, usize>
}

impl<'a> Graph<'a>{
	pub fn with_capacity(cap:usize)->Graph<'a>{
		let nodes = Vec::with_capacity(cap);
		let idces = HashMap::with_capacity(cap);
		Graph{
			nodes:nodes,
			indices:idces
		}
	}
	pub fn add<'b>(&'b mut self, name: &'a str, center: &'a str) {
		let mut index = self.nodes.len();
		let mut c = self.indices.get(center).cloned();
		if c.is_none() {
			c = Some(index);
			let o = Orbit { center: None, name : center };
	  	self.nodes.push(Box::new(o));
	  	self.indices.insert(center, index);

	  	index += 1
		}

		if self.has(name) {
	  	let oi = self.indices.get(name).cloned().unwrap();
	  	self.nodes[oi] = Box::new(Orbit { center: c, name });
		}else{
	  	let o = Orbit { center: c, name };
	  	self.nodes.push(Box::new(o));
	  	self.indices.insert(name, index);
		}
	}
	fn has(&self, name: &str)->bool{
		self.indices.get(name).is_some()
	}
	fn count_all(&self) -> usize{
		let mut total = 0;
		for (key, _) in self.indices.iter() {
			total += self.length(key);
		}
		total
	}
	fn find_way(&self, from: &str, to:&str)->usize{
		let o1 = self.get(from);
		let o2 = self.get(to);
		let c1 = o1.center.unwrap();
		let c2 = o2.center.unwrap();
		if c1 == c2 {
			0
		} else {
			let l1 = self.length(from);
			let l2 = self.length(to);
			if l1 < l2 {
				self.find_way(from, self.nodes[c2].name) + 1
			} else {
				self.find_way(self.nodes[c1].name, to) + 1
			}
		}
	}
	fn get(&self, name: &str)->Orbit{
		*self.nodes[self.indices.get(name).cloned().unwrap()]
	}
	fn length(&self, name: &str)->usize{
		let mut idx = self.indices.get(name).copied();

		let mut len = 0;
		loop {
			if len > 1000{
				println!("NO! length of {:?} is running in loops", name);
			}
			if len > 2000{
				panic!("NO! length of {:?} is running in loops", name);
			}
			match idx {
				Some(i) => {
					idx = (*self.nodes.get(i).unwrap()).center;
					len += 1;
				},
				None => break,
			}
		}
		len -1
	}
}

#[derive(Copy, Clone)]
struct Orbit<'a>{
	center:Option<usize>,
	name: &'a str
}

impl Orbit<'_> {
}

fn construct_ograph <'a>(data:&[&'a str])-> Graph<'a>{
	let mut back = Graph::with_capacity(data.len() + 1);

	for val in data{
		let vals: Vec<&str> = (*val).split(')').collect();
		let center_name = vals[0];
		let obj_name = vals[1];

		back.add(obj_name, center_name);
	}
	back
}

#[cfg(test)]
mod tests {
	// use crate::util::copy;
  use super::*;
    #[test]
    fn test1(){
    	let v = vec!["COM)B","B)C","C)D","D)E","E)F","B)G","G)H","D)I","E)J","J)K","K)L"];
    	let g = construct_ograph(&v);
    	assert_eq!(g.count_all(), 42);
    }
    #[test]
    fn test2(){
    	let v = vec!["COM)B","B)C","C)D","D)E","E)F","B)G","G)H","D)I","E)J","J)K","K)L","K)YOU","I)SAN"];
    	let g = construct_ograph(&v);
    	assert_eq!(g.find_way("YOU", "SAN"), 4);
    }
  }