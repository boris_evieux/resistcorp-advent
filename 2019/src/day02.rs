use std::thread::JoinHandle;
use std::collections::HashMap;
use std::thread;
use crate::util::*;

use std::sync::mpsc::{channel, Sender};

const INPUT_SEARCH_VAL: i64 = 19_690_720;

pub fn run<T>(_c: T) -> (i64, i64) {

    let v = read_input("./input/day02_intcode.txt");
    let s1 = set_and_run_code(copy(&v), 12, 2);
    let search = INPUT_SEARCH_VAL;
    for noun in 0..100 {
        for verb in 0..100 {
            let ret = set_and_run_code(copy(&v), noun, verb);
            if ret == search{
                return (s1, ret);
            }
        }
    }
    (s1, 0)
}
fn set_and_run_code(mut v: Vec<i64>, noun: i64, verb: i64)->i64{

    v[1] = noun;
    v[2] = verb;
    run_code(v, vec![], false).0
}
pub fn read_input(file: &str)->Vec<i64>{
    let mut v = vec![];
    read_data(file, |line|{
            line.split(',').for_each(|i|v.push(parse_value(i)));
        }
    );
    v
}

pub fn run_code(codes: Vec<i64>, input:Vec<i64>, debug: bool)->(i64, Vec<i64>, Vec<i64>){
    let (snd_from, rcv_here) = channel();
    let cp = copy(&codes);
    let (handle, snd_into) = IntComputer::run_code_async(cp, snd_from, String::from("single"), debug);
    for i in input {
        match snd_into.send(i){
            Err(_) => panic!("IntCode computer is not accepting msgs"),
            _ => continue
        };
    }
    let output = rcv_here.iter().collect();
    let mut comp = handle.join().expect("IntCode died unexpectedly");
    let ret = comp.read(0);
    (ret, output, comp.page_at(0).1.to_vec())
}
const PAGE_SIZE : i64 = 4096;
pub struct IntComputer{
    rel_base: i64,
    memory: Vec<Vec<i64>>,
    page_indices: HashMap<i64, usize>
}
impl IntComputer{
    pub fn new(code: &[i64]) ->IntComputer{
        let mem = vec![];
        let ind = HashMap::new();

        let mut ret = IntComputer{
            page_indices : ind,
            rel_base : 0,
            memory : mem,
        };
        let mut ptr = 0usize;
        let page_size = PAGE_SIZE as usize;
        loop{
            let (_, page) = ret.page_at(ptr as i64);
            let ptr_end = ptr + page_size;
            if ptr_end >= code.len() {
                page[0..code.len()].copy_from_slice(&code[ptr..]);
                break;
            }
            page.copy_from_slice(&code[ptr..ptr_end]);
            ptr = ptr_end;
        }
        ret
    }
    fn allocate(&mut self, page_index: i64) -> &mut Vec<i64> {
        let page = vec![0;PAGE_SIZE as usize];
        self.page_indices.insert(page_index, self.memory.len());
        self.memory.push(page);
        self.page_at(page_index).1
    }
    fn read_operation(&mut self, pos: i64) -> i64 {
        let ptr = (pos % PAGE_SIZE) as usize;
        let (_, page) = self.page_at(pos);
        assert!(page.len() > ptr);
        page[ptr]
    }
    fn page_at(&mut self, pos: i64) -> (i64, &mut Vec<i64>) {
        let page_index = pos / PAGE_SIZE;
        (page_index, match self.page_indices.get(&page_index) {
            Some(p) => &mut self.memory[*p],
            None =>self.allocate(page_index)
        })
    }
    fn read(&mut self, pos: i64) -> i64 {
        let (_, v) = self.page_at(pos);
        v[(pos%PAGE_SIZE) as usize]
    }
    fn write(&mut self, pos: i64, val: i64) {
        let (_, v) = self.page_at(pos);
        v[(pos%PAGE_SIZE) as usize] = val;
    }
    fn read_param(&mut self, pos: i64, mode: i64) -> i64 {
        let val = self.read(pos);
        match mode {
            0 => self.read(val as i64), //position mode
            1 => val, //immediate mode
            2 => self.read(val as i64 + self.rel_base), //relative mode
            _ => panic!("unsupported param mode `{:?}`", mode), //immediate mode
        }
    }
    pub fn run_code_async(mut cp: Vec<i64>, snd_from:Sender<i64>, name:String, debug: bool) -> (JoinHandle<IntComputer>, Sender<i64>) {

        let mut comp = IntComputer::new(&mut cp);

        if debug {
            println!("starting core {} in debug mode", name);
        }

        let in_name = String::from(&name);
        let mut ptr = 0i64;
        let mut done = false;
        let builder = thread::Builder::new().name(name.into());
        let (snd_into, rcv_inside) = channel();
        (builder.spawn(move ||{
            while !done{
                let operation = comp.read_operation(ptr);
                let opcode = operation % 100;
                let mode1 = digit_at(operation, 2);
                let mode2 = digit_at(operation, 3);
                let mode3 = digit_at(operation, 4);

                match opcode{
                    1 =>{
                        let v1 = comp.read_param(ptr+1, mode1);
                        let v2 = comp.read_param(ptr+2, mode2);
                        let dest = match mode3{
                            0 => comp.read(ptr+3),
                            2 => comp.rel_base + comp.read(ptr+3),
                            _ => panic!("unauthorized mode")
                        };
                        comp.write(dest as i64, v1 + v2);
                        ptr += 4;
                    },
                    2 =>{
                        let v1 = comp.read_param(ptr+1, mode1);
                        let v2 = comp.read_param(ptr+2, mode2);
                        let dest = match mode3{
                            0 => comp.read(ptr+3),
                            2 => comp.rel_base + comp.read(ptr+3),
                            _ => panic!("unauthorized mode")
                        };
                        comp.write(dest as i64, v1 * v2);
                        ptr += 4;
                    },
                    3 =>{
                        if debug {
                            println!("'{}' core waiting on input.", in_name);
                        }
                        let val = rcv_inside.recv().expect("no more input");
                        let dest = match mode1{
                            0 => comp.read(ptr+1),
                            2 => comp.rel_base + comp.read(ptr+1),
                            _ => panic!("unauthorized mode")
                        };
                        if debug {
                            println!("'{}' core received {}.", in_name, val);
                        }
                        comp.write(dest as i64, val);
                        ptr += 2;
                    },
                    4 =>{
                        let src = comp.read_param(ptr+1, mode1);
                        snd_from.send(src).expect("couldn't send over channel.");
                        if debug {
                            println!("'{}' core sent {}.", in_name, src);
                        }
                        ptr += 2;
                    },
                    5=>{
                        //jump-if-true
                        let v1 = comp.read_param(ptr+1, mode1);
                        let v2 = comp.read_param(ptr+2, mode2);

                        ptr = if v1 != 0 {
                            v2 as i64
                        }else{
                            ptr + 3
                        };
                    },
                    6=>{
                        //jump-if-false
                        let v1 = comp.read_param(ptr+1, mode1);
                        let v2 = comp.read_param(ptr+2, mode2);

                        ptr = if v1 == 0 {
                            v2 as i64
                        }else{
                            ptr + 3
                        };
                    },
                    7=>{
                        //less-than
                        let v1 = comp.read_param(ptr+1, mode1);
                        let v2 = comp.read_param(ptr+2, mode2);
                        let dest = match mode3{
                            0 => comp.read(ptr+3),
                            2 => comp.rel_base + comp.read(ptr+3),
                            _ => panic!("unauthorized mode")
                        };
         
                        let result = if v1 < v2 { 1 }else{ 0 };
                        comp.write(dest as i64, result);
                        ptr += 4;
                    },
                    8=>{
                        //equals
                        let v1 = comp.read_param(ptr+1, mode1);
                        let v2 = comp.read_param(ptr+2, mode2);
                        let dest = match mode3{
                            0 => comp.read(ptr+3),
                            2 => comp.rel_base + comp.read(ptr+3),
                            _ => panic!("unauthorized mode")
                        };
         
                        let result = if v1 == v2 { 1 }else{ 0 };
                        comp.write(dest as i64, result);
                        ptr += 4;

                    },
                    9=>{
                        let v1 = comp.read_param(ptr+1, mode1);
                        comp.rel_base += v1 as i64;
                        ptr += 2;
                    },
                    99=>{
                        done = true;
                        if debug {
                            println!("'{}' core done.", in_name);
                        }
                    },
                    code =>panic!("unknown opcode {} at pos {}.", code, ptr)
                }
            }
            drop(snd_from);
            drop(rcv_inside);
            comp
        }).unwrap(),
        snd_into)
    }
}
pub fn digit_at(op: i64, rtl_posit: u32) -> i64 {
    (op / 10i64.pow(rtl_posit)) % 10
}
pub fn internal_assert(output: Vec<i64>, expect: &[i64]){
    let len = expect.len();
    assert_eq!(&output[0..len], expect);
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1(){
        let mut v: Vec<i64> = [1,0,0,0,99].to_vec();
        v = run_code(v, vec![], false).2;
       internal_assert(v, &[2,0,0,0,99]);
    }

    #[test]
    fn test2(){
        let mut v: Vec<i64> = [2,3,0,3,99].to_vec();
        v = run_code(v, vec![], false).2;
       internal_assert(v, &[2,3,0,6,99]);
    }

    #[test]
    fn test3(){
        let mut v: Vec<i64> = [2,4,4,5,99,0].to_vec();
        v = run_code(v, vec![], false).2;
       internal_assert(v, &[2,4,4,5,99,9801]);
    }

    #[test]
    fn test4(){
        let mut v: Vec<i64> = [1,1,1,4,99,5,6,0,99].to_vec();
        v = run_code(v, vec![], false).2;
       internal_assert(v, &[30,1,1,4,2,5,6,0,99]);
    }

    #[test]
    fn test5(){
        let mut v: Vec<i64> = [1,9,10,3,2,3,11,0,99,30,40,50].to_vec();
        v = run_code(v, vec![], false).2;
       internal_assert(v, &[3500,9,10,70,2,3,11,0,99,30,40,50]);
    }
}