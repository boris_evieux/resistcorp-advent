use core::fmt::{Formatter,Error};
use core::hash::{Hash, Hasher};
use std::collections::HashSet;
use std::collections::HashMap;
use core::fmt::Debug;
use crate::util::*;
use std::f64::consts::PI;
use std::f64::consts::FRAC_PI_2;
use std::f64::consts::FRAC_PI_4;

const TWO_PI : f64 = PI * 2.0;
pub fn run<T>(_c: T) -> (i64, i64){
    let asters = make_grid_from_file("./input/day10_asteroids.txt");
    let best = best_point(&asters);
    let last = destroy_process(&asters, best.1, 200);
    let v2 = 100 * last.x + last.y;
    (
        best.0 as i64,
        v2.into()
    )
}

#[derive(Clone)]
struct Aster {
    pos: Point,
    name: char,
    lines: HashSet<Point>
}

impl Aster{
    fn new(name:char, x: i32, y: i32)-> Self {
        let lines = HashSet::new();
        let pos = Point{x, y};
        Aster{pos, name,lines}
    }
    fn size(&self)->usize {
        self.lines.len()
    }
}
impl PartialEq for Aster {
    fn eq(&self, other: &Self) -> bool {
        self.pos == other.pos
    }
}
impl Eq for Aster {}
impl Hash for Aster {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.pos.hash(state);
        self.name.hash(state);
    }
}
impl Debug for Aster {
    fn fmt(&self, f:&mut Formatter )->Result<(), Error>{
        write!(f, "asteroid at {:?}", self.pos )
    }
}
type Grid = Vec<Aster>;

fn best_point(data: &Grid)->(usize, Point){
    let mut best = (0 , Point{x: 0, y: 0});
    for aster in data {
        if aster.size() > best.0 {
            best = (aster.size(), aster.pos.clone())
        }
    }

    best
}
fn destroy_process(data: &Grid, station: Point, nth: usize) -> Point {

    let mut lines_of_sight : HashMap<Point, Vec<Point>> = HashMap::new();

    for aster in data {
        if aster.pos == station {
            continue;
        }
        let dir = take_direction(station, aster.pos);
        let mut the_points : Vec<Point> = match lines_of_sight.get(&dir) {
            Some(points) => points.to_vec(),
            None => Vec::new().to_vec()
        };
        the_points.push(aster.pos);
        the_points.sort_by_key(|position| square_dist(station, *position));
        lines_of_sight.insert(dir, the_points.to_vec());
    }
    let mut keys:Vec<Point> = Vec::with_capacity(lines_of_sight.len());
    for (key, _) in &lines_of_sight {
        keys.push(*key);
    }
    keys.sort_unstable_by(|a, b| take_angle(*a).partial_cmp(&take_angle(*b)).unwrap());

    // println!("we see {:?} lines", lines_of_sight.len());
    let mut i = 0;
    loop{
        let mut changed = false;
        for dir in &keys {
            let points: &mut Vec<Point> = lines_of_sight.get_mut(dir).unwrap();
            if points.len() > 0 {
                i+=1;
                let pt = points.remove(0);
                // println!("{}th destroyed {:?}", i, pt);
                // println!("line of sight was {:?}", dir);
                // println!("remaining in that dir {:?}", points);
                if i == nth {
                    // println!("last destroy {:?}", pt);
                    return pt;
                }
                changed = true;
            }
        }
        // println!("one  turn, {:?} destroyed", i);
        assert!(changed);
    }
}
fn push_line(asters: &mut Grid, line : &str, y: usize){
    let mut x = 0;
    // let mut line_of_asters = Vec::with_capacity(line.len());
    for c in line.chars() {
        match c {
            '.'     => (),
            name    => push_aster(asters, Aster::new(name, x as i32, y as i32))
        };
        x += 1;
    }
}
fn push_aster(asters: &mut Grid, mut aster : Aster){
    // let mut line_of_asters = Vec::with_capacity(line.len());
    for i in 0..asters.len() {
        let prev = &mut asters[i];
        let mut direction = take_direction(prev.pos, aster.pos);
        if !prev.lines.contains(&direction){
            prev.lines.insert(direction);
        }
        direction = take_direction(aster.pos, prev.pos);
        if !aster.lines.contains(&direction){
            aster.lines.insert(direction);
        }
    }
    asters.push(aster);
}
fn take_direction(from: Point, to: Point) -> Point{
    Point{
        x : to.x - from.x,
        y : to.y - from.y
    }.normalize()
}
fn fmt_angle(a:f64) -> String{
    format!("{:.8}", a)
}
fn take_angle(direction: Point) -> f64{
    let x = -direction.y as f64;
    let y = direction.x as f64;

    (y.atan2(x) + TWO_PI ) % TWO_PI
}
fn square_dist(from: Point, to: Point) -> i32{
    let vec = Point{
        x : to.x - from.x,
        y : to.y - from.y
    };
    vec.x * vec.x + vec.y * vec.y
}
fn make_grid_from_file(path: &str) ->Grid{
    let mut lines = vec![];
    read_data(path,
        |line| {
            let st = String::from(line);
            lines.push(st)
        }
    );
    make_grid(lines.iter().map(|s| &**s).collect())
}
fn make_grid(lines: Vec<&str>) ->Grid{
    let mut asters = vec![];
    for (y, line) in lines.iter().enumerate() {
        push_line(&mut asters, line, y)
    }
    asters
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(
            best_point(
                &make_grid([

                    ".#..#",

                    ".....",

                    "#####",

                    "....#",

                    "...##"].to_vec()
                )
            ),
            (8, Point{x: 3, y: 4})
        );
    }

    #[test]
    fn test2() {
        assert_eq!(
            best_point(
                &make_grid([

                    "......#.#.",
                    "#..#.#....",
                    "..#######.",
                    ".#.#.###..",
                    ".#..#.....",
                    "..#....#.#",
                    "#..#....#.",
                    ".##.#..###",
                    "##...#..#.",
                    ".#....####"

                    ].to_vec()
                )
            ),
            (33, Point{x: 5, y: 8})
        );
    }

    #[test]
    fn test3() {
        assert_eq!(
            best_point(
                &make_grid([
                    "#.#...#.#.",
                    ".###....#.",
                    ".#....#...",
                    "##.#.#.#.#",
                    "....#.#.#.",
                    ".##..###.#",
                    "..#...##..",
                    "..##....##",
                    "......#...",
                    ".####.###."
                    ].to_vec()
                )
            ),
            (35, Point{x: 1, y: 2})
        );
    }

    #[test]
    fn test4() {
        assert_eq!(
            best_point(
                &make_grid([
                    ".#..#..###",
                    "####.###.#",
                    "....###.#.",
                    "..###.##.#",
                    "##.##.#.#.",
                    "....###..#",
                    "..#.#..#.#",
                    "#..#.#.###",
                    ".##...##.#",
                    ".....#.#.."
                    ].to_vec()
                )
            ),
            (41, Point{x: 6, y: 3})
        );
    }
const BIG_TEST_GRID :[&str; 20] = [
                    ".#..##.###...#######",
                    "##.############..##.",
                    ".#.######.########.#",
                    ".###.#######.####.#.",
                    "#####.##.#.##.###.##",
                    "..#####..#.#########",
                    "####################",
                    "#.####....###.#.#.##",
                    "##.#################",
                    "#####.##.###..####..",
                    "..######..##.#######",
                    "####.##.####...##..#",
                    ".#####..#.######.###",
                    "##...#.##########...",
                    "#.##########.#######",
                    ".####.#.###.###.#.##",
                    "....##.##.###..#####",
                    ".#.#.###########.###",
                    "#.#.#.#####.####.###",
                    "###.##.####.##.#..##"
                    ];

    #[test]
    fn test5() {
        assert_eq!(
            best_point(
                &make_grid(BIG_TEST_GRID.to_vec())
            ),
            (210, Point{x: 11, y: 13})
        );
    }
    #[test]
    fn test6() {
        assert_eq!(
            destroy_process(
                &make_grid(BIG_TEST_GRID.to_vec()),
                Point{x : 11, y : 13},
                3
            ),
            Point{x: 12, y: 2}
        );
        assert_eq!(
            destroy_process(
                &make_grid(BIG_TEST_GRID.to_vec()),
                Point{x : 11, y : 13},
                10
            ),
            Point{x: 12, y: 8}
        );
        assert_eq!(
            destroy_process(
                &make_grid(BIG_TEST_GRID.to_vec()),
                Point{x : 11, y : 13},
                200
            ),
            Point{x: 8, y: 2}
        );
    }


    #[test]
    fn test7() {
        assert_eq!(
            destroy_process(
                &make_grid(BIG_TEST_GRID.to_vec()),
                Point{x : 11, y : 13},
                50
            ),
            Point{x: 16, y: 9}
        );
    }
    #[test]
    fn test8() {
        let grid = [
            ".#....#####...#..",
            "##...##.#####..##",
            "##...#...#.#####.",
            "..#.....#...###..",
            "..#.#.....#....##"
            ];
        assert_eq!(
            best_point(
                &make_grid(grid.to_vec())
            ),
            (30, Point{x: 8, y: 3})
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                1
            ),
            Point{x: 8, y: 1}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                2
            ),
            Point{x: 9, y: 0}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                3
            ),
            Point{x: 9, y: 1}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                4
            ),
            Point{x: 10, y: 0}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                5
            ),
            Point{x: 9, y: 2}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                6
            ),
            Point{x: 11, y: 1}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                7
            ),
            Point{x: 12, y: 1}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                18
            ),
            Point{x: 4, y: 4}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                27
            ),
            Point{x: 5, y: 1}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                36
            ),
            Point{x: 14, y: 3}
        );
        assert_eq!(
            destroy_process(
                &make_grid(grid.to_vec()),
                Point{x : 8, y : 3},
                35
            ),
            Point{x: 13, y: 3}
        );
    }

    #[test]
    fn test_geom() {
        assert_eq!(fmt_angle(take_angle(Point{x: 0, y :-1})), fmt_angle(0.0));
        assert_eq!(fmt_angle(take_angle(Point{x: 1, y :0})), fmt_angle(FRAC_PI_2));
        assert_eq!(fmt_angle(take_angle(Point{x: 0, y :1})), fmt_angle(PI));
        assert_eq!(fmt_angle(take_angle(Point{x: -1, y :0})), fmt_angle(3.0 * FRAC_PI_2));
        assert_eq!(fmt_angle(take_angle(Point{x: 1, y :1})), fmt_angle(3.0 * FRAC_PI_4));

        let d1 = take_direction(Point{x:11, y:13}, Point{x:12, y:1});
        let d2 = take_direction(Point{x:11, y:13}, Point{x:12, y:2});
        let d3 = take_direction(Point{x:11, y:13}, Point{x:13, y:0});
        println!("{:?}", take_angle(d1));
        println!("{:?}", take_angle(d2));
        println!("{:?}", take_angle(d3));

        assert!(take_angle(d1) < take_angle(d2));
        assert!(take_angle(d2) < take_angle(d3));
    }
}