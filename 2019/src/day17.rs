use core::fmt::Display;
use core::fmt::Error;
use core::fmt::Debug;
use std::fmt::Formatter;
use crate::days::day17::CompressSection::*;
use crate::days::day17::ScaffoldType::*;
use crate::days::day17::Move::*;
use cursive::CbSink;
use crate::days::day02::*;
use crate::util::*;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum ScaffoldType {
	Nothing,
	Unexplored,
	Explored
}
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
enum Move {
	Left,
	Right,
	Forward(usize)
}

impl Display for Move {
	fn fmt(&self, f:&mut Formatter )->Result<(), Error>{
		match self {
			Right => write!(f, "R"),
			Left => write!(f, "L"),
			Forward(x) => write!(f, "{}", x),

		}
	}
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct Path(Vec<Move>);

impl Path {
	fn len(&self) -> usize {
		self.0.len()
	}
}
impl Display for Path {
	fn fmt(&self, f:&mut Formatter )->Result<(), Error>{
		let value = self.0.iter().map(Move::to_string).collect::<Vec<String>>().join(",");
		write!(f, "{}", value)
	}
}

impl Display for ScaffoldType {
	fn fmt(&self, f:&mut Formatter )->Result<(), Error>{
		write!(f, "{}", self.ch())
	}
}
impl ScaffoldType{
	fn ch(&self )-> char{
		match self {
			Nothing => '.',
			Unexplored => '#',
			Explored => '@',

		}
	}
}

struct Scaffold{
	data : Vec<Vec<ScaffoldType>>
}

impl Scaffold {
	fn get(&self, pos: Point) -> ScaffoldType {
		let Point{x, y} = pos;
		if x < 0 || y < 0 ||y >= self.data.len() as i32 {
			Nothing
		}else{
			let line = self.data.get(y as usize).unwrap();
			if x >= line.len() as i32{
				Nothing
			}else{
				*line.get(x as usize).unwrap()
			}
		}
	}
	fn explored(&mut self, pos: Point) {
		let Point{x, y} = pos;
		self.data[y as usize][x as usize] = Explored;
	}
	fn all_explored(&mut self) -> bool{
		for line in &self.data {
			for &v in line {
				if v == Unexplored {
					return false;
				}
			}
		}
		true
	}
}

pub fn run(_cbs:CbSink) ->(i64, i64){
	let v = read_input("./input/day17_scaffolds.txt");
	let (_, output, _) = run_code(copy(&v), vec![], false);
	let mut st = String::with_capacity(output.len());
	output.iter().for_each(|val| {
		st.push(std::char::from_u32(*val as u32).unwrap())
	});
	println!("");
	println!("output :\n{}", st);
	let lines : Vec<&str> = st.split('\n').collect();
	let mut calibration = 0;
	let mut data: Vec<Vec<ScaffoldType>> = vec![];
	let mut bot_pos = ZERO;
	let mut bot_dir = 0;
	for y in 0..lines.len() {
		let line = lines[y];
		if line.len() < 2 {
			continue
		}
		let mut map_line = Vec::with_capacity(line.len());
		for x in 0..line.len() {
			let ch = line.chars().nth(x);

			let map_val = match ch {
				Some('#') => Unexplored,
				Some('.') => Nothing,
				Some(bot) => {
					bot_pos.x = x as i32;
					bot_pos.y = y as i32;
					bot_dir = match bot {
						'^' => 0,
						'>' => 1,
						'v' => 2,
						'<' => 3,
						 _  => {
						 	println!("unexpected char on line{}, char{} : {:?}", y, x, bot);
						 	0
						}
					};
					Explored
				},
				None => panic!("NO CHAR TO PARSE")
			};
			if x * y != 0 && x < line.len()-1 && y < lines.len()-1 && map_val == Unexplored {
				let ch_l = line.chars().nth(x-1);
				let ch_r = line.chars().nth(x+1);
				if ch_l == Some('#') && ch_r == Some('#') {
					let ch_u = lines[y-1].chars().nth(x);
					let ch_d = lines[y+1].chars().nth(x);
					if ch_u == Some('#') && ch_d == Some('#') {
						calibration += x * y;
					}
				}
			}
			map_line.push(map_val);
		}
		data.push(map_line);
	}
	println!("the bot starts at {:?}", bot_pos);
	let paths = find_all_paths(&Scaffold{data}, bot_pos, bot_dir);
	println!("found a total of {} possible paths", paths.len());


	for path in &paths {
		if let Some(found) = compress_a(path) {
			println!("the path {} is compressible !!!", path);
			let (comp, a, b, c) = found;

			break;
		}
	};
	// let full_path = "L,12,R,8,L,6,R,8,L,6,R,8,L,12,L,12,R,8,L,12,R,8,L,6,R,8,L,6,L12,R,8,L,6,R,8,L,6,R,8,L,12,L,12,R,8,L,6,R,6,L,12,R,8,L,12,L,12,R,8,L,6,R,6,L,12,L,6,R,6,L,12,R,8,L,12,L,12,R,8";
	(calibration 	as i64, 0)
}

fn find_all_paths(map: &Scaffold, bot_pos: Point, bot_dir: usize) -> Vec<Path>{
	let mut ret = vec![];
	let path = [Left;0];
	explore(map, bot_pos, bot_dir, 0, &path[..], &mut ret);
	ret.sort_by(|a, b| a.len().cmp(&b.len()));

	// for path in &ret[..1000] {
	// 	println!("{}", path);
	// }
	// for path in &ret[ret.len()-100..] {
	// 	println!("{}", path);
	// }
	ret
}
#[derive(Debug, Clone)]
enum CompressSection{
	RoutineA,
	RoutineB,
	RoutineC,
	Uncompressed(Path)
}
impl Display for CompressSection{
	fn fmt(&self, f: &mut Formatter)->Result<(), Error> {
		let value = match self {
			RoutineA => 'A',
			RoutineB => 'B',
			RoutineC => 'C',
			Uncompressed(_) => '?'
		};
		write!(f, "{}", value)
	}
}
struct CompressingPath (Vec<CompressSection>);

impl Display for CompressingPath{
	fn fmt(&self, f: &mut Formatter)->Result<(), Error> {
		let value: String = self.0.iter().map(|sect| sect.to_string()).collect::<Vec<String>>().join(",");
		write!(f, "{}", value)
	}
}

fn compress_a(path: &Path) -> Option<(CompressingPath, Path, Path, Path)> {

	let pattern_length = 2;
	loop {
		let pattern = Path(path.0[0..pattern_length].to_vec());
		if pattern.to_string().len() > 20 {
			break None
		}

		let indices = find_splits(pattern, Path(path.0[pattern_length..].to_vec()));

	}
}
fn compress_b(path: CompressingPath) -> Option<(CompressingPath, Path, Path)> {


	None
}

fn compress_c(path: CompressingPath) -> Option<(CompressingPath, Path)>{

	None
}

fn copy_section(routine : &CompressSection) -> CompressSection {
	match &*routine {
		Uncompressed(path) => Uncompressed(Path(copy(&path.0))),
		a => * a,
	}
}

fn construct_test(path:Path, indices: Vec<usize>, routine: CompressSection, routine_len: usize) -> CompressingPath {
	let mut test = vec![];
	let mut current_ptr = 0;

	for idx in indices {
		if idx > current_ptr {
			test.push(Uncompressed(Path(path.0[current_ptr..idx].to_vec())));
		}
		test.push(routine);

		current_ptr = idx + routine_len;
	}
	if current_ptr < path.len() {
		test.push(Uncompressed(Path(path.0[current_ptr..].to_vec())));
	}

	CompressingPath(test)
}

fn find_splits(pattern:Path, path:Path) -> Vec<Vec<usize>> {
	let possible_indices= vec![];
	let path_len = path.len();
	let pattern_len = pattern.len();

	for i in 0..path_len-pattern_len {
		if path.0[i..i+pattern_len] == pattern.0[..] {
			possible_indices.push(i);
			i+=pattern_len-1;//can't be bothered to deal with this one.
		}
	}

	let max_possibles = 2<<possible_indices.len() -1;

	let ret = vec![];
	for pattern in 1..max_possibles {
		let test = vec![];

		for i in 0..possible_indices.len() {
			let mask = 1 << i;

			if mask & pattern == mask {
				test.push(possible_indices[i]);
			}
		}
		ret.push(test);
	}
	ret
}

fn copy_map(map: &Scaffold) -> Scaffold {
	let mut data = Vec::with_capacity(map.data.len());
	for line in &map.data {
		let mut ln = vec![Nothing; line.len()];
		for (i, v) in line.iter().enumerate() {
			ln[i] = *v;
		}
		data.push(ln);
	}
	Scaffold{data}
}
fn print_map(map: &Scaffold, pos : Point, dir : usize)  {
	let mut data = String::with_capacity(map.data.len() * 50);
	for (y, line) in map.data.iter().enumerate() {
		for (x, v) in line.iter().enumerate() {
			if x == pos.x as usize && y == pos.y as usize {
				data.push(match dir {
					0 => '^',
					1 => '>',
					2 => 'v',
					3 => '<',
					_ => '?',
				});

			}else{
				data.push(v.ch());
			}
		}
		data.push('\n');
	}
	println!("{}", data);
}

fn explore(map: &Scaffold, bot_pos: Point, bot_dir: usize, current_run: usize, path_before: &[Move], all: &mut Vec<Path>) {
	let mut map = copy_map(map);
	let mut pos = bot_pos;
	let mut dir = bot_dir;
	let mut current_run = current_run;
	let mut path = copy(path_before);
	loop {
		map.explored(pos);
		let possibles = possible_moves(&map, pos, dir);
		match possibles.len() {
			0 => {
				if current_run > 0 {
					path.push(Forward(current_run));
				}
				let path = Path(path);
				// println!("path ends at {:?} {}", pos, dir);
				// println!(" >> {}", path);
				if map.all_explored(){
					// println!(" >> all explored");
					all.push(path);
				}
				break
			},
			1 => {
				let mv = possibles[0];
				apply(mv, &mut pos, &mut dir);
				if let Forward(1) = mv {
					current_run += 1;
				}else{
					if current_run > 0 {
						path.push(Forward(current_run));
					}
					path.push(mv);
					current_run = 0;
				}
			},
			l => {
				for i in 0..l {
					let mv = possibles[i];
					//localize
					let mut pos = pos;
					let mut dir = dir;
					let mut path = copy(&path);
					apply(mv, &mut pos, &mut dir);
					if let Forward(1) = mv {
						explore(&map, pos, dir, current_run+1, &path, all);
					}else{
						if current_run > 0 {
							path.push(Forward(current_run));
						}
						path.push(mv);
						apply(Forward(1), &mut pos, &mut dir);
						explore(&map, pos, dir, 1, &path, all);
					}
				}
				break;
			}
		}
	}
}

fn apply(mv: Move, pos : &mut Point, dir: &mut usize){
	match mv {
		Left  => *dir = (*dir + 3 ) % 4,
		Right => *dir = (*dir + 1 ) % 4,
		Forward(1) => *pos = pos.add(DIRS[*dir]),
		Forward(_) => panic!("can only apply one step moves"),
	}
}

fn possible_moves(map: &Scaffold, bot_pos: Point, bot_dir: usize) -> Vec<Move> {
	let mut ret = vec![];
	let forward = bot_pos.add(DIRS[bot_dir]);
	let left = bot_pos.add(DIRS[ ( bot_dir+3 ) % 4]);
	let right = bot_pos.add(DIRS[ ( bot_dir + 1) % 4]);
	if Unexplored == map.get(left) {
		ret.push(Left)
	}
	if Unexplored == map.get(right) {
		ret.push(Right)
	}
	if Unexplored == map.get(forward) {
		ret.push(Forward(1))
	}
	if Explored == map.get(forward) && 0 == ret.len() {
		ret.push(Forward(1))
	}
	ret
}
