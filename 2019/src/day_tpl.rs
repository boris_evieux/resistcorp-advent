use crate::util::*;


pub fn run() -> (i64, i64){
	let mut data = String::new();
	read_data("./input/dayXXX_image.txt",
		|line| data.push_str(line));

	intern_run()
}

fn intern_run()->(i32, i32){

  (0 , 0)
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(intern_run(), (2,2));
    }
}