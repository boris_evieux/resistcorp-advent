use cursive::views::{TextView, Dialog};
use cursive::CbSink;
use crate::util::*;

const SIZE : (usize, usize) = (25,6);

pub fn run(cs: CbSink) -> (i64, i64){
	let mut data = String::new();
	read_data("./input/day08_image.txt",
		|line| data.push_str(line));

	let data = &data[0..];
	let layer_length = SIZE.0 * SIZE.1;
	assert!(data.len() % layer_length == 0);
	let num_layers = data.len() / layer_length;
	println!("num_layers {:?}", num_layers);
	let mut best_layer1 = (num_layers, layer_length +1, [0; 10]);

	let mut final_img = [['2'; SIZE.0];SIZE.1];
	for layer_num in 0..num_layers {
		let layer_start = layer_length * layer_num;
		let mut histo = [0; 10];
		for i in 0..layer_length {
			let idx = layer_start+i;
			let digit = data[idx..idx+1].parse::<usize>().expect("digit failed to parse");
			histo[digit] += 1;
			let x = i % SIZE.0;
			let y = /*SIZE.1 - 1 - */i / SIZE.0;
			if final_img[y][x] == '2' {
				final_img[y][x] = match digit{
					0 => ' ',
					1 => '█',
					2 => '2',
					_ => panic!("unknown digit"),
				};
			}
		}
		// println!("in layer {:?} : {:?}", layer_num, histo);
		if histo[0] < best_layer1.1 {
			// println!("best so far");
			best_layer1 = (layer_num, histo[0], histo);
		}
	}
	let mut res_str = String::new();
	println!("actual result for day 8");
	for l in final_img.iter() {
		let mut s = String::with_capacity(SIZE.0);
		for c in l { s.push(*c) }
		res_str.push_str(&s);
		println!("{}", s);
		
		res_str.push('\n');
	}
	match cs.send(Box::new(|c|c.add_layer(Dialog::around(TextView::new(res_str))))){
		Err(_) => println!("error showing result for day 08"),
		Ok(_) => ()
	}
	let h = best_layer1.2;
	let ans1 = h[1] * h[2];
  (ans1 as i64 , -99)
}
