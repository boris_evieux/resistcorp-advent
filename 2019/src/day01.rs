
use crate::util::*;

pub fn run<T>(_c: T) -> (i64, i64){
    let mut total = (0, 0);

    let cb = |line: &str| {
                let mass = parse_value(line) as i64;
                total.0 += requirement1(mass);
                total.1 += requirement2(mass);
            };
    read_data("./input/day01_rocket-tyranny.txt", cb);

    total
}
fn requirement1(mass: i64)-> i64{
    let ret = (mass / 3) as i64 - 2;
    if ret <= 0{
        0
    }else{
        ret
    }
}
fn requirement2(mass: i64)-> i64{
	let req = (mass / 3) as i64 - 2;
    match req {

        x if x <=0 => 0,

        _ => requirement2(req) as i64 + req,

    }
}
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(requirement1(12), 2);
        assert_eq!(requirement1(14), 2);
        assert_eq!(requirement1(1969), 654);
        assert_eq!(requirement1(100756), 33583);
    }
    #[test]
    fn test2() {
        assert_eq!(requirement2(14), 2);
        assert_eq!(requirement2(1969), 966);
        assert_eq!(requirement2(100756), 50346);
    }
}