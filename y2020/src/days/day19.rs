use std::collections::BTreeSet;
use crate::days::day19::Rules::*;

use lib::make_main;
make_main!(2020, 19);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut rules = vec![""; 500];
	let mut it = lines.iter();
	let mut max = 0usize;
	loop {
		let line = *it.next().unwrap();
		if line == "" {
			break;
		}
		let mut split = line.split(": ");
		let id:usize = split.next().unwrap().parse().unwrap();
		let value = split.next().unwrap();
		if id >= max {
			max = id+1;
		}
		rules[id] = value;
	}
	rules.resize(max, "");
	let cant_part_2 = rules[0] != "8 11";
	let solution = solve(rules);
	let mut all_starts = vec![];
	let mut all_ends = vec![];
	if !cant_part_2 {
		if let Solved(tree_42) = &solution[42] {
			if let Solved(tree_31) = &solution[31] {
				for possible_42 in tree_42 {
					all_starts.push(&possible_42[..]);
				}
				for possible_31 in tree_31{
					all_ends.push(&possible_31[..]);
				}
				
			}else{panic!("rule 31 was not solved");}
		}else{panic!("rule 42 was not solved");}
	}else{
		panic!("can't solve part 2 because unexpected rule zero");
	}
	let mut a1 = 0;
	let mut a2 = 0;
	if let Solved(tree) = &solution[0] {
		for line in it {
			if tree.contains(&line.to_string()) {
				a1 += 1;
			}
			if is_match_8_then_11(line, &all_starts, &all_ends) {
				a2 += 1
			}
		}
	}else{
		panic!("rule zero was not solved {:?}", solution);
	}


	(a1.to_string(), a2.to_string())
}
fn is_match_8_then_11(line : &str, starts : &[&str], ends : &[&str]) -> bool {
// }
// fn is_match_8(line : &str, starts : &[&str]) -> bool {
	for &start in starts {
		if line.starts_with(start) {
			if is_match_8_then_11(&line[start.len()..], starts, ends) {
				return true;
			}
			if is_match_11(&line[start.len()..], starts, ends) {
				return true;
			}
		} 
	}
	return false;
}
fn is_match_11(line : &str, starts : &[&str], ends : &[&str]) -> bool {
	for &start in starts {
		for end in ends{
			if start.len() + end.len() > line.len() {
				continue;
			}
			let bounds = start.len()..(line.len()-end.len());
			if line.starts_with(start) && line.ends_with(end) {
				if bounds.len() == 0 || is_match_11(&line[bounds], starts, ends) {
					return true;
				}
			}
		}
	}
	false
}
#[derive(Debug)]
enum Rules {
	Simple(String),
	Solved(BTreeSet<String>),
	List(Vec<usize>),
	Alt(Vec<usize>, Vec<usize>),
	Half(BTreeSet<String>, Vec<usize>),
}
fn solve(rules : Vec<&str>) -> Vec<Rules> {
	let mut rules : Vec<Rules> = rules.iter().map(|rule|{
		if rule.is_empty() {
			Simple("".to_string())
		}else if rule.starts_with("\"") {
			Simple(rule[1..2].to_string())
		}else{
			let mut alt = rule.split(" | ");
			let first = alt.next().unwrap().split(" ").map(|n| n.parse().expect(&format!("impossible to parse {} in rule {}", n, rule))).collect();
			let second = alt.next().map(|l| l.split(" ").map(|n| n.parse().expect(&format!("impossible to parse {} in rule {}", n, rule))).collect());
			if second.is_some() {
				Alt(first, second.unwrap())
			}else{
				List(first)
			}
		}
	}).collect();
	let mut stack = vec![0];
	while !stack.is_empty() {
		let cursor = stack.pop().unwrap();
		let rule = &rules[cursor];
		match rule {
			List(indices) => {
				let solved = try_solve(indices, &rules);
				if let Some(tree) = solved {
					rules[cursor] = Solved(tree); 
				}else{
					stack.push(cursor);
					for &idx in indices {
						if ! is_solved(&rules[idx]) {
							stack.push(idx);
						}
					}
				}
			}
			Half(tree, indices) => {
				let solved = try_solve(indices, &rules);
				if let Some(mut other_tree) = solved {
					let mut new_tree = BTreeSet::new();
					for s in tree.iter() {
						new_tree.insert(s.to_string());
					}
					new_tree.append(&mut other_tree);
					rules[cursor] = Solved(new_tree); 
				}else{
					stack.push(cursor);
					for &idx in indices {
						if ! is_solved(&rules[idx]) {
							stack.push(idx);
						}
					}
				}
			}
			Alt(first, second) => {
				let sol_a = try_solve(first, &rules);
				let sol_b = try_solve(second, &rules);
				if sol_a.is_some() && sol_b.is_some(){
					let mut tree = sol_a.unwrap();
					tree.append(&mut sol_b.unwrap());
					rules[cursor] = Solved(tree); 
				}else if let Some(tree) = sol_a {
					rules[cursor] = Half(tree, second.to_vec()); 
				}else if let Some(tree) = sol_b {
					rules[cursor] = Half(tree, first.to_vec()); 
				}else{
					stack.push(cursor);
					for &idx in first {
						if ! is_solved(&rules[idx]) {
							stack.push(idx);
						}
					}
					for &idx in second {
						if ! is_solved(&rules[idx]) {
							stack.push(idx);
						}
					}

				}
			},
			_ => ()
		}
	}
	rules
}
fn try_solve(list : &[usize], rules : &[Rules]) -> Option<BTreeSet<String>> {
	if list.iter().any(|i| !is_solved(&rules[*i])) {
		None
	}else{
		let mut tree : BTreeSet<String> = BTreeSet::new();
		let mut scrap : BTreeSet<String> = BTreeSet::new();
		for &i in list {
			let rule = &rules[i];
			match rule {
				Simple(v) => {
					if tree.is_empty(){
						scrap.insert(v.to_string());
					}else{
						for s in tree.iter() {
							let mut s = s.to_string();
							s.push_str(&v);
							scrap.insert(s);
						}
					}
				}
				Solved(t) => {
					if tree.is_empty(){
						for s in t.iter() {
							scrap.insert(s.to_string());
						}
					}else{
						for s1 in tree.iter() {
							for s2 in t.iter() {
								let mut s = (&s1).to_string();
								s.push_str(s2);
								scrap.insert(s);
							}
						}
					}
				}
				_ => panic!("only solved rules here !")
			}
			tree.clear();
			tree.append(&mut scrap);
		}
		Some(tree)
	}
}
fn is_solved(rule : &Rules) -> bool {
	match rule {
		Simple(_) => true,
		Solved(_) => true,
		List(_) => false,
		Alt(_, _) => false,
		Half(_, _) => false,
	}
}
#[cfg(test)]
mod tests{
	#[test]
	pub fn units1(){
		let input = "0: 4 1 5
1: 2 3 | 3 2
2: 4 4 | 5 5
3: 4 5 | 5 4
4: \"a\"
5: \"b\"

ababbb
bababa
abbbab
aaabbb
aaaabbb";
		use lib::test_run1;
		test_run1!(input, 2);
	}
	#[test]
	pub fn units2(){
		let input = "42: 9 14 | 10 1
9: 14 27 | 1 26
10: 23 14 | 28 1
1: \"a\"
11: 42 31
5: 1 14 | 15 1
19: 14 1 | 14 14
12: 24 14 | 19 1
16: 15 1 | 14 14
31: 14 17 | 1 13
6: 14 14 | 1 14
2: 1 24 | 14 4
0: 8 11
13: 14 3 | 1 12
15: 1 | 14
17: 14 2 | 1 7
23: 25 1 | 22 14
28: 16 1
4: 1 1
20: 14 14 | 1 15
3: 5 14 | 16 1
27: 1 6 | 14 18
14: \"b\"
21: 14 1 | 1 14
25: 1 1 | 1 14
22: 14 14
8: 42
26: 14 22 | 1 20
18: 15 15
7: 14 5 | 1 21
24: 14 1

abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
bbabbbbaabaabba
babbbbaabbbbbabbbbbbaabaaabaaa
aaabbbbbbaaaabaababaabababbabaaabbababababaaa
bbbbbbbaaaabbbbaaabbabaaa
bbbababbbbaaaaaaaabbababaaababaabab
ababaaaaaabaaab
ababaaaaabbbaba
baabbaaaabbaaaababbaababb
abbbbabbbbaaaababbbbbbaaaababb
aaaaabbaabaaaaababaa
aaaabbaaaabbaaa
aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
babaaabbbaaabaababbaabababaaab
aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba";
		use lib::test_run;
		test_run!(input, 3, 12);
	}
}