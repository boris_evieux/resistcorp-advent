use lib::make_main;
make_main!(2020, 13);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let earliest = lines[0].parse::<u64>().unwrap();
	let busses = read_schedule(lines[1]);
	// busses.sort_by(|(a, _), (b, _)| b.cmp(a));
	let mut best_bus = (u64::MAX, u64::MAX - earliest);
	for bus in &busses {
			let bus_id = bus.0;
			let iterations = earliest / bus_id;
			let time = bus_id * (iterations + 1);
			let wait = time - earliest;
			if best_bus.1 > wait{
				best_bus = (bus_id, wait);
			}

	}
	let a1 = best_bus.0 * best_bus.1;
	// let mut loops = 1;
	// let biggest = busses[0].0;
	// let ref_idx = busses[0].1;
	// println!("{:?} {}", biggest, ref_idx);
	let a2 = earliest_bus(&busses);
	// 'a : loop {
	// 	let time_first = (busses[0].0 * loops) - ref_idx;
	// 	for i in 1..busses.len() {
	// 		let (bus_id, idx) = busses[i];
	// 		let time_target = time_first + idx;
	// 		if time_target % bus_id != 0 {
	// 			loops += 1;
	// 			// println!("bus {} can NOT depart at {}+{} : {}", bus_id, time_first, idx, time_target);
	// 			continue 'a;
	// 		}
	// 		// println!("bus {} can depart at {}+{} : {}", bus_id, time_first, idx, time_target);
	// 	}
	// 	a2 = time_first;
	// 	break 'a;
	// }
	(a1.to_string(), a2.to_string())
}
fn read_schedule(schedule : &str) -> Vec<(u64, u64)> {
	let mut idx = 0;
	let mut busses = vec![];
	for b in schedule.split(","){
		if b != "x" {
			let bus_id = b.parse::<u64>().unwrap();
			// println!("bus {} can pick me up at {}(wait : {})", bus_id, time, wait);
			busses.push((bus_id, idx));
		}
		idx +=1;
	}
	busses
}
fn earliest_bus(schedule : &Vec<(u64, u64)>) -> u64 {
	let mut partial_solutions = vec![];
	let n1 = schedule[0].0;
	// let mut current_max = 0;
	for i in 1..schedule.len() {
		let (n2, a) = schedule[i];
		let a = a as i64;
		// println!(" solving for {} and {} (+{})", n1, n2, a);
		let eu = euclid(n1, n2);
		// println!(" euclid (n1, n2) : {:?}", eu);
		let x0 = eu.1 * -a;
		let y0 = eu.2 * a;
		let gcd = eu.0;
		partial_solutions.push((x0, y0, gcd, n1, n2, a));
		// if current_max < x0 {
		// 	current_max = x0;
		// }
		// mult *= x;
		// println!(" >> {}({}) {} ({}, {:?})", x, x * n1, y, mult, partial_solutions);
	}
	let mut current_incr = partial_solutions[0].4;
	let mut current_start = partial_solutions[0].0;
	while current_start < 0 {
		current_start += current_incr as i64;
	}
	let mut current_start = current_start as u64;
	for i in 1..partial_solutions.len() {
		let (mut x, _, _, _, n2, _) = partial_solutions[i];
		while x < 0 {
			x += n2 as i64;
		}
		let mut x = x as u64;
		loop {
			if current_start < x {
				let diff = x - current_start;
				if diff % current_incr == 0 {
					current_start += diff;
				} else {
					current_start += ((diff / current_incr) * current_incr) + current_incr;
				}
			}else if x < current_start {
				let diff = current_start - x;
				if diff % n2 == 0 {
					x += diff;
				} else {
					x += ((diff / n2) * n2) + n2;
				}
			}else{
				break;
			}
			// println!(" >>> {} {}", x, y);
		}
		current_incr *= n2;
		// *sol = (x, y, gcd, n1, n2, a);
	}
	// if partial_solutions.iter().all(|sol| sol.0 == current_max){
	// 	// println!(" >> ({}) {:?}", current_max, partial_solutions);
	// 	break;
	// }
	// }
	//partial_solutions[0] as i64
	current_start * n1
}
fn euclid(a : u64, b : u64) -> (u64, i64, i64){
	let (mut pr, mut r) = (a as i64, b as i64);
	let (mut ps, mut s) = (1i64, 0i64);
	let (mut pt, mut t) = (0i64, 1i64);
	while r != 0 {
		let q = pr / r;
		// println!("{} = {}({}) + {}", pr, q, r, (pr - q * r));
		// println!("{} = {} - {}({})", r, pr, q, r);
		let nr = pr - q * r;
		let ns = ps - q * s;
		let nt = pt - q * t;
		pr = r;
		ps = s;
		pt = t;
		r = nr;
		s = ns;
		t = nt;
		// println!("r {} s {} t {}", r, s, t);
	}
	// println!("euclid for {:?} {:?}", a, b);
	// println!("bezout : {:?} {:?}", ps, pt);
	// println!("gcd : {:?}", pr);
	// println!("quotients : {:?} {:?}", t, s);
	(pr as u64, ps, pt)
	// if a < b {
	// 	euclid(b, a)
	// }else{
	// 	let q = a / b;
	// 	let r = a % b;
	// 	if r == 0 {
	// 		println!("{} = {} - {}({})", r, a, q, b);
	// 		(b, a as i64, -(q as i64))
	// 	}else if q == 1 {
	// 		println!("{} = {} - {}({})", r, a, q, b);
	// 		(1, a as i64, -(q as i64))
	// 	}else{
	// 		let (gcd, _, l_q) = euclid(b, r);
	// 		println!("{} = {} + ({})[{} - {}({})]", gcd, b, l_q, a, q, b);
	// 		let total_b = 1 + l_q;
	// 		// println!("{} = {}({}) - {}({})", gcd, b, l_q, a, q, b);
	// 		// println!("{} = {} - {}({})", r, a, q, b);
	// 		(gcd, q as i64, a as i64)
	// 	}
	// }
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn ex1(){
		let input = "939
7,13,x,x,59,x,31,19";use lib::test_run;

		test_run!(input, 295, 1068781);
	}
	#[test]
	pub fn gcd(){
		assert_eq!(euclid(141,34), (1, 7, -29));
		assert_eq!(euclid(17,13), (1, -3, 4));
	}
	#[test]
	pub fn given_units(){
		let busses = read_schedule("17,x,13,19");
		assert_eq!(earliest_bus(&busses), 3417);
		let busses = read_schedule("67,7,59,61");
		assert_eq!(earliest_bus(&busses), 754018);
		let busses = read_schedule("67,x,7,59,61");
		assert_eq!(earliest_bus(&busses), 779210);
		let busses = read_schedule("67,7,x,59,61");
		assert_eq!(earliest_bus(&busses), 1261476);
	}
	#[test]
	pub fn long_tests(){
		let busses = read_schedule("1789,37,47,1889");
		assert_eq!(earliest_bus(&busses), 1202161486);
	}
}