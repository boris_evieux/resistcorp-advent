use std::collections::HashSet;

const JMP: &str = "jmp";
const NOP: &str = "nop";
use lib::make_main;
make_main!(2020, 8);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut set = HashSet::new();
	let mut is_back_track = false;
	let mut back_track = Vec::with_capacity(1000);
	let mut loop_val = 0;

	let mut ptr = 0i64;
	let mut acc = 0i64;
	loop{
		let patch = if set.contains(&ptr) {
			if !is_back_track {
				loop_val = acc;
				is_back_track = true;
			}
			if back_track.is_empty() {
				panic!("patches didn't work!");
			}else {
				let (op, new_ptr, new_acc) = back_track.pop().unwrap();
				ptr = new_ptr;
				acc = new_acc;
				Some(op)
			}
		}else{
			set.insert(ptr);
			None
		};

		let line = lines[ptr as usize];
		let opcode = if let Some(op) = patch{
			op
		}else{
			&line[0..3]
		};
		let val : i64 = line[4..].parse().expect(&format!("can't parse {}", line));
		match opcode {
			"nop" => {
				if !is_back_track {
					back_track.push((JMP, ptr, acc));
				}
				ptr += 1
			}
			"jmp" => {
				if !is_back_track {
					back_track.push((NOP, ptr, acc));
				}
				ptr += val
			}
			"acc" => {
				acc += val;
				ptr += 1
			}
			_ => panic!("invalid opcode : {}", opcode),
		}
		if ptr == lines.len() as i64 {
			break; //end of pgm
		}else if ptr >= lines.len() as i64 || ptr < 0 {
			ptr = 0;
		}
	}
	(loop_val.to_string(), acc.to_string())
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn fields(){
		let input = "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6";use lib::test_run;

		test_run!(input, 5, 8);
	}
}