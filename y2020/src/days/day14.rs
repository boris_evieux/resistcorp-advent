use lib::util::Lines;
use std::collections::HashMap;

use lib::make_main;
make_main!(2020, 14);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let sum1 = part1(data);
	let sum2 = part2(data);
	(sum1.to_string(), sum2.to_string())
}
fn part2(data : &DayInput) -> u64{
	let mut memory : HashMap<u64, u64> = HashMap::new();
	let mut mask_on = 0u64;
	let mut mask_off = 0u64;
	let mut addr_overrides : Vec<u64> = Vec::with_capacity(1500);
	let mut tally = 0u64;
	for line in data.linesi() {
		let split : Vec<&str> = line.split(" = ").collect();
		let instruction : &str = split[0];
		let value : &str = split[1];
		if instruction == "mask" {
			let mut on = 0u64;
			let mut off = 0u64;
			for (i, ch) in value.char_indices() {
				let mask = 1 << (35 - i);
				match ch {
					'X' => (),
					'1' => {
						on |= mask;
					}
					'0' => {
						off |= mask;

					},
					_ => panic!("unexpected char for mask {} at idx {}", ch, i)
				}
			}
			mask_on = on;
			mask_off = off;
		}else {
			let last = instruction.len()-1;
			let address = instruction[4..last].parse::<u64>().unwrap();
			let orig_value = value.parse::<u64>().unwrap();
			// let address = address | mask_on;
			addr_overrides.clear();
			addr_overrides.push(address);
			let mut new_addr = vec![];
			for bit in 0..36{
				let mask = 1 << bit;
				let set_on = (mask_on & mask) == mask;
				let set_off = (mask_off & mask) == mask;
				// println!("bit {:?} : {} {}", bit, set_on, set_off);
				if !set_on && !set_off {
					// println!("overriding adress at bit {:?}", bit);
					for address in &addr_overrides{
						let add_on  = (address | mask_on) | mask;
						new_addr.push(add_on);
						let add_off = (address | mask_on) & !mask;
						new_addr.push(add_off);
					}
					addr_overrides.clear();
					for a in new_addr.drain(..){
						addr_overrides.push(a);
					}
				}
			}

			for address in addr_overrides.drain(..){
				let prev = memory.insert(address, orig_value);
				tally += orig_value;
				if let Some(val) = prev {
					tally -= val;
				}
			}
			addr_overrides.clear();
		}
	}
	tally
}
fn part1(data : &DayInput) -> u64{
	let mut memory : HashMap<u64, u64> = HashMap::new();
	let mut mask_on = 0u64;
	let mut mask_off = u64::MAX;
	for line in data.linesi() {
		let split : Vec<&str> = line.split(" = ").collect();
		let instruction : &str = split[0];
		let value : &str = split[1];
		if instruction == "mask" {
			// println!("changing mask to value {} (l : {})", value, value.len());
			let mut on = 0u64;
			let mut off = 0u64;
			for (i, ch) in value.char_indices() {
				let mask = 1 << (35 - i);
				match ch {
					'X' => (),
					'1' => {
						on |= mask;
					}
					'0' => {
						off |= mask;

					},
					_ => panic!("unexpected char for mask {} at idx {}", ch, i)
				}
			}
			mask_on = on;
			mask_off = !off;
			// println!("on  : {:064b}", mask_on);
			// println!("off : {:064b}", mask_off);
		}else {
			let last = instruction.len()-1;
			let address = instruction[4..last].parse::<u64>().unwrap();
			let orig_value = value.parse::<u64>().unwrap();
			// println!("parsed value{}", value);
			// println!("{:064b}", value);
			let value = (orig_value | mask_on) & mask_off;
			// println!("{:064b}", mask_on);
			// println!("{:064b}", mask_off);
			// println!("          vvvv");
			// println!("{:064b}", value);
			// println!("putting value {:?} at adress {}", value, &instruction[4..last]);
			// println!("");
			memory.insert(address, value);
		}
	}
	let mut sum1 = 0;
	for val in memory.values() {
		sum1 += val;
	}
	sum1
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn ex1(){
		let input = "mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
mem[8] = 11
mem[7] = 101
mem[8] = 0";
		lib::test_run1!(input, 165);
	}
	#[test]
	pub fn ex2(){
		let input = "mask = 000000000000000000000000000000X1001X
mem[42] = 100
mask = 00000000000000000000000000000000X0XX
mem[26] = 1";
		lib::test_run2!(input, 208);
	}
	#[test]
	pub fn constructed(){
		let input = "mask = X000000000000000000X00000000000100XX
mem[42] = 734201087";
		lib::test_run2!(input, 734201087 * 16i64);
	}
}