// use regex::Regex;

// fn create_regex()->Regex{
// 	Regex::new(r"(\d+)-(\d+) (\w): (\w+)").unwrap()
// }

use lib::make_main;
make_main!(2020, 2);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	//part 1
	// let re : Regex = create_regex();
	let mut total = 0;
	let mut total2 = 0;
  for line in data.linesi(){
			let ok = is_ok(line/*, &re*/);
      if ok.0 {
      	total += 1;
      }
      if ok.1 {
      	total2 += 1;
      }
  };
	// println!("day 02 /1 : {}", total);
	// println!("day 02 /2 : {}", total2);
	(total.to_string(), total2.to_string())

}

pub fn run2(data : &DayInput, _p : &Params)->(String, String){
	//part 1
	// let re : Regex = create_regex();
	let mut total = 0;
	let mut total2 = 0;
  for line in data.linesi(){
			let ok = is_ok(line/*, &re*/);
      if ok.0 {
      	total += 1;
      }
      if ok.1 {
      	total2 += 1;
      }
  };
	// println!("day 02 /1 : {}", total);
	// println!("day 02 /2 : {}", total2);
	(total.to_string(), total2.to_string())

}
fn is_ok(line : &str/*, re : &Regex*/)->(bool, bool){
	// let captures = re.captures(line).unwrap();
	// let min:usize = captures[1].parse::<usize>().unwrap();
	// let max:usize = captures[2].parse::<usize>().unwrap();
	// let letter = &captures[3];
	// let val = &captures[4];
	// let cnt = val.matches(letter).count();
	let mut dash_pos = 0;
	let mut space_pos = 0;
	let mut colon_pos = 0;
	let mut searched = "";
	let mut cnt = 0;
	for i in 0..line.len() {
		match &line[i..=i] {
			":" => colon_pos = i,
			"-" => dash_pos = i,
			" " if space_pos == 0 => space_pos = i,
			l if colon_pos == 0 => searched = l,
			l if l == searched => cnt += 1,
			_ =>(),
		}
	}
	let min:usize = line[0..dash_pos].parse::<usize>().unwrap();
	let max:usize = line[dash_pos+1..space_pos].parse::<usize>().unwrap();
	let min_idx:usize = colon_pos + 2 + min;
	let max_idx:usize = colon_pos + 2 + max;

	let ch_1 = &line[min_idx-1..min_idx];
	let ch_2 = &line[max_idx-1..max_idx];

	(cnt <= max && cnt >= min, (ch_1 == searched) ^ (ch_2 == searched))
}

#[cfg(test)]
mod tests{
	use super::*;
  #[test]
	pub fn example(){
		// let _re : Regex= create_regex();
    assert!(is_ok("1-3 a: abcde"/*, &re*/).0);
    assert!(!is_ok("1-3 b: cdefg"/*, &re*/).0);
    assert!(is_ok("2-9 c: ccccccccc"/*, &re*/).0);

    assert!(is_ok("1-3 a: abcde"/*, &re*/).1);
    assert!(!is_ok("1-3 a: abade"/*, &re*/).1);
    assert!(!is_ok("1-3 a: ebcde"/*, &re*/).1);
    assert!(is_ok("1-3 a: ebade"/*, &re*/).1);
    assert!(!is_ok("1-3 b: cdefg"/*, &re*/).1);
    assert!(!is_ok("2-9 c: ccccccccc"/*, &re*/).1);
	}
}