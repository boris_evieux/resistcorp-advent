use lib::make_main;
make_main!(2020, 4);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	// let (reg_hcl, reg_pid) = create_regexes();
	// 	let (reg_hcl, reg_pid) = (&reg_hcl, &reg_pid);
	let mut data : String = String::with_capacity(100);
  let mut valid1 = 0;
  let mut valid2 = 0;

  for line in lines {
			match *line {
				"" => {
					let valid = is_valid(&data);//, reg_hcl, reg_pid);
			  	if valid.0 {
			  		valid1 += 1
			  	}
			  	if valid.1 {
			  		valid2 += 1
			  	}
					data.clear();
				},
				other => {
					if data.len() > 0 {
						data.push_str(" ")
					}
					data.push_str(other)
				}
			}
  };
  let valid = is_valid(&data/*, reg_hcl, reg_pid*/);
	if valid.0 {
		valid1 += 1
	}
	if valid.1 {
		valid2 += 1
	}
	(valid1.to_string(), valid2.to_string())
}

fn is_valid(passport : &str/*, reg_hcl : &Regex, reg_pid : &Regex*/) -> (bool, bool) {

	let mut has_byr = false;
	let mut has_iyr = false;
	let mut has_eyr = false;
	let mut has_hgt = false;
	let mut has_hcl = false;
	let mut has_ecl = false;
	let mut has_pid = false;
	let mut _has_cid = false;
	let mut valid_byr = false;
	let mut valid_iyr = false;
	let mut valid_eyr = false;
	let mut valid_hgt = false;
	let mut valid_hcl = false;
	let mut valid_ecl = false;
	let mut valid_pid = false;
	for field_val in passport.split_whitespace() {
		// println!("{:?}", field_val);
		let mut field_val = field_val.splitn(2, ":");
		let field = field_val.next();
		let val = field_val.next();
		match field {
			Some("byr") =>{
				has_byr = true;
				valid_byr = is_valid_byr(val);
			} 
			Some("iyr") =>{
				has_iyr = true;
				valid_iyr = is_valid_iyr(val);
			} 
			Some("eyr") =>{
				has_eyr = true;
				valid_eyr = is_valid_eyr(val);
			} 
			Some("hgt") =>{
				has_hgt = true;
				valid_hgt = is_valid_hgt(val);
			} 
			Some("hcl") =>{
				has_hcl = true;
				valid_hcl = is_valid_hcl(val);
			} 
			Some("ecl") =>{
				has_ecl = true;
				valid_ecl = is_valid_ecl(val);
			} 
			Some("pid") =>{
				has_pid = true;
				valid_pid = is_valid_pid(val);
			} 
			Some("cid") => _has_cid = true,
			_ => {}
		}
	}

	(has_byr && has_iyr && has_eyr && has_hgt && has_hcl && has_ecl && has_pid,
		valid_byr && valid_iyr && valid_eyr && valid_hgt && valid_hcl && valid_ecl && valid_pid)
// has_cid
}

fn is_valid_byr(val :Option<&str>) -> bool{
	if let Some(Ok(val)) = val.map(&str::parse::<u16>){
		val >= 1920 && val <= 2002
	}	else {
		false
	}
}
fn is_valid_iyr(val :Option<&str>) -> bool{
	if let Some(Ok(val)) = val.map(&str::parse::<u16>){
		val >= 2010 && val <= 2020
	}	else {
		false
	}
}
fn is_valid_eyr(val :Option<&str>) -> bool{
	if let Some(Ok(val)) = val.map(&str::parse::<u16>){
		val >= 2020 && val <= 2030
	}	else {
		false
	}
}
fn is_valid_hgt(val :Option<&str>) -> bool{
	if let Some(Ok(val)) = val.map(|v|v.strip_suffix("cm")).flatten().map(&str::parse::<u16>) {
		val >= 150 && val <= 193
	}else if let Some(Ok(val)) = val.map(|v|v.strip_suffix("in")).flatten().map(&str::parse::<u16>) {
		val >= 59 && val <= 76
	}else {
		false
	}
}
fn is_valid_hcl(val :Option<&str>) -> bool{
	let val = val.unwrap();
	if let Ok(_) = i32::from_str_radix(&val[1..], 16) {
		val.starts_with("#")
	}else{
		false
	}
}
fn is_valid_ecl(val :Option<&str>) -> bool{
	match val {
		Some("amb") | Some("blu") | Some("brn") | Some("gry") | Some("grn") | Some("hzl") | Some("oth") => true,
		_ => false
	}
}
fn is_valid_pid(val :Option<&str>) -> bool{
	let val = val.unwrap();
	if let Ok(_) = val.parse::<u64>() {
		val.len() == 9
	}else {
		false
	}
}

#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn passports(){
		// let (reg_hcl, reg_pid) = create_regexes();
		// let (reg_hcl, reg_pid) = (&reg_hcl, &reg_pid);


		assert!(is_valid("ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
											byr:1937 iyr:2017 cid:147 hgt:183cm"/*, reg_hcl, reg_pid*/).0);

		assert!(!is_valid("iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
											hcl:#cfa07d byr:1929"/*, reg_hcl, reg_pid*/).0);

		assert!(is_valid("hcl:#ae17e1 iyr:2013
											eyr:2024
											ecl:brn pid:760753108 byr:1931
											hgt:179cm"/*, reg_hcl, reg_pid*/).0);

		assert!(!is_valid("hcl:#cfa07d eyr:2025 pid:166559648
											iyr:2011 ecl:brn hgt:59in"/*, reg_hcl, reg_pid*/).0);
		//part 2 : invalid
		assert!(!is_valid("eyr:1972 cid:100
												hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926"/*, reg_hcl, reg_pid*/).1);

		assert!(!is_valid("iyr:2019
												hcl:#602927 eyr:1967 hgt:170cm
												ecl:grn pid:012533040 byr:1946"/*, reg_hcl, reg_pid*/).1);

		assert!(!is_valid("hcl:dab227 iyr:2012
												ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277"/*, reg_hcl, reg_pid*/).1);

		assert!(!is_valid("hgt:59cm ecl:zzz
												eyr:2038 hcl:74454a iyr:2023
												pid:3556412378 byr:2007"/*, reg_hcl, reg_pid*/).1);
		//part 2 : valid
		assert!(is_valid("pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
											hcl:#623a2f"/*, reg_hcl, reg_pid*/).1);

		assert!(is_valid("eyr:2029 ecl:blu cid:129 byr:1989
											iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm"/*, reg_hcl, reg_pid*/).1);

		assert!(is_valid("hcl:#888785
											hgt:164cm byr:2001 iyr:2015 cid:88
											pid:545766238 ecl:hzl
											eyr:2022"/*, reg_hcl, reg_pid*/).1);

		assert!(is_valid("iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719"/*, reg_hcl, reg_pid*/).1);

	}
	#[test]
	pub fn fields(){
		// let (reg_hcl, reg_pid) = create_regexes();
		// let (reg_hcl, reg_pid) = (&reg_hcl, &reg_pid);

		assert!(is_valid_byr(Some("2002")));
		assert!(!is_valid_byr(Some("2003")));

		assert!(is_valid_hgt(Some("60in")));
		assert!(is_valid_hgt(Some("190cm")));
		assert!(!is_valid_hgt(Some("190in")));
		assert!(!is_valid_hgt(Some("190")));

		assert!(is_valid_hcl(Some("#123abc")));
		assert!(!is_valid_hcl(Some("#123abz")));
		assert!(!is_valid_hcl(Some("123abc")));

		assert!(is_valid_ecl(Some("brn")));
		assert!(!is_valid_ecl(Some("wat")));

		assert!(is_valid_pid(Some("000000001")));
		assert!(!is_valid_pid(Some("0123456789")));
	}
	// #[bench]
	// fn older( _bencher : &mut Bencher){
	// 	for _ in 0..1_000_000 {
	// 		is_valid("eyr:2029 ecl:blu cid:129 byr:1989
	// 							iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm");
	// 		is_valid("hcl:#cfa07d eyr:2025 pid:166559648
	// 											iyr:2011 ecl:brn hgt:59in");
	// 		//part 2 : invalid
	// 		is_valid("eyr:1972 cid:100
	// 												hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926");

	// 		is_valid("iyr:2019
	// 												hcl:#602927 eyr:1967 hgt:170cm
	// 												ecl:grn pid:012533040 byr:1946");

	// 		is_valid("hcl:dab227 iyr:2012
	// 												ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277");

	// 		is_valid("hgt:59cm ecl:zzz
	// 												eyr:2038 hcl:74454a iyr:2023
	// 												pid:3556412378 byr:2007");
	// 		//part 2 : valid
	// 		is_valid("pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
	// 											hcl:#623a2f");

	// 		is_valid("eyr:2029 ecl:blu cid:129 byr:1989
	// 											iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm");

	// 		is_valid("hcl:#888785
	// 											hgt:164cm byr:2001 iyr:2015 cid:88
	// 											pid:545766238 ecl:hzl
	// 											eyr:2022");

	// 		is_valid("iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719");
	// 	}
	// }
}

// fn create_regexes() -> (Regex, Regex){
// 	let reg_hcl : Regex = Regex::new(r"^#[a-f0-9]{6}$").expect("wrong HCL regex");
// 	let reg_pid : Regex = Regex::new(r"^[0-9]{9}$").expect("wrong PID regex");
// 	(reg_hcl, reg_pid)
// }