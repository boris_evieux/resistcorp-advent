use std::collections::BTreeSet;

use lib::make_main;
make_main!(2020, 10);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let nums = data.linesi().map(|s|s.parse::<u8>().unwrap()).collect::<BTreeSet<u8>>();
	let mut counts : [usize; 4] = [0;4];
	let mut copy = Vec::with_capacity(nums.len()+1);
	copy.push((0, 1u64));
	let mut i = 0;
	// let init = Instant::now();
	for num in &nums {
		let prev = copy[i].0;
		counts[(num - prev) as usize] += 1;
		let mut combi = 0;
		for j in 0..3{
			if j < copy.len() {
				let adapter = copy[i-j];
				let diff = num - adapter.0;
				if diff <= 3 {
					// println!("{} <- {:?}", num, adapter);
					combi += adapter.1
				}
			}
		}
		// println!(">> {} :: {}", num, combi);
		copy.push((*num, combi));
		i += 1;
	}
	counts[3] += 1;
	let a1 = counts[1] * counts[3];
	let a2 = copy[nums.len()].1;
	// println!("since start : {:?} compute {:?}, parse {:?}", s.elapsed(), init.elapsed(), s.elapsed() - init.elapsed());
	(a1.to_string(), a2.to_string())
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn ex1(){
		let input = "16
10
15
5
1
11
7
19
6
12
4";use lib::test_run;

		test_run!(input, 35, 8);
	}
	#[test]
	pub fn ex2(){
		let input = "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3";use lib::test_run;

		test_run!(input, 220, 19208);
	}
}