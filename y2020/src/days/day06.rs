struct Tally{
	answers : [ usize ; 26],
	people : usize,
	highest_idx : usize
}
impl Tally{
	fn new()->Self{
		let answers = [ 0usize ; 26];
		let people = 0;
		let highest_idx = 0;
		Self{answers, people, highest_idx}
	}
	fn add(&mut self, person_answers : &str){
		self.people += 1;
		for c in person_answers.chars() {
			let idx = c as usize - 97;
			self.answers[idx] += 1;
			if idx > self.highest_idx {
				self.highest_idx = idx;
			}
		}
	}
	fn tally_up(&mut self, num_some : &mut usize, num_all : &mut usize){
		for i in 0..=self.highest_idx {
			let num = self.answers[i];
			if num != 0 {
				*num_some += 1;
			}
			if num == self.people {
				*num_all += 1;
			}
			self.answers[i] = 0;
		}
		self.highest_idx = 0;
		self.people = 0;
	}
}

use lib::make_main;
make_main!(2020, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	// let mut set = HashSet::new();
	let mut tally = Tally::new();
	let mut num_some = 0;
	let mut num_all = 0;
	for line in lines {
		match *line{
			"" => {
				tally.tally_up(&mut num_some, &mut num_all);
			}
			l => {
				tally.add(l);
			}
		}
	}
	tally.tally_up(&mut num_some, &mut num_all);
	(num_some.to_string(), num_all.to_string())
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn fields(){
		let input = 
"abc

a
b
c

ab
ac

a
a
a
a

b";
		use lib::test_run;
		test_run!(input, 11, 6);
	}
}