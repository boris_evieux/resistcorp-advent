use std::collections::HashSet;
use std::collections::HashMap;

#[derive(Debug)]
struct BagColor{
	// index : usize,
	children_idx : usize,
	children : u8,
	// size : Option<u32>
}

use lib::make_main;
make_main!(2020, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	// let s = Instant::now();
	let mut nodes = Vec::with_capacity(1000);
	let mut possible_parents : Vec<Vec<usize>> = Vec::new();
	let mut rules : Vec<(usize, u8)> = Vec::with_capacity(1000);
	let mut node_sizes = Vec::with_capacity(1000);
	let mut map : HashMap<&str, usize> = HashMap::with_capacity(1000);
	let mut my_index = 0;
	for line in lines {
		let mut it = line.match_indices(" ").map(|t|t.0);
		it.next();
		let parent = &line[0..it.next().unwrap()];
		let children_idx = rules.len();
		let parent_index = if ! map.contains_key(parent) {
			let idx = nodes.len();
			nodes.push(BagColor{children_idx, children : 0});
			possible_parents.push(vec!());
			map.insert(parent, idx);
			// put_node(parent, &mut map, &mut nodes)
			idx
		}else{
			*map.get(parent).unwrap()
		};
		if parent == "shiny gold" {
			my_index = parent_index;
		}
		let mut children = 0;

		if ! line.ends_with("no other bags."){
			it.next();
			loop {
				if let Some(ptr) = it.next(){
					let namestart = it.next().unwrap();
					it.next();
					let nameend = it.next().unwrap();
					let num = line[ptr+1..namestart].parse::<u8>().unwrap();
					let name = &line[namestart+1..nameend];
					let child_index = if ! map.contains_key(name) {
						let children_idx = 0;
						let idx = nodes.len();
						nodes.push(BagColor{children_idx, children : 0});
						possible_parents.push(vec!());
						map.insert(name, idx);
						// put_node(parent, &mut map, &mut nodes)
						idx
					}else{
						*map.get(name).unwrap()
					};
					children += 1;
					possible_parents[child_index].push(parent_index);
					rules.push((child_index, num));
				}else{
					break;
				}

			}
			node_sizes.push(None);
		}else{
			// node_sizes.push(Some(0));
			node_sizes.push(None);
		}
		nodes[parent_index].children = children;
		nodes[parent_index].children_idx = children_idx;
		
		// println!("{:?} : {:?} : {:?}", parent_index, parent, nodes[parent_index]);
		// println!("{:?}", line);
		// println!("{:?}", rules);
	}
	// let a = Instant::now();
	let mut passed = HashSet::new();
	passed.insert(my_index);
	let mut to_check = vec![];
	let mut to_check2 = vec![];
	for i in &possible_parents[my_index] {
		to_check.push(*i);
	}
	loop{
		for i in to_check.drain(..) {
			if ! passed.contains(&i) {
				// let node = &nodes[i];
				// println!("possible parent : {}", node.name);
				passed.insert(i);
				for parent_i in &possible_parents[i] {
					to_check2.push(*parent_i);
				}
			}
		}
		if to_check2.is_empty() {
			break;
		}else{
			to_check.append(&mut to_check2);
		}
	}
	// let b = Instant::now();
	let my_bag_size = compute_node_size(my_index, &nodes, &mut node_sizes, &rules);
	// println!("day 7 parse : {:?}, list containers : {:?}, calc size : {:?}", s.elapsed(), a.elapsed(), b.elapsed());
	((passed.len()-1).to_string(), my_bag_size.to_string())
}
// fn put_node<'a> (name : &'a str, map : &mut HashMap<&'a str, usize>, nodes : &mut Vec<BagColor>)->usize{
// 		let index = nodes.len();
// 		map.insert(name, index);
// 		// let _name = name.to_string();
// 		let possible_parents = Vec::with_capacity(20);
// 		let must_contain = [None; 5];
// 		let children = 0;
// 		// let size = None;
// 		let node = BagColor{/*name, */possible_parents, must_contain, children/*, size*/};
// 		nodes.push(node);
// 		index
// }
fn compute_node_size(idx : usize, nodes : &Vec<BagColor>, sizes : &mut Vec<Option<u32>>, rules : &Vec<(usize, u8)>) -> u32 {
	let mut stack : Vec<usize> = Vec::with_capacity(50);
	let mut cursor = idx;
	// let mut depth = 0;
	// println!("{:?}", rules);
	// println!("{:?}", nodes);
	'outer : loop {
		// iter += 1;
		if let None = sizes[cursor] {
			let node = &nodes[cursor];
			// println!("{} {:?} searching size", "-".repeat(depth), node);
			let mut total = 0u32;
			let rulestart = node.children_idx;
			let ruleend = rulestart + (node.children as usize);
			// println!("{} {} {}", "-".repeat(depth), rulestart, ruleend);
			for i in rulestart..ruleend {
				let (child_i, num) = &rules[i as usize];
				// println!("{} {:?} wants {}x{:?}", "-".repeat(depth), node, num, nodes[*child_i]);
				if let Some(size) = sizes[*child_i] {
					// println!("{} {:?} adding size {}x{}", "-".repeat(depth), node, num, (1+size));
					total += (*num as u32) * (1 + size);
				}else{
					// println!("{} {:?} can't find size", "-".repeat(depth), node);
					// depth += 1;
					stack.push(cursor);
					cursor = *child_i;
					continue 'outer;
				}
			}
			sizes[cursor] = Some(total);
			// println!("{} {:?} has size {}", "-".repeat(depth), node, total);
			if idx == cursor {
				break;
			}
			cursor  = stack.pop().unwrap();
			// depth -= 1;
		}
	}
	sizes[idx].unwrap()
	// let mut total = 0;
	// for (child_i, num) in &noe.must_contain {
	// 	let child = &nodes[*child_i];
	// 	let size = node_size(&child, nodes) + 1;
	// 	println!("{} must contain {} x {} ({})", node.name, num, child.name, size);
	// 	total += (*num as u32) * size;
	// }
	// // node.size = Some(total);
	// total
	// if let Some(size) = node.size {
	// 	size
	// }else{
	// }
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn fields1(){
		let input = "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.";
		use lib::test_run;
		test_run!(input, 4, 32);
	}

	#[test]
	pub fn fields2(){
		let input = "shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.";
		use lib::test_run2;
		test_run2!(input, 126);
	}
}