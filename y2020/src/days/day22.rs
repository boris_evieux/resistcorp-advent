use std::collections::{BTreeSet, VecDeque};

use lib::make_main;
make_main!(2020, 22);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut deck1 = Vec::new();
	let mut deck2 = Vec::new();
	let mut it = lines.iter().peekable();
	assert_eq!(it.next(), Some(&"Player 1:"));
	loop {
		let line = *it.next().unwrap();
		if line == "" {
			break;
		}
		deck1.push(line.parse().unwrap());
	}
	assert_eq!(it.next(), Some(&"Player 2:"));
	while it.peek().is_some() {
		let line = *it.next().unwrap();
		deck2.push(line.parse().unwrap());
	}

	let a1 = part_1(&deck1, &deck2);
	let a2 = part_2(&deck1[..], &deck2[..], 1).1;

	(a1.to_string(), a2.to_string())
	// (42.to_string(), "life, the universe and everything".to_string()) // break all_solutions tests with this line
}
fn part_2(deck1 : &[u8], deck2: &[u8], game : usize) -> (bool, usize) {
	let mut history = BTreeSet::new();
	let mut stack1 :VecDeque<u8> = VecDeque::new();
	let mut stack2 = VecDeque::new();
	for c in deck1 {stack1.push_front(*c);}
	for c in deck2 {stack2.push_front(*c);}

	let mut sub_game = game;
	let mut round = 1;

	let mut infinite = false;
	while !stack2.is_empty() && !stack1.is_empty(){
		// println!("-- Round {:?} (Game {}) --", round, game);
		// println!("p1 deck : {:?}", stack1);
		// println!("p2 deck : {:?}", stack2);
		let hash1 = stack1.iter().map(u8::to_string).collect::<Vec<String>>().join(",");
		let hash2 = stack2.iter().map(u8::to_string).collect::<Vec<String>>().join(",");
		let hash = (hash1, hash2);
		if history.contains(&hash) {
			// println!("infinite round, player 1 wins");
			infinite = true;
			break;
		}
		history.insert(hash);
		let c1 = stack1.pop_back().unwrap() as usize;
		let c2 = stack2.pop_back().unwrap() as usize;
		// println!("p1 draws : {:?}", c1);
		// println!("p2 draws : {:?}", c2);
		let p1_wins = if c1 <= stack1.len() && c2 <= stack2.len() {
			let start1 = stack1.len()-c1;
			let start2 = stack2.len()-c2;
			let sub_deck1 = stack1.iter().skip(start1).rev().map(|c|*c).collect::<Vec<u8>>();
			let sub_deck2 = stack2.iter().skip(start2).rev().map(|c|*c).collect::<Vec<u8>>();
			sub_game += 1;
			// println!("Playing a sub-game to determine the winner...
// 
// === Game {} ===", sub_game);
			part_2(&sub_deck1[..], &sub_deck2[..], sub_game).0
		}else{
			c1 > c2
		};

		if p1_wins {
			// println!("p1 wins the round");
			stack1.push_front(c1 as u8);
			stack1.push_front(c2 as u8);
		}else{
			// println!("p2 wins the round");
			stack2.push_front(c2 as u8);
			stack2.push_front(c1 as u8);
		}
		round += 1;
	}
	let (p1_wins, stack) = if infinite || stack2.is_empty(){
		(true, stack1)
	}else{
		(false, stack2)
	};

	let mut a2 = 0usize;
	for i in 0..stack.len(){
		a2 += (stack[i] as usize) * (i+1);
	}
	(p1_wins, a2)
}
fn part_1(deck1 : &[u8], deck2: &[u8]) ->usize{
	let mut stack1 :VecDeque<u8> = VecDeque::new();
	let mut stack2 = VecDeque::new();
	for c in deck1 {stack1.push_front(*c);}
	for c in deck2 {stack2.push_front(*c);}
	// let mut round = 1;
	while !stack2.is_empty() && !stack1.is_empty(){
		// println!("-- Round {:?} --", round);
		// println!("p1 deck : {:?}", stack1);
		// println!("p2 deck : {:?}", stack2);
		let c1 = stack1.pop_back().unwrap();
		let c2 = stack2.pop_back().unwrap();
		// println!("p1 draws : {:?}", c1);
		// println!("p2 draws : {:?}", c2);
		if c1 > c2 {
			// println!("p1 wins the round");
			stack1.push_front(c1);
			stack1.push_front(c2);
		}else{
			// println!("p2 wins the round");
			stack2.push_front(c2);
			stack2.push_front(c1);
		}
		// println!("");
		// round += 1;
	}
	let winner = if stack1.is_empty(){
		stack2
	}else{
		stack1
	};

	let mut a1 = 0usize;
	for i in 0..winner.len(){
		a1 += (winner[i] as usize) * (i+1);
	}
	a1
}
#[cfg(test)]
mod tests{
	#[test]
	pub fn units1(){
		let input = "Player 1:
9
2
6
3
1

Player 2:
5
8
4
7
10";
		use lib::test_run;
		test_run!(input, 306, 291);
	}
}