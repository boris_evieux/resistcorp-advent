type RuleSet = [bool; 1000];

use lib::make_main;
make_main!(2020, 16);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut it = lines.iter();

	let mut rules : Vec<(String, RuleSet, Option<usize>)> = Vec::new();
	loop{
		let line = it.next().unwrap();
		if line == &"" {
			break;
		}
		let mut spl = line.split(": ");
		let class = spl.next().unwrap().to_string();
		let ruleset = spl.next().unwrap().split(" or ");
		let mut set : RuleSet = [false; 1000];
		for rule in ruleset {
			let mut vals = rule.split("-");
			let start = vals.next().unwrap().parse::<usize>().unwrap();
			let end = vals.next().unwrap().parse::<usize>().unwrap();
			for i in start..=end{
				set[i] = true;
			}
		}
		rules.push((class, set, None));
	}

	assert_eq!(it.next().unwrap(), &"your ticket:");
	let my_ticket = it.next().unwrap().split(",").map(|s|str::parse::<usize>(s).unwrap()).collect::<Vec<usize>>();
	assert_eq!(it.next().unwrap(), &"");
	assert_eq!(it.next().unwrap(), &"nearby tickets:");

	let mut invalid = Vec::with_capacity(10_000);
	let mut grid = vec![vec![true; rules.len()]; rules.len()];

	for line in it {
		let vals = line.split(",");
		let len_before = invalid.len();
		for (x, val) in vals.enumerate() {
			let val = val.parse::<usize>().unwrap();
			let mut valid = false;
			let mut validity = vec![];
			for (_, set, _) in &rules {
				if set[val] {
					valid = true;
					validity.push(true);
					//break;
				}else{
					validity.push(false);
				}
			}
			if !valid {
				// println!("invalid : {:?}", val);
				invalid.push(val);
			}else{
				for i in 0..rules.len() {
					grid[i][x] &= validity[i];
				}
			}
		}
		let len_after = invalid.len();
		if len_before == len_after {
			//valid ticket
		}
	}
	let mut prev_done = 0;
	loop {
		let mut num_done = 0;
		for i in 0..rules.len() {
			if let Some(_) = &rules[i].2 {
				num_done += 1;
			} else {
				let mut idx = 0;
				let mut num = 0;
				for j in 0..grid[0].len() {
					if grid[i][j] {
						idx = j;
						num += 1;
					}
				}
				if num == 1 {
					rules[i].2.replace(idx);
					for line in grid.iter_mut() {
						line[idx] = false;
					}
					num_done += 1;
				}
			}
		}
		if num_done == grid.len() {
			break;
		}
		if num_done == prev_done {
			panic!("day 16 is stuck!!!");
		}
		prev_done = num_done;
	}
	let mut tally = 1;
	for (name, _, pos) in rules {
		if name.starts_with("departure ") {
			tally *= my_ticket[pos.unwrap()];
		}
	}

	let a1 = invalid.iter().fold(0, |acc, v| acc + v);
	let a2 = tally;

	(a1.to_string(), a2.to_string())
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn ex1(){
		let input = "class: 1-3 or 5-7
row: 6-11 or 33-44
seat: 13-40 or 45-50

your ticket:
7,1,14

nearby tickets:
7,3,47
40,4,50
55,2,20
38,6,12";use lib::test_run;

		test_run!(input, 71, 1);
	}
// 	#[test]
// 	pub fn units(){
// 		let input = ".......#.
// ...#.....
// .#.......
// .........
// ..#L....#
// ....#....
// .........
// #........
// ...#.....";
// 		let lines = input.lines().collect::<Vec<&str>>();
// 		let grid = read(&lines);
// 		assert_eq!(grid[4][3], Empty);
// 		assert_eq!(neighbors2((3, 4), (9, 9), &grid), 8);
// 	}

}
