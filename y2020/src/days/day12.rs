use lib::make_main;
make_main!(2020, 12);
const DIRS : [(i32, i32); 4] = [
	(1,0),//EAST
	(0,-1),//SOUTH
	(-1,0),//WEST
	(0,1),//NORTH
];
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut pos = (0,0);
	let mut dir = 0;
	for line in lines {
		let instr = &line[..1];
		let num : i32 = line[1..].parse().unwrap();
		match instr {
			"N" => pos = (pos.0 + 0, pos.1 + num),
			"S" => pos = (pos.0 + 0, pos.1 - num),
			"E" => pos = (pos.0 + num, pos.1 + 0),
			"W" => pos = (pos.0 - num, pos.1 + 0),
			"F" => pos = (pos.0 + num * DIRS[dir].0, pos.1 + num * DIRS[dir].1),
			"L" => {
				let t = num / 90;
				dir = ((dir as i32 - t + 4) % 4) as usize;
			},
			"R" => {
				let t = num / 90;
				dir = ((dir as i32 + t + 4) % 4) as usize;
			},
			_ => panic!("unexpected instruction {}", line)
		}
		// println!("{} -> {:?} ({} {:?})", line, pos, dir, DIRS[dir]);
	}
	let a1 = pos.0.abs() + pos.1.abs(); 
	let mut pos = (0,0);
	let mut wp = (10, 1);
	for line in lines {
		let instr = &line[..1];
		let num : i32 = line[1..].parse().unwrap();
		match instr {
			"N" => wp = (wp.0 + 0, wp.1 + num),
			"S" => wp = (wp.0 + 0, wp.1 - num),
			"E" => wp = (wp.0 + num, wp.1 + 0),
			"W" => wp = (wp.0 - num, wp.1 + 0),
			"F" => pos = (pos.0 + num * wp.0, pos.1 + num * wp.1),
			"L" => {
				for _ in 0..(num / 90){
					wp = (-wp.1, wp.0)
				}
			},
			"R" => {
				for _ in 0..(num / 90){
					wp = (wp.1, -wp.0)
				}
			},
			_ => panic!("unexpected instruction {}", line)
		}
		// println!("{} -> {:?} ({:?})", line, pos, wp);
	}
	let a2 = pos.0.abs() + pos.1.abs(); 

	(a1.to_string(), a2.to_string())
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn ex1(){
		let input = "F10
N3
F7
R90
F11";
		use lib::test_run;
		test_run!(input, 25, 286);
}

	#[test]
	pub fn ex2(){
		let input = "F10
N3
F7
L180
F11";
		use lib::test_run;
		test_run!(input, 9, 66);


	}
}