const PADDING : usize = 7;
// const CHUNK_SIZE: i64 = 8;
// const CSIZE: usize = CHUNK_SIZE as usize;
// const CS8: u8 = CHUNK_SIZE as u8;
const WSIZE : usize = 1 + (PADDING * 2);
const XSIZE : usize = 8 + (PADDING * 2);
const YSIZE : usize = 8 + (PADDING * 2);
const ZSIZE : usize = 1 + (PADDING * 2);

type ChunkSlice = u64;
type Grid3D = Vec<ChunkSlice>;
#[derive(Eq, PartialEq, Hash, Debug)]
struct Index3D{
	x : i64,
	y : i64,
	z : i64
}
type Grid4D = Vec<ChunkSlice>;
#[derive(Eq, PartialEq, Hash, Debug)]
struct Index4D{
	w : i64,
	x : i64,
	y : i64,
	z : i64
}

use lib::make_main;
make_main!(2020, 17);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let allow_size_3d = ZSIZE * YSIZE * XSIZE / 64 + 1;
	let mut space_3d = vec![0; allow_size_3d];
	let allow_size_4d = allow_size_3d * WSIZE;
	let mut space_4d = vec![0; allow_size_4d];


	let w = PADDING;
	let z = PADDING;
	for y in 0..lines.len() {
		let line = lines[y];
		for (x, c) in line.char_indices(){
			match c {
				'#' => {
					set_3d((x + PADDING, y + PADDING, z), &mut space_3d, true);
					set_4d((w, x + PADDING, y + PADDING, z), &mut space_4d, true);
				}
				'.' => (),
				_ => panic!("unexpected character {:?}", c),
			}
		}
	}

	sim_3d(&mut space_3d);
	sim_4d(&mut space_4d);

	let mut a1 = 0;
	for chunk in &space_3d {
		a1 += chunk.count_ones();
	}

	let mut a2 = 0;
	for chunk in &space_4d {
		a2 += chunk.count_ones();
	}

	(a1.to_string(), a2.to_string())
}

fn sim_4d(mut space : &mut Grid4D) {

	let mut min_w = PADDING -1;
	let mut min_x = PADDING -1;
	let mut min_y = PADDING -1;
	let mut min_z = PADDING -1;

	let mut max_w = PADDING +1;
	let mut max_x = PADDING + 8 +1;
	let mut max_y = PADDING + 8 +1;
	let mut max_z = PADDING +1;

	for _ in 0..6 {
		let mut to_flip = vec![];
		for w in min_w..=max_w {
			for z in min_z..=max_z {
				for x in min_x..=max_x {
					for y in min_y..=max_y {
						let active = is_set_4d((w,x,y,z), &space);
						let mut count = 0;
						for l in w-1..=w+1 {
							for i in x-1..=x+1 {
								for j in y-1..=y+1 {
									for k in z-1..=z+1 {
										if l != w || i != x || j != y || k != z {
											if is_set_4d((l, i, j, k), &space) {
												count += 1;
											}else{
											}
										}
									}
								}
							}
						}
						if active {
							if count != 2 && count != 3 {
								to_flip.push((w,x,y,z, false));
							}
						}else{
							if count == 3 {
								to_flip.push((w,x,y,z, true));
							}
						}
					}
				}
			}
		}
		for (w,x,y,z, val) in to_flip {
			set_4d((w,x,y,z), &mut space, val);
			if val {
				if x <= min_x { min_x = x - 1; }
				if y <= min_y { min_y = y - 1; }
				if z <= min_z { min_z = z - 1; }
				if w <= min_w { min_w = w - 1; }
				if x >= max_x { max_x = x + 1; }
				if y >= max_y { max_y = y + 1; }
				if z >= max_z { max_z = z + 1; }
				if w >= max_w { max_w = w + 1; }
			}
		}
	}
}
fn sim_3d(mut space : &mut Grid3D) {
	let mut min_x = PADDING -1;
	let mut min_y = PADDING -1;
	let mut min_z = PADDING -1;

	let mut max_x = PADDING + 8 +1;
	let mut max_y = PADDING + 8 +1;
	let mut max_z = PADDING +1;

	for _ in 0..6 {
		// print_chunk(space3D.get(&Index3D{x: 0,y: 0,z: 0}).unwrap());
		let mut to_flip = vec![];
		for z in min_z..=max_z {
			for x in min_x..=max_x {
				for y in min_y..=max_y {
					let active = is_set_3d((x,y,z), &space);
					let mut count = 0;
					for i in x-1..=x+1 {
						for j in y-1..=y+1 {
							for k in z-1..=z+1 {
								if i != x || j != y || k != z {
									if is_set_3d((i, j, k), &space) {
										count += 1;
									}else{
									}
								}
							}
						}
					}
					if active {
						if count != 2 && count != 3 {
							to_flip.push((x,y,z, false));
						}
					}else{
						if count == 3 {
							to_flip.push((x,y,z, true));
						}
					}
				}
			}
		}
		for (x,y,z, val) in to_flip {
			set_3d((x,y,z), &mut space, val);
			if val {
				if x <= min_x { min_x = x - 1; }
				if y <= min_y { min_y = y - 1; }
				if z <= min_z { min_z = z - 1; }
				if x >= max_x { max_x = x + 1; }
				if y >= max_y { max_y = y + 1; }
				if z >= max_z { max_z = z + 1; }
			}
		}
	}
}
// fn print_chunk(chunk : &Chunk3D){
// 	let mut buff : String = String::with_capacity(85);//64+21
// 	for y in 0..CS8 {
// 		buff.clear();
// 		for z in 0..CSIZE {
// 			for x in 0..CS8 {
// 				let mask = 1 << (y * CS8 + x);
// 				if chunk[z] & mask == mask {
// 					buff.push('#');
// 				}else{
// 					buff.push('.');
// 				}
// 			}
// 			buff.push_str(" | ");
// 		}
// 		println!("{}", &buff);
// 	}
// }
fn to_chunk_3d(idx : (usize, usize, usize)) -> usize {
	let x = idx.0;
	let y = idx.1;
	let z = idx.2;

	  z * YSIZE * XSIZE
	+ y * XSIZE
	+ x

}
fn to_chunk_4d(idx : (usize, usize, usize, usize)) -> usize {
	let w = idx.0;
	let x = idx.1;
	let y = idx.2;
	let z = idx.3;

		w * ZSIZE * YSIZE * XSIZE
	+ z * YSIZE * XSIZE
	+ y * XSIZE
	+ x

}
// fn to_chunk_c(idx : usize) -> (usize, u8) {
// 	( idx / CSIZE, (idx % CSIZE) as u8 ) 
// }
fn set_3d(idx : (usize, usize, usize), space : &mut Grid3D, val : bool){
	let chunk_idx = to_chunk_3d(idx);
	let idx = chunk_idx / 64;
	let rem = chunk_idx % 64;
	if val{
		space[idx] |= 1 << rem;
	}else{
		space[idx] &= !(1 << rem);
	}
}
fn set_4d(idx : (usize, usize, usize, usize), space : &mut Grid4D, val : bool){
	let chunk_idx = to_chunk_4d(idx);
	let idx = chunk_idx / 64;
	let rem = chunk_idx % 64;
	if val{
		space[idx] |= 1 << rem;
	}else{
		space[idx] &= !(1 << rem);
	}
}
fn is_set_3d(idx : (usize, usize, usize), space : &Grid3D) -> bool{
	let chunk_idx = to_chunk_3d(idx);
	let idx = chunk_idx / 64;
	let rem = chunk_idx % 64;

	(space[idx] & 1 << rem) != 0
}
fn is_set_4d(idx : (usize, usize, usize, usize), space : &Grid4D) -> bool{
	let chunk_idx = to_chunk_4d(idx);
	let idx = chunk_idx / 64;
	let rem = chunk_idx % 64;
	(space[idx] & 1 << rem) != 0
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn ex1(){
		let input = ".#.
..#
###";use lib::test_run;

		test_run!(input, 112, 848);
	}
}
