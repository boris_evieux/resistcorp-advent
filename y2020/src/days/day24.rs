use std::collections::HashMap;

use lib::make_main;
make_main!(2020, 24);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut map = HashMap::new();
	let mut min = (0,0,0);
	let mut max = (0,0,0);
	for line in lines {
		let (q, r, s) = read_line(line);
		assert_eq!(0, q + r + s);
		let tile = map.entry((q,r,s)).or_insert(false);
		*tile = ! *tile;

		if q > min.0 { min.0 = q; }
		if q > max.0 { max.0 = q; }
		if r > min.1 { min.1 = r; }
		if r > max.1 { max.1 = r; }
		if s > min.2 { min.2 = s; }
		if s > max.2 { max.2 = s; }
	}
	let width = (max.0 - min.0) as usize + 211;
	let q0 = 105 - min.0;
	let height = (max.1 - min.1) as usize + 211;
	let r0 = 105 - min.1;
	let mut grid = vec![false; width * height];

	let mut a1 = 0;
	for ((q,r,_s), v) in map.iter() {
		if *v { 
			a1 += 1;
			grid[ (q0+q) as usize * width + (r0+r) as usize] = true;
		}
	}
	part_2(&mut grid, width, height);
	let a2 = grid.iter().filter(|b|**b).count();


	(a1.to_string(), a2.to_string())
	// (42.to_string(), "life, the universe and everything".to_string()) // break all_solutions tests with this line
}
fn part_2(grid : &mut [bool], width : usize, height : usize){
	let mut min_q = width;
	let mut max_q = 0;
	let mut min_r = height;
	let mut max_r = 0;

	let mut to_flip = vec![];

	for i in 0..width * height {
		let q = i%width;
		let r = i/width;
		if grid[i] {

			if  q <= min_q { min_q = q - 1; }
			if  q >= max_q { max_q = q + 1; }
			if  r <= min_r { min_r = r - 1; }
			if  r >= max_r { max_r = r + 1; }
		}
	}

	// println!("total black : {}", grid.iter().filter(|b|**b).count());
	for _t in 0..100 {
		// let x = (min_q + 2, min_r + 2);
		// let ns = neighbors(x.0, x.1);
		// for r in min_r-1..=max_r+1 {
		// 	print!("{}", " ".repeat(1 + r - min_r));
		// 	for q in  min_q-1..=max_q+1 {
		// 		let is_n = 'a:  loop {
		// 			for n in &ns {
		// 				if (q, r) == *n {
		// 					break 'a true;
		// 				}
		// 			}
		// 			break false;
		// 		};
		// 		if is_n {
		// 			print!("{} ", 'O');
		// 		}else if (q, r) == x {
		// 			print!("{} ", 'X');
		// 		}else if grid[r * width + q] {
		// 			print!("{} ", '#');
		// 		} else {
		// 			print!("{} ", '.');
		// 		}
		// 	}
		// 	print!("\n");
		// }
		for r in 1..height-1 {
			for q in 1..width-1 {
				let idx = r * width + q;
				let is_black = grid[idx];

				let neighbors = neighbors(q,r);
				let mut n = 0;
				for &(q, r) in &neighbors {
					let idx = r * width + q;
					if grid[idx] { n += 1; }
				}
				let new_is_black = if is_black {
					n == 1 || n == 2
				}else{
					n == 2
				};
				if new_is_black {
					if  q <= min_q { min_q = q - 1; }
					if  q >= max_q { max_q = q + 1; }
					if  r <= min_r { min_r = r - 1; }
					if  r >= max_r { max_r = r + 1; }
				}

				if new_is_black != is_black {
					to_flip.push(idx);
				}
			}
		}
		// println!(" at turn {} flipping {} tiles : {:?}", _t, to_flip.len(), to_flip.iter().map(|i|((i%width)-min_q, (i/width)-min_r)).collect::<Vec<(usize, usize)>>());
		for idx in to_flip.drain(..) {
			// let q = idx%width;
			// let r = idx/width;
			// if grid[idx] {
			// 	println!("tile {}({},{}) was black. flip to white", idx, q, r);
			// }else{
			// 	println!("tile {}({},{}) was black. flip to white", idx, q, r);
			// }
			grid[idx] = ! grid[idx];
		}
		// println!("total black : {}", grid.iter().filter(|b|**b).count());


	}
}
fn neighbors (q : usize, r : usize) -> [(usize, usize); 6] {
	
	[
		(q+1, r),
		((q-1) as usize, r),
		(q, r+1),
		(q, (r-1) as usize),
		(q+1, (r-1) as usize),
		((q-1) as usize, r+1),
	]
}
fn read_line(line : &str) -> (i32, i32, i32) {
	let it = line.chars();
	let mut q = 0;
	let mut r = 0;
	let mut s = 0;
	let mut prev = None;

	for c in it {
		match c {
			'e' => { 
				match prev {
					Some('n') => { r-=1; q+=1; }
					Some('s') => { r+=1; s-=1; }
					None 			=> { q+=1; s-=1; }
					_ 				=> panic!("")
				};
				prev = None;
			}
			'w' => { 
				match prev {
					Some('n') => { r-=1; s+=1; }
					Some('s') => { r+=1; q-=1; }
					None 			=> { q-=1; s+=1; }
					_ 				=> panic!("")
				};
				prev = None;
			}
			'n' => { prev = Some('n'); }
			's' => { prev = Some('s'); }
			 _  => panic!("")
		}
	}

	(q, r, s)
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn units1(){
		let input = "sesenwnenenewseeswwswswwnenewsewsw
neeenesenwnwwswnenewnwwsewnenwseswesw
seswneswswsenwwnwse
nwnwneseeswswnenewneswwnewseswneseene
swweswneswnenwsewnwneneseenw
eesenwseswswnenwswnwnwsewwnwsene
sewnenenenesenwsewnenwwwse
wenwwweseeeweswwwnwwe
wsweesenenewnwwnwsenewsenwwsesesenwne
neeswseenwwswnwswswnw
nenwswwsewswnenenewsenwsenwnesesenew
enewnwewneswsewnwswenweswnenwsenwsw
sweneswneswneneenwnewenewwneswswnese
swwesenesewenwneswnwwneseswwne
enesenwswwswneneswsenwnewswseenwsese
wnwnesenesenenwwnenwsewesewsesesew
nenewswnwewswnenesenwnesewesw
eneswnwswnwsenenwnwnwwseeswneewsenese
neswnwewnwnwseenwseesewsenwsweewe
wseweeenwnesenwwwswnew";
		use lib::test_run;
		test_run!(input, 10, 2208);
	}
}