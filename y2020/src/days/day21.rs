use self::Allergen::*;
use std::collections::{HashMap};
use regex::Regex;

#[derive(Debug, Eq, PartialEq)]
enum Allergen {
	Unknown(String),
	Known(String, String)
}

impl Clone for Allergen {
	fn clone(&self) -> Self {
		match self {
			Unknown(name) => Unknown(name.to_string()),
			Known(name, ingr) => Known(name.to_string(), ingr.to_string()),
		}
	}
}

use lib::make_main;
make_main!(2020, 21);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let reg = Regex::new("(.*) \\(contains (.*)\\)").expect("regex failed");
	let mut allergens : Vec<Allergen> = vec![];
	let mut definitions : Vec<(Vec<&str>, Vec<&str>)> = vec![];
	let mut map_possibles : HashMap<&str, Vec<usize>> = HashMap::new();
	let mut map_ingrs : HashMap<&str, String> = HashMap::new();
	for line in lines {
		let caps = reg.captures(line).unwrap();
		let ingr : Vec<&str> = caps.get(1).unwrap().as_str().split(" ").collect();
		let alls : Vec<&str> = caps.get(2).unwrap().as_str().split(", ").collect();

		let idx = definitions.len();
		for al in &alls{
			if let Some(v) = map_possibles.get_mut(al) {
				// println!("{:?} seen in {:?}. adding line {:?}", al, v, idx);
				(*v).push(idx);
			}else{
				// println!("{:?} never seen. added line {:?}", al, idx);
				map_possibles.insert(al, vec![idx]);
				allergens.push(Unknown(al.to_string()));
			}
		}
		definitions.push((alls, ingr));
	}
	let mut set : HashMap<&str, usize>= HashMap::new();
	loop{
		let mut changed = false;
		for i in 0..allergens.len() {

			let mut found = vec![];
			allergens[i] = match &allergens[i] {
				Known(a,b) => Known(a.to_string(),b.to_string()),
				Unknown(name) => {
					set.clear();
					let indices = map_possibles.get(name.as_str()).unwrap();
					// println!("{:?} is found on lines {:?}", name, indices);
					for &ind in indices {
						let (_, ingr) = &definitions[ind];
						for ingredient in ingr {
							if map_ingrs.get(ingredient) == None {
								if let Some(num) = set.get_mut(ingredient) {
									*num += 1;
								}else {
									set.insert(ingredient, 1);
								}
							}
						}
					}
					for (ingredient, &num) in set.iter() {
						if num == indices.len() {
							// println!("found one correspondence {:?} <> {:?}", name, ingredient);
							found.push((name, ingredient));
						}
					}
					if found.len() == 1{
						let (name, ingredient) = 
						found[0];
						changed = true;
						map_ingrs.insert(ingredient, name.clone());
						Known(name.to_string(), ingredient.to_string())
					}else{
						Unknown(name.to_string())
					}
				}

			};
		}
		if !changed {
			break;
		}
	}
	// println!("known ingredients : {:?}", map_ingrs);
	let mut a1 = 0;
	for (_, ingredients) in &definitions{
		for ingredient in ingredients {
			if map_ingrs.get(ingredient).is_none() {
				a1 += 1;
			}
		}
	}
	let mut a2 : Vec<(&str, String)> = map_ingrs.into_iter().collect();
	a2.sort_by(|(_,al1), (_,al2)|al1.cmp(&al2));
	// println!("after sort : {:?}", a2);
	let a2 = a2.iter().map(|(key,_)|*key).collect::<Vec<&str>>().join(",");

	(a1.to_string(), a2.to_string())
	// (42.to_string(), "life, the universe and everything".to_string()) // break all_solutions tests with this line
}
#[cfg(test)]
mod tests{
	#[test]
	pub fn units1(){
		let input = "mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
trh fvjkl sbzzf mxmxvkd (contains dairy)
sqjhc fvjkl (contains soy)
sqjhc mxmxvkd sbzzf (contains fish)";
		use lib::test_run;
		test_run!(input, 5, "mxmxvkd,sqjhc,fvjkl");
	}
}