

use lib::make_main;
make_main!(2020, 23);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();

	// let num = "389125467";
	let num = lines[0];
	let a1 = part_1(num, 100);
	let a2 = part_2(num);

	(a1, a2.to_string())
	// (42.to_string(), "life, the universe and everything".to_string()) // break all_solutions tests with this line
}

fn part_1(num : &str, turns : usize) -> String {
	let seed = num.chars().map(|c|c.to_digit(10).unwrap() as usize).collect::<Vec<usize>>();

	let vals = run_turns(&seed[..], 9, turns);

	let mut ret = String::with_capacity(8);
	let mut next = vals[1];
	for _ in 1..9 {
		ret.push_str(&next.to_string());
		next = vals[next];
	}
	ret
}

fn part_2(num : &str) -> String {
	let seed = num.chars().map(|c|c.to_digit(10).unwrap() as usize).collect::<Vec<usize>>();
	let max = 1_000_000;
	let vals = run_turns(&seed[..], max, 10_000_000);
	let n1 = vals[1] as u64;
	let n2 = vals[n1 as usize] as u64;
	let answer = n1 * n2;
	answer.to_string()
}
fn run_turns(seed : &[usize], max : usize, turns : usize)-> Vec<usize> {

	let mut data = vec![0;max+1];
	let mut prev = 0;
	for i in 0..seed.len() {
		let v = seed[i];
		data[prev] = v;
		prev = v;
	}
	for i in (seed.len()+1)..=max {
		data[prev] = i;
		prev = i;
	}
	data[prev] = data[0];

// let turns = 25;
	let mut current_cup = data[0];

	for _turn in 1..=turns {
		// println!("\n-- move {} --", _turn);
		// print_cups(&data[..], current_cup);

		let c1 = data[current_cup];
		let c2 = data[c1];
		let c3 = data[c2];

		let next_cup = data[c3];

		data[current_cup] = data[c3];

		// cups.push_back(c);
		// println!("pick up : {} {} {}", c1, c2, c3);
		let mut destination = current_cup - 1;
		while destination == 0 || destination == c1 || destination == c2 || destination == c3 {
			if destination == 0 {
				destination = max;
			}else{
				destination = destination - 1;
			}
		}
		// println!("destination : {}", destination);

		data[c3] = data[destination];
		data[destination] = c1;
		current_cup = next_cup;

	}
	println!("-- final --");
	print_cups(&data, current_cup);
	data
}

fn print_cups(cups : &[usize], cup : usize){

	print!("cups : ");
	let mut next = 0;
	for _ in 1..15 {
		let v = cups[next];
		if v == cup {
			print!("({}) ", v);
		}else{
			print!("{} ", v);
		}
		next = v;
	}
	print!("\n");
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn units1(){
		let line = "389125467";
		assert_eq!(part_1(line, 10), "92658374");
		assert_eq!(part_1(line, 100), "67384529");
	}
	#[test]
	pub fn units2(){
		let line = "389125467";
		assert_eq!(part_2(line), "149245887792");
	}
}