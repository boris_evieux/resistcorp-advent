use lib::make_main;
make_main!(2020, 25);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();

	let pk_door = lines[0].parse().unwrap();
	let pk_card = lines[1].parse().unwrap();

	let (_l1, _l2, a1) = part_1(pk_door, pk_card);

	(a1.to_string(), 0.to_string())
	// (42.to_string(), "life, the universe and everything".to_string()) // break all_solutions tests with this line
}

fn part_1(pk_1 : u64, pk_2 : u64) -> (usize, usize, u64) {
	let mut ls_1 = None;
	let mut ls_2 = None;

	let sn = 7;
	let mut v = 1;
	for loops in 1..usize::MAX {
		v *= sn;
		v %= 20201227;

		if v == pk_1 && ls_1 == None {
			ls_1 = Some(loops);
			if let Some(_) = ls_2 {
				break;
			}
		}
		if v == pk_2  && ls_2 == None {
			ls_2 = Some(loops);
			if let Some(_) = ls_1 {
				break;
			}
		}
	}
	let sn = pk_1;
	let mut v = pk_1;
	for _ in 1..ls_2.unwrap() {
		v *= sn;
		v %= 20201227;
	}


	(ls_1.unwrap(), ls_2.unwrap(), v)
}

#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn units1(){
		assert_eq!(part_1(5764801, 17807724), (8, 11, 14897079));
	}
}