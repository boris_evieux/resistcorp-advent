const ALL_SLOPES: [(usize, usize); 5] = [(1,1), (3,1), (5,1), (7,1), (1,2)];

use lib::util::Lines;
use lib::make_main;
make_main!(2020, 3);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
  let lines : Lines = data.linesv();
  // let slope = (3, 1);
  let all = count_all(lines, &ALL_SLOPES);
  let part1 = all[1];
  let sum = all.into_iter().fold(1, |acc, x| acc * x);
	// println!("day 03 /1 : {}", count(&lines, slope));
	// println!("day 03 /2 : {}", count_all(&lines));
	(
		part1.to_string(),
		sum.to_string()
	)
}

fn count_all(lines : Lines, slopes : &[(usize, usize)])->Vec<u64>{
  let h = lines.len();
  let w = lines[0].len();
  let mut answer = vec![0; slopes.len()];
  // let mut pos = (0, 0);
  for y in 0..h {
    for i in 0..slopes.len() {
      let sl = slopes[i];
      if (y % sl.1) == 0 {
        let turn = y / sl.1;
        let x = (turn * sl.0) % w;
      	let ch = &lines[y][x..=x];
      	if ch == "#" {
      		answer[i] += 1;
      	}
      }
    }
  	// let (x, y) = pos;
  	// let x = x % w;
    // let ch = lines[y][x..=x];
    // if ch == "#" {
    //  trees += 1;
    // }
  	// pos = (pos.0 + slope.0, pos.1 + slope.1);
  }
  answer
}
// fn count(lines : &Lines, slope : (usize, usize)) -> u64 {
//   let h = lines.len();
//   let w = lines[0].len();
//   let mut trees = 0;
//   let mut pos = (0, 0);
//   while pos.1 < h {
//    let (x, y) = pos;
//    let x = x % w;
//    let ch = lines[y].chars().nth(x).unwrap();
//    if ch == '#' {
//      trees += 1;
//    }
//    pos = (pos.0 + slope.0, pos.1 + slope.1);
//   }  
//   trees
// }
#[cfg(test)]
mod tests {
	use super::*;
	#[test]
	pub fn example(){
		let input = 
"..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#";use lib::test_run;

		test_run!(input, 7, 336);
	}
}