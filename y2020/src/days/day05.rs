use lib::make_main;
make_main!(2020, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut tree = [false;1000];
	let mut lowest = 1000;
	let mut highest = 0;
	for line in lines {
		let id = seat_id(line);
		tree[id] = true;
		if id < lowest {
			lowest = id;
		}
		if id > highest {
			highest = id;
		}
	};
	let mut missing = 0;
	for i in lowest..highest {
		if ! tree[i] {
			missing = i;
			break;
		}
	}
	(highest.to_string(), missing.to_string())
}
fn seat_id(input : &str)->usize{
	let mut id = 0;
	for c in input.chars() {
		id = id << 1;
		match c {
			'B' | 'R' => id |= 1,
			_ => ()
		}
	}
	id
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn example(){
		assert_eq!(seat_id("FBFBBFFRLR"), 357);
		assert_eq!(seat_id("BFFFBBFRRR"), 567);
		assert_eq!(seat_id("FFFBBBFRRR"), 119);
		assert_eq!(seat_id("BBFFBBFRLL"), 820);
	}
}