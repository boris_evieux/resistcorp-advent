use self::Ops::*;

use lib::make_main;
make_main!(2020, 18);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut sum1 = 0;
	let mut sum2 = 0;
	for line in lines {
		sum1 += eval1(line);
	}
	for line in lines {
		sum2 += eval2(line);
	}

	let a1 = sum1;
	let a2 = sum2;
	(a1.to_string(), a2.to_string())
}
#[derive(Eq, PartialEq, Debug)]
enum Ops {MUL, ADD, NOP, PAR}
fn eval1(line : &str) -> u64 {
	let mut stack = vec![];
	let mut current = (ADD, 0u64);
	for c in line.chars(){
		match c {
			'(' => {
				let old = current;
				stack.push(old);
				current = (ADD, 0);
			}
			')' => {
				assert_eq!(current.0, NOP);
				let x = current.1;
				let old = stack.pop().expect("stack was empty when ) happened");
				let new = match old {
					(ADD, y) => x + y,
					(MUL, y) => x * y,
					(_, _) => panic!("NO operation in stack"),
				};
				current = (NOP, new)
			}
			'+' => {
				assert_eq!(current.0, NOP);
				current.0 = ADD;
			}
			'*' => {
				assert_eq!(current.0, NOP);
				current.0 = MUL;
			}
			x if x.is_digit(10) => {
				let x = c.to_digit(10).unwrap() as u64;
				let new = match current {
					(ADD, y) => x + y,
					(MUL, y) => x * y,
					(_, _) => panic!("NO operation in stack"),
				};
				current = (NOP, new);
			}
			' ' => {}
			c => panic!("unexpected char {}", c)
		}
	}
	assert!(stack.is_empty());
	assert_eq!(current.0, NOP);
	current.1
}
fn eval2(line : &str) -> u64 {
	// println!("part 2 operation {:?}", line);
	let mut stack = vec![];
	let mut current = (NOP, 0u64);
	for c in line.chars(){
		match c {
			'(' => {
				stack.push((current.0, current.1));
				stack.push((PAR, 0));
				current = (ADD, 0);
				// println!("opened parenthesis. stack : {:?} :: {:?}", stack, current);
			}
			')' => {
				// println!("end of parenthesis {:?}", current);
				while let Some((op, val)) = stack.pop() {
					// println!("  >> roll back {:?} {}", op, val);
					match op{
						PAR => break,
						NOP => (),
						MUL => current.1 *= val,
						ADD => current.1 += val,
						// _ => panic!("unexpected remainder : {:?}", (op, val))
					}
				}
				let do_pop = if let Some((ADD, _)) = &stack.last() {
					true
				}else{
					false
				};
				if do_pop{
					let val = current.1;
					let (_, y) = stack.pop().unwrap();
					current = (NOP, val + y);
				}else{
					current.0 = NOP;
				}
				// println!("end of parenthesis {:?}", current);
				// println!("  >> stack {:?}", stack);
			}
			'+' => {
				assert_eq!(current.0, NOP);
				current.0 = ADD;
			}
			'*' => {
				stack.push((MUL, current.1));
				current = (NOP, 0);
				// println!("pushed mult. op is {:?}", current);
				// println!("  >> stack {:?}", stack);
			}
			x if x.is_digit(10) => {
				let x = c.to_digit(10).unwrap() as u64;
				current = match current {
					(ADD, y) => (NOP, x + y),
					(MUL, y) => panic!("forbidden state : mult "),
					(NOP, _) => (NOP, x),
					(PAR, _) => (NOP, x),
				};
				// println!("digit read : {} op is {:?}", x, current);
			}
			' ' => {}
			c => panic!("unexpected char {}", c)
		}
		// println!("{:?} :: {:?}", stack, current);
	}
	while !stack.is_empty() {
		let (op, val) = stack.pop().unwrap();
		// println!("end of expression {:?} {}", op, val);
		match op {
			MUL => current.1 *= val,
			ADD => current.1 += val,
			NOP => (),
			_ => panic!("unexpected op at the end {:?}", op)
		}
		// println!("{:?} :: {:?}", stack, current);
	}
	assert_eq!(current.0, NOP);
	// println!("{}", current.1);
	current.1
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn units1(){
		assert_eq!(eval1("1 + 2 * 3 + 4 * 5 + 6"), 71);
		assert_eq!(eval1("1 + (2 * 3) + (4 * (5 + 6))"), 51);
		assert_eq!(eval1("2 * 3 + (4 * 5)"), 26);
		assert_eq!(eval1("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 437);
		assert_eq!(eval1("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 12240);
		assert_eq!(eval1("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"), 13632);
	}
	#[test]
	pub fn units_yann(){
		assert_eq!(eval2("8 + (4 * 8 + 5 + 7 * 9 + 4) * 9 + (6 * 7 * 2 + 7)"), 405576);
		assert_eq!(eval2("(2 * 5 * 9 * 2 + 5) + ((2 + 9) + 2 * (2 * 6 + 7)) + 6 + 8 * 5"), 4910);
		assert_eq!(eval2("4 + (4 * 6) + 7 * 3 + 6 + 3"), 420);
		assert_eq!(eval2("(3 + 4 * 4 + 3 + 4) + 2 * 2"), 158);
	}
	#[test]
	pub fn units2(){
		assert_eq!(eval2("1 + 2 * 3 + 4 * 5 + 6"), 231);
		assert_eq!(eval2("1 + (2 * 3) + (4 * (5 + 6))"), 51);
		assert_eq!(eval2("2 * 3 + (4 * 5)"), 46);
		assert_eq!(eval2("5 + (8 * 3 + 9 + 3 * 4 * 3)"), 1445);
		assert_eq!(eval2("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"), 669060);
		assert_eq!(eval2("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"), 23340);
		assert_eq!(eval2("((2 + 4 * 9) + (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"), 552);
	}//                        54              210
}   //                              276