use std::ops::IndexMut;
use std::ops::Index;

struct Memory {
	data : Vec<[Option<usize>; CHUNK_SIZE ]>,
	defer : Vec<Option<usize>>
}
impl Memory{
	fn new() -> Self {
		let data = Vec::new();
		let defer = Vec::new();
		Self{data, defer}
	}
}
const CHUNK_SIZE: usize = 1024;
impl Index<usize> for Memory {
	type Output = Option<usize>;
	fn index(&self, index: usize) -> &Self::Output {
		let chunk = index / CHUNK_SIZE;
    if chunk < self.defer.len() {
    	if let Some(chunk_idx) = self.defer[chunk] {
				let idx = index % CHUNK_SIZE;
    		&self.data[chunk_idx][idx]
    	}else{
    		&None
    	}
    }else{
    	&None
    }
  }
}
impl IndexMut<usize> for Memory {
	fn index_mut(&mut self, index: usize) -> &mut Self::Output {
		let chunk_id = index / CHUNK_SIZE;
		let idx = index % CHUNK_SIZE;
  	if chunk_id >= self.defer.len() {
  		let addition = 1 + chunk_id - self.defer.len();
  		self.defer.reserve(addition);
  		for _ in 0..addition{
  			self.defer.push(None);
  		}
  	}
  	let chunk_idx = match self.defer[chunk_id] {
  		None => {
  			let c = [None; CHUNK_SIZE];
  			let chunk_idx = self.data.len();
  			self.defer[chunk_id] = Some(chunk_idx);
  			self.data.push(c);
  			chunk_idx
  		}
  		Some(chunk_idx) => chunk_idx
  	};
  	&mut self.data[chunk_idx][idx]
  }
}

use lib::make_main;
make_main!(2020, 15);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let numbers : Vec<usize> = data.trim().split(",").map(|s|s.parse::<usize>().expect("can't read input for d 15")).collect();
	let part1 = sequence(&numbers, 2020);
	let part2 = sequence(&numbers, 30_000_000);
	(part1.to_string(), part2.to_string())
}
fn sequence(numbers : &[usize], target : usize) -> usize{
	let mut mem = vec![usize::MAX; target];
	for i in 0..numbers.len() {
		mem[numbers[i]] = i+1;
	}
	let mut last_spoken = numbers[numbers.len()-1];
	for i in numbers.len()..target {
		let speak = match mem[last_spoken] {
			usize::MAX => 0,
			t => i - t
		};
		mem[last_spoken] = i;
		last_spoken = speak;
	}
	last_spoken
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn ex1(){
		let input = "0,3,6";use lib::test_run;

		test_run!(input, 436, 175594);
	}
	#[test]
	pub fn more_ex_1(){
		assert_eq!(sequence(&[1,3,2], 2020), 1);
		assert_eq!(sequence(&[2,1,3], 2020), 10);
		assert_eq!(sequence(&[1,2,3], 2020), 27);
		assert_eq!(sequence(&[2,3,1], 2020), 78);
		assert_eq!(sequence(&[3,2,1], 2020), 438);
		assert_eq!(sequence(&[3,1,2], 2020), 1836);
	}
	#[test]
	pub fn more_ex_2(){
		assert_eq!(sequence(&[1,3,2], 30000000), 2578);
		// assert_eq!(sequence(&[0,3,6], 30000000), 175594);
		// assert_eq!(sequence(&[2,1,3], 2020), 10);
		// assert_eq!(sequence(&[1,2,3], 2020), 27);
		// assert_eq!(sequence(&[2,3,1], 2020), 78);
		// assert_eq!(sequence(&[3,2,1], 2020), 438);
		// assert_eq!(sequence(&[3,1,2], 2020), 1836);
	}
}