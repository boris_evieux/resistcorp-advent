use lib::make_main;
make_main!(2020, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	// run_naive(lines)
	//part 1
	let mut input : Vec<i32> = Vec::with_capacity(10_000);
  for line in lines{
		let v_i = line.parse().unwrap();
		input.push(v_i);
  }
  input.sort();
	let mut sol_1 = 0;
	let mut sol_2 = 0;
  for i in 0..input.len(){
		let v_i = input[i];
  	for j in 0..input.len(){
			let v_j = input[j];
			if v_i+v_j == 2020 {
				sol_1 = v_i * v_j;
			}else if v_i+v_j > 2020 {
				break;
			}

			for k in (j+1)..input.len(){
				let v_k = input[k];
				if v_i+v_j+v_k == 2020 {
					sol_2 = v_i * v_j * v_k;
					break;
				}else if v_i+v_j+v_k > 2020 {
					break;
				}

			}
		}
		if sol_1 != 0 && sol_2 != 0
		{
			break;
		}
  }
	(sol_1.to_string(), sol_2.to_string())
	// (42.to_string(), "life, the universe and everything".to_string()) // break all_solutions tests with this line
}

pub fn run_naive(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	//part 1
	let mut sol_1 = 0;
	let mut sol_2 = 0;
	let mut input : Vec<i32> = Vec::with_capacity(10_000);
  'all : for line in lines{
		let v_i = line.parse().unwrap();
  	for j in 0..input.len(){
			let v_j = input[j];
			if v_i+v_j == 2020 {
				sol_1 = v_i * v_j;
			// }else if v_i+v_j > 2020 {
			// 	break;
			}

			for k in (j+1)..input.len(){
				let v_k = input[k];
				if v_i+v_j+v_k == 2020 {
					sol_2 = v_i * v_j * v_k;
				// }else if v_i+v_j+v_k > 2020 {
					break;
				}

			}
			if sol_1 != 0 && sol_2 != 0
			{
				break 'all;
			}
		}
		input.push(v_i);
  }
	(sol_1.to_string(), sol_2.to_string())
}