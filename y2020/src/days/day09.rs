use lib::util::Lines;
use lib::make_main;
make_main!(2020, 09);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines : Lines = data.linesv();
	// println!("it took {} instructions to get here", loops);
	let weakness1 = attack1(&lines, 25);
	let weakness2 = attack2(lines, weakness1);
	(weakness1.to_string(), weakness2.to_string())
}
fn attack1(lines : &Lines, window : usize)-> i64 {
	let mut buffer : Vec<i64> = vec![ 0 ; window];
	let mut index = 0;
	for line in lines {
		let num = line.parse::<i64>().expect("unexpected number format in day 9");
		// println!("{} : {}", index, num);

		if index >= window {
			let mut found = false;
			'out : for i in 0..window {
				for j in 0..i {
					let i = buffer[i];
					let j = buffer[j];
					if (i + j) == num {
						// println!("{} == {} + {}", num, i,j);
						found = true;
						break 'out
					}else{
						// println!("{} != {} + {}", num, i,j);
					}
				}
			}
			if ! found {
				return num;
			}
		}

		let idx = index % window;
		buffer[idx] = num;
		index += 1
	}
	panic!("no actionable number");
}
fn attack2(lines : Lines, weakness : i64) -> i64 {
	for index in 0..lines.len() {
		let line = lines[index];
		let num = line.parse::<i64>().expect("unexpected number format in day 9");

		let mut min = num;
		let mut max = 0;
		let mut tally = 0;
		let mut cursor = index ;
		// println!("new number {:?}", num);
		while tally < weakness {
			let sub_num = lines[cursor].parse::<i64>().expect("unexpected number format in day 9");
			tally += sub_num;
			if min > sub_num { min = sub_num }
			if max < sub_num { max = sub_num }
			// println!("tally {} min {}max {}", tally, min, max);
			if tally == weakness {
				//yass
				return min + max;
			}
			if cursor > 0 {
				cursor -= 1;
			}else{
				break;
			}
		}

	}
	panic!("no actionable window");
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn fields(){
		let input = "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576";
		let lines = input.lines().collect::<Vec<&str>>();
		assert_eq!(attack1(&lines, 5), 127);
		assert_eq!(attack2(lines, 127), 62);
	}
}