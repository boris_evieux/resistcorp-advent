struct Simulator{
	threshold : u8,
	num_seats : usize,
	links : Vec<Vec<usize>>
}
impl Simulator{
	fn new(taken : &Vec<usize>, num_seats : usize, width : usize, height : usize, part_2 : bool) -> Self {
		let mut links = vec![vec![];num_seats];
		let mut max_gap = 0;
		let mut scrap_neighbors = vec![0; NUM_DELTAS];

		for seat_y in 0..height{
			for seat_x in 0..width{
				let seat_idx = taken[seat_y * width + seat_x];
				if seat_idx != usize::MAX {
					if part_2{
							lines_of_sight(&mut scrap_neighbors, (seat_x, seat_y), (width, height), taken);
						}else{
							neighbors(&mut scrap_neighbors, (seat_x, seat_y), (width, height), taken);
						};
					for &neighbor in &scrap_neighbors{
						links[seat_idx].push(neighbor);
						links[neighbor].push(seat_idx);
						max_gap = usize::max((seat_idx as i32 - neighbor as i32 ).abs() as usize, max_gap);
					}

					// links[seat_idx].sort_by(|a, b| (seat_idx as i64 - *a as i64).abs().cmp(&(seat_idx as i64 - *b as i64).abs()));
				}
			}
		}
		// println!("max gap is {:?}", max_gap);
		let threshold = if part_2 {5} else {4};
		Self{links, num_seats, threshold}
	}
	fn sim_till_stable(&self) ->usize {
		
		let num_seats = self.num_seats as usize;
		// let mut nums = vec![0u8; num_seats];
		// let mut seats = vec![0u64; (num_seats / 64) + 1];
		let mut seats = vec![false; num_seats];
		let mut changed = Vec::with_capacity(num_seats);

		// let mut dur_count = Instant::now().elapsed();
		// let mut dur_flip = Instant::now().elapsed();
		
		loop {
			// num_full = 0;
			// changed = false;
			// let start = Instant::now();
			// for (dest, neighbor) in &self.links {
			// 	if is_full(&seats, *neighbor) {
			// 		nums[*dest] += 1;
			// 	}
			// 	if is_full(&seats, *dest) {
			// 		nums[*neighbor] += 1;
			// 	}
			// }
			// dur_count += start.elapsed();

			// let start = Instant::now();
			let range = 0..seats.len();
			// for i in range.into_par_iter() {
			for i in range {
				let links = &self.links[i];
				let mut flip = true;
				if seats[i] {
					let mut n = 0;
					for &neighbor in links {
						if seats[neighbor] {
							n += 1;
						}
					}
					flip = n >= self.threshold;
				}else{
					for &neighbor in links{
						if seats[neighbor] {
							flip = false;
							break;
						}
					}

				}
				if flip {
					changed.push(i);
				}

				// let row = seats[i];
				// let offset = 64 * i;
				// let last = usize::min(num_seats - offset, 64);
				// let mut empties = 0x00000000;
				// let mut threshs = 0x00000000;
				// // seen_none[i] = 0x00000000;
				// // seen_too_many[i] = 0x00000000;
				// for j in 0..last{
				// 	let mask = 1u64 << j;
				// 	if nums[offset + j] == 0 {
				// 		empties |= mask;
				// 	}
				// 	if nums[offset + j] >= self.threshold {
				// 		threshs |= mask;
				// 	}
				// 	nums[offset + j] = 0;
				// }
				// let new_val = (row & !threshs) | (!row & empties);
				// if new_val != row {
				// 	changed = true;
				// 	seats[i] = new_val;
				// }
			}
			if changed.is_empty() {
				break;
			}
			for i in changed.drain(..) {
				seats[i] = ! seats[i]
			}
			// dur_flip += start.elapsed();
		}
		// println!(" counts : {:?} flips : {:?}", dur_count, dur_flip);
		// num_full
		let mut ret = 0;
		for row in seats {
			if row {ret += 1;}
			// ret += row.count_ones();
		}
		ret as usize
	}
}

fn is_full(seats: &[u64], i : usize) -> bool{
	let idx = i / 64;
	let mask = 1 << (i % 64);
	seats[idx as usize] & mask == mask
}

use lib::make_main;
make_main!(2020, 11);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	// use std::time::Instant;
	// let start = Instant::now();
	let width = lines[0].len();
	let height = lines.len();
	let mut num_seats = 0;
	let mut positions = Vec::with_capacity(width * height);
	for y in 0..height {
		let s = &lines[y];
		for (x, c) in s.char_indices() {
			if c == 'L' {
				// indices.push((x, y, num_seats));
				positions.push((x, y));
				num_seats += 1;
			}
		}
	}
	positions.sort_by(|(xa,ya), (xb, yb)|(xa + ya).cmp(&(xb + yb)));
	let mut taken = vec![usize::MAX; width * height];
	for i in 0..positions.len() {
		let (x, y) = positions[i];
		taken[y * width + x] = i;
	}

	// println!("parse : {:?}", start.elapsed());
	// let start = Instant::now();
	let sim1 = Simulator::new(&taken, num_seats, width, height, false);
	// println!("build 1 : {:?}", start.elapsed());
	// let start = Instant::now();
	let sim2 = Simulator::new(&taken, num_seats, width, height, true);
	// println!("build 2 : {:?}", start.elapsed());
	// let start = Instant::now();

	let a1 = sim1.sim_till_stable();
	// println!("run 1 : {:?}", start.elapsed());
	// let start = Instant::now();
	let a2 = sim2.sim_till_stable();
	// println!("run 2 : {:?}", start.elapsed());


	(a1.to_string(), a2.to_string())
}
const NUM_DELTAS : usize = 4;
const DELTAS : [(i32, i32); NUM_DELTAS] = [
	/*(-1, -1), ( 0, -1), ( 1, -1),
		(-1,  0), */        ( 1,  0),
		(-1,  1), ( 0,  1), ( 1,  1) // */
];
fn neighbors(scrap : &mut Vec<usize>, (x, y) : (usize, usize), (w, h) : (usize, usize), grid : &Vec<usize>){
	scrap.clear();
	for i in 0..NUM_DELTAS {
		let (d_x, d_y) = DELTAS[i];
		let (x, y) = (x as i32 + d_x, y as i32 + d_y);
		match (x, y) {
			(x, _) if x < 0 => (),
			(_, y) if y < 0 => (),
			(x, _) if x >= w as i32 => (),
			(_, y) if y >= h as i32 => (),
			(x, y) => {
				let idx = grid[y as usize * w + x as usize];
				if idx != usize::MAX {
					scrap.push(idx);
				}
			}
		}
	}
}

fn lines_of_sight(scrap : &mut Vec<usize>, (x, y) : (usize, usize), (w, h) : (usize, usize), grid : &Vec<usize>){
	scrap.clear();
	for i in 0..NUM_DELTAS {
		let (d_x, d_y) = DELTAS[i];
		let (mut x, mut y) = (x as i32, y as i32);
		loop{
			x += d_x;
			y += d_y;
			match (x, y) {
				(x, _) if x < 0 => break,
				(_, y) if y < 0 => break,
				(x, _) if x >= w as i32 => break,
				(_, y) if y >= h as i32 => break,
				(x, y) => {
					let x = x as usize;
					let y = y as usize;
					let seat = grid[y * w + x];
					if seat != usize::MAX{
						scrap.push(seat);
						break;
					}
				}
			}
		}
	}
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn ex1(){
		let input = "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";use lib::test_run;

		test_run!(input, 37, 26);
	}
// 	#[test]
// 	pub fn units(){
// 		let input = ".......#.
// ...#.....
// .#.......
// .........
// ..#L....#
// ....#....
// .........
// #........
// ...#.....";
// 		let lines = input.lines().collect::<Vec<&str>>();
// 		let grid = read(&lines);
// 		assert_eq!(grid[4][3], Empty);
// 		assert_eq!(neighbors2((3, 4), (9, 9), &grid), 8);
// 	}

}
