use std::collections::HashMap;

use crate::days::day20::Dirs::*;
use std::fmt::{Display, Formatter, Result};

#[derive(Eq, PartialEq, Debug, Copy, Clone)]
enum Dirs {Up, Right, Down, Left}

struct Image {
	data : Vec<u128>,
	width : usize,
}
impl Image{
	fn from_data(sol : &[Tile], tile_w : usize, height : usize) -> Self {
		let mut data = vec![0u128; height * 8];
		let width = tile_w * 8;
		for chunk_j in 0..height{
			for line in 1..9 {
				let mut v = 0u128;
				let shift = (10 * (9-line)) + 1;
				let chunk_mask = 0xFFu128 << shift;
				for i in 0..tile_w {
					let mut tile_data = sol[chunk_j * tile_w + i].data & chunk_mask;
					tile_data >>= shift-1;
					v |= tile_data << ((tile_w - 1 - i) * 8)

				}
				data[chunk_j * 8 + line-1] = v;
			}
		}
		Self{data, width}
	}
}
impl Display for Image {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
    	let h = self.data.len();
    	for line in 0..h {
    		if line < h-1 {
        	write!(f, "{}\n", to_line_img(self.data[line], self.width))?;
    		}else{
	        write!(f, "{}", to_line_img(self.data[line], self.width))?;
    		}
    	}
    	Ok(())
    }
}

const MONSTAR : [&str; 3] = ["                  # ",
														 "#    ##    ##    ###",
														 " #  #  #  #  #  #   "];

impl Display for Dirs {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result {
        write!(f, "{:?}", self)
    }
}
const DIRS : [Dirs; 4] = [Up, Right, Down, Left];
const DIR_UP		: usize = 0;
const DIR_RIGHT	: usize = 1;
const DIR_DOWN	: usize = 2;
const DIR_LEFT	: usize = 3;

use lib::make_main;
make_main!(2020, 20);
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut tiles = vec![];
	let mut map : HashMap<u16, Vec<(usize, usize, bool)>> = HashMap::new();
	let mut line_idx = 0;
	while line_idx < lines.len() {
		let tile = Tile::read(&lines[line_idx..line_idx+11], tiles.len());

		for dir_i in 0..DIRS.len() {
			for &flip in &[true, false] {
				let dir = DIRS[dir_i];
				let side = tile.side(dir, flip);
				let entry = map.entry(side).or_insert_with(Vec::new);
				entry.push((tile.idx, dir_i, flip));
				// println!("{}:{}({}) : {} ({})", id, dir, flip, to_line(side), entry.len());
			}
		}

		// println!("read tile {}", tile.idx);
		// println!("{}", tile);
		tiles.push(tile);
		line_idx += 12;
	}
	for (_key, vals) in map.iter() {
		for i in 0..vals.len() {
			let (idx1, dir1, flip1) = vals[i];
			if !flip1{//matching sides match twice
				for j in (i+1)..vals.len() {
					let (idx2, dir2, _) = vals[j];
					{
						let v = &mut tiles[idx1].border[dir1];
						v.push( (idx2, dir2) );
					}
					{
						let v = &mut tiles[idx2].border[dir2];
						v.push( (idx1, dir1) );
					}
				} 
			}
		} 
	}

	let (width, solution) = solve(&mut tiles);
	let height = tiles.len()/width;

	let image = Image::from_data(&solution, width, height);
	let monstars = monstar_patterns();

	let mut a1 = solution[0].id as u64;
	a1 *= solution[width-1].id as u64;
	a1 *= solution[(height-1)*width].id as u64;
	a1 *= solution[tiles.len()-1].id as u64;

	let width = width * 8;
	let height = height * 8;

	let mut data = vec![vec![false; width]; height];
	let mut total = vec![vec![false; width]; height];

	let mut num = 0;
	for y in 0..height {
		for x in 0..width {
			let line = image.data[y];
			let mask = 1 << (width - x);
			if line & mask != 0u128{
				data[y][x] = true;
				total[y][x] = true;
				num += 1;
			}
		}
	}

	let mut m = 0;
	for y in 0..height {
		for x in 0..width {
			for monstar in &monstars{
				if is_pattern(monstar, &data, x, y){
					m+= 1;
					num -= 15;
					delete_pattern(monstar, &mut total, x, y);
				}
			}
		}
	}
	let mut a2 = 0;
	for y in 0..height {
		for x in 0..width {
			if total[y][x]{
				a2 += 1;
			}
		}
	}

	(a1.to_string(), a2.to_string())
}
fn is_pattern(monstar : &Vec<Vec<bool>>, data : &Vec<Vec<bool>>, x : usize, y : usize) -> bool {
	if monstar.len() + y >= data.len() {
		false
	}else if monstar[0].len() + x >= data[0].len() {
		false
	}else{
		for j in 0..monstar.len() {
			let line_m = &monstar[j];
			let line_d = &data[j+y];
			for i in 0..line_m.len(){
				if line_m[i] && !line_d[i + x] {
					return false;
				}
			}
		}
		true
	}
}
fn delete_pattern(monstar : &Vec<Vec<bool>>, data : &mut Vec<Vec<bool>>, x : usize, y : usize) {
	if monstar.len() + y >= data.len() {
	}else if monstar[0].len() + x >= data[0].len() {
	}else{
		for j in 0..monstar.len() {
			let line_m = &monstar[j];
			let line_d = &mut data[j+y];
			for i in 0..line_m.len(){
				if line_m[i] {
					line_d[i + x] = false;
				}
			}
		}
	}
}
fn solve (tiles: &mut [Tile]) -> (usize, Vec<Tile>) {
	//(width, (index, rotation, flip_x, flip_y))
	let sqrt = (tiles.len() as f32).sqrt();
	// println!("{} tiles, {} x {}", tiles.len(), sqrt, sqrt);

	let width = (tiles.len() as f32).sqrt() as usize;
	assert_eq!(width * width, tiles.len());
	let mut solved = false;
	while !solved {
		solved = true;
		let mut reduceable = vec![];
		for idx in 0..tiles.len(){
			let tile = &tiles[idx];
			for dir_i in 0..DIRS.len() {
				let border = &tile.border[dir_i];
				// println!("{}({}):{} has {} possibilities : {:?}", idx, tile.id, DIRS[dir_i], border.len(), border);
				match border.len() {
					0 => (),//a side
					1 => {
						let (other_idx, side) = border[0];
						let other = &tiles[other_idx];
						if other.border[side].len() > 1 {
							reduceable.push(((idx, dir_i), border[0]));
						}
					},// we can reduce
					_ => solved = false//for next time
				}
			}
		}
		if reduceable.len() == 0 {break;}
		// println!("reducing {:?}", reduceable);
		for ((idx, dir_i), (other_idx, side)) in reduceable {
			let other = &mut tiles[other_idx];
			other.border[side].clear();
			other.border[side].push((idx, dir_i));
		}
	}
	let mut sol = vec![];

	let mut seed = None;
	//find a seed corner
	'a : for tile in tiles.iter() {
		for i in 0..DIRS.len() {
			let j = (i + 1) % DIRS.len();
			if tile.border[i].is_empty() && tile.border[j].is_empty() {
				// println!("tile {} has a corner {}x{}", tile.id, DIRS[i], DIRS[j] );
				//a1 *= tile.id as u64;
				seed = Some((tile.idx, j));
				break 'a 
			}
		}
	}
	if let Some((tile_idx, rotation)) = seed {
		let mut seed = tiles[tile_idx].clone();
		// println!("seed borders before {:?}", seed.border);
		match rotation {
			0 => {}
			1 => {seed.flip_x();}
			2 => {seed.rotate(2);}
			3 => {seed.flip_y();}
			_ => {}
		}
		// println!("seed borders after {:?}", seed.border);
		// println!("{}", seed);
		sol.push(seed);

		for i in 1..tiles.len() {
			let x = i % width;
			let y = i / width;
			let up = if y > 0 {
				let tile = &sol[(y-1) * width + x];
				// println!("tile upwards {} has borders : {:?}", tile.id, tile.border);
				let target = tile.border[DIR_DOWN][0];
				Some((target, tile.side(Down, true)))
			}else{None};
			let left = if x > 0 {
				let tile = &sol[y * width + (x-1)];
				// println!("tile to left {} has borders : {:?}", tile.id, tile.border);
				let target = tile.border[DIR_RIGHT][0];
				Some((target, tile.side(Right, false)))
			}else{None};

			let new_tile = if left.is_some() && up.is_some() {
				let ((idx_up, dir_up), side_up) = up.unwrap();
				let ((idx_left, dir_left), side_left) = left.unwrap();
				// println!("tile is constrained up and left : {} {}", idx_up, idx_left);
				assert_eq!(idx_left, idx_up);
				let mut tile_copy = tiles[idx_up].clone();
				// println!("borders before {:?}", tile_copy.border);
				// println!("adding tile {} {} : {} rotated {}", x, y, tile_copy.id, dir_up);
				tile_copy.rotate(dir_up);
				// println!("borders after {:?}", tile_copy.border);
				if dir_up == dir_left -1 || dir_up == dir_left +3 {
					// println!("wrong corner, flipping over x");
					tile_copy.flip_x();
					// println!("borders after flip {:?}", tile_copy.border);

				} else if dir_up == dir_left + 1 || dir_up == dir_left -3 {
					//OK
				}else{
					panic!("constraints are not compatible : left wants {:?} and up wants {:?}", DIRS[dir_left], DIRS[dir_up]);
				}
				let my_up_side = tile_copy.side(Up, false);
				assert_eq!(side_up, my_up_side);

				let my_left_side = tile_copy.side(Left, true);
				assert_eq!(side_left, my_left_side);


				tile_copy
			}else if let Some(((idx, dir_i), side_prev)) = up {
				let mut tile_copy = tiles[idx].clone();
				// println!("adding tile {} {} : {} rotated {}", x, y, tile_copy.id, dir_i);
				// println!("borders before {:?}", tile_copy.border);

				tile_copy.rotate(dir_i);
				let my_up_side = tile_copy.side(Up, false);
				if my_up_side != side_prev{tile_copy.flip_x();}
				// println!("borders after {:?}", tile_copy.border);
				tile_copy
			}else if let Some(((idx, dir_i), side_prev)) = left {
				let mut tile_copy = tiles[idx].clone();
				// println!("adding tile {} {} : {} rotated {}", x, y, tile_copy.id, dir_i);
				// println!("borders before {:?}", tile_copy.border);

				tile_copy.rotate(dir_i+1);
				let my_left_side = tile_copy.side(Left, true);
				if my_left_side != side_prev {tile_copy.flip_y();}
				// println!("borders after {:?}", tile_copy.border);
				tile_copy
				
			}else{panic!("illegal state constructing the image");};

			// println!("{}", new_tile);
			sol.push(new_tile);

		}

		(width, sol)
	}else{
		panic!("could not find a corner to start with");
	}
}
fn monstar_patterns()-> Vec<Vec<Vec<bool>>>{
	let mut canon = vec![vec![false;20];3];
	for j in 0..MONSTAR.len() {
		let line = MONSTAR[j];
		for (i, c) in line.char_indices() {
			canon[j][i] = match c {
				' ' => false,
				'#' => true,
				_ => panic!("")
			}
		}
	}
	let mut ret = vec![];
	for _ in 0..4{
		ret.push(vec![vec![false;3];20]);
		ret.push(vec![vec![false;20];3]);
	}
	for y in 0..3 {
		for x in 0..20 {
			let v = canon[y][x];
			ret[0][x][y] = v;
			ret[1][y][x] = v;
			ret[2][19-x][2-y] = v;
			ret[3][2-y][19-x] = v;
			ret[4][x][2-y] = v;
			ret[5][2-y][x] = v;
			ret[6][19-x][y] = v;
			ret[7][y][19-x] = v;
		}
	}
	ret
}
#[derive(Clone)]
struct Tile{
	id:u16,
	idx:usize,
	data:u128,
	border:[Vec<(usize, usize)>;4]
}
const DOWN_MSK 	: u128 = 0b1111111111u128;
const UP_MSK 		: u128 = 0b1111111111u128 << 90;
impl Tile {
	fn read(lines : &[&str], idx : usize) -> Self {
		assert_eq!(lines.len(), 11);
		let id = lines[0][5..9].parse().expect("wrong id");
		let mut data = 0u128;
		for j in 0..10 {
			let line = lines[j+1];
			for (i, _c) in line.char_indices().filter(|(_,c)| *c == '#') {
				data |= 1 << (10 * (9-j) + (9-i));
			}
		}
		let border = [vec![], vec![], vec![], vec![]];
		Tile{id, idx, data, border}
	}
	fn left(&self) -> u16{
		let mut ret = 0u16;
		let data = self.data;
		for i in 0..10 {
			let idx = 10 * (9-i) + 9;
			let msk = 1 << idx;
			let b = (data & msk) >> (idx - i);
			ret |= b as u16;
		}
		ret
	}
	fn right(&self) -> u16{
		let mut ret = 0u16;
		let data = self.data;
		for i in 0..10 {
			let idx = 10 * i;
			let msk = 1 << idx;
			let b = (data & msk) >> (idx - i);
			ret |= b as u16;
		}
		ret
	}
	fn up(&self) -> u16{
		((self.data & UP_MSK) >> 90) as u16
	}
	fn down(&self) -> u16{
		Tile::flip_side((self.data & DOWN_MSK) as u16)
	}
	
	fn side(&self, dir : Dirs, flipped : bool) -> u16{
		let ret = match dir {
			Up => self.up(),
			Down => self.down(),
			Left => self.left(),
			Right => self.right(),
		};
		if flipped {
			Tile::flip_side(ret)
		}else{
			ret
		}
	}
	fn flip_side(data : u16) -> u16 {
		data.reverse_bits() >> 6
	}
	fn flip_x(&mut self){
		let mut data = 0u128;
		for y in 0..10{
			let mask = DOWN_MSK << (10 * y);
			let line = (self.data & mask) >> (10* y);
			let line = line.reverse_bits() >> 118;
			data |= line << (10*y); 
		}
		self.data = data;
		let b = &self.border;
		self.border = [b[0].to_vec(), b[3].to_vec(), b[2].to_vec(), b[1].to_vec()];
	}
	fn flip_y(&mut self){
		let mut data = 0u128;
		for y in 0..10{
			let mask = DOWN_MSK << (10 * y);
			let line = (self.data & mask) >> (10* y);
			data |= line << (10* (9-y)); 
		}
		self.data = data;
		let b = &self.border;
		self.border = [b[2].to_vec(), b[1].to_vec(), b[0].to_vec(), b[3].to_vec()];
	}
	fn rotate(&mut self, q_turns : usize) {
		let mut data = 0u128;
		let q_turns = q_turns % 4;
		let b = &self.border;
		self.border = [b[q_turns % 4].to_vec(), b[(q_turns + 1) % 4].to_vec(), b[(q_turns + 2) % 4].to_vec(), b[(q_turns + 3) % 4].to_vec()];
		match q_turns{
			0 => {data = self.data;}
			1 => {
				for y in 0..10 {
					let col = 9 - y;
					for x in 0..10 {

						let row = x;
						let src_idx = row * 10 + col;
						if self.data & (1<<src_idx) != 0 {
							let dst_idx = y * 10 + x;
							data |= 1 << dst_idx;
						}
					}
				}
			}
			2 => {
				data = self.data.reverse_bits() >> 28;
			}
			3 => {
				for y in 0..10 {
					let col = y;
					for x in 0..10 {
						let row = 9-x;
						let src_idx = row * 10 + col;
						if self.data & (1<<src_idx) != 0 {
							let dst_idx = y * 10 + x;
							data |= 1 << dst_idx;
						}
					}
				}
			}
			_ => panic!("can't happen")
		}
		self.data = data;

	}
}
impl Display for Tile {
	fn fmt(&self, f: &mut Formatter) -> Result{
		write!(f, "Tile {}:\n", self.id)?;
		for i in 0..10 {
			let offset = 10 * (9-i);
			let line = ((self.data & (DOWN_MSK << offset)) >> offset) as u16;
			if i <9 {
				write!(f, "{}\n", to_line(line))?;
			}else{
				write!(f, "{}", to_line(line))?;
			}
		}
		Ok(())
	}
}
fn char_to_img(c : char) -> char {
	match c {
		'0' => '.',
		'1' => '#',
		 c => panic!("unexpected character {:?}", c)
	}
}
fn to_line(b : u16) -> String{
	format!("{:010b}", b)
		.chars()
		.map(char_to_img)
		.collect::<String>()
}
fn to_line_img(b : u128, width : usize) -> String{
	let mut buf = String::new();
	for i in 0..width {
		let mask = 1 << (width-i);
		let c = if mask & b == mask {
			'#'
		}else{
			'.'
		};
		buf.push(c);
	}
	buf
}
#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn units1(){
		let input = "Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###

Tile 1951:
#.##...##.
#.####...#
.....#..##
#...######
.##.#....#
.###.#####
###.##.##.
.###....#.
..#.#..#.#
#...##.#..

Tile 1171:
####...##.
#..##.#..#
##.#..#.#.
.###.####.
..###.####
.##....##.
.#...####.
#.##.####.
####..#...
.....##...

Tile 1427:
###.##.#..
.#..#.##..
.#.##.#..#
#.#.#.##.#
....#...##
...##..##.
...#.#####
.#.####.#.
..#..###.#
..##.#..#.

Tile 1489:
##.#.#....
..##...#..
.##..##...
..#...#...
#####...#.
#..#.#.#.#
...#.#.#..
##.#...##.
..##.##.##
###.##.#..

Tile 2473:
#....####.
#..#.##...
#.##..#...
######.#.#
.#...#.#.#
.#########
.###.#..#.
########.#
##...##.#.
..###.#.#.

Tile 2971:
..#.#....#
#...###...
#.#.###...
##.##..#..
.#####..##
.#..####.#
#..#.#..#.
..####.###
..#.#.###.
...#.#.#.#

Tile 2729:
...#.#.#.#
####.#....
..#.#.....
....#..#.#
.##..##.#.
.#.####...
####.#.#..
##.####...
##..#.##..
#.##...##.

Tile 3079:
#.#.#####.
.#..######
..#.......
######....
####.#..#.
.#...#.##.
#.#####.##
..#.###...
..#.......
..#.###...";
		use lib::test_run;
		test_run!(input, 20899048083289u64, 273);
	}
	#[test]
	fn rotations(){
		let c = "Tile 2311:
..##.#..#.
##..#.....
#...##..#.
####.#...#
##.##.###.
##...#.###
.#.#.#..##
..#....#..
###...#.#.
..###..###";
		let canonical = Tile::read(&c.lines().collect::<Vec<&str>>()[..], 0);
		assert_eq!(c, format!("{}", canonical));
		let mut flipped_y = canonical.clone();
		flipped_y.flip_y();
		let expect = "Tile 2311:
..###..###
###...#.#.
..#....#..
.#.#.#..##
##...#.###
##.##.###.
####.#...#
#...##..#.
##..#.....
..##.#..#.";
		assert_eq!(expect, format!("{}", flipped_y));
		let mut flipped_x = canonical.clone();
		flipped_x.flip_x();
		let expect = "Tile 2311:
.#..#.##..
.....#..##
.#..##...#
#...#.####
.###.##.##
###.#...##
##..#.#.#.
..#....#..
.#.#...###
###..###..";
		assert_eq!(expect, format!("{}", flipped_x));
		let mut rotated_1 = canonical.clone();
		rotated_1.rotate(1);
		let expect = "Tile 2311:
...#.##..#
#.#.###.##
....##.#.#
....#...#.
#.##.##...
.##.#....#
#..##.#..#
#..#...###
.#.####.#.
.#####..#.";
		assert_eq!(expect, format!("{}", rotated_1));
		let mut rotated_2 = canonical.clone();
		rotated_2.rotate(2);
		let mut flipped_xy = canonical.clone();
		flipped_xy.flip_x();
		flipped_xy.flip_y();
		assert_eq!(format!("{}", flipped_xy), format!("{}", rotated_2));

		assert_eq!(expect, format!("{}", rotated_1));
		let mut rotated_3 = canonical.clone();
		rotated_3.rotate(3);
		rotated_3.rotate(1);
		assert_eq!(format!("{}", canonical), format!("{}", rotated_3));
	}
}