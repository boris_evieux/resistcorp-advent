pub mod days;
pub fn main(){}

#[cfg(test)]
mod tests{

    use crate::days;
    use lib::util::do_tests;

    #[test]
    pub fn all_solutions(){
        do_tests(2021, 1..=17, &days::all());
    }
}
