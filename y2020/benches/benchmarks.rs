
// fn all_days(c: &mut Criterion) {
//     let runner = run_day(1, false);
//     for day in &DAYS {
//         let name = match day {
//             Anon(_, idx) => format!("day {:02}", idx),
//             Named(_, _, name) => name.to_string()
//         };
//         c.bench_function(&name, |b| b.iter(|| runner(&day)));

//     }
// }

use criterion::{criterion_group, criterion_main, Criterion};


fn single_bench(c: &mut Criterion) {
    // c.bench_function("day 11 sequential x 1 ", |b| b.iter(|| day_seq()));
}// criterion_group!(benches, all_days);
criterion_group!(benches, single_bench);
criterion_main!(benches);
