use lib::util::Params;
use std::fs;
use criterion::{criterion_group, criterion_main, Criterion};

fn day_01(c: &mut Criterion) {
    let mut group = c.benchmark_group("day_1");
    use ::y2022::days::day01::run as old;
    use ::y2022::days::day01::run_bin_search as new;
    let p = Params::new(None, None);
    let input = fs::read("./input/day_01.txt").expect("file!!!");
    let input = input[..].into();
    group.bench_function("day 1 ", |b| b.iter(|| old(&input, &p)));
    group.bench_function("day 1 with binse", |b| b.iter(|| new(&input, &p)));
    group.finish();
}// criterion_group!(benches, all_days);

fn day_02(c: &mut Criterion) {
    let mut group = c.benchmark_group("day_2");
    use ::y2022::days::day02::run_old;
    use ::y2022::days::day02::run;
    let p = Params::new(None, None);
    let input = fs::read("./input/day_02.txt").expect("file!!!");
    let input = input[..].into();
    group.bench_function("day 2 ", |b| b.iter(|| run_old(&input, &p)));
    group.bench_function("day 2 done quick ", |b| b.iter(|| run(&input, &p)));
    group.finish();
}// criterion_group!(benches, all_days);
fn day_03(c: &mut Criterion) {
    let mut group = c.benchmark_group("day_3");
    use ::y2022::days::day03::run as current;
    use ::y2022::days::day03::run_next as next;
    let p = Params::new(None, None);
    let input = fs::read("./input/day_03.txt").expect("file!!!");
    let input = input[..].into();
    group.bench_function("day 3 current", |b| b.iter(|| current(&input, &p)));
    group.bench_function("day 3 trying!", |b| b.iter(|| next(&input, &p)));
    group.finish();
}// criterion_group!(benches, all_days);
fn day_04(c: &mut Criterion) {
    let mut group = c.benchmark_group("day_4");
    use ::y2022::days::day04::run as old;
    use ::y2022::days::day04::run_new as new;
    let p = Params::new(None, None);
    let input = fs::read("./input/day_04.txt").expect("file!!!");
    let input = input[..].into();
    group.bench_function("day 4 ", |b| b.iter(|| old(&input, &p)));
    group.bench_function("day 4 done quick ", |b| b.iter(|| new(&input, &p)));
    group.finish();
}// criterion_group!(benches, all_days);
fn day_06(c: &mut Criterion) {
    let mut group = c.benchmark_group("day_4");
    use ::y2022::days::day06::run_old as old;
    use ::y2022::days::day06::run as new;
    let p = Params::new(None, None);
    let input = fs::read("./input/day_06.txt").expect("file!!!");
    let input = input[..].into();
    group.bench_function("day 6 ", |b| b.iter(|| old(&input, &p)));
    group.bench_function("day 6 done quick ", |b| b.iter(|| new(&input, &p)));
    group.finish();
}// criterion_group!(benches, all_days);

criterion_group!(benches, day_06);
criterion_main!(benches);