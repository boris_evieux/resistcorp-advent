use crate::days::day01::run;
use lib::util::{DayInput, Params, run_day_file};

pub mod days;

pub fn main() {
    let result = run_day_file(days::day03::run, 2022, 3);

    println!("{}", result.0);
    println!("{}", result.1);
}


#[cfg(test)]
mod tests{

    use crate::days;
    use lib::util::do_tests;

    #[test]
    pub fn all_solutions(){
        do_tests(2021, 1..=1, &days::all());
    }
}
