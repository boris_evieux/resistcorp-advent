use std::collections::VecDeque;
use regex::Regex;
use lib::make_main;
make_main!(2022, 5);

use lazy_static::lazy_static;
lazy_static! {
    static ref RE: Regex = Regex::new(r"^move (\d+) from (\d+) to (\d+)$").unwrap();
}

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut p2 = 0;
	let mut it = data.linesi();
	let first = it.next().unwrap();
	let num_stacks = (first.len() +1)/4;
	let mut stacks1 = vec![VecDeque::with_capacity(500); num_stacks];
	let mut stacks2 = vec![VecDeque::with_capacity(500); num_stacks];

	read_stack(first, &mut stacks1, &mut stacks2);
	loop{
		if !read_stack(it.next().unwrap(), &mut stacks1, &mut stacks2) {
			break;
		}
	}
	assert_eq!(Some(""), it.next());
	let mut BUFF = VecDeque::new();

	for line in it {
		let caps = RE.captures(line).unwrap();
		let num = caps[1].parse::<usize>().unwrap();
		let start = caps[2].parse::<usize>().unwrap() - 1;
		let end = caps[3].parse::<usize>().unwrap() - 1;
		if start == end {
			continue;
		}

		for _i in 0..num {
			let c = stacks1[start].pop_back().expect("no item to pop");
			stacks1[end].push_back(c);
			let c = stacks2[start].pop_back().expect("no item to pop");
			BUFF.push_front(c);
		}
		stacks2[end].extend(BUFF.drain(..));
	}

	let p1 = stacks1.iter().map(|vd| vd.back().unwrap()).collect();
	let p2 = stacks2.iter().map(|vd| vd.back().unwrap()).collect();
	
	(p1, p2)
}

fn read_stack(line : &str, stacks1 : &mut Vec<VecDeque<char>>, stacks2 : &mut Vec<VecDeque<char>>) -> bool{
	let chars : Vec<char> = line.chars().collect();
	for i in 0..stacks1.len() {
		let ch = chars[(i*4)+1];
		match ch {
			' ' => (),
			c if c.is_ascii_uppercase() => {
				stacks1[i].push_front(c);
				stacks2[i].push_front(c);
			},
			c if c.is_digit(10) => {
				return false;
			},
			_ => panic!(""),
		}
	}
	true
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = "    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2";
		use lib::test_run;
		test_run!(input, "CMZ", "MCD");

	}
}
