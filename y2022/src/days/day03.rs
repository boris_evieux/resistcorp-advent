use lib::make_main;
make_main!(2022, 3);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;
	let mut group = vec![];
	for line in data.linesi() {
		group.push(line);
		let len = line.len()/2;
		let half1 = &line[..len];
		let half2 = &line[len..];

		assert_eq!(half1.len(), half2.len());

		let repeat = half1.chars().find(|c| half2.contains(*c)).unwrap();

		p1 += value(repeat);

		if(group.len()==3){
			p2 += badge(&group);
			group.clear();
		}

	}
	assert_eq!(0, group.len());
	
	(p1.to_string(), p2.to_string())
}
//SAME
pub fn run_next(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;
	let mut group = vec![];
	for line in data.linesi() {
		group.push(line);
		let len = line.len()/2;
		let half1 = &line[..len];
		let half2 = &line[len..];

		let repeat = half1.chars().find(|c| half2.contains(*c)).unwrap();

		p1 += value(repeat);

		if(group.len()==3){
			p2 += badge(&group);
			group.clear();
		}
	}
	
	(p1.to_string(), p2.to_string())
}

fn badge(group : &Vec<&str>) -> u32 {
	let badge = group[0].chars()
		.find(|c| group[1].contains(*c) && group[2].contains(*c));

	value(badge.unwrap())
}

fn value(c : char) -> u32 {
	let a = c as u32;
	if c.is_lowercase(){
		a - 96
	}else{
		a - 64 + 26
	}
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example_day03(){
		let input = "vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw";
		use lib::test_run;
		test_run!(input, 157, 70);
		use lib::test_run_f;
		test_run_f!(super::run_next, input, 157, 70);
	}
	#[test]
	pub fn values(){
		use super::value;
		assert_eq!(1, value('a'));
		assert_eq!(26, value('z'));
		assert_eq!(27, value('A'));
		assert_eq!(52, value('Z'));
	}
}
