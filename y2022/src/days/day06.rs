use std::collections::HashSet;
use lib::make_main;
make_main!(2022, 6);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;
	let data : &[u8] = data.into();

	for i in 4..data.len() {
		if window_different(&data[i-4..i]) {
			p1 = i;
			let i = usize::max(i, 14);
			for j in i..data.len() {
				if window_different(&data[j-14..j]) {
					p2 = j;
					break;
				}
			}
			break;
		}
	}
	
	(p1.to_string(), p2.to_string())
}
pub fn run_old(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;
	let data : &[u8] = data.into();

	for i in 4..data.len() {
		if window_different(&data[i-4..i]) {
			p1 = i;
			break;
		}
	}
	for i in 14..data.len() {
		if window_different(&data[i-14..i]) {
			p2 = i;
			break;
		}
	}
	
	(p1.to_string(), p2.to_string())
}

fn window_different(sub : &[u8]) -> bool {
	let len = sub.len();
	for i in 0..len{
		for j in 0..i {
			if sub[i..=i] == sub[j..=j] {
				return false;
			}
		}
	}
	
	true
}



#[cfg(test)]
mod tests{
	
	use lib::{test_run, test_run_f};
	#[test]
	pub fn example1(){
		let input = "mjqjpqmgbljsphdztnvjfqwrcgsmlb";
		test_run!(input, 7, 19);
		test_run_f!(super::run_old, input, 7, 19);
	}
	#[test]
	pub fn example2(){
		let input = "bvwbjplbgvbhsrlpgdmjqwftvncz";
		test_run!(input, 5, 23);
		test_run_f!(super::run_old, input, 5, 23);
	}
	#[test]
	pub fn example3(){
		let input = "nppdvjthqldpwncqszvftbrmjlhg";
		test_run!(input, 6, 23);
		test_run_f!(super::run_old, input, 6, 23);
	}
	#[test]
	pub fn example4(){
		let input = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg";
		test_run!(input, 10, 29);
		test_run_f!(super::run_old, input, 10, 29);
	}
	#[test]
	pub fn example5(){
		let input = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw";
		test_run!(input, 11, 26);
		test_run_f!(super::run_old, input, 11, 26);
	}
	#[test]
	pub fn win(){
		use super::window_different;
		use std::collections::HashSet;
		assert_eq!(true, window_different("a".as_bytes()));
		assert_eq!(true, window_different("abcd".as_bytes()));
		assert_eq!(false, window_different("accd".as_bytes()));
		assert_eq!(false, window_different("aaaaaaaaaaaaaaaa".as_bytes()));
		assert_eq!(false, window_different("bbcd".as_bytes()));
		assert_eq!(false, window_different("abca".as_bytes()));

	}
}
