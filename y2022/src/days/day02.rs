use lib::make_main;
make_main!(2022, 2);

pub fn run_old(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;
	
	for line in data.linesi() {
		let other = match line[..1].chars().next().unwrap() {
			'A'=>0,
			'B'=>1,
			'C'=>2,
			_ => panic!("")
		};
		let me = match line[2..].chars().next().unwrap() {
			'X'=>0,
			'Y'=>1,
			'Z'=>2,
			_ => panic!("")
		};
		let points1 = match (other, me){
			(0, 1) | (1, 2) | (2, 0) => 6,//won
			(0, 0) | (1, 1) | (2, 2) => 3,//draw
			(0, 2) | (1, 0) | (2, 1) => 0,//lost
			what => panic!("{:?}", what)
		};
		let points2 = me*3;//lose, draw,win
		let me2 = (other + 2 + me)%3;

		p1+= me + 1 + points1;
		p2+= points2 + 1 + me2;
	}


	(p1.to_string(), p2.to_string())
}
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;
	let bytes : &[u8] = data.into();
	let len = bytes.len();
	let num_lines = (len+1) / 4;

	for i in 0..num_lines {
		let opponent = bytes[i*4] as isize - 64; //A = 1
		let me = bytes[i*4+2] as isize - 87;//X = 1
		let result = result(opponent, me) * 3;

		let me_c = bytes[i*4+2] as char;

		p1 += me + result;
		let desired_result = me-1;
		let my_play = (opponent + desired_result + 1 ) % 3 + 1;

		p2 += desired_result * 3 + my_play;
	}
	(p1.to_string(), p2.to_string())
}

fn result(opponent : isize, me : isize) -> isize{
	(me + 4 - opponent) % 3
}

#[cfg(test)]
mod tests{
	use lib::util::Params;
use lib::util::run_day_file;
	use lib::test_run;
	use lib::test_run_f;
	
	#[test]
	pub fn example_day02(){
		let input = "A Y
B X
C Z";
		test_run!(input, 15, 12);
		test_run_f!(super::run_old, input, 15, 12);

	}

	#[test]
	pub fn full_day_2(){
		let a = run_day_file(super::run_old, 2022, 2);
		let b = run_day_file(super::run, 2022, 2);
		assert_eq!(a, b)
	}
	#[test]
	pub fn failing_line(){
		use std::fs;
		let input = fs::read_to_string("./input/day_02.txt").expect("file!!!");
		for line in input.lines() {
			let input = line.into();
			let a = super::run_old(&input, &Params::new(None, None));
			let b = super::run(&input, &Params::new(None, None));
			assert_eq!(a, b, "{line}");

		}
	}

	#[test]
	pub fn self_per_line(){
		use lib::test_run2;
		test_run!("A Y", 8, 4);
		test_run_f!(super::run_old, "A Y", 8, 4);
		test_run2!("B X", 1);
		test_run_f!(super::run_old, "B X", 1, 1);
		test_run2!("C Z", 7);
		test_run_f!(super::run_old, "C Z", 6, 7);
		test_run!("B Z", 9, 9);
		test_run_f!(super::run_old, "B Z", 9, 9);
	}
	#[test]
	pub fn results(){
		use super::result;
		//losses
		assert_eq!(0, result(0, 2));
		assert_eq!(0, result(1, 0));
		assert_eq!(0, result(2, 1));
		//draws
		assert_eq!(1, result(0, 0));
		assert_eq!(1, result(1, 1));
		assert_eq!(1, result(2, 2));
		//wins
		assert_eq!(2, result(0, 1));
		assert_eq!(2, result(1, 2));
		assert_eq!(2, result(2, 0));
	}
}
