use regex::Regex;
use core::ops::RangeBounds;
use std::ops::Range;
use lib::make_main;
make_main!(2022, 4);
use lazy_static::lazy_static;
lazy_static! {
    static ref RE: Regex = Regex::new(r"^(\d+)-(\d+),(\d+)-(\d+)$").unwrap();
}

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;

	for line in data.linesi() {
		let mut sp = line.split(',');
		let mut a = sp.next().map(|v| v.split('-')).unwrap();
		let mut b = sp.next().map(|v| v.split('-')).unwrap();

		let a_s = a.next().unwrap().parse::<i32>().unwrap();
		let a_e = a.next().unwrap().parse::<i32>().unwrap();
		let b_s = b.next().unwrap().parse::<i32>().unwrap();
		let b_e = b.next().unwrap().parse::<i32>().unwrap();

		if total(a_s, a_e, b_s, b_e) {
			p1 += 1
		}
		if partial(a_s, a_e, b_s, b_e) {
			p2 += 1
		}
	}
	
	(p1.to_string(), p2.to_string())
}
pub fn run_new(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;

	for line in data.linesi() {
		let mut sp = RE.captures(line).unwrap();

		let a_s = sp[1].parse::<i32>().unwrap();
		let a_e = sp[2].parse::<i32>().unwrap();
		let b_s = sp[3].parse::<i32>().unwrap();
		let b_e = sp[4].parse::<i32>().unwrap();

		if total(a_s, a_e, b_s, b_e) {
			p1 += 1
		}
		if partial(a_s, a_e, b_s, b_e) {
			p2 += 1
		}
	}
	
	(p1.to_string(), p2.to_string())
}

fn total<T:PartialOrd + Copy>(a_s : T, a_e : T, b_s : T, b_e : T) -> bool {
	let a = a_s..=a_e;
	let b = b_s..=b_e;
	(a.contains(&b_s) && a.contains(&b_e)) || (b.contains(&a_s) && b.contains(&a_e))
}

fn partial<T:PartialOrd + Copy>(a_s : T, a_e : T, b_s : T, b_e : T) -> bool {
	let a = a_s..=a_e;
	let b = b_s..=b_e;
	a.contains(&b_s) || a.contains(&b_e) || b.contains(&a_s) || b.contains(&a_e)
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = "2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8";
		use lib::test_run;
		test_run!(input, 2, 4);
		use lib::test_run_f;
		test_run_f!(super::run_new, input, 2, 4);

	}
}
