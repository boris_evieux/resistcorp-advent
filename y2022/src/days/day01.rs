use lib::make_main;
make_main!(2022, 1);

use lazy_static::lazy_static;
use std::sync::Mutex;

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut max = [0, 0, 0, 0];
	let mut tally = 0u32;

	for line in data.linesi(){
		if line == "" {
			max[0] = tally;
			max.sort();
			tally = 0;
		}else{
			tally += line.parse::<u32>().unwrap();
		}
	}
	max[0] = tally;
	max.sort();

	let p1 = max[3];
	let p2 = max[1] + max[2] + max[3];
	
	(p1.to_string(), p2.to_string())
}

pub fn run_bin_search(data : &DayInput, _p : &Params)->(String, String){
	let mut all = vec![0, 0, 0];
	let mut tally = 0;

	for line in data.linesi(){
		if line == "" {
			compare_insert(&mut all, tally, 3, 0);
			tally = 0;
		}else{
			tally += line.parse::<u32>().unwrap()
		}
	}
	compare_insert(&mut all, tally, 4, 0);

	let p1 = all[0];
	let p2: u32 = p1 + all[1] + all[2];
	
	(p1.to_string(), p2.to_string())
}
fn compare_insert<T : Ord + Copy>(all : &mut Vec<T>, value : T, len : usize, default : T){
	if value > all[0]{
		all.insert(0, value);
	}else if value > all[1]{
		all.insert(1, value);
	}else if value > all[2]{
		all.insert(2, value);
	}
	all.resize(len, default);
}
// fn compare_insert<T : Ord + Copy>(all : &mut Vec<T>, value : T, len : usize, default : T){
// 	let index = all.binary_search_by(|probe| value.cmp(&probe)).unwrap_or_else(|e| e);
// 	all.insert(index, value);
// 	all.resize(len, default);
// }

#[cfg(test)]
mod tests{
const test_case : &'static str = "1000
2000
3000

4000

5000
6000

7000
8000
9000

10000";
	
	#[test]
	pub fn example(){
		use lib::test_run;
		test_run!(test_case, 24000, 45000);
		use lib::test_run_f;
		test_run_f!(super::run_bin_search, test_case, 24000, 45000);
		assert_eq!(super::run_bin_search(&(test_case.into()), &lib::util::Params::new(None, None)), ("24000".to_string(), "45000".to_string()));
	}
	#[test]
	pub fn louis(){
		let order : Vec<&str> = vec![];
		let mut piles2 : Vec<Vec<char>> = vec![Vec::new(); 9];
		//tu remplis en lisant l'input...


		let mut pile_start = order[2].parse::<usize>().unwrap()-1;
		let mut pile_end   = order[3].parse::<usize>().unwrap()-1;
		let to_transfer : usize = order[1].parse().unwrap();
		let to_keep : usize = piles2[pile_start].len() - to_transfer;

		let queue = &piles2[pile_start].split_off(to_keep);
		piles2[pile_end].extend(queue);

	}
}
// #[bench]
// fn name(b: &mut test::Bencher) {
// 	b.iter(|| /* benchmark code */)
// }