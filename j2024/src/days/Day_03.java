package days;

import static org.junit.Assert.*;
import static util.Util.bi;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import util.Util.Result;

public class Day_03 {
  public static void main(String[] args) {
    var input = Paths.get(FILENAME);
    try{
      String in = new String(Files.readAllBytes(input), Charset.forName("UTF-8"));
      var p = new Parser(in);
      var s = p.solve();
      System.out.println(s.a());
      System.out.println(s.b());
    } catch (Exception e) {
      e.printStackTrace();
    }

  }


  static class Parser{
    private final Matcher matcher;
    private boolean hasNextCached;
    private boolean activeP2 = true;
    public Parser(String input){
      matcher = pattern.matcher(input);
    }
    public boolean hasNext(){
      if(!hasNextCached)
        hasNextCached = matcher.find();
      
      return hasNextCached;
    }
    public int nextMul(){
      if(hasNext()){
        hasNextCached = false;
        String matched = matcher.group();
        switch(matched){
          case "do()" :
          activeP2 = true;
          break;
          case "don't()" :
          activeP2 = false;
            break;
          default :
            return Integer.parseInt(matcher.group(3)) * Integer.parseInt(matcher.group(4));
        };

      }
      return 0;
    }
    public Result solve(){
      int sumA = 0;
      int sumB = 0;
      while(hasNext()){
        int nextMul = nextMul();
        sumA += nextMul;
        if(activeP2)
          sumB += nextMul;
      }
      return new Result(sumA, sumB);
    }
    static Pattern pattern = Pattern.compile("(do\\(\\))|(don't\\(\\))|mul\\((\\d{1,3}),(\\d{1,3})\\)");
  }

  static class Mul{

  }
  @Test
  public void testMul() {
    var p = new Parser("mul(2,4)");
    assertEquals(bi(8), p.solve().a());
    
  }
  @Test
  public void testDo() {
    var p = Parser.pattern.matcher("do()");
    assertTrue(p.find());
    p = Parser.pattern.matcher("don't()");
    assertTrue(p.find());
    p = Parser.pattern.matcher("do_not()");
    assertFalse(p.find());
    p = Parser.pattern.matcher("don't()_mul(5,5)");
    assertTrue(p.find());
    assertTrue(p.find());
    
  }
  @Test
  public void testPart1() {
    var p = new Parser(sample);
    assertEquals(bi(161), p.solve().a());
  }
  @Test
  public void testPart2() {
    var p = new Parser(sample2);
    Result solve = p.solve();
    assertEquals(new Result(161, 48), solve);
  }
  static String FILENAME = "days/day_03.txt";
  static String sample = "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))";
  static String sample2 = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";
}
