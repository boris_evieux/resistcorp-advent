package days;

import static org.junit.Assert.assertEquals;
import static util.Util.bi;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntToLongFunction;
import java.util.function.LongPredicate;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import util.Util.Result;

public class Day_01 {
    public static void main(String[] args) {
      var input = Paths.get("./days/day_01.txt");
      try(var  lines = Files.lines(input, Charset.forName("UTF-8"))){
        var answer = solve(lines.toList());
        System.out.println(answer.a());
        System.out.println(answer.b());
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
    
    private static Result solve(List<String> lines) {
      var intsA = new long[lines.size()];
      var intsB = new long[lines.size()];
      int index = 0;
      for(var line : lines){
        String[] parts = line.split("\\s+");
        int a = Integer.parseInt(parts[0]);
        int b = Integer.parseInt(parts[1]);
        intsA[index] = a;
        intsB[index] = b;
        index++;
      }
      Arrays.sort(intsA);
      Arrays.sort(intsB);
      int sum = 0;
      int sims = 0;
      for (int i = 0; i < intsA.length; i++) {
        long left = intsA[i];
        long right = intsB[i];
        sum += Math.abs(left - right);


        IntToLongFunction f = j -> intsB[j];
        LongPredicate p = x -> x == left;
        long count = IntStream.range(0, intsA.length)
        .mapToLong(f)
        .filter(p).count();
        sims+= count * left;
        // System.out.println(String.format("%d appears %d times : adding %d, sum is %d", left, count, count * left, sims));
        
      }
      
      return new Result(sum, sims);
    }

    @Test
    public void part1(){
      List<String> testList = test_input.lines().toList();
      Result testResult = solve(testList);
      assertEquals(bi(11), testResult.a());
    }
    @Test
    public void part2(){
      List<String> testList = test_input.lines().toList();
      Result testResult = solve(testList);
      assertEquals(bi(31), testResult.b());
    }

    final static String test_input = """
      3   4
      4   3
      2   5
      1   3
      3   9
      3   3
      """;
}