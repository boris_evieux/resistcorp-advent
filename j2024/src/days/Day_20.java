package days;

import util.Day;
import util.Util.Point;
import util.Util.Result;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.Util.*;
import static util.Util.Direction.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

public class Day_20 extends Day {
	private int m_minimumCheat = 100;//to tune for unit tests
	private int m_normalRoute = Integer.MAX_VALUE;

	public static void main(String[] args) {
		var day = new Day_20();
		day.run();
	}

	@Override
	public Result solve(String input) {
		var map = readMap(input, null);
		Point start = search(map, 'S');
		Point end = search(map, 'E');
		assertNotNull(start); assertNotNull(end);

		calculateDistances(map, start, end);
		
		m_normalRoute = get(m_distances, start, Integer.MAX_VALUE);

		List<Cheat> cheats = findCheats1(map);
		var a = countCheats(cheats);
		cheats = findCheats2(map);
		var b = countCheats(cheats);
		
		return new Result(a, b);
	}

	private long countCheats(List<Cheat> cheats) {
		var a = cheats.stream().filter(c -> c.gains >= m_minimumCheat).count();
		// var groups = cheats.stream().collect(Collectors.groupingBy(Cheat::gains));
		// groups.entrySet().stream().sorted((eA, eB) -> Integer.compare(eA.getKey(), eB.getKey())).forEach(e -> {
		// 	var k = e.getKey();
		// 	var v = e.getValue();
		// 	logln("There are %d cheats that save %d picoseconds.", v.size(), k);
		// });
		return a;
	}
	private List<Cheat> findCheats2(char[][] map) {
		List<Candidate> ptsWithin20 = getPattern20();
		return findCheats(map, ptsWithin20);
	}

	private List<Candidate> getPattern20() {
		Set<Candidate> ptsWithin20 = new HashSet<>();
		Direction[] vert = {UP, DOWN};
		Direction[] horz = {LEFT, RIGHT};

		for(int a = 0; a <= 20; a++){
			for(var dirV : vert){
				Point vDisp = dirV.dir.times(a);
				ptsWithin20.add(new Candidate(vDisp, a));
				for(var dirH : horz){
					for(int b = 1; b <= 20 - a; b++){
						Point hDisp = dirH.dir.times(b);
						ptsWithin20.add(new Candidate(vDisp.plus(hDisp), a+b));
					}
				}
			}
		}
		return ptsWithin20.stream().toList();
	}
	private List<Cheat> findCheats1(char[][] map) {
		List<Candidate> pattern = List.of(
			new Candidate(DOWN.dir.times(2), 2),
			new Candidate(UP.dir.times(2), 2),
			new Candidate(LEFT.dir.times(2), 2),
			new Candidate(RIGHT.dir.times(2), 2)
		);
		return findCheats(map, pattern);
	}

	private List<Cheat> findCheats(char[][] map, List<Candidate> pattern) {
		List<Cheat> found = new ArrayList<>();
		for(int row = 0; row < map.length; row++){
			var line = map[row];
			for(int col = 0; col < line.length; col++){
				var pos = new Point(row, col);
				if(!get(m_visiteds, pos, false)) continue;
				var distanceToEnd = get(m_distances, pos, Integer.MAX_VALUE);
				for(var candidate : pattern){
					var afterCheat = pos.plus(candidate.movement());
					if(!get(m_visiteds, afterCheat, false)) continue;
					var newDist = get(m_distances, afterCheat, Integer.MAX_VALUE);
					int gains = candidate.dist;
					if(newDist < distanceToEnd + gains){
						found.add(new Cheat(pos, afterCheat, distanceToEnd - newDist - gains));
					}
				}
			}
		}
		return found;
	}
	private record Cheat(Point from, Point to, int gains){}
	private record Candidate(Point movement, int dist){}

	private int[][] calculateDistances(char[][] map, Point start, Point end) {
		m_distances = new int[map.length][map[0].length];
		m_visiteds = new boolean[map.length][map[0].length];

		for(var line : m_distances) Arrays.fill(line, Integer.MAX_VALUE);
		Deque<Point> toTest = new ArrayDeque<>();
		set(m_distances, end, 0);
		toTest.add(end);

		//calculate all distances
		while(!toTest.isEmpty()){
			var current = toTest.pop();
			var currentD = get(m_distances, current, Integer.MAX_VALUE);
			set(m_visiteds, current, true);
			for(var dir : Direction.values()){
				var next = current.plus(dir);
				if(get(m_visiteds, next, true) || getChar(map, next, '#') == '#')
					continue;

				toTest.add(next);
				// if(next.equals(start))
				// 	logln("found start in %d", currentD+1);
				set(m_distances, next, currentD+1);
			}
		}
		// logln("got whole map distances");
		// StringBuilder sb = new StringBuilder();
		// for(int row = 0; row < map.length; row++){
		// 	var line = map[row];
		// 	var dist = m_distances[row];
		// 	sb.append(line);
		// 	sb.append("   >>  ");
		// 	for(int col = 0; col < line.length; col++){
		// 		Point pos = new Point(row, col);
		// 		char ch = getChar(map, pos, '#');
		// 		if(ch == '.'){
		// 			sb.append(get(m_distances, pos, 0) % 10);
		// 		}else{
		// 			sb.append(ch);
		// 		}
		// 	}
		// 	sb.append('\n');
		// }
		// logln("%s", sb);
		return m_distances;
	}

	public static Point search(char[][] map, char toFind){
		for(int row = 0; row < map.length; row++){
			var line = map[row];
			for( int col = 0; col < line.length; col ++){
				if(line[col] == toFind)
					return new Point(row, col);
			}
		}
		return null;
	}
	@Test
	public void testRoute() {
		solve(testInput);
		assertEquals(84, m_normalRoute);
	}
	@Test
	public void test1() {
		m_minimumCheat = 20;
		assertEquals(bi(5), solve(testInput).a());
	}
	@Test
	public void test2() {
		m_minimumCheat = 50;
		assertEquals(bi(285), solve(testInput).b());
	}
	@Test
	public void testPattern() {
		var pattern = getPattern20();
		boolean[][] map = new boolean[49][49];
		Point center = new Point(25, 25);
		for(var cd : pattern){
			set(map, center.plus(cd.movement()), true);
		}
		StringBuilder sb = new StringBuilder();
		for(var line : map){
			for(var isSet : line){
				sb.append(isSet ? '#': '.');
			}
			logln("%s", sb);
			sb.setLength(0);
		}
		assertEquals(841, pattern.size());
	}
	public static final String testInput = """
###############
#...#...#.....#
#.#.#.#.#.###.#
#S#...#.#.#...#
#######.#.#.###
#######.#.#...#
#######.#.###.#
###..E#...#...#
###.#######.###
#...###...#...#
#.#####.#.###.#
#.#...#.#.#...#
#.#.#.#.#.#.###
#...#...#...###
###############
	""";
	private int[][] m_distances;
	private boolean[][] m_visiteds;
}