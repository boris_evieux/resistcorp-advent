package days;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.Util.*;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

import org.junit.jupiter.api.Test;

import util.Day;
import util.Util;
import util.Util.Result;

public class Day_09 extends Day {
	public static void main(String[] args) {
		var day = new Day_09();
		day.run();
	}

	@Override
	public Result solve(String input) {
		List<Range> originalRanges = makeDiskMap(input);
		long checksumA = part1(originalRanges);

		//10070969252646 too high
		//6600986117342 too high
		//6373055193464 ?
		long checksumB = part2(originalRanges);

		return new Result(checksumA, checksumB);
	}

	private List<Range> makeDiskMap(String input) {
		List<Range> originalRanges = new ArrayList<>();
		var chars = Util.chars(input.trim());
		int currentFileID = 0;
		int currentRangeStart = 0;
		for (int i = 0; i < chars.length; i++) {
			char c = chars[i];
			var digit = Util.digit(c);
			boolean bIsFile = i%2 == 0;
			int fileID = bIsFile ? currentFileID++ : -1;
			originalRanges.add(new Range(currentRangeStart, digit, !bIsFile, fileID, originalRanges.size()));
			currentRangeStart += digit;
		}
		return Collections.unmodifiableList(originalRanges);
	}

	private long part2(List<Range> originalRanges) {
		List<Range> compactList = new ArrayList<>(originalRanges);

		logln("before compactation");
		printMap(compactList);

		Map<Integer, Range> filesByID = new HashMap<>();
		originalRanges.stream().filter(r -> !r.isEmpty && r.fileID != -1).forEach(r -> filesByID.put(r.fileID, r));
		List<Integer> idsDescendingOrder = new ArrayList<>(filesByID.keySet());
		idsDescendingOrder.sort(Comparator.reverseOrder());

		for (int fileId : idsDescendingOrder) {
			Range r = filesByID.get(fileId);
			if(tryCompact(compactList, r, fileId) != -1){
				// printMap(compactList);
			}
		}


		logln("after compactation");
		printMap(compactList);
		return checksum(compactList);
	}
	private int tryCompact(List<Range> compactList, Range rangeToMove, int id) {
		int end = rangeToMove.start;
		int block = 0;
		int idx = 0;
		while (block < end && idx < compactList.size()) {
			var candidate = compactList.get(idx);
			block += candidate.length;
			if (!candidate.isEmpty){
				idx++;
				continue;
			}
			
			if(candidate.length >= rangeToMove.length){
				// found a block that can fit the range
				// split the block
				Range newBlock = new Range(candidate.start, rangeToMove.length, false, id, idx);
				Range remainingBlock = new Range(newBlock.start + newBlock.length, candidate.length - newBlock.length, true, -1, idx+1);
				compactList.set(idx, newBlock);
				if(remainingBlock.length > 0)
					compactList.add(idx + 1, remainingBlock);
				
				int removeIndex = compactList.indexOf(rangeToMove);
				compactList.set(removeIndex, new Range(rangeToMove.start, rangeToMove.length, true, -1, -1));

				return block;
			}
			idx++;

			
		}
		return -1;
	}

	private long part1(List<Range> originalRanges) {
		ArrayDeque<Range> ranges = new ArrayDeque<>(originalRanges);
		List<Range> compacList = new ArrayList<>();
		int size = 0;
		Range currentLast = null;

		while(!(ranges.isEmpty() && currentLast == null)){
			Range r = ranges.pollFirst();
			if(r == null)
				r = new Range(size, currentLast.length, true, -1, -1);
				
			if(!r.isEmpty){
				compacList.add(r);
				size += r.length;
			}else{
				int toFill = r.length;
				do{
					if(currentLast == null || currentLast.isEmpty){
						currentLast = ranges.pollLast();
						if(currentLast == null){
							break;
						}
					}else{
						if(currentLast.length > toFill){
							compacList.add(new Range(size, toFill, false, currentLast.fileID, compacList.size()));
							currentLast = new Range(currentLast.start, currentLast.length - toFill, false, currentLast.fileID, currentLast.id);
							toFill = 0;
							size += toFill;
						}else{
							compacList.add(new Range(size, currentLast.length, false, currentLast.fileID, compacList.size()));
							toFill -= currentLast.length;
							size += currentLast.length;
							currentLast = null;
						}

					}
				}while(toFill > 0 );
			}
		}
		long checksum = checksum(compacList);
		return checksum;
	}

	private void printMap(Collection<Range> ranges) {
		int maxID = ranges.stream().mapToInt(r -> r.fileID).max().getAsInt();
		StringBuilder[] sb = IntStream.range(0, String.valueOf(maxID).length()).mapToObj(StringBuilder::new).toArray(StringBuilder[]::new);
		
		for (Range range : ranges) {
			range.print(sb);
		}
		for (StringBuilder s : sb) {
			logln("%s", s.toString());
		}
	}
	private long checksum(List<Range> compacList) {
		int blockID = 0;
		long sum = 0;
		// StringBuilder sb = new StringBuilder();
		for (Range range : compacList) {
			if(range.isEmpty){
				blockID += range.length;
				continue;
			}
			for (int i = 0; i < range.length; i++) {
				// sb.append(range.fileID);
				sum += blockID * range.fileID;
				blockID++;
			}
		}
		// logln("compacted : %s", sb.toString());
		return sum;
	}
		
	record Range(int start, int length, boolean isEmpty, int fileID, int id) {
		public void print(StringBuilder sb[]) {
			String dgt = isEmpty ? ".".repeat(sb.length) : String.valueOf(this.fileID);
			dgt = String.format("%1$" + sb.length + "s", dgt).replace(' ', '0');
			for (int j = 0; j < sb.length; j++) {
				char ch = dgt.charAt(j);
				for (int i = 0; i < length; i++){
					sb[j].append(ch);
				}
			}
		}
	}

	private int getFileIdx(List<Range> diskMap, int fileID) {
		for (int i = 0; i < diskMap.size(); i++) {
			if(diskMap.get(i).fileID == fileID)
				return i;
		}
		return -1;
	}

	@Test
	public void testA() {
			assertEquals(bi(1928), solve(testInput).a());
	}
	@Test
	public void testB() {
			assertEquals(bi(2858), solve(testInput).b());
	}

	// @Test
	// public void testMyError() {
	// 	String input = readInput();
	// 	List<Range> diskMap = makeDiskMap(input);
	// 	var compact = new ArrayList<>(diskMap);
	// 	int maxID = diskMap.stream().mapToInt(r -> r.fileID).max().getAsInt();
	// 	assertEquals(9999, maxID);
	// 	for(int fileID = 9999; fileID >= 9988; fileID--){
	// 		int fileIdx = getFileIdx(diskMap, fileID);
	// 		assertNotEquals(-1, fileIdx);
	// 		Range file = diskMap.get(fileIdx);
	// 		if(!file.isEmpty){
	// 			assertEquals(fileID, file.fileID);
	// 			tryCompact(compact, diskMap.get(fileID), fileID);
	// 		}
	// 	}
	// 	int fileIdx = getFileIdx(diskMap, 9988);
	// 		assertNotEquals(-1, fileIdx);
	// 		Range file = diskMap.get(fileIdx);
	// 		assertFalse(file.isEmpty);
	// 		int newPosition = tryCompact(compact, file, 9988);
	// 		assertNotEquals(-1, newPosition);
	// 		assertEquals(19, newPosition);
	// }


	private static final String testInput = "2333133121414131402";

}