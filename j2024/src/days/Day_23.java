package days;

import util.Day;

import org.junit.jupiter.api.Test;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.*;
import static util.Util.*;


public class Day_23 extends Day {
	public static void main(String[] args) {
		var day = new Day_23();
		day.run();
	}

	@Override
	public Result solve(String input) {
		Set<String> computers = new HashSet<>();
		Set<Link> links = new HashSet<>();
		input.lines().forEach(line -> {
			var link = Link.make(line);
			links.add(link);
			computers.add(link.a());
			computers.add(link.b());
		});
		logln("there are %d computers", computers.size());

		var threeNets = partA(new ArrayList<>(computers), links);
		// logln("found %d nets of 3 interconnected comps", threeNets.size());
		// logln("filtering those that contain t", threeNets.size());
		var a = threeNets.stream()
			// .peek(st -> logln(st.toString()))
			.filter(st -> st.anyStart('t'))
			// .peek(st -> logln(" > yep"))
			.count();

		var biggest = partB(computers, links);
		logln("the biggest subNet is %d comps", biggest.size());
		logln(" pass : %s", biggest.passcode());
		return new Result(a, biggest.size());
	}

	public SubNet partB(Collection<String> computers, Set<Link> links){
		SubNet biggest = new SubNet(Set.of());
		SubNet scratch = new SubNet();
		for(String root : computers){
			scratch.clear();
			scratch.grow(root);
			for(String other : computers){
				if(root.equals(other))
					continue;
				if(scratch.isCompleteWith(other, links))
					scratch.grow(other);
			}
			if(biggest.size() < scratch.size())
				biggest.replaceWith(scratch);
		}

		return biggest;
	}

	public List<SubNet> partA(List<String> computers, Set<Link> links){
		List<SubNet> nets = new ArrayList<>();
		for(int i = 0; i < computers.size(); i++){
			var a = computers.get(i);
			for(int j = 0; j < i; j++){
				var b = computers.get(j);
				if(!links.contains(Link.make(a, b)))
					continue;
				for(int k = 0; k < j; k++){
					var c = computers.get(k);
					if( links.contains(Link.make(a, c)) 
						&& links.contains(Link.make(b, c)))
							nets.add(new SubNet(List.of(a, b, c)));
				}
			}
		}
		return nets;
	}

	private static record Link(String a, String b){

		static Link make(String line){
			var a = line.substring(0,2);
			var b = line.substring(3,5);
			return make(a, b);
		}

		private static Link make(String a, String b) {
			if(a.compareTo(b) < 0)
				return new Link(a, b);
			return new Link(b, a);
		}
	}

	private static class SubNet{
		private final Set<String> comps;
		public SubNet(Collection<String> from){
			comps = new HashSet<>(from);
		}
		public SubNet(){
			comps = new HashSet<>();
		}
		@Override
		public String toString(){
			return String.join("-", comps);
		}
		public String passcode(){
			return comps.stream().sorted().collect(Collectors.joining(","));
		}

		public int size(){ return comps.size();}

		public boolean anyStart(char start){
			return comps.stream().anyMatch(s -> s.charAt(0) == start);
		}

		public boolean isCompleteWith(String computer, Set<Link> links){
			for(String mine : comps){
				if(!links.contains(Link.make(computer, mine)))
					return false;
			}
			return true;
		}
		public void grow(String c){
			comps.add(c);
		}
		public void clear(){
			comps.clear();
		}
		public void replaceWith(SubNet other){
			clear();
			comps.addAll(other.comps);
		}
	}

	@Test
	public void test() {
		assertEquals(new Result(7, 4), solve(testInput));
	}
	public static final String testInput = """
kh-tc
qp-kh
de-cg
ka-co
yn-aq
qp-ub
cg-tb
vc-aq
tb-ka
wh-tc
yn-cg
kh-ub
ta-co
de-co
tc-td
tb-wq
wh-td
ta-ka
td-qp
aq-cg
wq-ub
ub-vc
de-ta
wq-aq
wq-vc
wh-yn
ka-de
kh-ta
co-tc
wh-qp
tb-vc
td-yn
	""";
}