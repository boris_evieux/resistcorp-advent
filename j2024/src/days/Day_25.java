package days;

import util.Day;
import java.util.*;
import java.util.stream.*;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static util.Util.*;

public class Day_25 extends Day {
	public static void main(String[] args) {
		var day = new Day_25();
		day.run();
	}

	static final char[] KEY_TOP = ".....".toCharArray();
	@Override
	public Result solve(String input) {
		var parts = input.split("\\n\\n");

		List<int[]> locks = new ArrayList<>();
		List<int[]> keys = new ArrayList<>();

		for(var part : parts){
			var data = readMap(part, (a, b)->{});
			boolean isKey = Arrays.equals(KEY_TOP, data[0]);
			var list = isKey ? keys : locks;
			int[] ints = toInts(data, isKey);
			list.add(ints);
			logln("%s : %s", isKey ? "key" : "lock", IntStream.of(ints).mapToObj(String::valueOf).collect(Collectors.joining(",")));
		}

		var a = 0;

		for(var lock : locks)
			for(var key : keys)
				if(fit(key, lock))
					a++;
			

		return new Result(a, 0);
	}

	public boolean fit(int[] key, int[] lock){
		for(int i = 0; i < 5; i++)
			if(key[i] > lock[i])
				return false;
		return true;
	}

	public int[] toInts(char[][] map, boolean isKey){
		var comp = isKey ? '#' : '.';
		int[] ret = {0, 0, 0, 0, 0};
		for(int row = 5; row >= 1; row--){
			for(int col = 0; col < 5; col++){
				if(getChar(map, row, col, '?') == comp)
					ret[col]++;
			}
		}
		return ret;
	}

	@Test
	public void test() {
		assertEquals(bi(3), solve(testInput).a());
	}
	public static final String testInput = """
#####
.####
.####
.####
.#.#.
.#...
.....

#####
##.##
.#.##
...##
...#.
...#.
.....

.....
#....
#....
#...#
#.#.#
#.###
#####

.....
.....
#.#..
###..
###.#
###.#
#####

.....
.....
.....
#....
#.#..
#.#.#
#####
	""";
}