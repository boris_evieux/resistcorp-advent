package days;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static util.Util.*;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentLinkedDeque;
import java.util.stream.IntStream;
import java.util.stream.LongStream;
import java.util.stream.Stream;

import org.junit.Test;

import util.Day;
import util.Util.Result;

public class Day_11 extends Day {
	static final int cores = Runtime.getRuntime().availableProcessors();
	private static final ConcurrentLinkedDeque<SwappingBuffers> s_pool = new ConcurrentLinkedDeque<SwappingBuffers>();

	
	public static void main(String[] args) {
		//not 16060418697
		var day = new Day_11();
		day.run();
	}
	private Map<BigInteger, Computation> m_cache = new HashMap<>();
	public Computation of(BigInteger stone){
		return m_cache.computeIfAbsent(stone, Computation::new);
	}
	public Computation of(int stone){
		return m_cache.computeIfAbsent(bi(stone), Computation::new);
	}

	class Computation {
		public final BigInteger root;
		public final List<BigInteger> steps;
		public Computation nextLeft;
		public Computation nextRight;
		
		public Computation(BigInteger stone){
			this.root = stone;
			this.steps = new ArrayList<>();
		}

		public BigInteger sizeAfter(int i) {
			return sizeAfter(i, 0);
		}

		public BigInteger sizeAfter(int num, int indent) {
			if(num == 0) return BigInteger.ONE;
			if(steps.size() < num){
				// logln(indent, "Computing for %d at %d", root, num);
				if(nextLeft == null){
					if(root == BigInteger.ZERO){
						nextLeft = of(BigInteger.ONE);
					} else if(numDigits(root) % 2 == 0){
						var split = bi(pow10(numDigits(root) / 2));
						BigInteger left = root.divide(split);
						nextLeft = of(left);
						nextRight = of(root.subtract(left.multiply(split)));
						
					} else {
						nextLeft = of(root.multiply(bi(2024)));
					}
				}
				List<BigInteger> newSteps = new ArrayList<>(num);
				nextLeft.sizeAfter(num-1, indent + 1);//precompute
				if(nextRight != null){
					// logln(indent, "results are %d (%s) and %d (%s)", nextLeft.root, nextLeft.steps, nextRight.root, nextRight.steps);
					nextRight.sizeAfter(num-1, indent + 1);//precompute
					for(int j = 0; j < num; j++){
						newSteps.add(nextLeft.sizeAfter(j).add(nextRight.sizeAfter(j, indent + 1)));
					}
				}else{
					// logln(indent, "result is %d (%s)", nextLeft.root, nextLeft.steps);
					for(int j = 0; j < num; j++){
						newSteps.add(nextLeft.sizeAfter(j, indent + 1));
					}
				}
				// logln(indent, " > %d steps : %s", num, newSteps);
				// for(int j = 1; j < num; j++){
				// 	if(newSteps.get(j).compareTo(newSteps.get(j-1)) < 0){
				// 		// logln( " > WHATTTT!!!! %d < %d", newSteps.get(j), newSteps.get(j-1));
				// 		// logln( "");

				// 	}
				// }
				steps.clear();
				steps.addAll(newSteps);
			}
			return steps.get(num-1);
		}
	}
	
	@Override
	public Result solve(String input) {
		var stones = read(input);
		
		int iterA = 25;
		int iterB = 75;
		return solve(stones, iterA, iterB);
	}
	
	private Result solve(List<Long> stones, int iterA, int iterB) {

		List<Computation> computations = stones.stream().map(bi -> m_cache.computeIfAbsent(bi(bi), Computation::new)).toList();

		BigInteger sumA = BigInteger.ZERO;
		BigInteger sumB = BigInteger.ZERO;
		for(var computation : computations){
			sumB = sumB.add(computation.sizeAfter(iterB));
			sumA = sumA.add(computation.sizeAfter(iterA));
		}
		// SwappingBuffers buffers = new SwappingBuffers();
		// buffers.init(stones);
		
		// return blinkDivideAndConquer(iterA, iterB, buffers, SwappingBuffers.SPLIT_AT);
		return new Result(sumA, sumB);
	}
	
	public static SwappingBuffers takeFromPool() {
		var v = s_pool.pollFirst();
		if(v != null)
		return v;
		return new SwappingBuffers();
	}
	public static void returnToPool(SwappingBuffers buffers) {
		s_pool.addLast(buffers);
	}
	
	private static Result blinkDivideAndConquer(int iterA, int iterB, SwappingBuffers buffers, int splitThreshold) {
		Result accumulator = new Result(0, 0);
		
		int i;
		for(i = 0; i < iterB; i++){
			
			buffers.blinkOnce();
			buffers.swap();
			
			if(buffers.length() > splitThreshold){
				
				int remainingA = iterA-i;
				int remainingB = iterB-i;
				int maxSize = buffers.length() / 128;
				
				var add = IntStream.range(0, 128).parallel().mapToObj( val -> {
					
					var split = buffers.split(maxSize);
					var result = blinkDivideAndConquer(remainingA, remainingB, split, splitThreshold);
					returnToPool(split);
					return result;
				}).reduce(new Result(0, 0), (acc, result) -> {
					return acc.add(result);
				});
				
				if(i == iterA - 1){
					accumulator = accumulator.add(add);
				}else{
					accumulator = accumulator.add(new Result(BigInteger.ZERO, add.b()));
				}
			}
			
			if(i == iterA - 1)
			accumulator = accumulator.add(new Result(buffers.length(), 0));
			
		}
		assertEquals(iterB, i);
		return accumulator.add(new Result(0, buffers.length()));
	}
	
	private static List<Long> read(String input) {
		return Stream.of(input.trim().split(" ")).map(Long::parseLong).toList();
	}
	
	public static int numDigits(BigInteger number){
		if(number.compareTo(BigInteger.ONE) < 0)
			throw new IllegalArgumentException("Number must be positive");
		return number.toString().length();
	}
	
	public static int pow10(int n){
		return (int) Math.pow(10, n);
	}
	
	private static List<Long> blink(List<Long> stones, int times) {
		var buffers = new SwappingBuffers();
		buffers.init(stones);
		for(int iteration = 0; iteration < times; iteration++){
			buffers.blinkOnce();
			buffers.swap();
		}
		return buffers.currentList();
	}
	
	private List<Long> blink(List<Long> stones) {
		return blink(stones, 1);
	}
	
	private static class SwappingBuffers {
		public static final int BUFFERSIZE = 2_000_000;
		public static final int SPLIT_AT = 1_000_000;// safe but costly
		
		long[] a;
		long[] b;
		int max;
		int index;
		public SwappingBuffers() {
			a = new long[BUFFERSIZE];
			b = new long[BUFFERSIZE];
		}
		public void swap() {
			long[] temp = a;
			a = b;
			b = temp;
			index = 0;
		}
		public void init(Iterable<Long> stone) {
			index = 0;
			max = 0;
			for(var s : stone){
				a[max++] = s;
			}
		}
		public int length() {
			return max;
		}
		public void init(LongStream stones) {
			index = 0;
			max = 0;
			stones.forEach(stone -> a[max++] = stone);
		}
		public List<Long> currentList() {
			return currentStream().boxed().toList();
		}
		private LongStream currentStream() {
			return LongStream.of(a).limit(max);
		}
		synchronized public void blinkOnce() {
			for(int i = 0; i < max; i++){
				var stone = a[i];
				if(stone == 0){
					b[index++] = 1l;
				} else if(numDigits(bi(stone)) % 2 == 0){
					int split = pow10(numDigits(bi(stone)) / 2);
					b[index++] = stone / split;
					b[index++] = stone - (stone / split) * split;
					
				} else {
					b[index++] = stone * 2024;
				}
			}
			max = index;
		}
		public synchronized SwappingBuffers split(int len) {
			int toSplit = max - len;
			var split = takeFromPool();
			
			var rigthHalf = IntStream.range(toSplit, max).mapToLong(i -> a[i]);//skip first half
			split.init(rigthHalf);
			max = toSplit;
			return split;
		}
	}
	
	@Test
	public void testLog10() {
		assertEquals(1, numDigits(bi(1)));
		assertEquals(2, numDigits(bi(10)));
		assertEquals(2, numDigits(bi(99)));
		assertEquals(3, numDigits(bi(100)));
		assertEquals(4, numDigits(bi(1000)));
		assertEquals(5, numDigits(bi(10000)));
	}
	@Test
	public void testPow10() {
		assertEquals(1, pow10(0));
		assertEquals(10, pow10(1));
		assertEquals(100, pow10(2));
		assertEquals(1000, pow10(3));
		assertEquals(10000, pow10(4));
	}
	
	@Test
	public void testPart1() {
		var input = "125 17";
		var result = solve(read(input), 25, 25);
		assertNotNull(result);
		assertEquals(BigInteger.valueOf(55312), result.a());
	}
	@Test
	public void testPart1Mine() {
		var input = readInput();
		var result = solve(read(input), 25, 30);
		assertNotNull(result);
		assertEquals(BigInteger.valueOf(198075), result.a());
	}
	@Test
	public void testBlink(){
		var input = "0 1 10 99 999";
		var read = read(input);
		assertIterableEquals(Stream.of(0l, 1l, 10l, 99l, 999l).toList(), read);
		var after = blink(read);
		assertIterableEquals(Stream.of(1l, 2024l, 1l, 0l, 9l, 9l, 2021976l).toList(), after);
	}
	@Test
	public void test6Blinks(){
		var read = read("125 17");
		var after = blink(read, 6);
		assertIterableEquals(Stream.of(2097446912l, 14168l, 4048l, 2l, 0l, 2l, 4l, 40l, 48l, 2024l, 40l, 48l, 80l, 96l, 2l, 8l, 6l, 7l, 6l, 0l, 3l, 2l).toList(), after);
	}
	@Test
	public void test6BlinksMethod3(){
		var after = solve(List.of(125l, 17l), 3, 6);
		assertEquals(new Result(5, 22), after);
	}
	@Test
	public void test6BlinksMethod2(){
		SwappingBuffers buffers = new SwappingBuffers();
		buffers.init(read("125 17"));
		var after = blinkDivideAndConquer(3, 6, buffers, 3);
		assertEquals(new Result(5, 22), after);
	}
	@Test
	public void test6BlinkSplit(){
		var read = read("125 17");
		var buffers = new SwappingBuffers();
		buffers.init(read);
		var comparison = blinkDivideAndConquer(15, 20, buffers, SwappingBuffers.SPLIT_AT);
		buffers.init(read);
		var result = blinkDivideAndConquer(15, 20, buffers, 10000);
		assertEquals(comparison, result);
	}
	@Test
	public void testSplit(){
		var read = read("2097446912 14168 4048 2 0 2 4 40 48 2024 40 48 80 96 2 8 6 7 6 0 3 2");
		var buffers = new SwappingBuffers();
		buffers.init(read);
		assertEquals(22, buffers.length());
		var split = buffers.split(11);
		assertEquals(11, buffers.length());
		assertEquals(11, split.length());
	}
	@Test
	public void testComputations(){
		var c = of(17);
		assertEquals(bi(1), c.sizeAfter(0));
		assertEquals(bi(2), c.sizeAfter(1));//1 7
		assertEquals(bi(2), c.sizeAfter(2));//2024 14168
		assertEquals(bi(3), c.sizeAfter(3));//20 24 28676032
		assertEquals(bi(6), c.sizeAfter(4));//2 0 2 4 2867 6032
	}
}