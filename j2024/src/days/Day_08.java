package days;

import static org.junit.Assert.assertEquals;
import static util.Util.bi;
import static util.Util.isValid;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;

import util.Day;
import util.Util.Point;
import util.Util.Result;

public class Day_08 extends Day {
	public static void main(String[] args) {
		var day = new Day_08();
		day.run();
	}

	@Override
	public Result solve(String input) {
		String[] lines = input.lines().toArray(String[]::new);
    char[][] map = new char[lines.length][];
		Map<Character, List<Point>> antennas = new HashMap<>();

    for (int row = 0; row < lines.length; row++) {
      var line = lines[row];
      map[row] = line.toCharArray();
      for(int col = 0; col < line.length(); col++){
				Point pt = new Point(row, col);
        char c = line.charAt(col);
				if(c != '.') {
					antennas.computeIfAbsent(c, ArrayList::new).add(pt);
				}
      }
    }
		Set<Point> p1 = new HashSet<>();
		Set<Point> p2 = new HashSet<>();
		// for each antenna type, find the 2 antinodes
		for (var entry : antennas.entrySet()) {
			var ant = entry.getValue();
			for(int i = 0; i < ant.size(); i++) {
				for(int j = i + 1; j < ant.size(); j++) {
					Point a = ant.get(i);
					Point b = ant.get(j);

					Point v = a.minus(b); // from b to a
					Point nodeA = a.plus(v);
					if(isValid(map, nodeA)){
						p1.add(nodeA);
					}
					Point nodeB = b.minus(v);
					if(isValid(map, nodeB))
						p1.add(nodeB);

					//P2
					//TODO : simplify v ?
					p2.add(a);
					p2.add(b);
					Point c = nodeA;
					while(isValid(map, c)){
						p2.add(c);
						c = c.plus(v);
					}
					c = nodeB;
					while(isValid(map, c)){
						p2.add(c);
						c = c.minus(v);
					}
				}
			}
		}

		return new Result(p1.size(), p2.size());
	}

	@Test
	public void testPart1() {
		var result = solve(testInput);
		assertEquals(bi(14), result.a());
	}
	@Test
	public void testPart2() {
		var result = solve(testInput);
		assertEquals(bi(34), result.b());
	}

private static String testInput = """
............
........0...
.....0......
.......0....
....0.......
......A.....
............
............
........A...
.........A..
............
............
	""";
}
