package days;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.Util.bi;
import static util.Util.longs;

import org.junit.jupiter.api.Test;

import util.Day;
import util.Util.Result;

public class Day_07 extends Day {
  public static void main(String[] args) {
    var day = new Day_07();
    day.run();
  }

  public Result solve(String input) {
    long partA = 0;
    long partB = 0;
    String[] lines = input.lines().toArray(String[]::new);
    for (String line : lines) {
      String[] parts = line.split(": ");
      long res = Long.parseLong(parts[0]);
      long[] range = longs(parts[1], " ");
      if(possible(res, range, 0, false)) {
        partA += res;
      }
      if(possible(res, range, 0, true)) {
        partB += res;
      }
    }
    return new Result(partA, partB);
  } 

  private boolean possible(long res, long[] range, int idx, boolean concat) {
    long item = range[idx];
    if(idx == range.length-1) {
      return res == item;
    }
    
    var next = range[idx+1];
    range[idx+1] = next + item;
    var plus = possible(res, range, idx+1, concat);
    range[idx+1] = next * item;
    var times = possible(res, range, idx+1, concat);
    range[idx+1] = conc(item, next);
    var conc = concat && possible(res, range, idx+1, concat);
    range[idx+1] = next;
    return plus || times || conc;
  }
  private static long conc(long a, long b) {
    return Long.parseLong(String.valueOf(a) + String.valueOf(b));
  }
  @Test
  public void testPart1() {
    Result res = solve(TEST_INPUT);
    assertEquals(bi(3749), res.a());
  }
  @Test
  public void testPart2() {
    Result res = solve(TEST_INPUT);
    assertEquals(bi(11387), res.b());
  }

  

  private static final String TEST_INPUT = """
    190: 10 19
    3267: 81 40 27
    83: 17 5
    156: 15 6
    7290: 6 8 6 15
    161011: 16 10 13
    192: 17 8 14
    21037: 9 7 18 13
    292: 11 6 16 20
        """;;
}
