package days;

import util.Day;
import util.Util.Point;

import static org.junit.Assert.*;
import static util.Util.*;

import java.util.List;
import java.util.PriorityQueue;

import org.junit.jupiter.api.Test;

public class Day_18 extends Day {
  public static void main(String[] args) {
    var day = new Day_18();
    day.run();
  }

  @Override
  public Result solve(String input) {
    return solve(input, 71, 71, 1024);
  }

  public Result solve(String input, int mapW, int mapH, int limit) {
    var points = input.lines().map(Point::read).toList();
    boolean[][] map = new boolean[mapH][mapW];
    for(var pt : points.subList(0, limit)){
      map[pt.row()][pt.col()] = true;
    };
    long a = 0, b = 0;

    Point start = new Point(0, 0);
    Point end = new Point(mapW - 1, mapH - 1);
    a = dijkstra(start, end, map);
    Point byteCut = solveP2(limit, points, mapW, mapH);
    logln("first byte : %d,%d", byteCut.col(), byteCut.row());
    return new Result(a, b);
  }

  private Point solveP2(int limit, List<Point> points, int mapW, int mapH) {
    boolean[][] map = new boolean[mapH][mapW];
    Point start = new Point(0, 0);
    Point end = new Point(mapW - 1, mapH - 1);
    int boundsMin = limit;
    int boundsMax = points.size() - 1;
    while(boundsMin != boundsMax){
      for (var row : map) {
        for(int i = 0; i < row.length; i++){
          row[i] = false;
        }
      }
      int mid = boundsMin + (boundsMax - boundsMin) / 2;
      Point bytePos = points.get(mid);
      for (int i = 0; i <= mid; i++) {
        var pt = points.get(i);
        map[pt.row()][pt.col()] = true;
      }
      logln("bounds %d %d : looking at %d", boundsMin, boundsMax, mid);
      print(map);
      var res = dijkstra(start, end, map);
      if(res < Integer.MAX_VALUE){
        logln(" > found a way at byte %d / %d (%s)", mid, points.size(), bytePos);
        boundsMin = mid + 1;
      } else {
        logln(" > NO way at byte %d / %d (%s)", mid, points.size(), bytePos);
        boundsMax = mid;
      }
      logln("new bounds %d %d (%s)", boundsMin, boundsMax, bytePos);
    }

    Point byteCut = points.get(boundsMax);
    return byteCut;
  }

  public static long dijkstra(Point start, Point end, boolean[][] map) {
    var dist = new int[map.length][map[0].length];
    for (int i = 0; i < dist.length; i++) {
      for (int j = 0; j < dist[0].length; j++) {
        dist[i][j] = Integer.MAX_VALUE;
      }
    }
    dist[start.row()][start.col()] = 0;
    var visited = new boolean[map.length][map[0].length];
    var pq = new PriorityQueue<Point>((o1, o2) -> {
      return Integer.compare(dist[o1.row()][o1.col()], dist[o2.row()][o2.col()]);
    });
    pq.add(start);
    while (!pq.isEmpty()) {
      var current = pq.poll();
      if (visited[current.row()][current.col()]) {
        continue;
      }
      visited[current.row()][current.col()] = true;
      if (current.equals(end)) {
        break;
      }
      for (var dir : Direction.values()) {
        var next = current.plus(dir);

        if (!isValid(map, next) || map[next.row()][next.col()]) {
          //outside or obstacle
          continue;
        }
        if (dist[next.row()][next.col()] > dist[current.row()][current.col()] + 1) {
          dist[next.row()][next.col()] = dist[current.row()][current.col()] + 1;
          pq.add(next);
        }
      }
    }
    return dist[end.row()][end.col()];
  }
  @Test
  public void test() {
    assertEquals(bi(22), solve(testInput, 7, 7, 12).a());
  }
  @Test
  public void test2() {
    var points = testInput.lines().map(Point::read).toList();
    assertEquals(new Point(1, 6), solveP2(12, points, 7, 7));
  }
  public static final String testInput = """
5,4
4,2
4,5
3,0
2,1
6,3
2,4
1,5
0,6
3,3
2,6
5,1
1,2
5,5
2,5
6,5
1,4
0,4
6,4
1,1
6,1
1,0
0,5
1,6
2,0
  """;
}