package days;

import util.Day;
import util.Util;
import util.Util.Direction;
import util.Util.Point;
import util.Util.Result;

import static org.junit.Assert.*;
import static util.Util.*;
import static util.Util.Direction.RIGHT;
import static util.Util.Direction.UP;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

public class Day_16 extends Day {
  public static void main(String[] args) {
    var day = new Day_16();
    day.run();
  }

  @Override
  public Result solve(String input) {
    var maze = new Maze(input);
    var bestPaths = calcBestPath(maze);
    var score = bestPaths.get(0).score;
    var visited = new HashSet<Point>();
    for (Path path : bestPaths) {
      path.visit(maze.map, visited);
      
    }
    // logln("we found %d paths", bestPaths.size());
    // print(maze.map);

    return new Result(score, visited.size());//I don't count the end ofthe maze
  }
  
  private List<Path> calcBestPath(Maze maze) {
    List<Path> bestPaths = new ArrayList<>();
    bestPaths.add(new Path(null, p(maze.end, UP), Integer.MAX_VALUE));
    Map<Pos, Integer> visited = new HashMap<>();

    List<Path> candidates = new ArrayList<>();
    candidates.add(new Path(null, p(maze.start, RIGHT), 0));
    while(!candidates.isEmpty()){
      Path current = candidates.remove(0);
      Path best = bestPaths.get(0);
      
      Pos end = current.pos;
      Point endPos = end.pos;
      if(visited.containsKey(end)){
        if(visited.get(end) < current.score )
        continue;
      }
      visited.put(end, current.score);
      if(endPos.equals(maze.end)){
        if(current.score <= best.score){
          if(current.score < best.score)
            bestPaths.clear();
            
          bestPaths.add(current);
        }
        
        continue;
      }
      var fwd = endPos.plus(end.dir());
      if(maze.isFree(fwd)){
        var nexPath = makePath(current, p(fwd, end.dir));
        candidates.add(0, nexPath);
      }
      candidates.add(makePath(current, p(endPos, end.dir.left())));
      candidates.add(makePath(current, p(endPos, end.dir.right())));
      // logln("checked one path, still %d to go", candidates.size());
    }
    return bestPaths;
  }

  private Path makePath(Path previous, Pos add) {
    if(previous.pos.dir == add.dir)
      return new Path(previous, add, previous.score + 1);
    return new Path(previous, add, previous.score + 1000);
  }

  private static Pos p(Point p, Direction d) { return new Pos(p, d); }

  private static record Pos(Point pos, Direction dir){}

  private static record Path(Path parent, Pos pos, int score){
    int length(){
      if(parent == null)
        return 1;
      return parent.length() +1;
    }
    void visit(char[][] map, Set<Point> set){
      set.add(pos.pos);
      setChar(map, pos.pos, '0');
      if(parent != null)
        parent.visit(map, set);
    }
  }

  private static class Maze{
    private final Point start;
    private final Point end;
    private final char[][] map; 
    Maze(String input){
      Util.Box<Point> st = new Util.Box<>();
      Util.Box<Point> e = new Util.Box<>();
      map = readMap(input, (pt, c)->{
        if(c == 'S') st.put(pt);
        if(c == 'E') e.put(pt);
      });
      assertTrue(st.isPresent() && e.isPresent());
      start = st.take();
      end = e.take();
    }

    public boolean isFree(Point p) {
      return getChar(map, p) != '#';
    }
  }
  @Test
  public void testMaze1() {
    assertEquals(new Result(7036, 45), solve(testInputMaze1));
  }
  @Test
  public void testMaze2() {
    assertEquals(new Result(11048, 64), solve(testInputMaze2));
  }

  public static final String testInputMaze1 = """
###############
#.......#....E#
#.#.###.#.###.#
#.....#.#...#.#
#.###.#####.#.#
#.#.#.......#.#
#.#.#####.###.#
#...........#.#
###.#.#####.#.#
#...#.....#.#.#
#.#.#.###.#.#.#
#.....#...#.#.#
#.###.#.#.#.#.#
#S..#.....#...#
###############
  """;
  public static final String testInputMaze2 = """
#################
#...#...#...#..E#
#.#.#.#.#.#.#.#.#
#.#.#.#...#...#.#
#.#.#.#.###.#.#.#
#...#.#.#.....#.#
#.#.#.#.#.#####.#
#.#...#.#.#.....#
#.#.#####.#.###.#
#.#.#.......#...#
#.#.###.#####.###
#.#.#...#.....#.#
#.#.#.#####.###.#
#.#.#.........#.#
#.#.#.#########.#
#S#.............#
#################
  """;
}