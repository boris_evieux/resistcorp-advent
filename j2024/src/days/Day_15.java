package days;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.Util.*;
import static util.Util.Direction.LEFT;
import static util.Util.Direction.RIGHT;

import org.junit.jupiter.api.Test;

import util.Day;
import util.Util.Direction;
import util.Util.Point;
import util.Util.Result;


public class Day_15 extends Day {
	public static void main(String[] args) {
		var day = new Day_15();
		day.run();
	}
	
	@Override
	public Result solve(String input) {
		var mapAndInstructions = input.split("\n\n");
		Robot robot = new Robot();
		var map = readMap(mapAndInstructions[0], (pt, c) -> {
			if (c == '@') robot.pos = pt;
		});
		var instructions = chars(mapAndInstructions[1]);
		long part2 = part2(map, instructions);

		for(var instruction : instructions) {
			if(!Direction.isDir(instruction)) continue;
			robot.move(map, instruction);
		}
		long a = 0;
		for(int row = 0; row < map.length; row++) {
			for(int col = 0; col < map[row].length; col++) {
				if (map[row][col] == 'O'){
					a += gps(row, col);
				};
			}
		}
		
		return new Result(a, part2);
	}

	private long part2(char[][] smallmap, char[] instructions) {
		Robot robot = new Robot();
		var map = new char[smallmap.length][];
		for (int row = 0; row < map.length; row++) {
			char[] lineToCopy = smallmap[row];
			map[row] = new char[lineToCopy.length * 2];
			for (int col = 0; col < lineToCopy.length; col++) {
				char c = lineToCopy[col];
				switch (c) {
					case 'O' : 
						map[row][col * 2] = '[';
						map[row][col * 2 + 1] = ']';
						break;
					case '@' : 
						map[row][col * 2] = '@';
						map[row][col * 2 + 1] = '.';
						robot.pos = new Point(row, col * 2);
						break;
					case '#' :
					case '.' :
						map[row][col * 2] = c;
						map[row][col * 2 + 1] = c;
						break;

					default : char[] cs3 = smallmap[row];
						throw new IllegalArgumentException("Invalid char: " + cs3[col]);
				}
			}
		}
		// logln("initital state");
		// print(map);
		for(var instruction : instructions) {
			if(!Direction.isDir(instruction)) continue;
			robot.move(map, instruction);
			// logln("%nMove %c: ", instruction);	
			// print(map);
		}
		long b = 0;
		for(int row = 0; row < map.length; row++) {
			for(int col = 0; col < map[row].length; col++) {
				if (map[row][col] == '['){
					b += gps(row, col);
				};
			}
		}
		return b;
	}
	private static long gps(int row, int col) {
		return row * 100 + col;
	}

	private static boolean pushBox(char[][] map, Point pos, char dirC) {
		Direction dir = Direction.read(dirC);
		Point boxPos = pos.plus(dir.dir);
		switch (getChar(map, boxPos)){
			case '.' :
				setChar(map, boxPos, 'O');
				return true;
			case 'O' :
				return pushBox(map, boxPos, dirC);
		}
		return false;
	}
	private static boolean pushBox2(char[][] map, Point leftPart, Point rightPart, Direction dir, boolean commit) {
		assertTrue(getChar(map, leftPart) == '[' && getChar(map, rightPart) == ']');
		Point nextL = leftPart.plus(dir);
		Point nextR = rightPart.plus(dir);
		var nextLC = getChar(map, nextL);
		var nextRC = getChar(map, nextR);
		boolean canMoveL;
		boolean canMoveR;
		switch (dir){
			case LEFT :
				if(nextLC == ']'){
					Point toTheLeft = nextL.plus(LEFT);
					canMoveL = canMoveR = pushBox2(map, toTheLeft, nextL, dir, false);
				}else
					canMoveL = canMoveR = nextLC == '.';
				break;
			case RIGHT :
				if(nextRC == '['){
					Point toTheRight = nextR.plus(RIGHT);
					canMoveL = canMoveR = pushBox2(map, nextR, toTheRight, dir, false);
				}else
					canMoveL = canMoveR = nextRC == '.';
				break;
			case UP, DOWN :
 				canMoveL = nextLC == '.';
 				canMoveR = nextRC == '.';
				if(nextLC == '[' && nextRC == ']')
					canMoveL = canMoveR = pushBox2(map, nextL, nextR, dir, false);
				if(nextLC == ']')
					canMoveL = pushBox2(map, nextL.plus(LEFT), nextL, dir, false);
				if(nextRC == '[')
					canMoveR = pushBox2(map, nextR, nextR.plus(RIGHT), dir, false);
				break;
			default : throw new IllegalArgumentException("Invalid direction: " + dir);
		}
		if(canMoveL && canMoveR){
			if(commit){
				if(nextLC == '[' && nextRC == ']')
					pushBox2(map, nextL, nextR, dir, true);
				if(nextLC == ']' && dir != RIGHT)
					pushBox2(map, nextL.plus(LEFT), nextL, dir, true);
				if(nextRC == '[' && dir != LEFT)
					pushBox2(map, nextR, nextR.plus(RIGHT), dir, true);
				setChar(map, nextL, '[');
				setChar(map, nextR, ']');
				if(dir != LEFT)
					setChar(map, leftPart, '.');
				if(dir != RIGHT)
					setChar(map, rightPart, '.');
			}
			return true;
		}


		return false;
	}

	private static class Robot {
		Point pos;
		public boolean move(char[][] map, char dirC) {
			Direction dir = Direction.read(dirC);
			Point newPos = pos.plus(dir.dir);
			boolean moved = switch(getChar(map, newPos)) {
				case '.' -> true;
				case 'O' -> pushBox(map, newPos, dirC);
				case '[' -> pushBox2(map, newPos, newPos.plus(RIGHT), dir, true);
				case ']' -> pushBox2(map, newPos.plus(LEFT), newPos, dir, true);
				default -> false;
			};
			if (moved){
				setChar(map, pos, '.');
				pos = newPos;
				setChar(map, pos, '@');
			}
			return true;
		}
	}
	
	@Test
	public void testSmallInput() {
		assertEquals(bi(2028), solve(testInputSmall).a());
	}
	@Test
	public void testSmallInput2() {
		assertEquals(bi(618), solve(testInputSmall2).b());
	}
	@Test
	public void testInput() {
		assertEquals(new Result(10092, 9021), solve(testInput));
	}
	
	private static final String testInputSmall = """
	########
	#..O.O.#
	##@.O..#
	#...O..#
	#.#.O..#
	#...O..#
	#......#
	########
	
	<^^>>>vv<v>>v<<
	""";
	private static final String testInputSmall2 = """
#######
#...#.#
#.....#
#..OO@#
#..O..#
#.....#
#######

<vv<<^^<<^^
""";

	private static final String testInput = """
##########
#..O..O.O#
#......O.#
#.OO..O.O#
#..O@..O.#
#O#..O...#
#O..O..O.#
#.OO.O.OO#
#....O...#
##########

<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^
vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<
<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><
^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^
<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>
v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^
		""";
}
