package days;

import static org.junit.Assert.assertEquals;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Test;

import util.Util.Result;

import static util.Util.bi;
import static util.Util.intsSpace;

public class Day_02 {
  public static void main(String[] args) {
    var input = Paths.get(FILENAME);
    try(var  lines = Files.lines(input, Charset.forName("UTF-8"))){
      var answer = solve(lines.toList());
      System.out.println(answer.a());
      System.out.println(answer.b());
    } catch (Exception e) {
      e.printStackTrace();
    }

  }
      
  private static Result solve(List<String> lines) {
    int p1 = 0;
    int p2 = 0;
    int lineNo = 0;
    for(var line : lines){
      lineNo++;
      int[] parts = intsSpace(line);
      int unSafe = unsafeIdx(parts);
      if(unSafe == -1){
        p1 ++;
        p2 ++;
      } else {
        for(int i = 0; i < parts.length; i++){
          int[] copy = getCopyWithout(parts, i);
          if(unsafeIdx(copy) == -1){
            p2++;
            break;
          }
        }
        // int[] copyA = getCopyWithout(parts, unSafe);
        // int[] copyB = getCopyWithout(parts, unSafe + 1);
        
        // if(unsafeIdx(copyA) == -1 || unsafeIdx(copyB) == -1){
        //   p2++;
        // }else{
        //   System.out.println(
        //     String.format(
        //       "Line %d (%s) is unsafe even removing \n - at %d (%s)\n - or at %d(%s)", 
        //       lineNo, line, unSafe, s(copyA), (unSafe+1), s(copyB)));
        // }
      }
    }
    return new Result(p1, p2);
  }

  private static int[] getCopyWithout(int[] parts, int unSafe) {
    int[] copy = new int[parts.length -1];
    int iB = 0;
    for (int i = 0; i < parts.length; i++)
      if(i != unSafe)
        copy[iB++] = parts[i];
    return copy;
  }

  private static String s(int[] ints) {
    var sb = new StringBuilder();
    for (int i : ints)
      sb.append(i).append(" ");
    return sb.toString();
  }

  private static int unsafeIdx(int[] parts) {
    var a = parts[0];
    var b = parts[1];
    int dist = Math.abs(a - b);
    if(dist > 3 || dist < 1) return 0;
    var isDescending = a > b;
    var isAscending = a < b;

    for (int i = 2; i < parts.length; i++) {
      a = parts[i - 1];
      b = parts[i];
      if(isDescending && a <= b) return i-1;
      if(isAscending && a >= b) return i-1;
      dist = Math.abs(a - b);
      if(dist > 3 || dist < 1) return i-1;
    }
    return -1;
  }

  @Test
  public void testPartOne() {
    var input = TEST_EXEMPLE.lines().toList();
    var result = solve(input);
    assertEquals(bi(2), result.a());
  }

  @Test
  public void testPartTwo() {
    var input = TEST_EXEMPLE.lines().toList();
    var result = solve(input);
    assertEquals(bi(4), result.b());
  }
  @Test
  public void cornerCase1() {
    var line = intsSpace("68 72 73 74 74");
    var result = unsafeIdx(line);
    assertEquals(0, result);
  }
  @Test
  public void cornerCase2() {
    var line = intsSpace("69 72 73 74 74");
    var result = unsafeIdx(line);
    assertEquals(3, result);
  }
  // @Test
  public void cornerCase3() {
    var line = intsSpace("54 54 53 50 48 43 38");
    var result = unsafeIdx(line);
    assertEquals(0, result);
    int[] copyA = getCopyWithout(line, 0);
    assertEquals(-1, unsafeIdx(copyA));
  }

  private static final String FILENAME = "./days/day_02.txt";
  
  private static final String TEST_EXEMPLE = """
7 6 4 2 1
1 2 7 8 9
9 7 6 2 1
1 3 2 4 5
8 6 4 4 1
1 3 6 7 9
""";;
}
