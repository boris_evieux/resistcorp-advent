package days;

import static org.junit.Assert.assertEquals;
import static util.Util.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.jupiter.api.Test;

import util.Util;
import util.Util.Point;
import util.Util.Result;

public class Day_06 {

  // not 1871 : too high
  private static final List<Direction> EMPTY = List.of();
  public static void main(String[] args) {
    Day_06 day = new Day_06();
    String fileContents = Util.readFile("days/day_06.txt");
    Result solve = day.solve(fileContents);
    System.out.println("Part 1 : " + solve.a());
    System.out.println("Part 2 : " + solve.b());
  }

  private Result solve(String input) {
    String[] lines = input.lines().toArray(String[]::new);
    char[][] map = new char[lines.length][];
    Point guardPos = null;
    Direction guardDir = Direction.UP;
    for (int row = 0; row < lines.length; row++) {
      var line = lines[row];
      map[row] = line.toCharArray();
      for(int idx = 0; idx < line.length(); idx++){
        char c = line.charAt(idx);
        if(c == '^' || c == 'v' || c == '<' || c == '>'){
          guardPos = new Point(row, idx);
          guardDir = Direction.read(c);
        }
      }
    }
    if(guardPos == null) throw new RuntimeException("Guard not found");
    if(guardDir == null) throw new RuntimeException("Guard direction not found");
    
    Result result = getResult(map, guardPos, guardDir, true);


    return result;
  }

  private Result getResult(char[][] map, final Point startPos, final Direction startDir, boolean doCheckLoops) {
    Set<Point> blockable = new HashSet<>();
    Map<Point, List<Direction>> visited = new HashMap<>();

    int numPositive = 0;
    
    var guardPos = startPos;
    var guardDir = startDir;
    while(isValid(map, guardPos)){
      if(visited.getOrDefault(guardPos, EMPTY).contains(guardDir)){
        return null;
      }
      visited.computeIfAbsent(guardPos, (p)->new ArrayList<>()).add(guardDir);
      
      Point next = guardPos.plus(guardDir.dir);
      boolean canMove = getChar(map, next) != '#';
      if(!canMove){
        Direction rightDirection = rotateRight(guardDir);
        guardDir = rightDirection;
      } else {
        if(doCheckLoops && isValid(map, next) && next != startPos){
          var c = getChar(map, next);
          setChar(map, next, '#');
          if(getResult(map, startPos, startDir, false) == null){
            if(c != '.')
              logln("Blocked %s", next); 

            setChar(map, next, 'O');
            // print(map);
            if(blockable.add(next));
              logln("%dth loop detected at %s", blockable.size(), next);
            
            numPositive++;

          }
          setChar(map, next, c);
        }
  
        guardPos = next;
      }
    }
    if(doCheckLoops){
      logln("Num positive: %d, set size %d", numPositive, blockable.size());
    }
    return new Result(visited.size(), blockable.size());
  }

  private Direction rotateRight(Direction guardDir) {
    return switch(guardDir){//todo
      case UP -> Direction.RIGHT;
      case DOWN -> Direction.LEFT;
      case LEFT -> Direction.UP;
      case RIGHT -> Direction.DOWN;
    };
  }

  @Test
  public void testPart1() {
    Result res = solve(TEST_INPUT);
    assertEquals(bi(41), res.a());
  }
  @Test
  public void testPart2() {
    Result res = solve(TEST_INPUT);
    assertEquals(bi(6), res.b());
  }

  

  private static final String TEST_INPUT = """
....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...
""";
  private String string;
}
