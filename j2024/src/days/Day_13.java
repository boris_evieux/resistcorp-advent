package days;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;
import static util.Util.bi;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import org.junit.Test;

import util.Day;
import util.Util.Point;
import util.Util.Result;

import static util.Util.*;

public class Day_13 extends Day {
	public static void main(String[] args) {
		var day = new Day_13();
		day.run();
	}
	
	@Override
	public Result solve(String input) {
		var machines = Machine.parse(input);
		var resultA = machines.stream().mapToLong(m -> solveA(m)).sum();
		var resultB = machines.stream().mapToLong(m -> solveB(m)).sum();
		return new Result(resultA, resultB);
	}
	
	private long solveA(Machine machine) {
		Pointl target = machine.target;
		return solveClosed(machine, target);
	}
	private long solveB(Machine machine) {
		Pointl target = machine.target.plus(new Pointl(10000000000000l, 10000000000000l));

		return solveClosed(machine, target);
	}

	private long solveClosed(Machine machine, Pointl target){
			//we have a two equation system with two unknowns A and B
		// Xa * A + Xb * B = Xt
		// Ya * A + Yb * B = Yt
		// we can solve this with Cramer's rule
		// A = (Xt * Yb - Yt * Xb) / (Xa * Yb - Ya * Xb)
		// B = (Xa * Yt - Ya * Xt) / (Xa * Yb - Ya * Xb)long Xt = target.x();
		long Xt = target.x();
		long Yt = target.y();
		long Xa = machine.buttonA.x();
		long Ya = machine.buttonA.y();
		long Xb = machine.buttonB.x();
		long Yb = machine.buttonB.y();
		//there are two solutions
		long sol1 = solve(Xt, Yt, Xa, Ya, Xb, Yb);
		long sol2 = solve(Yt, Xt, Ya, Xa, Yb, Xb);
		if(sol1 == 0)
			return sol2;
		if(sol2 == 0)
			return sol1;
		return Math.min(sol1, sol2);
	}

	private long solve(long Xt, long Yt, long Xa, long Ya, long Xb, long Yb) {
		long det = Xa * Yb - Ya * Xb;
		long l1 = Xt * Yb - Yt * Xb;
		long l2 = Xa * Yt - Ya * Xt;
		if(det == 0 || l1 % det != 0 || l2 % det != 0) {
			return 0;
		}
		long A = l1 / det;
		long B = l2 / det;

		return 3 * A + B;
	}

	
	private long solve(Machine machine, Pointl target) {
		long gcdX = gcd(machine.buttonA.x(), machine.buttonB.x());
		long gcdY = gcd(machine.buttonA.y(), machine.buttonB.y());
		if(target.x() % gcdX != 0 || target.y() % gcdY != 0) {
			return 0;
		}
		var position = new Pointl(0, 0);
		int pressesA = 0;
		while(position.y() < target.y() && position.x() < target.x()) {
			var dist = target.minus(position);
			if(dist.y() % machine.buttonB.y() == 0 && dist.x() % machine.buttonB.x() == 0) {
				long pressesRow = dist.y() / machine.buttonB.y();
				long pressesCol = dist.x() / machine.buttonB.x();
				if(pressesRow == pressesCol) {
					return pressesA * 3 + pressesRow;
				}
			}
			
			
			position = position.plus(machine.buttonA);
			pressesA++;
		}
		return 0;
	}

	public long gcd(long a, long b){
		if(b == 0) return a;
		return gcd(b, a % b);
	}
	
	private record Machine(Pointl target, Pointl buttonA, Pointl buttonB) {
		public static Machine make(String input) {
			var m = PATTERN.matcher(input);
			assertTrue(m.find());
			var target = new Pointl(Integer.parseInt(m.group(5)), Integer.parseInt(m.group(6)));
			var buttonA = new Pointl(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)));
			var buttonB = new Pointl(Integer.parseInt(m.group(3)), Integer.parseInt(m.group(4)));
			return new Machine(target, buttonA, buttonB);
		}
		public static List<Machine> parse(String input) {
			var mi = input.trim().split("\\n\\n");
			
			return Stream.of(mi).map(i -> make(i)).toList();
		}
		private static final Pattern PATTERN = Pattern.compile("""
		Button A: X\\+(\\d+), Y\\+(\\d+)
		Button B: X\\+(\\d+), Y\\+(\\d+)
		Prize: X=(\\d+), Y=(\\d+)""", Pattern.MULTILINE);
	}
	
	@Test
	public void testParseMachine() {
		var machines = Machine.parse(EXAMPLE_INPUT);
		assertEquals(4, machines.size());
	}
	@Test
	public void testPart1() {
		assertEquals(bi(480), solve(EXAMPLE_INPUT).a());
	}
	@Test
	public void testPartB() {
		var machines = Machine.parse(EXAMPLE_INPUT);
		assertEquals(4, machines.size());
		assertEquals(0, solveB(machines.get(0)));
		assertNotEquals(0, solveB(machines.get(1)));
		assertEquals(0, solveB(machines.get(2)));
		assertNotEquals(0, solveB(machines.get(3)));

	}
	
	
	private static final String EXAMPLE_INPUT = """
	Button A: X+94, Y+34
	Button B: X+22, Y+67
	Prize: X=8400, Y=5400
	
	Button A: X+26, Y+66
	Button B: X+67, Y+21
	Prize: X=12748, Y=12176
	
	Button A: X+17, Y+86
	Button B: X+84, Y+37
	Prize: X=7870, Y=6450
	
	Button A: X+69, Y+23
	Button B: X+27, Y+71
	Prize: X=18641, Y=10279
	""";
}