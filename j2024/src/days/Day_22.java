package days;

import util.Day;
import util.Util.Result;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.*;
import static util.Util.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Day_22 extends Day {
	public static void main(String[] args) {
		var day = new Day_22();
		day.run();
	}

	@Override
	public Result solve(String input) {
		long partA = 0;
		List<long[]> monkeys = input.lines().map(
			line -> generate(pi(line)).limit(2001).toArray()
		).toList();
		Set<fourchanges> windows = new HashSet<>();

		for(long[] monkeyPrices : monkeys){
			partA += monkeyPrices[2000];
			monkeyPrices[0] %= 10;//seed
			monkeyPrices[1] %= 10;
			monkeyPrices[2] %= 10;
			monkeyPrices[3] %= 10;

			for(int i = 4; i < 2001; i++){
				monkeyPrices[i] %= 10;
				int a = (int) (monkeyPrices[i-3] - monkeyPrices[i-4]);
				int b = (int) (monkeyPrices[i-2] - monkeyPrices[i-3]);
				int c = (int) (monkeyPrices[i-1] - monkeyPrices[i-2]);
				int d = (int) (monkeyPrices[i-0] - monkeyPrices[i-1]);
				windows.add(new fourchanges(a, b, c, d));
			}
		}

		logln("part A : %d", partA);
		logln("found %d windows", windows.size());

		var partB = windows.parallelStream().map(window -> apply(window, monkeys))
			.max((a, b) -> a.compareTo(b)).get();

			logln("best window found : %s", partB);

		return new Result(partA, partB.bananas());
	}

	record fourchanges(int a, int b, int c, int d){
		public boolean recognize(long[] prices, int dIndex){
			return prices[dIndex-3] - prices[dIndex-4] == a()
					&& prices[dIndex-2] - prices[dIndex-3] == b()
					&& prices[dIndex-1] - prices[dIndex-2] == c()
					&& prices[dIndex-0] - prices[dIndex-1] == d();
		}
	}
	record monkeyResult(fourchanges instruction, long bananas) implements Comparable<monkeyResult>{
		@Override public int compareTo(monkeyResult o) {
			return Long.compare(bananas(), o.bananas());
		}
	}

	public monkeyResult apply(fourchanges window, List<long[]> monkeys){
		long sum = 0;
		for(long[] monkey : monkeys){
				for(int i = 4; i < monkey.length; i++){
					if(window.recognize(monkey, i)){
						sum += monkey[i];
						break;
					}
				}
		}

		return new monkeyResult(window, sum);
	}

	public LongStream generate(long seed){
		return LongStream.iterate(seed, this::next);
	}

	public long next(long value){
		long res1 = mixAndPrune(value * 64, value);
		long res2 = mixAndPrune(res1 / 32, res1);
		long res3 = mixAndPrune(res2 * 2048, res2);
		return res3;
	}

	public long mixAndPrune(long value, long into){
		long mix = value ^ into;
		return mix % 16777216;
	}
	@Test
	public void testMixPrune() {
		//mix
		assertEquals(37, 42 ^15);
		//prune
		assertEquals(16113920, 100000000 % 16777216);
	}
	@Test
	public void testGen() {
		var expected = List.of(123l, 15887950l, 16495136l, 527345l, 704524l, 1553684l, 12683156l, 11100544l, 12249484l, 7753432l, 5908254l);
		assertEquals(expected, generate(123).limit(11).boxed().toList());
	}

	@Test
	public void testPart1(){
		assertEquals(bi(37327623), solve(testInput1).a());
	}
	public static final String testInput1 = """
1
10
100
2024
	""";
	@Test
	public void testPart2(){
		assertEquals(bi(23), solve(testInput2).b());
	}
	public static final String testInput2 = """
1
2
3
2024
	""";
}