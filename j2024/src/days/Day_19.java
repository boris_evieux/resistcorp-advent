package days;

import util.Day;
import util.Util.Result;

import static org.junit.Assert.*;
import static util.Util.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class Day_19 extends Day {
	private final Map<String, Long> cache = new HashMap<>();
	// 407858650 too low
	// 1016700771200474 < via code found on net
	// my bug was an overflow of b as I was using an int instead of a long as "b" in proc "possible"
	public static void main(String[] args) {
		var day = new Day_19();
		day.run();
	}

	@Override
	public Result solve(String input) {
		var ab = input.split("\n\n");
		var patterns = ab[0].split(", ");
		var designs = ab[1].lines().toList();
		var a = 0l;
		var b = 0l;

		for (var design : designs) {
			var total = possible(design, patterns);
			if(total > 0){
				a++;
				b += total;
			}
		}

		return new Result(a, b);
	}

	private long possible(String design, String[] patterns) {
		if(cache.containsKey(design)) return cache.get(design);
		var sum = 0l;
		for (var pattern : patterns) {
			if(design.equals(pattern)) {
				sum++;
			} else if(design.startsWith(pattern)) {
				var sub = design.substring(pattern.length());
				sum += possible(sub, patterns);
			}
		}
		cache.put(design, sum);
		return sum;
	}

	@Test
	public void test() {
		assertEquals(new Result(6, 16), solve(testInput));
	}
	@Test
	public void testBRGR() {
		assertEquals(2l, possible("brgr", "r, wr, b, g, bwu, rb, gb, br".split(", ")));
	}
	@Test
	public void testBRGBRG() {
		assertEquals(13l, possible("brgbrg", "r, b, g, rg, gb, br".split(", ")));
	}
	@Test
	public void testBRGBRG3() {
		assertEquals(233l, possible("brgbrgbrgbrg", "r, b, g, rg, gb, br".split(", ")));
	}
	@Test
	public void testBRGBRG6() {
		assertEquals(75025l, possible("brgbrgbrgbrgbrgbrgbrgbrg", "r, b, g, rg, gb, br".split(", ")));
	}
	@Test
	public void testRRBGBR() {
		assertEquals(6l, possible("rrbgbr", "r, wr, b, g, bwu, rb, gb, br".split(", ")));
	}
	@Test
	public void testGBBR() {
		assertEquals(4l, possible("gbbr", "r, wr, b, g, bwu, rb, gb, br".split(", ")));
	}
	@Test
	public void testUBWU() {
		assertEquals(0l, possible("ubwu", "r, wr, b, g, bwu, rb, gb, br".split(", ")));
	}
	@Test
	public void testBWURRG() {
		assertEquals(1l, possible("bwurrg", "r, wr, b, g, bwu, rb, gb, br".split(", ")));
	}
	static String myTowels = "brgru, burbw, wrwg, rbbbg, bwrb, rurbw, gbbgbu, bubggb, ggrb, gburb, wguwgbw, grwwrgbb, rwu, ggurgwb, wbb, rru, rwgb, bbrr, brgw, rrrrbu, bbuugrbb, www, rggg, bwbuw, wwuw, rwgrb, wruw, wwug, wu, wbg, grrrwbu, rbr, wru, bbgubur, wbbu, ugb, urrgw, wbgwg, bwrgwrg, gugbb, rruuwb, gbwg, u, uwbr, wbbww, rugu, guggu, burbbu, wub, gwbw, gbrw, brur, wr, rbrrugw, uuur, bwb, rurw, rruguww, gwguur, gur, ggwbug, uubru, wrbbub, wggrwr, brwu, uugu, wugbwgu, burug, gu, rgbggb, ggg, gwu, uwbg, rrbrw, rg, guwrw, rw, bwbwguw, gubbur, gbur, wbu, bwub, rrggb, gbwbb, rguur, gbgu, wb, rub, urwb, bb, wgw, rwr, gbu, wwbug, uuwwgwg, guug, bwrwuw, uwg, rugg, wbw, rbgg, rwrwwb, rurgu, uug, rbwb, rbbwwg, gwruru, bwugggw, rwbr, wbbr, rrr, rrwg, ubu, uww, wuurr, gwggu, bbg, rrggbr, ubrruwb, ugg, bbw, gwwgbw, ubrbgr, wg, wrg, wur, r, wwrbw, bwgu, uuwrw, wug, gwrw, rww, gwuu, rgg, rrb, ruwwwgu, rurgb, ww, rrw, rrwug, wuwrww, wuruwg, gwwb, grbg, brrub, urbbbwr, wwwwuwu, bbu, ub, wwgubu, rbw, wuugg, rr, wbub, ggr, buugu, rgw, rwgr, wgwuu, bgb, wwurrgu, uuwgw, uwuw, guggwgug, bggbbb, ruuwur, rgrbgru, wwuggwbw, brrwgur, bbrruwwg, ubw, wrw, ruwwg, ugbwwu, wrbu, grg, rgr, rbwwbuw, br, ugrbb, bwrwrb, gguu, buurg, wgwr, gbr, gub, rrrgggrr, bru, ubwwb, rurr, bggb, brgbbuug, bggbuuru, wwur, rbb, grgu, wrru, wubu, wgu, w, wwgb, brgub, rwbbr, ggb, ruwbr, wuw, gbg, bwbuwu, bbuu, guuu, bgrw, wrgwr, ubgrwrug, bgu, rrwu, rwburb, bggr, rur, gbb, rwwwwr, bug, urb, rug, bgg, ubruw, rgrw, gr, wwu, wrbbg, ubb, grrr, buw, rububu, gwbru, bbbgwurw, uggggr, bbbuug, grwb, ugbwuu, ruu, rugwb, gbuub, wrubw, ggbr, ggw, ugww, gwg, ugr, wwr, ubugb, wuu, wggg, buuur, rrrwrgb, rwbg, rrg, bwwrrw, wbrrugbg, wwgwrruu, guw, wruggw, uru, ruw, bgbrr, grb, brr, rrbbgbw, uwu, ubbgwr, bwwwg, brgr, urgrbr, ubuurrr, ruwrgbb, bwr, rgu, wugwwu, guu, rrwrbu, wuwg, uwugggg, rbbugrww, rbu, grbu, brgb, uuwgg, ggwb, uuwbrrw, brwg, uwb, rgwgg, gugu, rrurbb, rgbgr, grugg, grub, rwubgw, wrrg, gurbu, ggu, uwwb, uub, gwwurr, urur, gubrw, urgrrrg, gwrbw, wgwwu, urw, bwbb, wggbrrwu, grrgrbw, wbuwrg, wugu, wrwwgr, wgr, bgrug, brb, rb, uwwg, bg, ugrwgu, brg, bruu, urr, gg, uuu, uuw, ggugu, bbbgg, urbrrb, grgrb, rurg, guuub, gww, grr, burw, uwuu, wrb, wbwrr, bwgrug, grw, wgwb, gwb, gwurbrb, rwbur, uwgw, bwruubr, uwggb, gru, rrurru, rwbgb, brw, bur, rbgu, ugw, wwg, ubgwg, g, ubr, wbr, bbr, wgrr, wgb, wbwb, bggguub, ggwubrr, rbg, uw, bgrrggu, wgwu, bubbu, bub, bu, uwwgr, bgbu, ubg, ru, bwrbgug, wwb, wwwgb, brgwbwgg, rrwwwr, gubbb, wrwrg, wgg, rwg, bwrrw, uur, rbbwbr, gurwub, ruuub, uwwr, wgrgww, wrgbrrwg, uwbwugg, wbbrrrr, rgwrb, rwgw, brww, guugbugw, ggguwbr, ur, wbbw, wwgbw, bwu, urg, bgur, gb, ug, rrgggwu, ubgu, wubb, urubuguu, bww, rwb, grwguw, ruwg, uu, bbgwu, ruub, uggbug, wugrgg, ugbuub, ruuuwg, gwr, uggruwwr, wrr, gug, ugu, gbugru, bbwbwwu, wwwu, wgbubuu, rwugww, urgubrwg, rgbwuw, wwbrb, buu, uurbbr, bgr, rwrug, bguw, wbwubbu, bgw, wguu";
	@Test
	public void testL1() {
		assertEquals(3547731588l, possible("rwgbwbggrbgbwggwguwwuuwwrbwurwwggbwwwbubwbwgwur", myTowels.split(", ")));
	}

	public static final String testInput = """
r, wr, b, g, bwu, rb, gb, br

brwrr
bggr
gbbr
rrbgbr
ubwu
bwurrg
brgr
bbrgwb
	""";
}