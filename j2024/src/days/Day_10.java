package days;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.Util.*;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import org.junit.Test;

import util.Day;
import util.Util.Direction;
import util.Util.Point;
import util.Util.Result;

public class Day_10 extends Day {
	public static void main(String[] args) {
		var day = new Day_10();
		day.run();
	}

	@Override
	public Result solve(String input) {
		List<Point> heads = new ArrayList<>();
		var map = getHeads(input, heads);

		int sumA = 0;
		int sumB = 0;
		Set<Point> summits = new HashSet<>();
		Set<Trail> trails = new HashSet<>();
		for(Point head : heads){
			summits.clear();
			trails.clear();
			score(map, head, summits, trails);
			sumA += summits.size();
			sumB += trails.size();
		}

		return new Result(sumA, sumB);
	}

	private List<Point> getHeads(String input) {
		List<Point> heads = new ArrayList<>();
		getHeads(input, heads);
		return heads;
	}

	private char[][] getHeads(String input, List<Point> heads) {
		String[] lines = input.lines().toArray(String[]::new);
    char[][] map = new char[lines.length][];
    for (int row = 0; row < lines.length; row++) {
      var line = lines[row].trim();
      map[row] = line.toCharArray();
      for(int idx = 0; idx < line.length(); idx++){
        var pt = new Point(row, idx);
        char c = line.charAt(idx);
        if(c == '0'){
          heads.add(pt);
        }
      }
    }
		return map;
	}

	private int rate(char[][] map, Point head) {
		Set<Trail> trails = new HashSet<>();
		score(map, head, new HashSet<>(), trails);
		
		return trails.size();
	}
	private int score(char[][] map, Point head) {
		Set<Point> foundSummits = new HashSet<>();
		score(map, head, foundSummits, new HashSet<>());
		
		return foundSummits.size();
	}

	private void score(char[][] map, Point head, Set<Point> foundSummits, Set<Trail> trails) {
		Stack<Trail> toCheck = new Stack<>();
		toCheck.add(new Trail(head));
		while(!toCheck.isEmpty()){
			var currentTrail = toCheck.pop();
			Point currentPos = currentTrail.last();
			char currentValue = getChar(map, currentPos);
			for(Direction dir : Direction.values()){
				Point next = currentPos.plus(dir);
				char nextValue = getChar(map, next);
				if(nextValue == currentValue + 1){
					var nextTrail = currentTrail.with(next);
					if(nextValue == '9'){
						foundSummits.add(next);
						trails.add(nextTrail);
					}
					else
						toCheck.add(nextTrail);
				}
			}
		}
		// logln("for trailhead at %s, found %d summits", head, foundSummits.size());
		// logln("  >> with %s different trails", trails.size());
		// for (Trail trail : trails) {
		// 	logln("  >>> %s", trail);
		// }
	}


	private record Trail(List<Point> points, Point last) {
		public Trail(Point p) {
			this(List.of(), p);
		}
		public Trail with(Point p) {
			List<Point> newPoints = new ArrayList<>(points);
			newPoints.add(p);
			return new Trail(Collections.unmodifiableList(newPoints), p);
		}
	}

	@Test
	public void testPart1s() {
		var res = solve(testInput);
		assertEquals(bi(1), res.a());
	}
	@Test
	public void testPart1() {
		var res = solve(testInput2);
		assertEquals(bi(36), res.a());
	}
	@Test
	public void testPart2() {
		var res = solve(testInput2);
		assertEquals(bi(81), res.b());
	}
	@Test
	public void testHeads(){
		var heads = getHeads(testInput);
		assertEquals(1, heads.size());
		heads = getHeads(testInput2);
		assertEquals(9, heads.size());
	}
	@Test
	public void testScore(){
		List<Point> heads = new ArrayList<>();
		var map = getHeads(testInput, heads);
		assertEquals(1, score(map, heads.get(0)));
	}
	@Test
	public void testScore2(){
		List<Point> heads = new ArrayList<>();
		var map = getHeads(testInput2, heads);
		assertEquals(5, score(map, heads.get(2)));
	}
	@Test
	public void testRating(){
		String M = """
.....0.
..4321.
..5..2.
..6543.
..7..4.
..8765.
..9....
""";
		List<Point> heads = new ArrayList<>();
		var map = getHeads(M, heads);
		assertEquals(1, score(map, heads.get(0)));
		assertEquals(3, rate(map, heads.get(0)));
	}

	private static String testInput = """
0123
1234
8765
9876
		""";
	private static String testInput2 = """
89010123
78121874
87430965
96549874
45678903
32019012
01329801
10456732
		""";
}