package days;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import util.Day;
import util.Util.Result;

import static util.Util.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day_17 extends Day {
  public static void main(String[] args) {
    var day = new Day_17();
    day.run();
  }

  @Override
  public Result solve(String input) {
    Computer computer = new Computer(input);
    computer.run();
    List<Integer> output = computer.output;
		int[] program = computer.program;
		logln("%s", output);

		int[] lastOut = new int[program.length];
		List<Integer> compare = new ArrayList<>();
		compare.add(program[program.length -1]);
		long A = 0;

		//analyze
		Map<Integer, Integer> outputToInput = new HashMap<>();

		int a = 0;
		do{
			computer.run(a);
			outputToInput.put(output.get(0), a%8);
			a++;
		}while(outputToInput.size() < 7);


		while(!Arrays.equals(lastOut, program)){
			for(;!output.equals(compare) && output.size() <= program.length; A++){
				computer.run(A);
				assertEquals(compare.size(), output.size());
				for(int i = 1; i < compare.size(); i++){
					assertEquals(compare.get(i), output.get(i));
				}
			}
			if(output.size() != lastOut.length){

				compare.add(0, program[program.length - compare.size() - 1]);
				Arrays.fill(lastOut, 0);
				for (int i = 0; i < output.size(); i++) {
					lastOut[i] = output.get(i);
				}
				A <<= 3;
			}else if(output.size()>lastOut.length)
				throw new IllegalStateException();
		}

    // for(int i = 0; i <= 200000; i++) {
    //   computer.reboot();
    //   if(computer.run(i, true)) {
    //     logln("Found solution for a=%d", i);
    //     return new Result(0, i);
    //   }
    // }
    // ThreadLocal<Computer> c2 = ThreadLocal.withInitial(() -> new Computer(input));

    // var b = LongStream.range(0, 2000_000_000_000l).mapToObj(a -> {
    //   Computer c = c2.get();
    //   for(long i = a * 1000_000; i < a * 1000_000 + 1000_000; i++) {
    //     c.reboot();
    //     if(c.run(i, true)) {
    //       logln("Found solution for a=%d", a);
    //       return OptionalLong.of(i);
    //     }
    //   }
    //   logln("nothing between %d and %d", a * 1000_000, a * 1000_000 + 1000_000);
    //   return OptionalLong.empty();
    // }).filter(OptionalLong::isPresent).findFirst().orElse(OptionalLong.empty());
    // if(b.isEmpty()) 
    //   logln("No solution found");
    return new Result(0, A);
  }

  class Computer {
    int ip;
    long A;
    long B;
    long C;
    final int[] program;
    final List<Integer> output;

    Computer(String input) {
      var lines = input.split("\n");
      String a = lines[0].split(" ")[2];
      String b = lines[1].split(" ")[2];
      String c = lines[2].split(" ")[2];
      String pg = lines[4].split(" ")[1];
      A = pi(a);
      B = pi(b);
      C = pi(c);
      program = ints(pg, ",");
      output = new ArrayList<>();
    }

    Computer(int[] program) {
      A = 0;
      B = 0;
      C = 0;
      this.program = program;
      output = new ArrayList<>();
    }

    public void reboot() {
      A = 0;
      B = 0;
      C = 0;
      ip = 0;
      output.clear();
    }

    public void run() {
      run(A);
    }

    public boolean run(long a) {
			reboot();
      A = a;

      while (ip < program.length) {
        int opcode = program[ip];
        int operand = program[ip + 1];
        long combo = combo(operand);
        int ipBefore = ip;
        switch (opcode) {
          case ADV:
            if (combo == -1)
              throw new RuntimeException("Invalid operand");
            A = A / (1 << combo);
            break;
          case BXL:
            B ^= operand;
            break;
          case BST:
            if (combo == -1)
              throw new RuntimeException("Invalid operand");
            B = combo % 8;
            break;
          case JNZ:
            ip = A != 0 ? operand : ip + 2;
            break;
          case BXC:
            B = B ^ C;
            break;
          case OUT:
            if (combo == -1)
              throw new RuntimeException("Invalid operand");
            int val = (int) (combo % 8);
            output.add(val);
            // logln("for seed %d we are outputting %s", a, output);
            break;
          case BDV:
            if (combo == -1)
              throw new RuntimeException("Invalid operand");
            B = A / (1 << combo);
            break;
          case CDV:
            if (combo == -1)
              throw new RuntimeException("Invalid operand");
            C = A / (1 << combo);
            break;
        }
        if (ip == ipBefore)
          ip += 2;
      }
      if (output.size() != program.length)
        return false;
      for (int i = 0; i < output.size(); i++) {
        if (output.get(i) != program[i])
          return false;
      }
      return true;
    }

    // opcodes
    public static final int ADV = 0;
    public static final int BXL = 1;
    public static final int BST = 2;
    public static final int JNZ = 3;
    public static final int BXC = 4;
    public static final int OUT = 5;
    public static final int BDV = 6;
    public static final int CDV = 7;

    public long combo(int operand) {
      return switch (operand) {
        case 0, 1, 2, 3 -> operand;
        case 4 -> A;
        case 5 -> B;
        case 6 -> C;
        case 7 -> -1;
        default -> throw new RuntimeException("Invalid operand");
      };
    }
  }

  @Test
  public void test() {
    var c = new Computer(testInput);
    c.run();
    assertEquals(List.of(4, 6, 3, 5, 6, 3, 5, 2, 1, 0), c.output);
  }
  @Test
  public void test2() {
    var c = new Computer(testInput2);
    assertFalse(c.run(0));
    c.reboot();
    assertFalse(c.run(1000));
    c.reboot();
    assertFalse(c.run(500));
  }
  @Test
  public void test2bis() {
    var c = new Computer(testInput2);
    assertTrue(c.run(117440));
  }
  @Test
  public void test3() {
    assertEquals(bi(117440), solve(testInput2).b());
  }
  @Test
  public void test4() {
    int[] pg = {2,6};
    Computer c = new Computer(pg);
    c.C = 9;
    c.run();
    assertEquals(1, c.B);
  }
  @Test
  public void test5() {
    int[] pg = { 5, 0, 5, 1, 5, 4};
    Computer c = new Computer(pg);
    c.A = 10;
    c.run();
    assertEquals(List.of(0,1,2), c.output);
  }
  @Test
  public void test6() {
    int[] pg = { 0, 1, 5, 4, 3, 0 };
    Computer c = new Computer(pg);
    c.A = 2024;
    c.run();
    assertEquals(List.of(4, 2, 5, 6, 7, 7, 7, 7, 3, 1, 0), c.output);
    assertEquals(0, c.A);
  }
  @Test
  public void test7() {
    int[] pg = { 1, 7 };
    Computer c = new Computer(pg);
    c.B = 29;
    c.run();
    assertEquals(26, c.B);
  }

  @Test
  public void test8() {
    int[] pg = { 4, 0 };
    Computer c = new Computer(pg);
    c.B = 2024;
    c.C = 43690;
    c.run();
    assertEquals(44354, c.B);
  }
  public static final String testInput = """
Register A: 729
Register B: 0
Register C: 0

Program: 0,1,5,4,3,0
  """;
  private static final String testInput2 = """
Register A: 2024
Register B: 0
Register C: 0

Program: 0,3,5,4,3,0
  """;
}