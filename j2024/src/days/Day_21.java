package days;

import util.Day;
import util.Util.Direction;
import util.Util.Point;
import util.Util.Result;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static util.Util.*;
import static util.Util.Direction.*;

import java.math.BigInteger;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;

public class Day_21 extends Day {
	//for an initial subPath (in between A) what are the optimal path sizes down the line of recursion.
	private final Map<Recursion, BigInteger> cache = new HashMap<>();
	private final Map<Point, Leg> verticals = new HashMap<>();
	private final Map<Point, Leg> horizontals = new HashMap<>();
	// 303949770908958 too low
	// 306335137543664
	// 756113204938092 too high
	public static void main(String[] args) {
		var day = new Day_21();
		day.run();
	}
	
	final StringBuilder sb = new StringBuilder();
	
	@Override
	public Result solve(String input) {
		BigInteger a = BigInteger.ZERO;
		BigInteger b = BigInteger.ZERO;
		for (var line : input.lines().map(String::trim).toList()) {
			var num = bi(pi(line.substring(0, 3)));
			b = b.add(compute(line, 26).multiply(num));
			a = a.add(compute(line, 3).multiply(num));
		}
		return new Result(a, b);
	}
	
	private BigInteger compute(String line, int recursions){
		var possibles = calcPaths(line);

		return shortestRoute(possibles, recursions-1); 
		
		
	}

	Pattern A = Pattern.compile("A");

	public BigInteger shortestRoute(Collection<List<Leg>> possible, int recursions){
		if(recursions == 0)
			return bi(shortest(possible));

		BigInteger best = bi(Long.MAX_VALUE).multiply(bi(Long.MAX_VALUE));
		for(var route : possible){
			BigInteger length = BigInteger.ZERO;
			for(var leg : route){

				length = length.add(shortestLeg(leg, recursions));
			}

			if(length.compareTo(best) < 0)
				best = length;
		}

		return best;
	}
	public BigInteger shortestLeg(Leg leg, int recursions){
		Recursion key = new Recursion(leg, recursions);
		if(cache.containsKey(key))
			return cache.get(key);

		Set<List<Leg>> possibles = calcPaths(leg);
		var sh = shortestRoute(possibles, recursions-1);
		cache.put(key, sh);
		return sh; 
	}

	public List<Direction> dirs(String leg){
		return leg.chars().mapToObj(i -> Direction.read((char)i)).toList();
	}

	public String legToString(Collection<Direction> leg){
		StringBuilder sb = new StringBuilder();

		for(var d : leg) sb.append(d.toChar());
		sb.append('A');

		return sb.toString();
	}

	// all the possible initial paths (excluding broken ones)
	public Set<List<Leg>> calcPaths(String code){
		Button[] pad = NumPad;
		Point pos = search(pad, 'A');
		Point[] dests = new Point[code.length()];
		for(int i = 0; i < code.length(); i++)
			dests[i] = search(pad, code.charAt(i));
		
		var list = new HashSet<List<Leg>>();

		combineMoves(pad, pos, dests, 0, list, new ArrayDeque<>());

		logln("found %d ways to type %s : ", list.size(), code);
		for(var way : list)
			logln("  > %s", str(way));
		logln("");

		return list;
	}

	// all the possible derived paths
	public Set<List<Leg>> calcPaths(Leg leg){
		Button[] pad = DirPad;
		Point pos = search(pad, 'A');
		Point[] dests = new Point[leg.length()+1];
		int i = 0;
		for(Direction dir : leg){
			dests[i++] = search(pad, dir.toChar());
		}
		dests[i++] = pos;//back to A
		var list = new HashSet<List<Leg>>();

		combineMoves(pad, pos, dests, 0, list, new ArrayDeque<>());

		logln("found %d ways to type %s : ", list.size(), leg);
		for(var way : list)
			logln("  > %s", str(way));
		logln("");

		return list;
	}

	private void combineMoves(Button[] pad, Point pos, Point[] dests, int index, Set<List<Leg>> into, Deque<Leg> stack){
		if(index == dests.length){
			into.add(Collections.unmodifiableList(new ArrayList<>(stack)));
			return;
		}
		Point dest = dests[index];
		Leg v = move(pad, pos, dest.minus(pos), true);
		Leg h = move(pad, pos, dest.minus(pos), false);
		if(v != null){
			stack.addLast(v);//iteration is first to last
			combineMoves(pad, dest, dests, index + 1, into, stack);
			stack.removeLast();
		}
		if(h != null){
			stack.addLast(h);
			combineMoves(pad, dest, dests, index + 1, into, stack);
			stack.removeLast();
		}
	}
	private record Leg(Direction[] sequence) implements Iterable<Direction>{

		@Override
		public Iterator<Direction> iterator() {
			return stream().iterator();
		}

		private Stream<Direction> stream() {
			return Stream.of(sequence());
		}

		public int length(){return sequence().length;}

		@Override
		public final int hashCode() {
			return Arrays.hashCode(sequence());
		}

		@Override
		public final boolean equals(Object arg0) {
			if(arg0 instanceof Leg l)
				return Arrays.equals(sequence(), l.sequence());
			return false;
		}

		@Override
		public final String toString() {
			return stream().map(Direction::toChar).map(String::valueOf).collect(Collectors.joining()) + "A";
		}

	}
	private record Recursion(Leg leg, int depth){}

	/**
	* +---+---+---+
	* | 7 | 8 | 9 |
	* +---+---+---+
	* | 4 | 5 | 6 |
	* +---+---+---+
	* | 1 | 2 | 3 |
	* +---+---+---+
	*     | 0 | A |
	*     +---+---+
	*/
	static Button[] NumPad = {
		new Button(0, 0, '7'),	new Button(0, 1, '8'),	new Button(0, 2, '9'),
		new Button(1, 0, '4'),	new Button(1, 1, '5'),	new Button(1, 2, '6'),
		new Button(2, 0, '1'),	new Button(2, 1, '2'),	new Button(2, 2, '3'),
		new Button(3, 0, 'X'),	new Button(3, 1, '0'),	new Button(3, 2, 'A'),
	};
	/**
	*     +---+---+
	*     | ^ | A |
	* +---+---+---+
	* | < | v | > |
	* +---+---+---+
	*/
	static Button[] DirPad = {
		new Button(0, 0, 'X'),	new Button(0, 1, '^'),	new Button(0, 2, 'A'),
		new Button(1, 0, '<'),	new Button(1, 1, 'v'),	new Button(1, 2, '>')
	};
	
	record Button(Point pos, char ch){
		Button(int row, int col, char ch){ this(new Point(row, col), ch); }
	}
	
	public Point search(Button[] pad, char c){
		for(var btn : pad)
			if(btn.ch == c) return btn.pos();
		throw new IllegalArgumentException("No such char : '" + c + "'");
	}
	
	public char search(Button[] pad, Point pt){
		for(var btn : pad)
			if(btn.pos().equals(pt)) return btn.ch();
		throw new IllegalArgumentException("No such pt : '" + pt + "'");
	}
	
	private Leg move(Button[] pad, Point pos, Point movement, boolean vertFirst) {
		sb.setLength(0);
		int col = movement.col();
		int row = movement.row();
		Point horz = new Point(0, col);
		Point vert = new Point(row, 0);
		boolean legal = vertFirst ? legal(pad, pos.plus(vert)) : legal(pad, pos.plus(horz));
		if(!legal)
			return null;

		if(vertFirst)
			return verticals.computeIfAbsent(movement, this::verticalFirst);
		return horizontals.computeIfAbsent(movement, this::horizontalFirst);
	}

	private Leg verticalFirst(Point mvt){
		int col = mvt.col();
			int row = mvt.row();
			var ret = new Direction[Math.abs(col) + Math.abs(row)];
			Direction dirV = row < 0 ? UP : DOWN;
			Direction dirH = col < 0 ? LEFT : RIGHT;
			int i = 0;
			for(int j = 0; j < Math.abs(row); j++) ret[i++] = dirV;
			for(int j = 0; j < Math.abs(col); j++) ret[i++] = dirH;

			return new Leg(ret);
	}
	private Leg horizontalFirst(Point mvt){
		int col = mvt.col();
			int row = mvt.row();
			var ret = new Direction[Math.abs(col) + Math.abs(row)];
			Direction dirV = row < 0 ? UP : DOWN;
			Direction dirH = col < 0 ? LEFT : RIGHT;
			int i = 0;
			for(int j = 0; j < Math.abs(col); j++) ret[i++] = dirH;
			for(int j = 0; j < Math.abs(row); j++) ret[i++] = dirV;

			return new Leg(ret);
	}

	private boolean legal(Button[] pad, Point pos){
		return search(pad, pos) != 'X';
	}

	private void assertShortest(String seqExpected, Set<List<Leg>> seq){
		assertTrue(seq.stream().map(this::str).anyMatch(str -> str.equals(seqExpected)));
		var min = shortest(seq);
		assertEquals(seqExpected.length(), min);
	}

	private int shortest(Collection<List<Leg>> seq) {
		return seq.stream().mapToInt(this::countPresses).min().orElse(Integer.MAX_VALUE);
	}
	private int countPresses(List<Leg> seq) {
		return seq.stream().mapToInt(a -> a.length()).sum() + seq.size(); // (one A after each "leg")
	}
	private String str(List<Leg> seq) {
		sb.setLength(0);
		for(var leg : seq){
			for(var d : leg)
				sb.append(d.toChar());
			sb.append('A');
		}
		return sb.toString();
	}


	@Test
	public void testSimpleSequences() {
		var seq = calcPaths("029A");
		assertShortest("<A^A>^^AvvvA", seq);
		assertEquals(bi("<A^A^>^AvvvA".length()), compute("029A", 1));
		assertEquals(bi("v<<A>>^A<A>AvA<^AA>A<vAAA>^A".length()), compute("029A", 2));
		assertEquals(bi("<vA<AA>>^AvAA<^A>A<v<A>>^AvA^A<vA>^A<v<A>^A>AAvA^A<v<A>A>^AAAvA<^A>A".length()), compute("029A", 3));
		// seq = solve(DirPad, seq);
		// assertEquilogln("v<<A>>^A<A>AvA<^AA>A<vAAA>^A", seq);
		// seq = solve(DirPad, seq);
		// assertEquals(68, seq.length());
		// assertEquilogln("<vA<AA>>^AvAA<^A>A<v<A>>^AvA^A<vA>^A<v<A>^A>AAvA^A<v<A>A>^AAAvA<^A>A", seq);
	}
	// @Test
	// public void test179A(){
	// 	var seq = solve(NumPad, "179A");
	// 	seq = solve(DirPad, seq);
	// 	seq = solve(DirPad, seq);
	// 	assertEquilogln("<v<A>>^A<vA<A>>^AAvAA<^A>A<v<A>>^AAvA^A<vA>^AA<A>A<v<A>A>^AAAvA<^A>A", seq);
	// }
	@Test
	public void testLine1(){
		var val = compute("029A", 3);
		assertEquals(bi(68), val);
	}
	@Test
	public void testLine2(){
		var val = compute("980A", 3);
		assertEquals(bi(60), val);
	}
	@Test
	public void testLine3(){
		var val = compute("179A", 3);
		assertEquals(bi(68), val);
	}
	@Test
	public void testLine4(){
		var val = compute("456A", 3);
		assertEquals(bi(64), val);
	}
	@Test
	public void testLine5(){
		var val = compute("379A", 3);
		assertEquals(bi(64), val);
	}
	@Test
	public void testA(){
		var r = solve(testInput);
		assertEquals(bi(126384), r.a());
	}
	@Test
	public void testB(){
		var r = solve(testInput);
		//result for test input computed with someone else's code
		assertEquals(bi("154115708116294"), r.b());
	}
	public static final String testInput = """
	029A
	980A
	179A
	456A
	379A
	""";
}