package days;

import static org.junit.Assert.assertEquals;
import static util.Util.isChar;

import org.junit.jupiter.api.Test;

import util.Util;
import util.Util.Point;

public class Day_04 {
  
  public static void main(String[] args) {
    Day_04 day = new Day_04();
    String fileContents = Util.readFile(input_file);
    System.out.println(day.solveP1(fileContents));
    System.out.println(day.solveP2(fileContents));
  }
  
  private int solveP1(String testInput) {
    char[][] map = testInput.lines().map(Util::chars).toArray(char[][]::new);
    int sum = 0;
    
    for (int row = 0; row < map.length; row++) {
      for (int col = 0; col < map[row].length; col++) {
        sum += countWords(map, row, col);
      }
    }
    return sum;
  }
  private int solveP2(String testInput) {
    char[][] map = testInput.lines().map(Util::chars).toArray(char[][]::new);

    // char[][] out = new char[map.length][3];
    // for (int row = 0; row < map.length; row++) {
    //   out[row] = new char[map[row].length];
    //   Arrays.fill(out[row], '.');
    // }
    int sum = 0;
    
    for (int row = 1; row < map.length-1; row++) {
      for (int col = 1; col < map[row].length-1; col++) {
        if(countCrosses(map, row, col)){
          // Point tl = new Point(row-1, col-1);
          // setChar(out, tl, getChar(map, tl));
          // Point tr = new Point(row-1, col+1);
          // setChar(out, tr, getChar(map, tr));
          // Point bl = new Point(row+1, col-1);
          // setChar(out, bl, getChar(map, bl));
          // Point br = new Point(row+1, col+1);
          // setChar(out, br, getChar(map, br));
          // Point pos = new Point(row, col);
          // setChar(out, pos, getChar(map, pos));
          sum++;
        }
      }
    }
    // print(out);
    return sum;
  }

  private boolean countCrosses(char[][] map, int row, int col) {
    Point pos = new Point(row, col);
    if(isChar(map, pos, 'A')) {
      var diag1SAM = isChar(map, pos.plus(TL), 'S') && isChar(map, pos.plus(BR), 'M');
      var diag2SAM = isChar(map, pos.plus(TR), 'S') && isChar(map, pos.plus(BL), 'M');
      var diag1MAS = isChar(map, pos.plus(TL), 'M') && isChar(map, pos.plus(BR), 'S');
      var diag2MAS = isChar(map, pos.plus(TR), 'M') && isChar(map, pos.plus(BL), 'S');
      if( (diag1SAM || diag1MAS) && (diag2SAM || diag2MAS)) {
        return true;
      }
    }
    return false;
  }
  
  private int countWords(char[][] map, int row, int col) {
    Point pos = new Point(row, col);
    int sum = 0;
    if(isChar(map, pos, 'X')) {
      Point[] directions = neighbours();
      for (Point dir : directions) {
        Point M = pos.plus(dir);
        Point A = M.plus(dir);
        Point S = A.plus(dir);
        if(isChar(map, M, 'M') && isChar(map, A, 'A') && isChar(map, S, 'S')) {
          sum++;
        }
      }
    }
    return sum;
  }
  
  private static final Point TL = new Point(-1, -1);
  private static final Point BR = new Point(1, 1);
  private static final Point TR = new Point(-1, 1);
  private static final Point BL = new Point(1, -1);
  private Point[] neighbours() {
    return new Point[] {
      TL, new Point(-1, 0), new Point(-1, 1),
      new Point(0, -1),                 new Point(0, 1),
      new Point(1, -1), new Point(1, 0), new Point(1, 1)
    };
  }

  @Test
  public void testPart1(){
    assertEquals(18, solveP1(test_input));
  }
  @Test
  public void testPart2(){
    assertEquals(9, solveP2(test_input));
  }
  
  private static final String input_file = "days/day_04.txt";
  private static final String test_input = """
MMMSXXMASM
MSAMXMSMSA
AMXSXMAAMM
MSAMASMSMX
XMASAMXAMM
XXAMMXXAMA
SMSMSASXSS
SAXAMASAAA
MAMMMXMMMM
MXMXAXMASX
""";
}
