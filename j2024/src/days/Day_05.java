package days;

import static org.junit.Assert.assertEquals;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;

import org.junit.Test;

import util.Util;
import util.Util.Result;

public class Day_05 {

  public static void main(String[] args) {
    var d = new Day_05();
    var input = Util.readFile("days/day_05.txt");
    Result result = d.solve(input);
    System.out.println("Part 1: " + result.a());
    System.out.println("Part 2: " + result.b());
  }
  private static HashSet<Integer> EMPTY = new HashSet<>();

  private Result solve(String inputTest) {
    var sp = inputTest.split("\n\n");
    var rules = sp[0].lines().map(Rule::read).toList();
    var updates = sp[1].lines().map(s -> Util.ints(s.trim())).toList();
    Map<Integer, Set<Integer>> before = new HashMap<>();

    for (var rule : rules) {
      before.computeIfAbsent(rule.b, HashSet::new).add(rule.a);
    }
    int resultA = 0;
    int resultB = 0;
    for(var update : updates){
      int l = update.length;
      int middle = update[l/2];
      boolean isCorrect = true;
      c : for(int i = 0; i < l; i++){
        int current = update[i];
        for(var j = 0; j < i; j++){
          int previous = update[j];
          if(before.getOrDefault(previous, EMPTY).contains(current)){
            isCorrect = false;
            break c;
          }
        }
      }
      if(isCorrect){
        resultA += middle;
      }else{
        var sorted = sortUpdate(update, before);
        resultB += sorted.get(l/2);
      }
    }
    return new Result(resultA, resultB);
  }

  static List<Integer> sortUpdate(int[] update, Map<Integer, Set<Integer>> rules){
    Comparator<Integer> comp = (a, b) -> {
      var needsToBeBeforeA = rules.getOrDefault(a, EMPTY);
      var needsToBeBeforeB = rules.getOrDefault(b, EMPTY);
      if(needsToBeBeforeA.contains(b))
        return -1;
      if(needsToBeBeforeB.contains(a))
        return 1;
      return 0;
    };
    return IntStream.of(update).boxed().sorted(comp).toList();
  }

  static record Rule(int a, int b) {
    static Rule read(String line) {
      var sp = line.trim().split("\\|");
      return new Rule(Integer.parseInt(sp[0]), Integer.parseInt(sp[1]));
    }
  }

  @Test
  public void testParts() {
    var d = new Day_05();
    var result = d.solve(INPUT_TEST);
    assertEquals(new Result(143, 123), result);
  }

  private static final String INPUT_TEST = """
    47|53
    97|13
    97|61
    97|47
    75|29
    61|13
    75|53
    29|13
    97|29
    53|29
    61|53
    97|53
    61|29
    47|13
    75|47
    97|75
    47|61
    75|61
    47|29
    75|13
    53|13
    
    75,47,61,53,29
    97,61,53,29,13
    75,29,13
    75,97,47,61,53
    61,13,29
    97,13,75,29,47
""";

}
