package days;


import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static util.Util.bi;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Stack;

import org.junit.jupiter.api.Test;

import util.Day;
import util.Util;
import util.Util.Direction;
import util.Util.Point;
import util.Util.Result;

public class Day_12 extends Day {
		public static void main(String[] args) {
				var day = new Day_12();
				day.run();
		}

		@Override
		public Result solve(String input) {
			Map<Character, Set<Point>> starts = new HashMap<>();
			char[][] map = Util.readMap(input, (pt, c) -> {
				starts.computeIfAbsent(c, HashSet::new).add(pt);
			});
			long price = 0;
			long discounted = 0;
			for (var entry : starts.entrySet()) {
				var r = calculate(map, entry.getValue(), entry.getKey());
				for (var region : r) {
					price += region.area * region.perimeter;
					discounted += region.area * region.sides;
				}
			}

			return new Result(price, discounted);
		}

		private static List<Region> calculate(char[][] map, Set<Point> starts, char c) {
			Set<Point> visited = new HashSet<>();
			Stack<Point> stack = new Stack<>();
			List<Region> regions = new ArrayList<>();
			List<Point> perimeter = new ArrayList<>();
			List<Side> sides = new ArrayList<>();
			while(!starts.isEmpty()) {
				int area = 0;
				Point start = getStart(starts);
				stack.push(start);
				while(!stack.isEmpty()) {
					Point pos = stack.pop();
				
					starts.remove(pos);
					if(!visited.add(pos))
						continue;
					
					for(Direction dir : Util.Direction.values()) {
						Point neighbor = pos.plus(dir);
						if (Util.isChar(map, neighbor, c)) {
							stack.push(neighbor);
						} else{
							perimeter.add(neighbor);
							List<Side> applicable = sides.stream().filter(side -> side.add(neighbor, dir)).toList();
							if(applicable.isEmpty())
								sides.add(Side.makeSide(neighbor, dir));
							else{
								var first = applicable.get(0);
								for(var side : applicable) {
									sides.remove(side);
									if(first != side)
										assertTrue(first.eat(side));
								}
								sides.add(first);
							}
						}
					}
					area++;
				}
				Side collapsed = null;
				do{
					collapsed = collapse(sides);
					if(collapsed != null)
						sides.remove(collapsed);
				} while (collapsed != null);
				
				if(area > 0)
					regions.add(new Region(perimeter.size(), area, sides.size()));
				perimeter.clear();
				sides.clear();
			}

			return regions;
		}

		private static Side collapse(List<Side> sides){
			for(int i = 0; i < sides.size(); i++){
				var sideA = sides.get(i);
				for(int j = i + 1; j < sides.size(); j++){
					var sideB = sides.get(j);
					if(sideA.eat(sideB))
						return sideB;
				}
			}
			return null;
		}

		private static Point getStart(Set<Point> starts) {
			Iterator<Point> iterator = starts.iterator();
			Point start = iterator.next();
			starts.remove(start);
			return start;
		}

		@Test
		public void testCalc() {
			var input = """
AAAA
BBCD
BBCC
EEEC
""";	
			var map = Util.readMap(input, (pt, c) -> {});
			Set<Point> starts = new HashSet<>();
			starts.add(new Point(0, 0));
			var result = calculate(map, starts, 'A');
			assertEquals(List.of(new Region(10, 4, 4)), result);
		
		}
		public static boolean isHorizontalFence(Direction separating) {
			return separating == Direction.UP || separating == Direction.DOWN;
		}

		private record Region(int perimeter, int area, int sides){}
		private record Side(Set<Point> points, Direction dir, float rowOrCol){
			public static Side makeSide(Point p, Direction from) {
				Set<Point> points = new HashSet<>(List.of(p));
				boolean h = isHorizontalFence(from);
				return new Side(points, from, h ? p.row() : p.col());
			}

			public boolean add(Point point, Direction from){
				if(from != dir())
					return false;
				boolean isNeighbour = points.stream().anyMatch(p -> p.isNeighbour(point));
				if(isNeighbour) {
					points.add(point);
				}
				return isNeighbour;

			}
			public boolean eat(Side other) {
				boolean sameR = rowOrCol() == other.rowOrCol();
				if(dir() != other.dir() || !sameR)
					return false;
				for(var point : other.points) {
					if(points.stream().anyMatch(p -> p.isNeighbour(point))) {
						points.addAll(other.points);
						return true;
					}
				}
				return false;
			}
		}

		@Test
		public void testBasic() {
			var input = """
AAAA
BBCD
BBCC
EEEC
""";
			var result = solve(input);
			assertEquals(bi(140), result.a());
		}
		@Test
		public void testBasicp2() {
			var input = """
AAAA
BBCD
BBCC
EEEC
""";
			var result = solve(input);
			assertEquals(bi(80), result.b());
		}

		@Test
		public void testTiny() {
				var input = """
OOOOO
OXOXO
OOOOO
OXOXO
OOOOO
				""";
				var result = solve(input);
				assertEquals(new Result(772, 436), result);
		}

		@Test
		public void testEShape() {
				var input = """
EEEEE
EXXXX
EEEEE
EXXXX
EEEEE
""";
				var result = solve(input);
				assertEquals(bi(236), result.b());
		}
		@Test
		public void testHard() {
				var input = """
AAAAAA
AAABBA
AAABBA
ABBAAA
ABBAAA
AAAAAA
""";
				var result = solve(input);
				assertEquals(bi(368), result.b());
		}
}