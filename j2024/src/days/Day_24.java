package days;

import util.Day;
import util.Util.Result;

import org.junit.jupiter.api.Test;
import java.util.*;
import java.util.regex.Pattern;

import static org.junit.Assert.*;
import static util.Util.*;

public class Day_24 extends Day {
	private boolean isAndMode = false;//for the third unit test
	private boolean isAddMode = true;//for the third unit test

	public static void main(String[] args) {
		var day = new Day_24();
		day.run();
	}
	
	@Override
	public Result solve(String input) {
		var sp = input.split("\\n\\n");
		Map<String, Boolean> solved = new HashMap<>();
		
		sp[0].lines().forEach(init -> {
			String name = init.substring(0, 3);
			solved.put(name, init.endsWith("1"));
		});
		List<op> initialState = sp[1].lines().map(op::read).toList();
		
		
		var zRegs = initialState.stream().map(op::res).filter(k -> k.startsWith("z"))
			.sorted().toArray(String[]::new);

		long p1 = simulate(zRegs, initialState, new HashMap<>(solved));
		Map<String, op> reversed = computeBackGraph(initialState);
		var toSwap = new ArrayList<>(identifyWrongWires(reversed, zRegs));

		toSwap.sort((a, b)-> a.compareTo(b));
		
		logln("%d wires to swap : %s", toSwap.size(), toSwap);
		
		return new Result(p1, toSwap.size());
	}
	
	private long simulate(String[] zRegs, List<op> initialState, Map<String, Boolean> solved){
		long a = 0;
		List<op> toSolve =  new ArrayList<>(initialState);
		
		while(!toSolve.isEmpty()){
			toSolve.removeIf( op -> {
				return op.apply(solved);
			});
		}
		for(int i = zRegs.length-1; i >=0; i--){
			String reg = zRegs[i];
			assertEquals(String.format("z%02d", i), reg);
			boolean b = solved.get(reg);
			a <<= 1;
			if(b)
			a |= 1;
		}
		return a;
	}
	
	private Map<String, op> computeBackGraph(List<op> ops) {
		Map<String, op> ret = new HashMap<>();
		for(var op : ops){
			assertFalse("only one wire to one register", ret.containsKey(op.res()));
			ret.put(op.res(), op);
		}
		return ret;
	}
	
	private Collection<String> identifyWrongWires(Map<String, op> graphIn, String[] zRegs){
		if(isAndMode)
			return rectifyAND(graphIn, zRegs.length);

		if(!isAddMode) return List.of();

		var ret = new HashSet<String>();
		Map<String, op> graph = new HashMap<>(graphIn); // copy
		String remainder = "";
		for(int i = 0; i < zRegs.length; i++){
			String inX = String.format("x%02d", i);
			String inY = String.format("y%02d", i);
			String outZ = String.format("z%02d", i);

			op carry;
			if(i != 0){
				//il y a un remainder à vérifier
				op baseXOR = find(graph, new op(inX, inY, Operator.XOR, "??"));
				assertNotNull(baseXOR);
				op output = graph.get(outZ);
				// maybeSwap(graphIn, ret, graph, outZ, baseXOR);

				op baseAND = find(graph, new op(inX, inY, Operator.AND, "??"));
				op remAND = find(graph, new op(remainder, baseXOR.res(), Operator.AND, "??"));
				carry = remAND != null ? find(graph, new op(baseAND.res(), remAND.res(), Operator.OR, "??")) : null;

				int correctness = isCorrectAdd(outZ, remainder, baseXOR, baseAND, output, remAND, carry);

				switch(correctness){
					case 0 :
						logln("for %s, the graph is correct.", outZ);
						remainder = carry.res();
						break;
					case FLAG_OUT | FLAG_CARRY:
						swap(ret, graph, new op(remainder, baseXOR.res(), Operator.AND, "??"), output);
						remAND = find(graph, new op(remainder, baseXOR.res(), Operator.AND, "??"));
						carry = remAND != null ? find(graph, new op(baseAND.res(), remAND.res(), Operator.OR, "??")) : null;
						remainder = carry.res();
						break;
					case FLAG_OUT | FLAG_REM:
						swap(ret, graph, new op(remainder, baseXOR.res(), Operator.AND, "??"), output);
						remAND = find(graph, new op(remainder, baseXOR.res(), Operator.AND, "??"));
						carry = remAND != null ? find(graph, new op(baseAND.res(), remAND.res(), Operator.OR, "??")) : null;
						remainder = carry.res();
						break;
					default: 
						throw new IllegalStateException("???");
				}
			}else{
				//on veut directement XOR vers outZ
				op xOR = new op(inX, inY, Operator.XOR, outZ);
				maybeSwap(graphIn, ret, graph, outZ, xOR);
				carry = find(graph, new op(inX, inY, Operator.AND, "??"));
				remainder = carry.res();
			}

		}
		return ret;
	}
	static final int FLAG_OUT = 1 << 0;
	static final int FLAG_REM = 1 << 1;
	static final int FLAG_CARRY = 1 << 2;

	private int isCorrectAdd(String outExpected, String remainder, op baseXOR, op baseAND, op output, op remAND, op carry){
		var ret = 0;
		boolean out = output.equiv(new op(baseXOR.res(), remainder, Operator.XOR, "???"));
		boolean okCarry = carry != null;
		boolean okRem = remAND != null;
		if(!out){
			logln("for %s, the output isn't the correct XOR", outExpected, output);
			ret |= FLAG_OUT;
		}
		if(! okRem){
			logln("for %s, couldn't find intermediate AND", outExpected);
			ret |= FLAG_REM;
		}else if(! okCarry){
			logln("for %s, couldn't find a carry", outExpected);
			ret |= FLAG_CARRY;
		}
		return ret;
	}

	private Collection<String> rectifyAND(Map<String, op> graphIn, int length){
		var list = new HashSet<String>();

		Map<String, op> graph = new HashMap<>(graphIn); // copy
		for(int i = 0; i < length; i++){
			String inX = String.format("x%02d", i);
			String inY = String.format("y%02d", i);
			String outZ = String.format("z%02d", i);

			var opExp = new op(inX, inY, Operator.AND, outZ);
			maybeSwap(graphIn, list, graph, outZ, opExp);

		}
		return list;
	}

	private void maybeSwap(Map<String, op> graphIn, Set<String> list, Map<String, op> graph, String outZ, op opExp) {
		var op = graphIn.get(outZ);
		if(opExp.equiv(op)){
			logln("for %s, the graph is correct.", outZ);
		}else{
			swap(list, graph, opExp, op);
		}
	}

	private void swap(Set<String> list, Map<String, op> graph, op opExp, op op) {
		var swapOp = find(graph, opExp);
		assertNotNull(swapOp);
		var swapKey = swapOp.res();
		graph.put(swapKey, op.with(swapKey));
		graph.put(op.res(), opExp);

		list.add(swapKey);
		list.add(op.res());
		logln("for %s, swap with.", swapKey);
	}

	private op find(Map<String, op> graph, op opExp) {
		return graph.values().stream()
					.filter(o -> {
						return opExp.equiv(o);
					}).findFirst().orElse(null);
	}

	// private void findDeps(Map<String, op> graph, ArrayDeque<String> scratchStack, HashSet<String> scratch, String reg) {
	// 	scratchStack.clear();
	// 	scratch.clear();
	// 	scratchStack.push(reg);
	// 	while(!scratchStack.isEmpty()){
	// 		var current = scratchStack.pop();
	// 		var op = graph.get(current);
	// 		if(op == null){
	// 			scratch.add(current);
	// 		}else{
	// 			scratchStack.add(op.a()); scratchStack.add(op.b());
	// 		}
	// 	}
	// }
	
	public enum Operator{AND, OR, XOR;}
	public record op(String a, String b, Operator op, String res){
		static Pattern lineP = Pattern.compile("(\\w\\w\\w) (AND|OR|XOR) (\\w\\w\\w) -> (\\w\\w\\w)");
		static op read(String line){
			var matcher = lineP.matcher(line.trim());
			assertTrue(matcher.find());
			return new op(matcher.group(1), matcher.group(3), Operator.valueOf(matcher.group(2)), matcher.group(4));
		}
		
		public boolean apply(Map<String, Boolean> solved){
			if(!solved.containsKey(a()) || !solved.containsKey(b()))
			return false;
			var vA = solved.get(a());
			var vB = solved.get(b());
			var r = switch(op()){
				case OR -> vA | vB;
				case AND -> vA & vB;
				case XOR -> vA ^ vB;
				default -> throw new IllegalArgumentException();
			};
			solved.put(res(), r);
			return true;
		}
		public boolean equiv(op other){
			return other.op == op()
					&& (
					     (other.a().equals(a()) && other.b().equals(b()))
						|| (other.a().equals(b()) && other.b().equals(a()))
					);
		}

		public op with(String res){ return new op(a, b, op, res); }
	}
	@Test
	public void test1() {
		isAddMode = false;
		assertEquals(bi(4), solve(testInput).a());
	}
	@Test
	public void test2() {
		isAddMode = false;
		assertEquals(bi(2024), solve(testInput2).a());
	}
	@Test
	public void testSwap() {
		isAndMode = true;
		assertEquals(bi(4), solve(testInput3).b());
	}
	public static final String testInput = """
	x00: 1
	x01: 1
	x02: 1
	y00: 0
	y01: 1
	y02: 0
	
	x00 AND y00 -> z00
	x01 XOR y01 -> z01
	x02 OR y02 -> z02
	""";
	public static final String testInput2 = """
	x00: 1
	x01: 0
	x02: 1
	x03: 1
	x04: 0
	y00: 1
	y01: 1
	y02: 1
	y03: 1
	y04: 1
	
	ntg XOR fgs -> mjb
	y02 OR x01 -> tnw
	kwq OR kpj -> z05
	x00 OR x03 -> fst
	tgd XOR rvg -> z01
	vdt OR tnw -> bfw
	bfw AND frj -> z10
	ffh OR nrd -> bqk
	y00 AND y03 -> djm
	y03 OR y00 -> psh
	bqk OR frj -> z08
	tnw OR fst -> frj
	gnj AND tgd -> z11
	bfw XOR mjb -> z00
	x03 OR x00 -> vdt
	gnj AND wpb -> z02
	x04 AND y00 -> kjc
	djm OR pbm -> qhw
	nrd AND vdt -> hwm
	kjc AND fst -> rvg
	y04 OR y02 -> fgs
	y01 AND x02 -> pbm
	ntg OR kjc -> kwq
	psh XOR fgs -> tgd
	qhw XOR tgd -> z09
	pbm OR djm -> kpj
	x03 XOR y03 -> ffh
	x00 XOR y04 -> ntg
	bfw OR bqk -> z06
	nrd XOR fgs -> wpb
	frj XOR qhw -> z04
	bqk OR frj -> z07
	y03 OR x01 -> nrd
	hwm AND bqk -> z03
	tgd XOR rvg -> z12
	tnw OR pbm -> gnj
	""";
	public static final String testInput3 = """
x00: 0
x01: 1
x02: 0
x03: 1
x04: 0
x05: 1
y00: 0
y01: 0
y02: 1
y03: 1
y04: 0
y05: 1

x00 AND y00 -> z05
x01 AND y01 -> z02
x02 AND y02 -> z01
x03 AND y03 -> z03
x04 AND y04 -> z04
x05 AND y05 -> z00
	""";
}