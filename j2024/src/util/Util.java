package util;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.BiConsumer;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Util {
  public enum Direction {
	  UP(new Point(-1, 0)),
	  DOWN(new Point(1, 0)),
	  LEFT(new Point(0, -1)),
	  RIGHT(new Point(0, 1));
	  
	  public final Point dir;
	  
	  Direction(Point point) {
	    dir = point;
	  }
	
	  public static Direction read(char c) {
	    return switch(c){
	      case '^' -> Direction.UP;
	      case 'v' -> Direction.DOWN;
	      case '<' -> Direction.LEFT;
	      case '>' -> Direction.RIGHT;
	      default -> throw new RuntimeException(String.format("Invalid direction : %c", c));
	    };
	  }

		public static boolean isDir(char instruction) {
			return switch(instruction) {
				case '^', 'v', '<', '>' -> true;
				default -> false;
			};
		}

    public Direction right() {
      return switch(this){
        case UP -> RIGHT;
        case DOWN -> LEFT;
        case LEFT -> UP;
        case RIGHT -> DOWN;
      };
    }
    public Direction left() {
      return switch(this){
        case UP -> LEFT;
        case DOWN -> RIGHT;
        case LEFT -> DOWN;
        case RIGHT -> UP;
      };
    }
		public char toChar(){
			return switch(this){
				case UP -> '^';
				case DOWN -> 'v';
				case LEFT -> '<';
				case RIGHT -> '>';
			};
		}
	}
	public static class Box<T>{
    private T content;
    public T put(T t){
      T prev = content;
      content = t;
      return prev;
    }
    public T take(){
      return put(null);
    }
    public boolean isPresent(){
      return content != null;
    }
  }
  private Util(){}
  public static boolean isChar(char[][] map, Point pos, char c) {
    int row = pos.row(); int col = pos.col();
    return isValid(map, row, col) && map[row][col] == c;
  }
	public static BigInteger bi(long a) {
		return BigInteger.valueOf(a);
	}
	public static BigInteger bi(String a) {
		return new BigInteger(a);
	}
	public static int pi(String input){
		return Integer.parseInt(input, 10);
	}
  public static record Result(BigInteger a, BigInteger b) {
		public Result(long a, long b) {
			this(bi(a), bi(b));
		}
		public Result(int a, int b) {
			this(bi(a), bi(b));
		}

		public Result add(Result other) {
			return new Result(a.add(other.a), b.add(other.b));
		}
	}

  public static record Point(int row, int col) {
    public Point plus(Point other) {
      return new Point(row + other.row, col + other.col);
    }
		public Point minus(Point b) {
			return new Point(row - b.row, col - b.col);
		}
    @Override
    public final String toString() {
      return String.format("(%d, %d)", row, col);
    }
		public Point plus(Direction dir) {
			return plus(dir.dir);
		}
		public boolean isNeighbour(Point other) {
			return Stream.of(Direction.values()).anyMatch(d -> plus(d).equals(other));
		}
		public Point mod(Point limits) {
			if(row < limits.row() || col < limits.col()) return plus(limits).mod(limits);
			var row = this.row % limits.row;
			var col = this.col % limits.col;
			return new Point(row, col);
		}
		public Point times(int times) {
			return new Point(row * times, col * times);
		}
    public static Point read(String input){
      var spl = input.split(",");
      int col = Integer.parseInt(spl[0]);
      int row = Integer.parseInt(spl[1]);
      return new Point(row, col);
    }
  }
  public static record Pointl(long x, long y) {
    public Pointl plus(Pointl other) {
      return new Pointl(x + other.x, y + other.y);
    }
		public Pointl minus(Pointl b) {
			return new Pointl(x - b.x, y - b.y);
		}
    @Override
    public final String toString() {
      return String.format("(%d, %d)", x, y);
    }
  }

  // character maps
  public static char[] chars(String s) {
    return s.toCharArray();
  }
  public static <T> boolean isValid(T[][] map, Point pos) {
    return isValid(map, pos.row(), pos.col());
  }

  public static <T> boolean isValid(T[][] map, int row, int col) {
    return row >= 0 && row < map.length && col >= 0 && col < map[row].length;
  }
  public static <T> T getAt(T[][] map, Point pos, T defaultV) {
    int row = pos.row(); int col = pos.col();
    if (!isValid(map, pos)) {
      return defaultV;
    }
    return map[row][col];
  }
  public static boolean isValid(boolean[][] map, Point pos) {
    return isValid(map, pos.row(), pos.col());
  }

  public static boolean isValid(boolean[][] map, int row, int col) {
    return row >= 0 && row < map.length && col >= 0 && col < map[row].length;
  }
  public static boolean get(boolean[][] map, Point pos, boolean defaultV) {
    int row = pos.row(); int col = pos.col();
    if (!isValid(map, pos)) {
      return defaultV;
    }
    return map[row][col];
  }
  public static boolean isValid(int[][] map, Point pos) {
    return isValid(map, pos.row(), pos.col());
  }

  public static boolean isValid(int[][] map, int row, int col) {
    return row >= 0 && row < map.length && col >= 0 && col < map[row].length;
  }
  public static int get(int[][] map, Point pos, int defaultV) {
    int row = pos.row(); int col = pos.col();
    if (!isValid(map, pos)) {
      return defaultV;
    }
    return map[row][col];
  }
  public static void set(int[][] map, Point pos, int val) {
    int row = pos.row(); int col = pos.col();
    map[row][col] = val;
  }
  public static void set(boolean[][] map, Point pos, boolean val) {
    int row = pos.row(); int col = pos.col();
    map[row][col] = val;
  }


  public static boolean isValid(char[][] map, Point pos) {
    return isValid(map, pos.row(), pos.col());
  }

  public static boolean isValid(char[][] map, int row, int col) {
    return row >= 0 && row < map.length && col >= 0 && col < map[row].length;
  }
	public static char getChar(char[][] map, int row, int col, char defaultV) {
		if (!isValid(map, row, col)) {
      return defaultV;
    }
    return map[row][col];
	}
  public static char getChar(char[][] map, Point pos, char defaultV) {
    int row = pos.row(); int col = pos.col();
    return getChar(map, row, col, defaultV);
  }
  public static char getChar(char[][] map, Point pos) {
    return getChar(map, pos, '?');
  }
  
  public static void setChar(char[][] map, Point pos, char c) {
    map[pos.row()][pos.col()] = c;
  }

  public static void print(char[][] out) {
    for (int row = 0; row < out.length; row++) {
      for (int col = 0; col < out[row].length; col++) {
        System.out.print(out[row][col]);
      }
      System.out.println();
    }
  }
  public static void print(boolean[][] out) {
    for (int row = 0; row < out.length; row++) {
      for (int col = 0; col < out[row].length; col++) {
        System.out.print(out[row][col]? '#' : '.');
      }
      System.out.println();
    }
  }
	public static char[][] readMap(String input, BiConsumer<Point, Character> process) {
		String[] lines = input.lines().toArray(String[]::new);
    char[][] map = new char[lines.length][];
    for (int row = 0; row < lines.length; row++) {
      var line = lines[row].trim();
      map[row] = line.toCharArray();
			if(process == null)
				continue;
      for(int idx = 0; idx < line.length(); idx++){
        var pt = new Point(row, idx);
        char c = line.charAt(idx);
        process.accept(pt, c);
      }
    }
		return map;
	}

  public static void logln(int indent, String fmt, Object... args) {
    System.out.println(repeat("| ", indent) + String.format(fmt, args));
	}

  public static void logln(String fmt, Object... args) {
    System.out.println(String.format(fmt, args));
  }

	public static String repeat(String s, int times) {
		return new String(new char[times]).replace("\0", s);
	}

  public static String readFile(String string) {
    Path path = Paths.get(string);
    return readFile(path);
  }
	static String readFile(Path path) {
		try {
      return Files.readString(path);
    } catch (IOException e) {
      throw new RuntimeException("File not read : " + path.toAbsolutePath());
    }
	}
  
  public static Pattern SPC = Pattern.compile("\\s+");
  public static Pattern VRG = Pattern.compile(",");

  public static int[] intsSpace(String line) {
    return ints(line, SPC);
  }

  public static int[] ints(String line) {
    return VRG.splitAsStream(line).mapToInt(Integer::parseInt).toArray();
  }
  public static int[] ints(String line, Pattern p) {
    return p.splitAsStream(line).mapToInt(Integer::parseInt).toArray();
  }
  public static int[] ints(String line, String p) {
    String[] split = line.split(p);
    return Stream.of(split).mapToInt(Integer::parseInt).toArray();
  }
  public static long[] longs(String line, String p) {
    String[] split = line.split(p);
    return Stream.of(split).mapToLong(Long::parseLong).toArray();
  }
	public static int digit(char c) {
		if (c >= '0' && c <= '9') {
			return c - '0';
		}
		throw new IllegalArgumentException("Not a digit : " + c);
	}

}
