package util;

import static util.Util.*;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Pattern;

import util.Util.Result;

public abstract class Day {
	private static final Path PATH_TO_INPUTS = Paths.get("days");
	protected final int dayNumber;
	protected Path inputPath;
	private String dayName;
	
	protected Day(){
		String name = this.getClass().getSimpleName();
		Pattern pattern = Pattern.compile("Day_(\\d+)");
		var matcher = pattern.matcher(name);
		if(!matcher.find())
		throw new RuntimeException("Day class name does not match pattern");
		
		dayNumber = Integer.parseInt(matcher.group(1));
		dayName = String.format("day_%02d.txt", dayNumber);
		inputPath = PATH_TO_INPUTS.resolve(dayName);
		// logln("Created day %d %s %s", dayNumber, dayName, inputPath);
	}

	public void run(){
		Result result = solve();
		logln("%d", result.a());
		logln("%d", result.b());
	}

	public Result solve(){
		return solve(readInput());
	}

	protected String readInput() {
		return Util.readFile(inputPath);
	}
	
	public abstract Result solve(String input);
}
