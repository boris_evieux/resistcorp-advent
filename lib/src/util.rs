use std::{error, error::Error};
use std::{env, fs, fs::File, fmt};
use std::io::{Read, Write, BufReader, BufRead};
use std::ops::RangeBounds;
use serde::{Deserialize, Serialize};

pub type Day = fn(&DayInput, &Params)->(String, String);

pub type Lines<'a> = Vec<&'a  str>;
#[derive(Clone)]
pub struct DayInput (String);

impl DayInput{
    pub fn linesv(&self) -> Lines{
        self.0.lines().collect()
    }
    pub fn linesi(&self) -> std::str::Lines{
        self.0.lines()
    }
    pub fn trim(&self) -> &str{
        self.0.trim()
    }
}

impl Into<String> for DayInput{
    fn into(self) -> String{
        self.0.to_string()
        // String::from_utf8(self.0.to_vec()).expect("bad input")
    }
}

impl <'a> From<&'a String> for DayInput{
    fn from(s : &'a String) -> Self{
        DayInput(s.to_string())
    }
}
impl From<String> for DayInput{
    fn from(s : String) -> Self{
        DayInput(s)
    }
}

impl <'a> From<&'a str> for DayInput{
    fn from(s : &'a str) -> Self{
        DayInput(s.to_string())
    }
}

impl<'a> Into<&'a str> for &'a DayInput{
    fn into(self) -> &'a str{
        &self.0
        // std::str::from_utf8(self.0).expect("bad input")
    }
}

impl<'a> From<&'a[u8]> for DayInput{
    fn from( val : &'a[u8]) -> Self{
        DayInput(String::from_utf8(val.to_vec()).expect("not utf8"))
    }
}
impl<'a> Into<&'a[u8]> for &'a DayInput{
    fn into(self) -> &'a[u8]{
        self.0.as_bytes()
    }
}

// Change the alias to `Box<dyn error::Error>`.
pub type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

#[derive(Debug)]
pub struct Params{
    pub year : Option<usize>,
    pub day : Option<usize>,
    pub runs : usize,
    pub parallel : bool,
    pub optional_file : Option<String>
}
#[derive(Debug)]
struct ParamError<'a> (&'a str);
impl Error for ParamError<'_>{}
impl fmt::Display for ParamError<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Params{
    pub fn new(year : Option<usize>, day : Option<usize>) -> Self{
        let runs = 1;
        let parallel = false;
        let optional_file = None;
        let mut p = Self{year, day, runs, parallel, optional_file};
        let args: Vec<String> = env::args().skip(1).collect();
        for arg in args {
            p.parse(&arg);
        }
        p
    }
    pub fn parse(&mut self, arg : &str) -> bool{
        let int = arg.parse::<usize>().ok();
         if let Some(i) = int {
            if self.year.is_none() {
                self.year = Some(i);
                true
            }else if self.day.is_none(){
                self.day = Some(i);
                true
            } else {
                self.runs = i;
                true
            }
        }else if arg == "-p" {
            //TODO : parallelize
            true
        }else if fs::metadata(arg).is_ok() {
            self.optional_file = Some(arg.to_owned());
            true
        } else {
            false
        }
    }
    pub fn is_ready(&mut self) -> bool{
        if let Some(file) = &self.optional_file{
            use regex::{Regex};
            if self.year.is_none(){
                let re = Regex::new(r"(\d{4})").unwrap();
                self.year = re.captures(file).and_then(|c|c.get(1))
                        .and_then(|m|Some(m.as_str()))
                        .and_then(|m|m.parse::<usize>().ok());
            }
            if self.day.is_none(){
                let re = Regex::new(r"day_(\d\d).txt").unwrap();
                self.day = re.captures(file).and_then(|c|c.get(1))
                        .and_then(|m|Some(m.as_str()))
                        .and_then(|m|m.parse::<usize>().ok());
            }
        }
        self.year.is_some() && self.day.is_some()
    }
    pub fn has_file(&self) -> bool{
        self.optional_file.is_some()
    }
    pub fn open(&mut self) -> Result<File>{
        if let Some(file) = &self.optional_file{
            use regex::{Regex};
            if self.year.is_none(){
                let re = Regex::new(r"(\d{4})").unwrap();
                self.year = re.captures(file).and_then(|c|c.get(1))
                        .and_then(|m|Some(m.as_str()))
                        .and_then(|m|m.parse::<usize>().ok());
            }
            if self.day.is_none(){
                let re = Regex::new(r"day_(\d\d).txt").unwrap();
                self.day = re.captures(file).and_then(|c|c.get(1))
                        .and_then(|m|Some(m.as_str()))
                        .and_then(|m|m.parse::<usize>().ok());
            }
            
            Ok(File::open(file)?)
        }else{
            Err(Box::new(ParamError("no file given")))
        }
    }
}
#[derive(Serialize, Deserialize)]
struct Expectation {
    day: usize,
    part1: String,
    part2: String,
}
pub fn do_tests(_year : usize, days_to_run : impl RangeBounds<usize>, days : &[Day]) -> usize{
    let capture_args = Some("all_solutions".to_string()) ==  std::env::args().nth(1);

    let data = fs::read_to_string("./input/expected.json").expect("expected result file not found");
  let results: Vec<Expectation> = serde_json::from_str(&data).expect("error parsing expected data");

  for result in &results {
    let d = result.day;
    if days_to_run.contains(&d) {
        let day_num = format!("{:02}", d);
        let day_file = format!("./input/day_{}.txt", day_num);
        println!("DAY {:?} ({})", d, day_file);
        let data : String = fs::read_to_string(day_file).expect("input file not found");
        let data :DayInput = data[..].into();
        let (part1, part2) = if capture_args {
            let mut log = String::new();
            use gag::BufferRedirect;
            let mut print_redirect = BufferRedirect::stdout().unwrap();
            let r = days[d-1](&data, &Params::new(None, None));
            print_redirect.read_to_string(&mut log).expect("couldn't capture log");
            assert_eq!(&log, "", "day {:?} logged to stdin : {:?}", d, log);
            r
        }else{
            days[d-1](&data, &Params::new(None, None))

        };
        assert_eq!(&result.part1, &part1, "part 1 of day {:?} failed, expected {:?}, got {:?}", d, result.part1, part1);
        assert_eq!(&result.part2, &part2, "part 2 of day {:?} failed, expected {:?}, got {:?}", d, result.part2, part2);
        // let mut output: Vec<u8> = Vec::new();
        // run_day_io(days[d-1], &mut Params::new(None, None), &mut data.as_bytes(), &mut output).expect("");
        // let lines = std::str::from_utf8(&output).unwrap().lines().collect::<Vec<&str>>();
        // assert_eq!(lines.len(), 2);
        // assert_eq!(lines[0], result.part1);
        // assert_eq!(lines[1], result.part2);

        println!(">>> ok");
    }
  }
  results.len()
}

pub fn run_day(fun : Day, year : usize, day : usize) {
    let mut p = Params::new(Some(year), Some(day));
    let r = run_day_io(fun, &mut p, &mut std::io::stdin(), &mut std::io::stdout());

    if let Err(e) = r {
        eprintln!("error running day{}/{} :\n{}", day, year, e);
    }
}

pub fn run_day_file(day : Day, year : usize, num : usize) -> (String, String) {
    let input = fs::read_to_string(format!("./input/day_{:02}.txt", num)).expect("file!!!");
    let input = input[..].into();
    day(&input, &Params::new(None, None))
}

pub fn run_day_io(day : Day,
    p: &mut Params,
    input: &mut impl Read,
    output: &mut impl Write
    ) -> Result<()> {

    let mut buffer = String::with_capacity(10*1024*1024);

    let mut reader : Box<dyn BufRead> = if p.has_file(){
        let f = p.open().unwrap();
        Box::new(BufReader::new(f)) 
    }else{
        Box::new(BufReader::new(input))
    };

    if let Err(e) = reader.read_to_string(&mut buffer) {
        write!(output, "NO")?;
        write!(output, "INPUT")?;
        Err(Box::new(e))
    }else {
        let input = buffer.into();
        let result = match p.runs {
            0 => ("zero".to_string(), "iterations".to_string()),
            1 => day(&input, p),
            runs => {
                for _ in 1..runs{
                    let _ = day(&input, p);
                }
                day(&input, p)
            }
        };

        write!(output, "{}\n", result.0)?;
        write!(output, "{}\n", result.1)?;

        Ok(())


    }
}
#[macro_export]
macro_rules! make_main {
    ($year: expr, $day : expr) => {
        use lib::util::{DayInput, Params, run_day};
        pub fn main(){
            run_day(self::run, $year, $day);
        }
    };
}

#[macro_export]
macro_rules! test_run {
    ($input: expr, $p1: expr, $p2 : expr) => {
        assert_eq!(super::run(&$input.into(), &lib::util::Params::new(None, None)), ($p1.to_string(), $p2.to_string()));
    };
}

#[macro_export]
macro_rules! test_run_f {
    ($f: expr, $input: expr, $p1: expr, $p2 : expr) => {
        assert_eq!($f(&$input.into(), &lib::util::Params::new(None, None)), ($p1.to_string(), $p2.to_string()));
    };
}
#[macro_export]
macro_rules! test_run1 {
    ($input: expr, $p1: expr) => {
        assert_eq!(super::run(&$input.into(), &lib::util::Params::new(None, None)).0, $p1.to_string());
    };
}
#[macro_export]
macro_rules! test_run2 {
    ($input: expr, $p2: expr) => {
        assert_eq!(super::run(&$input.into(), &lib::util::Params::new(None, None)).1, $p2.to_string());
    };
}

#[macro_export]
macro_rules! make_days {
    ($last: expr) => {
        use lib::seq_macro::seq;
        use lib::util::{Params, DayInput};

        seq!(N in 01..=$last {
            pub mod day#N;
        });

        seq!{N in 01..=$last {
            const DAYS: [fn(&DayInput, &Params)->(String, String); $last] = [
                #(day#N::run,)*
            ];
        }}
        pub fn all()->&'static[fn(&DayInput, &Params)->(String, String)]{
            &DAYS
        }
    };
}
