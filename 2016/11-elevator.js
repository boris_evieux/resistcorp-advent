const input = [//the starting state
	//hand-parsed :)
	//lowercase = chip, uppercase = generator
	["S", "s", "P", "p"],
	["T", "r", "R", "C", "c"],
	["t"],
	[],
];

function cloneState(state){
	const ret = [
		[...state[0]],
		[...state[1]],
		[...state[2]],
		[...state[3]]
	];
	ret.elevatorPos = state.elevatorPos || 0;
	return ret;
}

function possibleNextStates(state){
	const 	ret = [],
			startPos = state.elevatorPos,
			startFl = state[startPos];
	for(let i1 = startFl.length -1; i1 >= 0; --i1){
		const 	toMove = startFl[i1];
		//test moving one thing
		if(startPos > 0){
			const testState = doMove(state, -1, i1, -1);
			if(isStateValid(testState))
				ret.push(testState);
		}
		if(startPos < 3){
			const testState = doMove(state, +1, i1, -1);
			if(isStateValid(testState))
				ret.push(testState);
		}
		for(let i2 = i1 -1; i2 >= 0; --i2){
			const 	toMove = startFl[i2];
			//test moving one thing
			if(startPos > 0){
				const testState = doMove(state, -1, i1, i2);
				if(isStateValid(testState))
					ret.push(testState);
			}
			if(startPos < 3){
				const testState = doMove(state, +1, i1, i2);
				if(isStateValid(testState))
					ret.push(testState);
			}
		}
	}
	return ret;
}

//i2 is optionnal, pass -1 if not used
function doMove(state, direction, i1, i2){
	const newState = cloneState(state);
	let startElPos = state.elevatorPos,
		endElPos = startElPos + direction;
	const value1 = newState[startElPos][i1]
	newState[startElPos].splice(i1, 1);
	newState[endElPos].push(value1);
	newState.elevatorPos = endElPos;
	if(i2 > -1){
		const value2 = newState[startElPos][i2]
		newState[startElPos].splice(i2, 1);
		newState[endElPos].push(value2);
		// console.log("moved", value1, "and", value2, "from", startElPos, "to", endElPos);
	}else{
		// console.log("moved", value1, "from", startElPos, "to", endElPos);
	}
	// console.log("params :", i1, i2, direction, startElPos);
	// console.log("before :", state);
	// console.log("after :", newState);
	return newState;
}
function floorIsValid(floor){
	// console.log("    determining validitiy of floor :", floor)
	const chips = floor.filter( chipFilter );
	if(chips.length === 0){
		// console.log("    VALID because no chip.")
		return true;
	}
	const gens = floor.filter( floorFilter );
	if(gens.length === 0){
		// console.log("    VALID because no gen.")
		return true;
	}

	for(let chip of chips ){
		if( ! gens.includes(chip.toUpperCase()) ){
			// console.log("    INVALID because.",  chip);
			return false;
		}
	}
	// console.log("    VALID.")
	return true;
}
const areStatesDifferent = (st1, st2) => !areStatesEqual(st1, st2);
function areStatesEqual(st1, st2){
	if(st1.elevatorPos !== st2.elevatorPos)
		return false;
	for(let i = 0; i < 4; ++i){
		const 	fl1 = st1[i],
				fl2 = st2[i];
		if(fl1.length != fl2.length)
			return false;
		for(let j of fl1){
			if(!fl2.includes(j))
				return false;
		}
	}
	return true;
};
const isStateWinning = state => state[3].length === 10;
const isStateLosing  = state => !isStateWinning(state);
function isStateValid(state){
	// console.log("determining validitiy of state :", state)
	if( state.every(floorIsValid) ){
		// console.log("VALID")
		return true;
	}
	// console.log("INVALID")
	return false;
}
// floors are uppercase
const floorFilter = obj => obj.toUpperCase() === obj;
// chips are lowercase
const chipFilter = obj => obj.toLowerCase() === obj;

//OK let's do this!
function main(){
	let moves = 0,
		possibleStates = [
			cloneState(input)
		], pastStates = [];
	while(possibleStates.every(isStateLosing)){
		moves ++
		const next = [];
		for(let possible of possibleStates){
			next.push(...possibleNextStates(possible))
		}
		if(moves === 20){
			console.error("too many steps taken !")
			return;
		}
		console.log("we count", next.length, "possible steps after", moves, "moves.")
		pastStates.push(...possibleStates);
		possibleStates = next.filter(
			st1 => 
				pastStates.every(
					st2 => areStatesDifferent(st1, st2)
				)
			);
		console.log("once compressed, that's", possibleStates.length)
	}
	console.log("finished after", moves, "moves !");
	console.log("here is the winning state :");
	console.log(possibleStates.filter(isStateWinning)[0]);
}
main();
