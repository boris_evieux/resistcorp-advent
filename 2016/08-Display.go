package main

import "fmt"
import "os"
import "bufio"

import "strings"
import "strconv"

const H int = 6
const W int = 50

type display [H][W]bool

var Atoi func(string) (int, error) = strconv.Atoi

func main() {
	file, err := os.Open("08-Display.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	disp := display{}
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		str := scanner.Text()
		do(str, &disp)
	}

	fmt.Println("total", countOn(&disp))
}

func countOn(disp *display) int {
	ret := 0
	for _, arr := range disp {
		for _, val := range arr {
			if val {
				ret++
			}
		}
	}
	return ret
}

func do(action string, disp *display) {
	instructions := strings.Split(action, " ")
	if len(instructions) < 1 {
		return
	}
	switch instructions[0] {
	case "rect":
		split := strings.Split(instructions[1], "x")
		w, _ := Atoi(split[0])
		h, _ := Atoi(split[1])
		for y := 0; y < h; y++ {
			for x := 0; x < w; x++ {
				disp[y][x] = true
			}
		}

		break
	case "rotate":
		index, _ := Atoi(strings.Split(instructions[2], "=")[1])
		number, _ := Atoi(instructions[4])
		if instructions[1] == "row" {
			for number > 0 {
				rotateRow(index, disp)
				number--
			}
		} else if instructions[1] == "column" {
			for number > 0 {
				rotateColumn(index, disp)
				number--
			}
		}
		break
	}
	fmt.Println("after", action)
	fmt.Println(disp)

}

func rotateColumn(index int, disp *display) {
	prev := disp[H-1][index]
	for i := 0; i < H; i++ {
		disp[i][index], prev = prev, disp[i][index]
	}
}

func rotateRow(index int, disp *display) {
	prev := disp[index][W-1]
	for i := 0; i < W; i++ {
		disp[index][i], prev = prev, disp[index][i]
	}
}

func (disp *display) String() string {
	ret := ""
	for _, arr := range disp {
		for _, val := range arr {
			if val {
				ret += "#"
			} else {
				ret += "."
			}
		}
		ret += "\n"
	}
	return ret
}
