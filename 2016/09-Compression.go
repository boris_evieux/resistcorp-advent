package main

import "fmt"
import "strconv"
import "io/ioutil"


type section struct{
	index int
	text string
	length int
	repCount int
	repLength int
}
func main() {
	const phase int = 2;
	data, err := ioutil.ReadFile("09-Compression.txt")
	if err != nil{
		panic("error reading file")
	}

	currentTally, ok := treatDataSection(data[:], phase)
	if ok {
		fmt.Println("done. here's the final length : ")
		fmt.Println(strconv.FormatInt(currentTally, 10))
	}else{
		panic("error parsing main section")
	}
}
func treatDataSection(data []byte, phase int) (int64, bool){
	tally := int64(0)
	for index := 0; index < len(data); {
		section, ok := readSection(data, index)
		if !ok {
			panic("error reading section at col " + strconv.Itoa(index))
		}
		index += section.length
		if section.repCount != 0 {
			if phase == 1{
				tally += int64(section.repCount * section.repLength)
			}else{
				subTally, ok := treatDataSection(data[index:index + section.repLength], phase)
				if !ok {
					return -1, false
				}
				tally += subTally * int64(section.repCount)
			}
			index += section.repLength
		}else{
			tally += int64(section.length)
		}
	}
	return tally, true
}
func readSection(data []byte, index int) (*section, bool){
	section := section{}
	section.index = index

	if(data[index] == '('){
		//marker section
		xIndex := -1
		i := index+1
		for ; data[i] != ')'; i++{
			if data[i] == 'x' {
				xIndex = i
			}
		}
		if xIndex == -1 {
			return nil, false
		}
		repLength, errA := strconv.Atoi(string(data[index+1:xIndex]))
		repCount, errB := strconv.Atoi(string(data[xIndex+1:i]))
		if(errA != nil ||errB != nil){
			fmt.Println("error parsing rep data")
			return nil, false
		}
		section.repCount  = repCount
		section.repLength = repLength
		section.length = i-index+1
	}else{
		i := index
		for ; i<len(data) && data[i] != '(';i++{}
		section.length = i-index
	}
	section.text = string(data[index:index+section.length])
	// fmt.Printf("section : %v", section)
	// fmt.Println()
	return &section, true
}