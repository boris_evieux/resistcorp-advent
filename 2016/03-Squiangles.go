package main

import "fmt"
import "os"
import "bufio"

import "strings"
import "sort"
import "regexp"

import "strconv"

func main() {
	file, err := os.Open("03-Squiangles.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)

	reg := regexp.MustCompile(`\s+`)
	total, legit := 0, 0
	for scanner.Scan() {
		str := strings.Trim(scanner.Text(), " ")
		tri1 := reg.Split(str, -1)
		scanner.Scan()
		str = strings.Trim(scanner.Text(), " ")
		tri2 := reg.Split(str, -1)
		scanner.Scan()
		str = strings.Trim(scanner.Text(), " ")
		tri3 := reg.Split(str, -1)

		for i := 0; i < 3; i++ {

			a, _ := strconv.Atoi(tri1[i])
			b, _ := strconv.Atoi(tri2[i])
			c, _ := strconv.Atoi(tri3[i])
			fmt.Println(a, b, c)

			triI := []int{a, b, c}

			sort.Ints(triI)
			a, b, c = triI[0], triI[1], triI[2]
			total++
			if (a + b) > c {
				fmt.Println("OK", a, b, c)
				legit++
			} else {
				fmt.Println("NOK", a, b, c)
			}
		}
	}
	fmt.Printf("%d triangles (%d legit)", total, legit)
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}
func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
