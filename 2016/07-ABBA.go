package main

import "fmt"
import "os"
import "bufio"

import "strings"
import "regexp"

func main() {
	file, err := os.Open("07-ABBA.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()
	regBrackets := regexp.MustCompile(`\[[a-z]{4,}\]`)
	isABBAString := func(str string) bool {
		for i := 0; i < len(str)-3; i++ {
			if str[i] != str[i+1] && str[i] == str[i+3] && str[i+1] == str[i+2] {
				return true
			}
		}
		return false
	}
	isABBAProtocol := func(str string) bool {
		subs := regBrackets.FindAllStringSubmatch(str, -1)
		for _, sub := range subs {
			if isABBAString(sub[0]) {
				return false
			}
		}
		for _, sub := range regBrackets.Split(str, -1) {
			if isABBAString(sub) {
				return true
			}
		}
		return false
	}
	isABAProtocol := func(str string) bool {
		newStr := strings.Join(regBrackets.Split(str, -1), "___")
		hypernets := strings.Join(regBrackets.FindAllString(str, -1), "___")
		fmt.Println(newStr, "|", hypernets, "<<<", str)
		for i := 0; i < len(newStr)-2; i++ {
			a, b := newStr[i], newStr[i+1]
			aba := fmt.Sprintf("%c%c%c", a, b, a)
			bab := fmt.Sprintf("%c%c%c", b, a, b)
			if a != b && a == newStr[i+2] {
				index := strings.Index(hypernets, bab)
				if index != -1 {
					fmt.Println("found", aba, "at", i, "and", bab, "at", index)
					return true
				}
				fmt.Println(hypernets, "contains NOT", bab, strings.Index(hypernets, bab))
			}
		}
		return false
	}
	scanner := bufio.NewScanner(file)

	numberABBA := 0
	numberABA := 0

	for scanner.Scan() {
		str := scanner.Text()
		if isABBAProtocol(str) {
			numberABBA++
		}
		if isABAProtocol(str) {
			numberABA++
		}
	}

	fmt.Println("done :")
	fmt.Println("ABBA :", numberABBA)
	fmt.Println("ABA :", numberABA)
}
