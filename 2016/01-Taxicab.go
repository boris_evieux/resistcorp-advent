package main

import "fmt"
import "strings"
import "strconv"

func main() {
	input := strings.Split(
		"L1, R3, R1, L5, L2, L5, R4, L2, R2, R2, L2, R1, L5, R3, L4, L1, L2, R3, R5, L2, R5, L1, R2, L5, R4, R2, R2, L1, L1, R1, L3, L1, R1, L3, R5, R3, R3, L4, R4, L2, L4, R1, R1, L193, R2, L1, R54, R1, L1, R71, L4, R3, R191, R3, R2, L4, R3, R2, L2, L4, L5, R4, R1, L2, L2, L3, L2, L1, R4, R1, R5, R3, L5, R3, R4, L2, R3, L1, L3, L3, L5, L1, L3, L3, L1, R3, L3, L2, R1, L3, L1, R5, R4, R3, R2, R3, L1, L2, R4, L3, R1, L1, L1, R5, R2, R4, R5, L1, L1, R1, L2, L4, R3, L1, L3, R5, R4, R3, R3, L2, R2, L1, R4, R2, L3, L4, L2, R2, R2, L4, R3, R5, L2, R2, R4, R5, L2, L3, L2, R5, L4, L2, R3, L5, R2, L1, R1, R3, R3, L5, L2, L2, R5",
		", ")

	locations := make(map[string]struct{})
	empty := struct{}{}
	h, v := 0, 0
	curFIndex := 0

	functions := []func(){
		func() { h++ },
		func() { v++ },
		func() { h-- },
		func() { v-- },
	}
	for _, s := range input {
		dir := s[0]
		num, _ := strconv.Atoi(s[1:])
		if dir == 'L' {
			fmt.Println("turning left")
			if curFIndex <= 0 {
				curFIndex = 3
			} else {
				curFIndex--
			}
		} else {
			fmt.Println("turning right")
			if curFIndex >= 3 {
				curFIndex = 0
			} else {
				curFIndex++
			}
		}
		fmt.Println("moving ", num)
		for i := 0; i < num; i++ {
			functions[curFIndex]()
			fmt.Println(h, v)
			location := strconv.Itoa(h) + " " + strconv.Itoa(v)
			_, ok := locations[location]
			if ok {
				fmt.Println("hm... already gotten here!")
				ha := abs(h)
				va := abs(v)
				fmt.Println(ha + va)
				return
			} else {
				locations[location] = empty
			}
		}
	}
	if h < 0 {
		h = -h
	}
	if v < 0 {
		v = -v
	}

	fmt.Println(h + v)
}

func abs(a int) int {
	if a < 0 {
		return -a
	}
	return a
}
