package main

import "fmt"
import "os"
import "bufio"

import "strings"
import "regexp"

import "sort"
import "strconv"

func main() {
	file, err := os.Open("04-Rooms.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	reg := regexp.MustCompile(`(\d+)\[([a-z]+)\]`)
	scanner := bufio.NewScanner(file)

	total, legit := 0, 0
	for scanner.Scan() {
		counts := make(map[rune]int)
		split := strings.Split(scanner.Text(), "-")
		for _, s := range split[:len(split)-1] { //iterate over all -1
			for _, c := range s {
				val, has := counts[c]
				if has {
					counts[c] = val + 1
				} else {
					counts[c] = 1
				}
			}
		}
		list := make(RuneCountList, len(counts))
		i := 0
		for key, val := range counts {
			list[i] = Pair{key, val}
			i++
		}
		sort.Sort(list)
		match := reg.FindStringSubmatch(split[len(split)-1])
		//		fmt.Println(split)
		//		fmt.Println(list, list.StringFirstFive())
		//		fmt.Println(match[2], list.StringFirstFive(), match[1])
		total++
		if match[2] == list.StringFirstFive() {
			id, _ := strconv.Atoi(match[1])
			legit += id
			name := RoomName(split[:len(split)-1], id)
			if strings.HasPrefix(name, "northpole") {
				fmt.Println(name, "is legit (", id, ")")
			}
		} else {
			//fmt.Println("is NOT legit !")
		}
	}
	fmt.Printf("%d rooms (%d legit)", total, legit)
}

type Pair struct {
	Key   rune
	Value int
}

type RuneCountList []Pair

func (p Pair) String() string {
	return fmt.Sprintf("%c : %d", p.Key, p.Value)
}
func (l RuneCountList) String() string {
	ret := "[ "
	for i, p := range l {
		if i == 0 {
			ret += p.String()
		} else {
			ret += " | " + p.String()
		}
	}
	return ret + " ]"
}
func (l RuneCountList) StringFirstFive() string {
	ret := ""
	for i := 0; i < 5; i++ {
		ret += string(l[i].Key)
	}
	return ret
}

func (l RuneCountList) Len() int { return len(l) }
func (l RuneCountList) Less(i, j int) bool {
	if l[i].Value != l[j].Value {
		return l[i].Value > l[j].Value
	}
	return l[i].Key < l[j].Key
}
func (l RuneCountList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }

func RoomName(list []string, id int) string {
	ret := ""
	for i, s := range list {
		if i > 0 {
			ret += " "
		}
		ret += Rotate(s, id)
	}

	return ret
}
func Rotate(s string, id int) string {
	num := id % 26
	ret := ""
	for _, c := range s {
		if c == '-' {
			ret += " "
			continue
		}
		c = rune(int(c) + num)
		if c > 'z' {
			c -= 26
		}
		ret += string(c)
	}

	return ret
}
