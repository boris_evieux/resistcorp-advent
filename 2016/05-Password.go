package main

import "fmt"
import "strconv"

import "crypto/md5"
import "encoding/hex"

const input = "uqwqemis"

//const input = "abc"

func main() {
	pass := "________"
	index := 0
	done := 0
	//indices := make(chan int, 8)
	//done := make(chan string, 8)

	for done < 8 && index < 1000000000 {
		test := []byte(input + strconv.Itoa(index))
		//fmt.Println("testing", test)
		sum := md5.Sum(test)
		h := hex.EncodeToString(sum[0:])
		if h[0:5] == "00000" {
			//var bsum []byte = sum[:]
			c := string(h[5])
			p := string(h[6])
			fmt.Println("sum at", index, ":", h, "(", c, p, ")")
			index, err := strconv.Atoi(c) //another index :)
			if err == nil && index < 8 && pass[index] == '_' {
				if index == 0 {
					pass = p + pass[1:]
				} else {
					pass = pass[0:index] + p + pass[index+1:]
				}
				done++
				fmt.Println("pass :", pass, done)
			}
		}
		index++
	}
	fmt.Println(pass)
}
func Test(index int) string {
	return ""
}
