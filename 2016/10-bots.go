package main

import "os"
import "fmt"
import "bufio"
import "regexp"
import "strconv"

type bot struct{
	lowDest int//positive : bot, negative : output 
	highDest int
	holding int
	id int
}
const INVALID = -11111111111111
func (b *bot) acceptValue(val int, bots *[]bot, outputs *[]int) {
	// fmt.Printf("giving value %d to bot %d, %v", val, b.id, b)
	// fmt.Println()
	if b.holding != -1 {
		// fmt.Printf(">>>passing")
		// fmt.Println()
		min, max := minMax(val, b.holding)

		// fmt.Printf(" min and max are %d and %d", min, max)
		// fmt.Println()

		b.holding = -1
		if b.highDest == INVALID || b.lowDest == INVALID {
			panic(fmt.Sprintf("bot %d is not ready to accept second value.", b.id))
		}
		if min == 17 && max == 61 {
			fmt.Printf("!!!!!!!!!!!!!! bot %d is THE ONE. !!!!!!!!!!!!!!!!!!!!!", b.id)
			fmt.Println()
		}
		if(b.lowDest >= 0){
			(*bots)[b.lowDest].acceptValue(min, bots, outputs)
		}else{
			(*outputs)[-1-b.lowDest] = min
		}

		if(b.highDest >= 0){
			(*bots)[b.highDest].acceptValue(max, bots, outputs)
		}else{
			(*outputs)[-1-b.highDest] = max
		}

	}else{
		b.holding = val
	}
}
func minMax(a,b int) (int, int) {
	if(a < b){
		return a, b
	}
	return b, a
}
func main() {
	values := make([]int, 0, 210)//hardcoded the number of bots ...
	outputArray := [1000]int{}

	file, err := os.Open("10-bots.txt")
	if err != nil {
		fmt.Println(err)
		panic("couldn't read file!")
	}
	defer file.Close()

	botArray := [210]bot{}//hardcoded the number of bots ...
	for i := 0; i <210; i++ {
		thebot := &botArray[i]
		thebot.id = i;
		thebot.holding = -1
		thebot.lowDest = INVALID
		thebot.highDest = INVALID
	}
	bots := botArray[:]
	outputs := outputArray[:]

	regBotConfig := regexp.MustCompile(`bot (\d+) gives low to (bot|output) (\d+) and high to (bot|output) (\d+)`)
	regBotInput := regexp.MustCompile(`value (\d+) goes to bot (\d+)`)
	scanner := bufio.NewScanner(file)
	line := 0
	delayedInputs := make([]string, 0, 100)

	for scanner.Scan() {
		line ++
		str := scanner.Text()
		config := regBotConfig.FindStringSubmatch(str)
		if config != nil {
			botId,_  := strconv.Atoi(config[1])
			thebot := &bots[botId]
			botIdL,_  := strconv.Atoi(config[3])

			if config[2] == "bot" {
				thebot.lowDest =  botIdL
			}else{
				thebot.lowDest = -1-botIdL
			}

			botIdH,_  := strconv.Atoi(config[5])
			if config[4] == "bot" {
				thebot.highDest = botIdH
			}else{
				thebot.highDest = -1-botIdH
			}
		}else{
			// fmt.Println("delaying " + str)
			delayedInputs = append(delayedInputs, str)
		}
	}
	for _, str := range(delayedInputs) {
		// fmt.Println("accepting " + str)
		input := regBotInput.FindStringSubmatch(str)
		fmt.Println(input)
		if input != nil {
			value, _ := strconv.Atoi(input[1])
			botId,_  := strconv.Atoi(input[2])
			thebot := &bots[botId]
			index := len(values)
			values := append(values, value)
			thebot.acceptValue(values[index], &bots, &outputs)
		}else{
			panic("line is not recognized : " + str)
		}
	}
	fmt.Println(outputs)
	fmt.Printf("outputs : %v", outputs[0] * outputs[1] * outputs[2])
	fmt.Println()

}