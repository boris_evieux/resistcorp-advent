package main

import "fmt"
import "os"
import "sort"
import "bufio"

func main() {
	file, err := os.Open("06-Repeats.txt")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	repetitions := [8]map[rune]int{
		make(map[rune]int),
		make(map[rune]int),
		make(map[rune]int),
		make(map[rune]int),
		make(map[rune]int),
		make(map[rune]int),
		make(map[rune]int),
		make(map[rune]int),
	}
	for scanner.Scan() {
		str := scanner.Text()
		for i, c := range str {
			counts := repetitions[i]
			val, has := counts[c]
			if has {
				counts[c] = val + 1
			} else {
				counts[c] = 1
			}
		}
	}
	lists := [8]RuneCountList{
		make(RuneCountList, 26),
		make(RuneCountList, 26),
		make(RuneCountList, 26),
		make(RuneCountList, 26),
		make(RuneCountList, 26),
		make(RuneCountList, 26),
		make(RuneCountList, 26),
		make(RuneCountList, 26),
	}
	for i, counts := range repetitions {
		list := lists[i]
		id := 0
		for key, val := range counts {
			list[id] = Pair{key, val}
			id++
		}
		sort.Sort(sort.Reverse(list))

	}
	msg := ""
	for _, list := range lists {
		fmt.Println(list[0])
		msg += string(list[0].Key)
	}
	fmt.Println("done :")
	fmt.Println(lists[0])
	fmt.Println(msg)
}

type Pair struct {
	Key   rune
	Value int
}

type RuneCountList []Pair

func (p Pair) String() string {
	return fmt.Sprintf("%c : %d", p.Key, p.Value)
}
func (l RuneCountList) String() string {
	ret := "[ "
	for i, p := range l {
		if i == 0 {
			ret += p.String()
		} else {
			ret += " | " + p.String()
		}
	}
	return ret + " ]"
}
func (l RuneCountList) StringFirstFive() string {
	ret := ""
	for i := 0; i < 5; i++ {
		ret += string(l[i].Key)
	}
	return ret
}

func (l RuneCountList) Len() int { return len(l) }
func (l RuneCountList) Less(i, j int) bool {
	if l[i].Value != l[j].Value {
		return l[i].Value > l[j].Value
	}
	return l[i].Key < l[j].Key
}
func (l RuneCountList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
