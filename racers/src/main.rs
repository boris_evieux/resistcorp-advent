use lib::util::DayInput;
use std::io::{Read, Write};
use lib::util::*;

use ::y2022::days as days2022;
use ::y2021::days as days2021;
use ::y2020::days as days2020;

fn main() {
    let mut p : Params = Params::new(None, None);

    let _ = run(&mut p, &mut std::io::stdin(), &mut std::io::stdout());
}
pub fn invalid_day(_data : &DayInput, _p : &Params)->(String, String){
    ("FUCK".to_string(), "SHIT".to_string())
}

const NO_DAYS : [fn(&DayInput, &Params)->(String, String); 0] = [];
pub fn day(year : usize, i_1based : usize) ->Day {
    let days = match year{
        2020 => days2020::all(),
        2021 => days2021::all(),
        2022 => days2022::all(),
        _ => &NO_DAYS
    };
    match i_1based {
        i if i <= days.len() => days[i-1],
        _ => invalid_day
    } 
}

fn run(
    p : &mut Params,
    input: &mut impl Read,
    output: &mut impl Write

) -> Result<()> {
    if !p.is_ready() {
        panic!("not enough parameters given");
    }
    let year = p.year.as_ref().ok_or("no year")?;
    let day_n = p.day.as_ref().ok_or("no day")?;

    let day = day(*year, *day_n);
    run_day_io(day, p, input, output)
}
#[cfg(test)]
mod tests {
    use super::*;
    use std::fs;

    #[test]
    fn fails_on_bad_days() {
        let mut output: Vec<u8> = Vec::new();
        let mut p = Params::new(Some(2021), Some(100));
        run(&mut p, &mut "\n".as_bytes(), &mut output).unwrap();
        assert_eq!(std::str::from_utf8(&output).unwrap(), "FUCK\nSHIT\n");
    }
    #[test]
    fn works_on_day1_both_years() {
        let mut output: Vec<u8> = Vec::new();
        let mut p = Params::new(Some(2021), Some(1));
        let file = fs::read_to_string("../y2021/input/day_01.txt").unwrap();
        let mut input = file.as_bytes();

        run(&mut p, &mut input, &mut output).unwrap();
        assert_eq!(std::str::from_utf8(&output).unwrap(), "1692\n1724\n");

        let mut output: Vec<u8> = Vec::new();
        let file = fs::read_to_string("../y2020/input/day_01.txt").unwrap();
        let mut p = Params::new(Some(2020), Some(1));
        let mut input = file.as_bytes();

        run(&mut p, &mut input, &mut output).unwrap();
        assert_eq!(std::str::from_utf8(&output).unwrap(), "987339\n259521570\n");

    }
    #[test]
    fn works_on_day1_file_only() ->Result<()> {
        let mut p = Params::new(None, None);
        let mut output: Vec<u8> = Vec::new();
        let mut input = "".as_bytes();
        p.optional_file = Some("../y2021/input/day_01.txt".to_string());
        run(&mut p, &mut input, &mut output)?;
        assert_eq!(std::str::from_utf8(&output).unwrap(), "1692\n1724\n");
        Ok(())
    }
    #[test]
    fn works_on_day1_full_args() {
        let mut output: Vec<u8> = Vec::new();
        let year = Some(2021);
        let day = Some(1);
        let runs = 1;
        let parallel = false;
        let mut input = "".as_bytes();
        let optional_file = Some("../y2021/input/day_01.txt".to_string());
        let mut p = Params{year, day, runs, parallel, optional_file};
        run(&mut p, &mut input, &mut output).unwrap();
        assert_eq!(std::str::from_utf8(&output).unwrap(), "1692\n1724\n");
    }
}