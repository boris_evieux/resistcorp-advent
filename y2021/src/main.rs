pub mod days;

// pub fn days() ->Vec<Days<'static>> {
//     days::all().iter().enumerate().map(|(i,d)| Days::runner(d, i+1)).collect()
// }

pub fn main() {
    // let start = Instant::now();
    // let args: Vec<String> = env::args().collect();
    // let mut parallel = false;
    // let mut profile = false;
    // let mut iterations = 1;
    // let mut quiet = false;
    // let mut test = false;
    // let mut run_others = false;
    // let mut days_to_run = 1..=days::all().len();
    // let mut i = 1;
    // loop {
    //     if i >= args.len(){
    //         break;
    //     }
    //     let arg = &args[i];
    //     match &arg[..] {
    //         "--chrono" | "-c" => profile = true,
    //         "--test" | "-t" => test = true,
    //         "--experimental" | "-x" => run_others = true,
    //         "--number" | "-n" => {
    //             i += 1;
    //             let iter_arg = &args[i];
    //             iterations = iter_arg.parse().expect("number of iteration couldn't be parsed");
    //         },
    //         "--quiet" | "-q" => quiet = true,
    //         "--parallel" | "-p" => parallel = true,
    //         "--days" | "-d" => {
    //             i += 1;
    //             let day_arg = &args[i];
    //             let vals : Vec<&str> = day_arg.split("..").collect();

    //             days_to_run = match vals.len() {
    //                 2 => vals[0].parse::<usize>().expect("failed to parse start of day range")..=vals[1].parse().expect("failed to parse end of day range"),
    //                 1 => {
    //                     let single = vals[0].parse::<usize>().expect("failed to parse single day");
    //                     single..=single
    //                 }
    //                 _ => panic!("unrecognized day range")
    //             };
    //         },
    //         _ => println!("unparsed arg : {}", arg)
    //     };
    //     i += 1;
    // }

    // parallel &= iterations > 8;

    // let num_days = days_to_run.end() - days_to_run.start() +1;
    // let run_day = run_day(iterations, parallel);
    // let start_loop = Instant::now();
    // let d = days();
    // let actual_runs : Vec<&Days> = d.iter().filter(|day|{
    //             match day {
    //                 Invalid(_) => false,
    //                 Named(_, idx, _) => days_to_run.contains(idx) && run_others,
    //                 Anon(_, idx) => days_to_run.contains(idx)
    //             }
    //         }).collect();

    // if actual_runs.is_empty() {
    //     println!("nothing to run. exiting after {:?}", start.elapsed());
    //     std::process::exit(0);
    // }

    // let results : Vec<(String, (String, String), Duration)> = if parallel{
    //     actual_runs.into_par_iter().map(|d|run_day(d)).collect()
    // }else{
    //     actual_runs.iter().map(|d|run_day(d)).collect()
    // };
    // if quiet {
    //     println!("engaging quiet mode !");
    // }
    // let mut total_selves = Duration::from_millis(0);
    // for ( day_num, result, time) in results{
    //     let (part1, part2) = result;
    //     if !quiet{
    //         println!("{} /1 :\n{}", day_num, part1);
    //         println!("{} /2 :\n{}", day_num, part2);
    //     }
    //     total_selves += time;
    //     if profile {
    //         println!("{} ran in {:?}", day_num, time);
    //     }
    // }
    // if test {
    //     do_tests(days_to_run, &days());
    // }


    // println!("solved {} days in {}, {} times", num_days, if parallel{"parallel"}else{"serial"}, iterations);
    // if profile {
    //     println!("it took {:?}, addition of self times is {:?}, the program reports a total of {:?}", start_loop.elapsed(), total_selves, start.elapsed());
    // }
}


#[cfg(test)]
mod tests{

    use crate::days;
    use lib::util::do_tests;

    #[test]
    pub fn all_solutions(){
        do_tests(2021, 25..=25, &days::all());
    }
}
