
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0i64,0i64);

	(a.0.to_string(), a.1.to_string())
}
lib::make_main!(2021, 1);
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"#############
#...........#
###B#C#B#D###
  #A#D#C#A#
  #########";
		lib::test_run1!(input, 12521);
	}
}
