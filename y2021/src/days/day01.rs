use lib::make_main;
make_main!(2021, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut p1 = 0;
	let mut p2 = 0;
	let mut prev = (None, None, None);
	for line in data.linesi(){
		let a = line.parse::<i32>().unwrap();
		if let Some(b) = prev.0{
			if a > b {
				p1+=1;
			}
			if let Some(c) = prev.1 {
				if let Some(d) = prev.2 {
					if a+b+c > b+c+d {
						p2+=1;
					}
				}
			}
		}

		prev = (Some(a), prev.0, prev.1);
	}
	(p1.to_string(), p2.to_string())
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"199
200
208
210
200
207
240
269
260
263";
		use lib::test_run;
		test_run!(input, 7, 5);

	}
}
