use lib::make_main;
make_main!(2021, 7);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0, 0);
	let mut positions = data.trim().split(',').map(|s|s.parse::<isize>().unwrap()).collect::<Vec<_>>();
	positions.sort();
	let median = positions[positions.len()/2];
	let mut max = 0;
	for &pos in &positions {
		a.0 += (pos-median).abs();
		if pos > max {
			max = pos;
		}
	}
	a.1 = isize::MAX;
	for i in 0..max {
		let mut fuel = 0;
		for pos in &positions {
			let dist = (pos-i).abs();
			fuel += dist * (dist + 1) / 2;
		}
		if fuel < a.1 {
			a.1 = fuel;
		}
	}
	(a.0.to_string(), a.1.to_string())
}

#[cfg(test)]
mod tests{
	#[test]
	pub fn example(){
		let input = "16,1,2,0,4,2,7,1,2,14";
		lib::test_run!(input, 37, 168);
	}
}
