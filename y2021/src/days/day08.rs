use std::collections::{HashSet,VecDeque};
use lib::make_main;
make_main!(2021, 8);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0, 0i32);
	let mut occ = [0;10];
	let mut unsolved : HashSet<&str> = HashSet::new();
	let mut solves;
	for line in data.linesi() {
		solves = [255u8;10];
		let parts :Vec<&str> = line.split(" | ").collect();
		let part1 = parts[0].trim();
		let part2 = parts[1].trim();
		let digits1  :Vec<&str>= part1.split(" ").collect();
		let digits2  :Vec<&str>= part2.split(" ").collect();
		let end = digits2.len();
		for (i, digit) in digits2.iter().chain(&mut digits1.iter()).enumerate() {
			let pt = str2bits(digit);
			//println("{:?}->{} ({:b})", digit, pt, pt);
			let d = match digit.len() {
				2 => 1,
				3 => 7,
				4 => 4,
				7 => 8,
				_ => 0
			};
			if d != 0 {
				if i < end{
					occ[d] += 1;
				}
				solves[d] = pt;
			}else{
				unsolved.insert(digit);
			}
		}
		//println("first pass {:?}", solves);
		let mut unsolved = unsolved.drain().collect::<VecDeque<&str>>();
		solve(&mut unsolved, &mut solves);
		for i in 0..4 {
			let d = digits2[i];
			let pt = str2bits(d);
			if let Some(pos) = solves.iter().position(|&s| s == pt) {
				//println("digit {} : {:?}", i, pos);
				a.1 += 10_i32.pow(3-(i as u32)) * (pos as i32);
			}else{
				panic!("unsolved pattern {} {:b}", d, pt);
			}
		}
	}
	// let top = solves[7] & !solves[1];
	//println("{:?}", occ);
	a.0 = occ[1] + occ[4] + occ[7] + occ[8];
	(a.0.to_string(), a.1.to_string())
}
fn solve(unsolved : &mut VecDeque<&str>, solves : &mut [u8;10]){
	let mut runs = 0;
	while unsolved.len() > 1 {
		// println!("<<{:?}", solves);
		let last = unsolved.pop_back().unwrap();
		let pt = str2bits(last);
		let mut fil : Vec<&str>= vec![];
		for s in unsolved.iter(){
			if str2bits(&s) != pt {
				fil.push(&s);
			}
		}
		*unsolved = VecDeque::from(fil);
		let nb = pt.count_ones();
		// println!("{:?} ({:b})", last, pt);
		// println!(">>{:?}", unsolved);
		match nb {
			6 => {// 0,9,6
				if (solves[1] & pt) != solves[1] {
					solves[6] = pt;
					//println("{} ->6", last);
				}else if (solves[4] | pt) == pt{
					solves[9] = pt;
					//println("{} ->9", last);
				}else{
					solves[0] = pt;
					//println("{} ->0", last);
				}
			}
			5 => {//2,3,5
				if (solves[1] & pt) == solves[1] {
					solves[3] = pt;
					//println("{} ->3", last);
				}else if (solves[6] != 255) && (solves[6] | pt) == solves[6] {
					solves[5] = pt;
					//println("{} ->5", last);
				}else if (solves[9] != 255) && (solves[9] | pt) == solves[6] {
					solves[5] = pt;
					//println("{} ->5", last);
				}else{
					unsolved.push_front(last);
				}
			}
			_ => panic!("unexpected segments : {:?}", nb)
		}
		// println!("<<{:?}", solves);
		runs += 1;
		if runs > 50 {
			break;
		}
	}
	let last = unsolved.pop_back().unwrap();
	//println("remaining {}", last);
	//println("->2");
	solves[2] = str2bits(last);
	if solves[9] == 255{
		//println("unsolved 9");
	}
	if solves[0] == 255{
		//println("unsolved 0");
	}
	if solves[3] == 255{
		//println("unsolved 3");
	}
}
fn str2bits(pattern : &str)->u8{
	let mut ret = 0;
	for c in pattern.chars() {
		let i = match c {
			'a' => 8,
			'b' => 7,
			'c' => 6,
			'd' => 5,
			'e' => 4,
			'f' => 3,
			'g' => 2,
			'h' => 1,
			_ => panic!("")
		};
		ret |= 1<<(8-i);

	}
	ret
}

#[cfg(test)]
mod tests{
	#[test]
	pub fn example_hard(){
		let input = 
"acedgfb cdfbe gcdfa fbcad dab cefabd cdfgeb eafb cagedb ab | cdfeb fcadb cdfeb cdbaf";
		lib::test_run!(input, 0, 5353);

	}
	#[test]
	pub fn example_soft(){
		let input = 
"be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce";
		lib::test_run!(input, 26, 61229);

	}
}
