use std::ops::RangeInclusive;
use regex::Regex;
use lib::make_main;
make_main!(2021, 1);

// const POOL : [Vec<Region>; 2] = [vec![], vec![]];

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0i64,0i64);
	let mut cubes = vec![false;101*101*101];
	let reg = r();
	for line in data.linesi() {
		let c = reg.captures(line).unwrap();
		let on = &c[1] == "on";
		let rx = p(&c[2], &c[3], -50, 50);
		for x in rx {
			let x = x+50;			
			let ry = p(&c[4], &c[5], -50, 50);
			for y in ry {
				let y = y+50;
				let rz = p(&c[6], &c[7], -50, 50);
				for z in rz {
					let z = z+50;
					if let Some(i) = idx(x,y,z,101) {
						cubes[i] = on
					}
				}
			}
		}
	}
	a.0 = cubes.iter().filter(|&b|*b).count()as i64;
	let mut regions : Vec<Region> = Vec::with_capacity(5000);
	let mut next : Vec<Region> = Vec::with_capacity(5000); 
	for line in data.linesi() {
		// let prev = regions.iter().map(Region::size).sum::<i64>();
		next.clear();
		let new_reg = Region::parse(line);
		//println!("new region {:?}", new_reg);
		if new_reg.on {
			//println!(" >> it's ON! (+{})", new_reg.size());
			next.push(new_reg);
		}
		for old in &regions {
			old.split(&new_reg, &mut next);
		}
		let tmp = next;
		next = regions;
		regions = tmp;
		// println!("there are now {:?} regions", regions.len());
		// let sum = regions.iter().map(Region::size).sum::<i64>();
		//println!(" >>> {} ON cubes ({} from {})", sum, sum-prev, prev);
	}
	//println!("there are {:?} regions", regions.len());
	a.1 = regions.iter().map(Region::size).sum::<i64>();

	(a.0.to_string(), a.1.to_string())
}
fn idx(x : isize, y : isize, z : isize, stride : isize) -> Option<usize> {
	if x < 0 || x > stride {
		None
	}else if y < 0 || y > stride {
		None
	}else if z < 0 || z > stride {
		None
	}else{
		let x = x as usize;
		let y = y as usize;
		let z = z as usize;
		let s = stride as usize;
		Some(z * s * s + y * s + x)
	}
}
fn p(s : &str, e : &str, min : isize, max : isize) -> RangeInclusive<isize> {
	let s = s.parse().unwrap();
	let e = e.parse().unwrap();
	if s > max || e < min {
		0..=-1
	}else{
		let s = isize::max(min, s);
		let e = isize::min(max, e);
		s..=e
	}
}
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
struct Region {
	min_x : isize,
	min_y : isize, 
	min_z : isize,
	max_x : isize,
	max_y : isize, 
	max_z : isize,
	on : bool
}

impl Region {
	fn parse(line : &str) -> Self {
		let reg = r();
		let c = reg.captures(line).unwrap();
		let on = &c[1] == "on";
		let rx = p(&c[2], &c[3], isize::MIN, isize::MAX);
		let ry = p(&c[4], &c[5], isize::MIN, isize::MAX);
		let rz = p(&c[6], &c[7], isize::MIN, isize::MAX);
		let min_x = *rx.start();
		let max_x = *rx.end();
		let min_y = *ry.start();
		let max_y = *ry.end();
		let min_z = *rz.start();
		let max_z = *rz.end();
		Self{on, min_x, max_x, min_y, max_y, min_z, max_z}
	}
	fn size(&self) -> i64 {
		((self.max_x - self.min_x+1) * (self.max_y - self.min_y+1) * (self.max_z - self.min_z+1)) as i64
	}
	fn overlap(&self, other : &Self) -> bool {
		self.min_x <= other.max_x && self.max_x >= other.min_x
		&& self.min_y <= other.max_y && self.max_y >= other.min_y
		&& self.min_z <= other.max_z && self.max_z >= other.min_z
	}
	fn split(&self, other : &Self, into : &mut Vec<Region>) {
		//println!("splitting {:?} with {:?}", self, other);
		let mut remain = *self;
		if !self.overlap(other) {
			//println!("adding whole {:?} (+{})", self, self.size());
			into.push(*self);
		}else{
			if remain.min_x < other.min_x {
				let mut copy = remain;
				copy.max_x = other.min_x-1;
				//println!("adding left_part {:?} (+{})", copy, copy.size());
				into.push(copy);
				remain.min_x = other.min_x;
			}
			if remain.max_x > other.max_x {
				let mut copy = remain;
				copy.min_x = other.max_x+1;
				//println!("adding right_part {:?} (+{})", copy, copy.size());
				into.push(copy);
				remain.max_x = other.max_x;
			}
			if remain.min_y < other.min_y {
				let mut copy = remain;
				copy.max_y = other.min_y-1;
				//println!("adding top_part {:?} (+{})", copy, copy.size());
				into.push(copy);
				remain.min_y = other.min_y;
			}
			if remain.max_y > other.max_y {
				let mut copy = remain;
				copy.min_y = other.max_y+1;
				//println!("adding bottom_part {:?} (+{})", copy, copy.size());
				into.push(copy);
				remain.max_y = other.max_y;
			}
			if remain.min_z < other.min_z {
				let mut copy = remain;
				copy.max_z = other.min_z-1;
				//println!("adding front_part {:?} (+{})", copy, copy.size());
				into.push(copy);
				remain.min_z = other.min_z;
			}
			if remain.max_z > other.max_z {
				let mut copy = remain;
				copy.min_z = other.max_z+1;
				//println!("adding back_part {:?} (+{})", copy, copy.size());
				into.push(copy);
				remain.max_z = other.max_z;
			}
		}
	}
}

static mut R : Option<Regex> = None;
fn r()->&'static Regex{
	
	unsafe{
		if R.is_none() {
			let r = Regex::new(r"^(on|off) x=([\d-]+)..([\d-]+),y=([\d-]+)..([\d-]+),z=([\d-]+)..([\d-]+)$");
			R = r.ok();
		}
		if let Some(r) = &R {
			r
		}else{
			 panic!("no regex!");
		}
	}
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"on x=10..12,y=10..12,z=10..12
on x=11..13,y=11..13,z=11..13
off x=9..11,y=9..11,z=9..11
on x=10..10,y=10..10,z=10..10";
		lib::test_run!(input, 39, 39);
		let input = 
"on x=-20..26,y=-36..17,z=-47..7
on x=-20..33,y=-21..23,z=-26..28
on x=-22..28,y=-29..23,z=-38..16
on x=-46..7,y=-6..46,z=-50..-1
on x=-49..1,y=-3..46,z=-24..28
on x=2..47,y=-22..22,z=-23..27
on x=-27..23,y=-28..26,z=-21..29
on x=-39..5,y=-6..47,z=-3..44
on x=-30..21,y=-8..43,z=-13..34
on x=-22..26,y=-27..20,z=-29..19
off x=-48..-32,y=26..41,z=-47..-37
on x=-12..35,y=6..50,z=-50..-2
off x=-48..-32,y=-32..-16,z=-15..-5
on x=-18..26,y=-33..15,z=-7..46
off x=-40..-22,y=-38..-28,z=23..41
on x=-16..35,y=-41..10,z=-47..6
off x=-32..-23,y=11..30,z=-14..3
on x=-49..-5,y=-3..45,z=-29..18
off x=18..30,y=-20..-8,z=-3..13
on x=-41..9,y=-7..43,z=-33..15
on x=-54112..-39298,y=-85059..-49293,z=-27449..7877
on x=967..23432,y=45373..81175,z=27513..53682";
		lib::test_run1!(input, 590784);
		lib::test_run!("on x=-5..47,y=-31..22,z=-19..33
on x=-44..5,y=-27..21,z=-14..35
on x=-49..-1,y=-11..42,z=-10..38
on x=-20..34,y=-40..6,z=-44..1
off x=26..39,y=40..50,z=-2..11
on x=-41..5,y=-41..6,z=-36..8
off x=-43..-33,y=-45..-28,z=7..25
on x=-33..15,y=-32..19,z=-34..11
off x=35..47,y=-46..-34,z=-11..5
on x=-14..36,y=-6..44,z=-16..29
on x=-57795..-6158,y=29564..72030,z=20435..90618
on x=36731..105352,y=-21140..28532,z=16094..90401
on x=30999..107136,y=-53464..15513,z=8553..71215
on x=13528..83982,y=-99403..-27377,z=-24141..23996
on x=-72682..-12347,y=18159..111354,z=7391..80950
on x=-1060..80757,y=-65301..-20884,z=-103788..-16709
on x=-83015..-9461,y=-72160..-8347,z=-81239..-26856
on x=-52752..22273,y=-49450..9096,z=54442..119054
on x=-29982..40483,y=-108474..-28371,z=-24328..38471
on x=-4958..62750,y=40422..118853,z=-7672..65583
on x=55694..108686,y=-43367..46958,z=-26781..48729
on x=-98497..-18186,y=-63569..3412,z=1232..88485
on x=-726..56291,y=-62629..13224,z=18033..85226
on x=-110886..-34664,y=-81338..-8658,z=8914..63723
on x=-55829..24974,y=-16897..54165,z=-121762..-28058
on x=-65152..-11147,y=22489..91432,z=-58782..1780
on x=-120100..-32970,y=-46592..27473,z=-11695..61039
on x=-18631..37533,y=-124565..-50804,z=-35667..28308
on x=-57817..18248,y=49321..117703,z=5745..55881
on x=14781..98692,y=-1341..70827,z=15753..70151
on x=-34419..55919,y=-19626..40991,z=39015..114138
on x=-60785..11593,y=-56135..2999,z=-95368..-26915
on x=-32178..58085,y=17647..101866,z=-91405..-8878
on x=-53655..12091,y=50097..105568,z=-75335..-4862
on x=-111166..-40997,y=-71714..2688,z=5609..50954
on x=-16602..70118,y=-98693..-44401,z=5197..76897
on x=16383..101554,y=4615..83635,z=-44907..18747
off x=-95822..-15171,y=-19987..48940,z=10804..104439
on x=-89813..-14614,y=16069..88491,z=-3297..45228
on x=41075..99376,y=-20427..49978,z=-52012..13762
on x=-21330..50085,y=-17944..62733,z=-112280..-30197
on x=-16478..35915,y=36008..118594,z=-7885..47086
off x=-98156..-27851,y=-49952..43171,z=-99005..-8456
off x=2032..69770,y=-71013..4824,z=7471..94418
on x=43670..120875,y=-42068..12382,z=-24787..38892
off x=37514..111226,y=-45862..25743,z=-16714..54663
off x=25699..97951,y=-30668..59918,z=-15349..69697
off x=-44271..17935,y=-9516..60759,z=49131..112598
on x=-61695..-5813,y=40978..94975,z=8655..80240
off x=-101086..-9439,y=-7088..67543,z=33935..83858
off x=18020..114017,y=-48931..32606,z=21474..89843
off x=-77139..10506,y=-89994..-18797,z=-80..59318
off x=8476..79288,y=-75520..11602,z=-96624..-24783
on x=-47488..-1262,y=24338..100707,z=16292..72967
off x=-84341..13987,y=2429..92914,z=-90671..-1318
off x=-37810..49457,y=-71013..-7894,z=-105357..-13188
off x=-27365..46395,y=31009..98017,z=15428..76570
off x=-70369..-16548,y=22648..78696,z=-1892..86821
on x=-53470..21291,y=-120233..-33476,z=-44150..38147
off x=-93533..-4276,y=-16170..68771,z=-104985..-24507", 474140, 2758514936282235u64);

	}
}
