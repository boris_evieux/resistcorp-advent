use std::collections::HashMap;
use lib::make_main;
make_main!(2021, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0,0);
	let mut lines = data.linesi();
	let polymer = lines.next().unwrap().to_string();
	let _skip = lines.next();
	let mut splits : HashMap<&str, &str> = HashMap::new();
	let mut pairs : HashMap<String, u64> = HashMap::new();
	let mut occurences : HashMap<&str, u64> = HashMap::new();
	for i in 0..polymer.len() {
		let counter = occurences.entry(&polymer[i..=i]).or_insert(0);
    *counter += 1;
	}

	for i in 1..polymer.len() {
		let pair = &polymer[i-1..=i];
		let counter = pairs.entry(pair.to_string()).or_insert(0);
    *counter += 1;
	}
	for line in lines{
		splits.insert(&line[..2], &line[6..]);
	}
	for step in 1..=40 {
		let mut n = HashMap::<String, u64>::new();

		for (pair, &count) in pairs.iter(){
			let insert = splits.get(&pair[..]).unwrap();
			let counter = occurences.entry(insert).or_insert(0);
	    *counter += count;
	    let mut a = pair[..1].to_string();
	    let mut b = insert.to_string();
	    a.push_str(insert);
	    b.push_str(&pair[1..]);
	    let counter = n.entry(a.to_string()).or_insert(0);
    	*counter += count;
	    let counter = n.entry(b).or_insert(0);
    	*counter += count;
		}

		if step == 10{
			a.0 = calc(&occurences);
		}
		pairs = n;
		// println!("after step {} : {:?}", step, polymer);
	}
	a.1 = calc(&occurences);
	(a.0.to_string(), a.1.to_string())
}
fn calc(occurences : &HashMap::<&str, u64>) -> u64{
	let mut values = occurences.values().collect::<Vec<_>>();
	values.sort();
	*values.last().unwrap() - *values[0]
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";
		use lib::test_run;
		test_run!(input, 1588, 2188189693529u64);
	}
}
