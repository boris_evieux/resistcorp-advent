use lib::make_main;
make_main!(2021, 11);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut a = (0, 0);
	let mut grid = [[0;10]; 10];
	for y in 0..10 {
		for (x, c) in lines[y].chars().enumerate() {
			grid[y][x] = c.to_digit(10).unwrap()
		}
	}

	let mut loops = 0;
	a.1 = loop {
		let mut flashes = 0;
		// if i < 10 || i%10 ==0 {
		// 	println!("after {} steps :", i);
		// 	for y in 0..10 {
		// 		println!("{}", grid[y].iter().map(|&d|char::from_digit(d, 10).unwrap()).collect::<String>());
		// 	}
		// }
		let mut to_flash = vec![];
		let mut flashed = [[false;10];10];
		for y in 0..10 {
			for x in 0..10 {
				grid[y][x] = match grid[y][x] {
					i if i < 9 => i+1,
					9 => {
						flashed[y][x] = true;
						to_flash.push((x, y));
						0
					},
					d => panic!("{}", d)
				}
			}
		}
		while to_flash.len() > 0 {
			let mut n = vec![];
			for (x, y) in to_flash {
				flashes += 1;
				let ry = match y {
					0 => 0..=1,
					9 => 8..=9,
					i => i-1..=i+1,
				};
				for y in ry {
					let rx = match x {
						0 => 0..=1,
						9 => 8..=9,
						i => i-1..=i+1,
					};
					for x in rx {
						if !flashed[y][x]{
							grid[y][x] = match grid[y][x] {
								i if i < 9 => i+1,
								9 => {
										flashed[y][x] = true;
										n.push((x, y));
									0
								},
								_ => panic!("")
							}
						}
					}
				}
			}
			to_flash = n;
		}
		if loops < 100{
			a.0 += flashes;
		}
		loops +=1;
		if flashes == 100 {
			break loops;
		}
	};


	(a.0.to_string(), a.1.to_string())
}

#[cfg(test)]
mod tests{
	use lib::test_run;
	
	#[test]
	pub fn example(){
		let input = 
"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";use lib::test_run;

		test_run!(input, 1656, 195);
	}
}
