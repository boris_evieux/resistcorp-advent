use lib::make_main;
make_main!(2021, 2);

pub fn run_old(data : &DayInput, _p : &Params)->(String, String){
	let mut aim = 0;
	let mut x = (0, 0);
	let mut y = (0, 0);
	for line in data.linesi(){
		let mut  spl = line.split(" ");
		let dir = spl.next();
		let v = spl.next().unwrap().parse::<i32>().unwrap();
		match dir {
			Some("forward") => {
				x.0 += v;
				y.1 += aim * v;
				x.1 += v;
			},
			Some("up") => {
				y.0 -= v;
				aim -= v;
			},
			Some("down") =>{
				y.0 += v;
				aim += v;
			},
			_ => (),
		}
	}
	let a = (x.0 * y.0, x.1 * y.1);
	(a.0.to_string(), a.1.to_string())
}

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let bytes :&[u8] = data.into();
	let mut aim = 0;
	let mut x = (0, 0);
	let mut y = (0, 0);
	const FORWARD : u8 = 102;//"f"
	const UP : u8 = 117;//"u"
	const DOWN : u8 = 100;//"d"
	const F_IDX :usize = 8;
	const U_IDX :usize = 3;
	const D_IDX :usize = 5;
	let mut i = 0;
	let l = bytes.len()-1;
	while i < l {
		if bytes[i] == FORWARD{
			i += F_IDX;
			let v = (bytes[i] - 48) as u64;
			x.0 += v;
			y.1 += aim * v;
			x.1 += v;
		}else if bytes[i] == UP {
			i += U_IDX;
			let v = (bytes[i] - 48) as u64;
			y.0 -= v;
			aim -= v;
		}else if bytes[i] == DOWN {
			i += D_IDX;
			let v = (bytes[i] - 48) as u64;
			y.0 += v;
			aim += v;
		}
		i+=2;
	}
	let a = (x.0 * y.0, x.1 * y.1);
	(a.0.to_string(), a.1.to_string())
}


pub fn run_clever(data : &DayInput, _p : &Params)->(String, String){
	let mut aim = 0;
	let mut x = (0, 0);
	let mut y = (0, 0);
	for line in data.linesi(){
		match &line[..1] {
			"f" => {
				let v = line[8..].parse::<i32>().unwrap();
				x.0 += v;
				y.1 += aim * v;
				x.1 += v;
			},
			"u" => {
				let v = line[3..].parse::<i32>().unwrap();
				y.0 -= v;
				aim -= v;
			},
			"d" =>{
				let v = line[5..].parse::<i32>().unwrap();
				y.0 += v;
				aim += v;
			},
			_ => (),
		}
	}
	let a = (x.0 * y.0, x.1 * y.1);
	(a.0.to_string(), a.1.to_string())
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = "forward 5
down 5
forward 8
up 3
down 8
forward 2";
		use lib::test_run;
		test_run!(input, 150, 900);

	}
}
