use regex::Regex;
use lib::make_main;
make_main!(2021, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let input : &str = data.trim();

	let caps = r().captures(input).unwrap();
	let x_min = caps[1].parse::<i32>().unwrap();
	let x_max = caps[2].parse::<i32>().unwrap();
	let y_min = caps[3].parse::<i32>().unwrap();
	let y_max = caps[4].parse::<i32>().unwrap();

	let max_d = -1-y_min;
	let max_vy = (max_d * (max_d+1)) / 2;


	let mut ok_y = vec![];
	for vyy in y_min..=max_vy {
		let mut y = 0;
		let mut vy = vyy;
		loop {
			if y >= y_min && y <= y_max{
				ok_y.push(vyy);
				break;
			}
			if y < y_min {
				break;
			}
			y += vy;
			vy -=1;
		}
	}
	let mut ok_x = vec![];
	for vxx in 0..=x_max {
		let mut x = 0;
		let mut vx = vxx;
		loop {
			if x >= x_min && x <= x_max{
				ok_x.push(vxx);
				break;
			}
			if x > x_max {
				break;
			}
			x += vx;
			vx -=1;
			if vx <= 0 {
				break;
			}
		}
	}

	let mut a = (max_vy, 0);
	for vxx in &ok_x {
		for vyy in &ok_y {
			let mut vx = *vxx;
			let mut vy = *vyy;
			let mut x = 0;
			let mut y = 0;

			loop {
				x += vx;
				y += vy;
				vy -= 1;
				if vx > 0 {
					vx -= 1;
				}
				if y >= y_min && y <= y_max && x >= x_min && x <= x_max{
					a.1 += 1;
					break;
				}
				if x > x_max || y < y_min {
					break;
				}
			}
		}
	}

	(a.0.to_string(), a.1.to_string())
}
static mut R : Option<Regex> = None;
fn r()->&'static Regex{
	unsafe{
		if R.is_none() {
			 R = Some(Regex::new(r"^target area: x=([\d-]+)..([\d-]+), y=([\d-]+)..([\d-]+)$").unwrap());
		}
		if let Some(r) = &R {
			r
		}else{
			 panic!("no regex!");
		}
	}
}

#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn example(){
		lib::test_run!("target area: x=20..30, y=-10..-5", 45, 112);
	}
}
