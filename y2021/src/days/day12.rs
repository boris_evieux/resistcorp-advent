use std::collections::{HashMap, HashSet};
use lib::{make_main};
make_main!(2021, 12);

struct Node {
	name : String,
	is_big : bool,
	neighbors : Vec<usize>
}
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0, 0);
	let mut graph : Vec<Node> = vec![];
	let mut map : HashMap<String, usize> = HashMap::new();
	for line in data.linesi() {
		let start = line.split('-').nth(0).expect("");
		let end = line.split('-').nth(1).expect("");
		let s_i = if let Some(&i) = map.get(start) {
			i
		}else{
			let name = start.to_string();
			let is_big = name.to_uppercase() == name;
			let neighbors = vec![];
			let n = Node{name, is_big, neighbors};
			let v = graph.len();
			graph.push(n);
			v
		};
		let e_i = if let Some(&i) = map.get(end) {
			i
		}else{
			let name = end.to_string();
			let is_big = name.to_uppercase() == name;
			let neighbors = vec![];
			let n = Node{name, is_big, neighbors};
			let v = graph.len();
			graph.push(n);
			v
		};
		map.insert(start.to_string(), s_i);
		map.insert(end.to_string(), e_i);
		graph[s_i].neighbors.push(e_i);
		graph[e_i].neighbors.push(s_i);
	}
	a.0 = count_paths(&graph, map["start"], &vec![]);
	a.1 = count_paths2(&graph, map["start"], &vec![], false);
	(a.0.to_string(), a.1.to_string())
}

fn count_paths(graph : &[Node], start:usize, traversed : &[usize])->u32{
	let mut visited = HashSet::new();
	visited.insert(start);
	for t in traversed{visited.insert(*t);}
	let mut tr = vec![];
	for t in traversed{tr.push(*t);}
	tr.push(start);
	// let names = tr.iter().map(|&i| &graph[i].name[..]).collect::<Vec<&str>>();
	let node = &graph[start];
	let mut paths = 0;
	for &n in &node.neighbors {
		let node = &graph[n];
		if node.name == "end" {
			paths += 1;
		}else if node.is_big || !visited.contains(&n) {
			paths += count_paths(graph, n, &tr);
		}
	}
	paths
}
fn count_paths2(graph : &[Node], start:usize, traversed : &[usize], did_backoff : bool)->u32{
	let mut visited = HashSet::new();
	visited.insert(start);
	for t in traversed{visited.insert(*t);}
	let mut tr = vec![];
	for t in traversed{tr.push(*t);}
	tr.push(start);
	// let names = tr.iter().map(|&i| &graph[i].name[..]).collect::<Vec<&str>>();
	let node = &graph[start];
	let mut paths = 0;
	for &n in &node.neighbors {
		let node = &graph[n];
		if node.name == "end" {
			paths += 1;
		}else if node.is_big || !visited.contains(&n){
			paths += count_paths2(graph, n, &tr, did_backoff);
		}else if !did_backoff && !node.is_big && node.name != "end" && node.name != "start" {
			paths += count_paths2(graph, n, &tr, true);
		}
	}
	paths
}

#[cfg(test)]
mod tests{
	
	use lib::test_run;
	#[test]
	pub fn example1(){
		let input = 
"start-A
start-b
A-c
A-b
b-d
A-end
b-end";

		use lib::test_run;
		test_run!(input, 10, 36);
	}
	#[test]
	pub fn example2(){
		let input = 
"dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";
		use lib::test_run;
		test_run!(input, 19, 103);
	}
	#[test]
	pub fn example3(){
		let input = 
"fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";

		use lib::test_run;
		test_run!(input, 226, 3509);
	}
}
