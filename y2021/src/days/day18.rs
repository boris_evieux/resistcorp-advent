use std::str::Chars;
use self::SFN::*;

use lib::make_main;
make_main!(2021, 1);

// #[derive(PartialEq, Eq, Clone, Debug)]
// 	struct Root(Vec<SFN>, u8);
// #[derive(PartialEq, Eq, Clone, Debug)]
// 	struct N(u8);
// #[derive(PartialEq, Eq, Clone, Debug)]
// 	struct P(u8,u8);
#[derive(PartialEq, Eq, Clone, Debug)]
enum SFN {
	// Root(Root),
	N(u32),
	P(Box<SFN>, Box<SFN>)
}
impl SFN {
	fn parse(s : &str) -> Self{
		Self::parse_(&mut s.chars())
	}
	fn parse_(chars : &mut Chars) -> Self{
		let f = chars.next().unwrap();
		match f{
			'[' => {
				let l = SFN::parse_(chars);
				if chars.next() != Some(',') { panic!("no comma!");}
				let r = SFN::parse_(chars);
				if chars.next() != Some(']') { panic!("no close!");}
				P(Box::new(l), Box::new(r))
			},
			x if x.is_digit(10) =>{
				N(x.to_digit(10).unwrap())
			},
			c => panic!("unexpected {:?}", c)
		}
	}
	fn to_string(&self) ->String{
		let mut s = String::new();
		self.in_string(&mut s);
		s
	}
	fn is_num(&self) -> bool{
		match self {
			N(_) => true,
			P(_, _) => false
		}
	}
	fn in_string(&self, s :&mut String){
		match self {
			N(n) => s.push_str(&n.to_string()),
			P(l, r) => {
				s.push('[');
				l.in_string(s);
				s.push(',');
				r.in_string(s);
				s.push(']');
			}
		}
	}
	fn add(&mut self, b : Self) {
		let l = match self {
			N(n) => N(*n),
			P(l, r) => P(Box::new(*l.clone()),Box::new(*r.clone())),
		};
		*self = P(Box::new(l), Box::new(b));
		// println!("after add : {:?}", self.to_string());
		self.reduce();
		// println!("after reduce : {:?}", self.to_string());
	}
	fn reduce(&mut self) {
		while self.explode() || self.split(){  }
	}
	fn explode(&mut self) -> bool {
		let mut stack : Vec<(&mut SFN, u32)> = vec![(self, 0)];
		let mut left_most : Option<&mut SFN> = None;
		let mut to_add : Option<u32> = None;
		while stack.len() > 0 {
			let (num, depth) = stack.pop().unwrap();
			match num {
				N(n) => {
					if let Some(m) = to_add {
						*num = SFN::N( *n + m );
						return true;//end of explode
					}
					left_most = Some(num);
				},
				P(l, r) if depth >= 4 && to_add.is_none() => {
					//start explode
					if let N(l) = **l {
						if let N(r) = **r {
							if let Some(N(dest)) = left_most {
								*dest += l;
							}
							*num = SFN::N(0);
							to_add = Some(r)
						}else { panic!("exploding pair is not two nums (R) {}", num.to_string()); }
					}else { panic!("exploding pair is not two nums (L) {}", num.to_string()); }
				}
				P(l, r) => {
					stack.push((r, depth+1));
					stack.push((l, depth+1));
				}
			}
		}
		to_add.is_some()
	}
	fn split(&mut self) -> bool {
		let mut stack : Vec<&mut SFN> = vec![self];
		while stack.len() > 0 {
			let val = stack.pop().unwrap();
			match val {
				N(x) if *x > 9 =>{
					let v = *x as f64;
					*val = P(Box::new(N((v/2.0).floor() as u32)), Box::new(N((v/2.0).ceil() as u32)));
					return true;
				},
				P(l, r) => {
					stack.push(r);
					stack.push(l);
				},
				_ => {}
			}
		}
		false
	}
	fn val(&self) -> u32 {
		match self {
			N(n) => *n,
			P(l, r) => {
				l.val() * 3 + r.val() * 2
			}
		}
	}
}
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0,0);
	let lines = data.linesv();
	let mut num_p1 = SFN::parse(lines[0]);
	for i in 1..lines.len() {
		num_p1.add(SFN::parse(lines[i]));
		let mut nb_a = SFN::parse(lines[i-1]);
		let mut nb_b = SFN::parse(lines[i]);
		nb_a.add(SFN::parse(lines[i]));
		nb_b.add(SFN::parse(lines[i-1]));
		if nb_a.val() > a.1 {
			a.1 = nb_a.val();
		}
		if nb_b.val() > a.1 {
			a.1 = nb_b.val();
		}
	}

	a.0 = num_p1.val();

	(a.0.to_string(), a.1.to_string())
}

#[cfg(test)]
mod tests{
	use super::*;
	
	#[test]
	pub fn example1(){
		lib::test_run1!("[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]
[[[5,[2,8]],4],[5,[[9,9],0]]]
[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]
[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]
[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]
[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]
[[[[5,4],[7,7]],8],[[8,3],8]]
[[9,3],[[9,9],[6,[4,9]]]]
[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]
[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]", 4140);
	}

	#[test]
	pub fn example2(){
		lib::test_run1!("[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]
[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]
[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]
[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]
[7,[5,[[3,8],[1,4]]]]
[[2,[2,2]],[8,[8,1]]]
[2,9]
[1,[[[9,3],9],[[9,0],[0,7]]]]
[[[5,[7,4]],7],1]
[[[[4,2],2],6],[8,7]]", 3488);
	}
	#[test]
	pub fn additions(){
		let mut n = SFN::parse("[1,1]");
		n.add(SFN::parse("[2,2]"));
		n.add(SFN::parse("[3,3]"));
		n.add(SFN::parse("[4,4]"));
		assert_eq!("[[[[1,1],[2,2]],[3,3]],[4,4]]", n.to_string());
		let mut n = SFN::parse("[[[[4,3],4],4],[7,[[8,4],9]]]");
		n.add(SFN::parse("[1,1]"));
		assert_eq!("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]", n.to_string());
	}
	#[test]
	pub fn magnitudes(){
		assert_eq!(SFN::parse("[[1,2],[[3,4],5]]").val(), 143);
		assert_eq!(SFN::parse("[[[[0,7],4],[[7,8],[6,0]]],[8,1]]").val(), 1384);
		assert_eq!(SFN::parse("[[[[1,1],[2,2]],[3,3]],[4,4]]").val(), 445);
		assert_eq!(SFN::parse("[[[[3,0],[5,3]],[4,4]],[5,5]]").val(), 791);
		assert_eq!(SFN::parse("[[[[5,0],[7,4]],[5,5]],[6,6]]").val(), 1137);
		assert_eq!(SFN::parse("[[[[8,7],[7,7]],[[8,6],[7,7]]],[[[0,7],[6,6]],[8,7]]]").val(), 3488);
	}
	#[test]
	pub fn exploding(){
		let mut n = SFN::parse("[[[[[9,8],1],2],3],4]");
		assert!(n.explode());
		assert_eq!("[[[[0,9],2],3],4]", n.to_string());

		let mut n = SFN::parse("[7,[6,[5,[4,[3,2]]]]]");
		assert!(n.explode());
		assert_eq!("[7,[6,[5,[7,0]]]]", n.to_string());

		let mut n = SFN::parse("[[6,[5,[4,[3,2]]]],1]");
		assert!(n.explode());
		assert_eq!("[[6,[5,[7,0]]],3]", n.to_string());

		let mut n = SFN::parse("[[3,[2,[1,[7,3]]]],[6,[5,[4,[3,2]]]]]");
		assert!(n.explode());
		assert_eq!("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]", n.to_string());

		let mut n = SFN::parse("[[3,[2,[8,0]]],[9,[5,[4,[3,2]]]]]");
		assert!(n.explode());
		assert_eq!("[[3,[2,[8,0]]],[9,[5,[7,0]]]]", n.to_string());
	}
	#[test]
	pub fn parsing(){
		let s = "[[1,2],[[3,4],5]]";
		let n = SFN::parse(s);
		let e = n.to_string();
		assert_eq!(e, s);
		// lib::test_run!("target area: x=20..30, y=-10..-5", 45, 112);
	}
}
