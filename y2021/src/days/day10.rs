use lib::make_main;
make_main!(2021, 10);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut a = (0, 0);
	let mut scores2 = vec![];
	let mut nb = 0;
	for line in lines{
		let sc = scores(line);
		a.0 += sc.0;
		if sc.1 > 0 {
			scores2.push(sc.1);
			nb +=1;
		}
	}
	scores2.sort();
	a.1 = scores2[nb/2];
	(a.0.to_string(), a.1.to_string())
}

fn scores(line : &str) -> (u64, u64) {
	let mut stack : Vec<char> = vec![];
	for c in line.chars() {
		match c {
			'(' => stack.push(')'),
			'[' => stack.push(']'),
			'{' => stack.push('}'),
			'<' => stack.push('>'),
			')'|']'|'}'|'>' =>
				if stack.pop() != Some(c){
					return (score(c), 0);
				},
			_ => panic!(""),
		}
	}
	let mut sc = 0;
	stack.reverse();
	for c in stack {
		sc *= 5;
		sc += match c {
			')'=>1,
			']'=>2,
			'}'=>3,
			'>'=>4,
			 _ => panic!("")
		}
	}
	(0, sc)
}
fn score(c : char) -> u64 {
	match c {
		')'=> 3,
		']'=> 57,
		'}'=> 1197,
		'>'=> 25137,
		 _ =>panic!("")
	}
}

#[cfg(test)]
mod tests{

	#[test]
	pub fn example(){
		let input = 
"[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]";

		lib::test_run!(input, 26397, 288957);
	}
}
