use lib::make_main;
make_main!(2021, 3);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut a = (0u64, 0u64);
	let num_sizes = lines[0].len();
	let lines : Vec<u16> = lines.iter().map(|n| u16::from_str_radix(n, 2).unwrap()).collect();
	let total = lines.len();
	let mut frequencies = vec![0; num_sizes];
	for i in 0..num_sizes {
		let msk = 1<<(num_sizes-i-1);
		frequencies[i] = lines.iter().filter(|&line| line & msk !=0).count();
	}
	let mut gamma = 0;
	for i in 0..num_sizes {
		let frq = frequencies[i];
		if frq >= total - frq {
			gamma += 1 << num_sizes-i-1;
		}
	}
	let mask = (1 << num_sizes) -1;
	let epsilon = mask & !gamma;

	let mut cand_ox : Vec<u16>= lines.iter().cloned().collect();
	let mut cand_co2 : Vec<u16>= lines.iter().cloned().collect();
	for i in 0..num_sizes {
		let mask = 1<< num_sizes-i-1;
		if cand_ox.len() > 1 {
			let frq = cand_ox.iter().filter(|&n|n & mask != 0).count();
			let frq = if frq >= cand_ox.len() - frq {mask} else {0};
			cand_ox = cand_ox.iter().filter( |&n| n & mask == frq).cloned().collect();
		}
		if cand_co2.len() > 1 {
			let frq = cand_co2.iter().filter(|&n|n & mask != 0).count();
			let frq = if frq < cand_co2.len() - frq {mask} else {0};
			cand_co2 = cand_co2.iter().filter( |&n| n & mask == frq).cloned().collect();
		}
	}
	a.0 = epsilon * gamma;
	a.1 = cand_co2[0] as u64 * cand_ox[0] as u64;

	(a.0.to_string(), a.1.to_string())
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = "00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010";

		lib::test_run!(input, 198, 230);

	}
}
