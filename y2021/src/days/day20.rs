use itertools::Itertools;
use lib::make_main;
make_main!(2021, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0,0);
	let mut lines = data.linesi();

	let algo = line_2_bline(lines.next().unwrap());
	lines.next();//skip

	let mut grid = lines.map(line_2_bline).collect::<Vec<Vec<bool>>>();

	let mut background = false;
	// print(&grid);
	for i in 0..50 {
		// println!("");
		grid = enhance(&grid, &algo, background);
		background ^= algo[0];
		if i == 1 {
			for l in &grid {
				for v in l {
					if *v {
						a.0 +=1;
					}
				}
			}
		}
		// print(&grid);
	}
	for l in &grid {
		for v in l {
			if *v {
				a.1 +=1;
			}
		}
	}

	(a.0.to_string(), a.1.to_string())
}
fn print(grid : &[Vec<bool>]){
	for line in grid {
		let l = line.iter().map(bool_2_char).join("");
		println!("{}", l);
	}
}
fn line_2_bline(l : &str) -> Vec<bool>
{
	l.chars().map(char_2_bool).collect::<Vec<bool>>()
}
fn char_2_bool(v : char) -> bool
{
	v == '#'
} 
fn bool_2_char(v : &bool) -> char
{
	if *v {'#'} else {'.'}
} 
fn enhance(grid : &[Vec<bool>], algo : &[bool], bcg : bool) -> Vec<Vec<bool>>
{
	let w = grid[0].len();
	let h = grid.len();
	let mut ret = vec![vec![false; w + 2]; h + 2];
	for x in 0..w+2 {
		for y in 0..h+2{
			let idx = idx(grid, (x, y), (w, h), bcg);
			ret[y][x] = algo[idx];
		}
	}
	ret
} 
fn idx(grid : &[Vec<bool>], (ix, iy) : (usize, usize), (w, h) : (usize, usize), b : bool) -> usize {
	let mut ret = 0;
	let w = w as isize;
	let h = h as isize;
	let ix = ix as isize;
	let iy = iy as isize;
	let background = if b {1} else {0};
	for i in 0..9 {
		let x = ix -2 + i % 3;
		let y = iy -2 + i / 3;
		let dark = if x < 0 || y < 0 {
			background
		} else if x >= w || y >= h {
			background
		} else if !grid[y as usize][x as usize] {
			0
		} else {
			1
		};
		ret |= dark << (8-i);
	}
	ret
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..###..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#..#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#......#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#.....####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.......##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#

#..#.
#....
##..#
..#..
..###";
		lib::test_run!(input, 35, 3351);

	}
}
