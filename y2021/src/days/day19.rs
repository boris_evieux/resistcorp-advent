use std::cmp::Ordering::*;
use std::cmp::Ordering;
use std::ops::{Add, Sub};
use std::collections::HashSet;
use lib::make_main;
make_main!(2021, 1);
use itertools::Itertools;
#[derive(PartialEq, Eq, Clone, Copy, Debug)]
struct Point { x:i32, y:i32, z:i32 }
struct Scanner {
	points : Vec<Point>
}
type Rotation = fn(Point)->Point;

impl Point{
	fn new(x : i32, y : i32,z : i32)->Self{
		Self{x, y, z}
	}
}
impl Add for Point{
	type Output = Self;
	fn add(self, other : Self) ->Self {
		let x = self.x + other.x;
		let y = self.y + other.y;
		let z = self.z + other.z;
		Self{x, y, z}
	}
}
impl Sub for Point{
	type Output = Self;
	fn sub(self, other : Self) ->Self {
		let x = self.x - other.x;
		let y = self.y - other.y;
		let z = self.z - other.z;
		Self{x, y, z}
	}
}
impl Ord for Point {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.x != other.x {
        	return self.x.cmp(&other.x)
        }
        if self.y != other.y {
        	return self.y.cmp(&other.y)
        }
        if self.z != other.z {
        	return self.z.cmp(&other.z)
        }
        Equal
    }
}

impl PartialOrd for Point {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}
const ROTATIONS : [Rotation; 24] = [
	|Point{x, y, z}| Point::new(  x,  y,  z), //identity
	|Point{x, y, z}| Point::new(  x, -y, -z), // 
	|Point{x, y, z}| Point::new(  x,  z, -y), //
	|Point{x, y, z}| Point::new(  x, -z,  y), //
	|Point{x, y, z}| Point::new( -x,  y, -z), // 
	|Point{x, y, z}| Point::new( -x, -y,  z), // 
	|Point{x, y, z}| Point::new( -x,  z,  y), // 
	|Point{x, y, z}| Point::new( -x, -z, -y), // end x 

	|Point{x, y, z}| Point::new(  y,  x, -z), // 
	|Point{x, y, z}| Point::new(  y, -x,  z), // 
	|Point{x, y, z}| Point::new(  y,  z,  x), // 
	|Point{x, y, z}| Point::new(  y, -z, -x), // 
	|Point{x, y, z}| Point::new( -y,  x,  z), // 
	|Point{x, y, z}| Point::new( -y, -x, -z), // 
	|Point{x, y, z}| Point::new( -y,  z, -x), // 
	|Point{x, y, z}| Point::new( -y, -z,  x), // end Y

	|Point{x, y, z}| Point::new(  z,  x,  y), // 
	|Point{x, y, z}| Point::new(  z, -x, -y), // 
	|Point{x, y, z}| Point::new(  z,  y, -x), // 
	|Point{x, y, z}| Point::new(  z, -y,  x), // 
	|Point{x, y, z}| Point::new( -z,  x, -y), // 
	|Point{x, y, z}| Point::new( -z, -x,  y), // 
	|Point{x, y, z}| Point::new( -z,  y,  x), // 
	|Point{x, y, z}| Point::new( -z, -y, -x), // end Z
];

fn parse(s : &str) -> Option<i32> {
	s.parse::<i32>().ok()
}
pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0,0);
	let mut lines = data.linesi();
	let mut scanners = vec![];
	let mut unique : HashSet<Point> = HashSet::new();
	let mut i = 0;
	while let Some(line) = lines.next(){
		if line != format!("--- scanner {} ---", i) {
			panic!("not a header line {:?}", line);
		}
		let mut points = vec![];
		while let Some(line) = lines.next(){
			if line != "" {
				let mut spl = line.split(',');
				let x = spl.next().and_then(parse).unwrap();
				let y = spl.next().and_then(parse).unwrap();
				let z = spl.next().and_then(parse).unwrap();
				points.push(Point{x,y,z});
			} else {break;}
			i+=1;
		}
		scanners.push(Scanner{points});
	}

	a.0 = 0;
	(a.0.to_string(), a.1.to_string())
}

#[cfg(test)]
mod tests{
	use super::*;
	
	// #[test]
	pub fn example1(){
		lib::test_run!("", 4140, 3993);
	}
	
	// #[test]
	pub fn comms(){
		let pt = Point::new(-15, 18, 153);
		for rotation in ROTATIONS {
			let rt = rotation(pt);
			let cl = rotation(rt);
			assert_eq!(pt, cl);
			assert_ne!(pt, rt);
		}
	}
}
