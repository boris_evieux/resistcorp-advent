use std::collections::HashSet;
lib::make_main!(2021, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut dots : Vec<(usize, usize)> = vec![];
	let mut folds = vec![];
	let mut passed = false;
	for line in data.linesi() {
		if line == "" {
			passed = true;
		}else if passed {
			folds.push(line);
		}else{
			let mut sp = line.split(",");
			dots.push((sp.next().unwrap().parse().unwrap(), sp.next().unwrap().parse().unwrap()));
		}
	}
	let mut a = (0,String::new());
	let mut grid = fold(folds[0], &dots);
	a.0 = grid.len();
	for &fld in &folds[1..] {
		grid = fold(fld, &grid);
	}
	const DOT: u8 = 0x2E_u8;
	const HSH: u8 = 0x23_u8;
	let mut lines = [[DOT; 39]; 6];
	for (x, y) in grid {
		lines[y][x] = HSH;
	}
	for i in 0..lines.len() {
		let line = lines[i].iter().map(|&x|char::from(x)).collect::<String>();
		a.1.push_str(&line);
		if i < lines.len()-1 {
			a.1.push('\n');
		}
	}
	(a.0.to_string(), a.1)
}

fn fold(line : &str, dots : &[(usize, usize)]) -> Vec<(usize, usize)> {
	let mut grid : HashSet<(usize, usize)> = HashSet::new();
	let val = line[13..].parse::<usize>().unwrap();
	let axis = &line[11..13];
	let func = match axis {
		"x=" => fold_left,
		"y=" => fold_up,
		other => panic!("{:?}", other)
	};
	for dot in dots {
		grid.insert(func(*dot, val));
	}
	grid.into_iter().collect::<Vec<_>>()
}
fn fold_up((x, y) : (usize, usize), val : usize) -> (usize, usize){
	if y<=val { (x,y) } else {(x, 2*val-y)}
}
fn fold_left((x, y) : (usize, usize), val : usize) -> (usize, usize){
	if x<=val { (x,y) } else {(2*val-x, y)}
}

#[cfg(test)]
mod tests{

	#[test]
	pub fn example(){
		let input = 
"6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5";
		use lib::test_run1;
		test_run1!(input, 17);

	}
}
