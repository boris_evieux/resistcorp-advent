type Cell = Option<(usize, usize, u32)>;
use lib::make_main;
make_main!(2021, 9);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut a = (0, 0);
	let w = lines[0].len();
	let h = lines.len();
	let mut blines : Vec<Vec<u32>> = vec![vec![0;w];h];
	for y in 0..lines.len() {
		let line = lines[y].to_string().into_bytes();
		for i in 0..line.len() {
			blines[y][i] = line[i] as u32 - 48;
		}
	}
	let blines = blines; // unmut
	let mut lows = vec![];

	for y in 0..lines.len() {
		let line = &blines[y];
		for x in 0..line.len() {
			let height = line[x];
			let [up, right, down, left] = adjh(&blines, (w,h), (x,y));

			if  height < left &&  height < right && 
					height < up   &&  height < down {
					a.0 += (height + 1) as i32;
					lows.push((x,y, height));
			}
		}
	}
	// let mut merged :Vec<Option<usize>> = vec![None; lows.len()];
	let mut sizes :Vec<u32> = vec![];
	let size = (w,h);

	let mut pointers :Vec<Vec<Option<usize>>> = vec![vec![None; w];h];
	for i in 0..lows.len() {
		let low = lows[i];
		if let Some(p) = pointers[low.1][low.0]{
			panic!(">>already seen : {} !", p);
			continue;
		}
		let basin_i = sizes.len();
		sizes.push(0);
		let mut to_explore = vec![low];
		let mut next = vec![];
		while to_explore.len() > 0 {
			for (x, y, _) in to_explore.drain(..) {
				if pointers[y][x] != None {
					continue;
				}

				pointers[y][x] = Some(basin_i);
				sizes[basin_i] += 1;
				for cell in adj(&blines, size, (x, y)) { if let Some((nx,ny,nh)) = cell {
					if nh < 9 && pointers[ny][nx] == None {
						next.push((nx, ny, nh));
					}
				}}
			}
			let n = next;
			next = to_explore;
			to_explore = n;
		}
	}
	// for i in 0..sizes.len(){
	// 	for y in 0..h {
	// 		let mut line = vec!['.'; w];
	// 		for x in 0..w {
	// 			if pointers[y][x] == Some(i){
	// 				line[x] = (48+blines[y][x] as u8) as char;
	// 			}
	// 		}
	// 		println!("{:?}", line.iter().collect::<String>());
	// 	}
	// }
	sizes.sort();
	let l = sizes.len();
	a.1 = sizes[l-1] * sizes[l-2] * sizes[l-3];


	(a.0.to_string(), a.1.to_string())
}
fn adjh(blines : &[Vec<u32>], size : (usize, usize), pos : (usize, usize)) -> [u32; 4]{
	let val = adj(blines, size, pos);
	let mp = |c|match c{
		None => 10,
		Some((_,_,h))=>h
	};
	val.map(mp)
}
fn adj(blines : &[Vec<u32>], size : (usize, usize), pos : (usize, usize)) -> [Cell; 4]{
	let (w, h) = size;
	let (x, y) = pos;
	let hx = |y : usize|Some((x, y, blines[y][x]));
	let hy = |x : usize|Some((x, y, blines[y][x]));
	let (up, down) = match y {
		0 => (None, hx(y+1)),
		y if y==(h-1) => (hx(y-1), None),
		_ => (hx(y-1), hx(y+1))
	};
	let (left, right) = match x {
		0 => (None, hy(x+1)),
		x if x==(w-1) => (hy(x-1), None),
		_ => (hy(x-1), hy(x+1))
	};
	[up, right, down, left]
}

#[cfg(test)]
mod tests{
	#[test]
	pub fn example(){
		let input = 
"2199943210
3987894921
9856789892
8767896789
9899965678";
		lib::test_run!(input, 15, 1134);

	}
}
