use std::convert::TryInto;
use self::Operand::*;
use self::Var::*;
use self::Op::*;

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0u64,0i64);
	let program = Program::read(data);

	for i in 0..14 {
		for d in 0..9 {
			let r = program.run_digit(i, d+1);
			println!("{}.{} : {:?}({})", i, d, r, r.z == 0);
			let v = 11_111_111_111_111u64 + 10_u64.pow(i as u32) * d as u64;
			let r = program.run(v);
			println!("{} : {:?}({})", v, r, r.z == 0);
		}
	}

	(a.0.to_string(), a.1.to_string())
}

#[derive(Debug)]
enum Operand {
	V(Var),
	C(i64)
}
impl Operand {
	fn read(data : &str) -> Self {
		match data {
			"x" => V(X),
			"y" => V(Y),
			"z" => V(Z),
			"w" => V(W),
			 v  => C(v.parse().unwrap()),
		}
	}
	fn value(&self, mem : &Mem) -> i64 {
		match self {
			V(v) => v.value(mem),
			C(v) => *v,
		}
	}
}
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
struct Mem { x : i64, y : i64, z : i64, w : i64 }
#[derive(Debug)]
enum Var {X,Y,Z,W}
impl Var {
	fn value(&self, mem : &Mem) ->i64 {
		match self {
			X => mem.x,
			Y => mem.y,
			Z => mem.z,
			W => mem.w
		}
	}
	fn save(&self, mem : &mut Mem, value : i64) {
		match self {
			X => mem.x = value,
			Y => mem.y = value,
			Z => mem.z = value,
			W => mem.w = value
		}
	}
}
#[derive(Debug)]
enum Op {
	INP(Var),
	ADD(Var, Operand),
	MUL(Var, Operand),
	DIV(Var, Operand),
	MOD(Var, Operand),
	EQL(Var, Operand),
}
impl Op {
	fn read(data : &str) -> Self {
		let op = &data[..3];
		let a = Operand::read(&data[4..5]);
		let b = if data.len() > 5 { &data[6..] } else { "" };

		if let V(a) = a {
			match op {
				"inp" => INP(a),
				"add" => ADD(a, Operand::read(b)),
				"mul" => MUL(a, Operand::read(b)),
				"div" => DIV(a, Operand::read(b)),
				"mod" => MOD(a, Operand::read(b)),
				"eql" => EQL(a, Operand::read(b)),
				_     => panic!("")
			}
		}else{
			panic!("");
		}
	}
	fn apply(&self, mem : &mut Mem, next : &mut dyn FnMut() -> i64){
		println!("{:?}", self);
		match self {
			INP(a) => a.save(mem, next()),
			ADD(a, b) => {
				let v = a.value(&mem) + b.value(&mem);
				a.save(mem, v)
			},
			MUL(a, b) => {
				let v = a.value(&mem) * b.value(&mem);
				a.save(mem, v)
			},
			DIV(a, b) => {
				let v = a.value(&mem) / b.value(&mem);
				a.save(mem, v)
			},
			MOD(a, b) => {
				let v = a.value(&mem) % b.value(&mem);
				a.save(mem, v)
			},
			EQL(a, b) => {
				let v = if a.value(&mem) ==b.value(&mem) {1} else {0};
				a.save(mem, v)
			},
		}
		println!("{:?}", mem);
	}
}

#[derive(Debug)]
struct Program (Vec<Op>, [usize;15]);
impl Program {
	fn read(data : &DayInput) -> Self {
		let mut ret = vec![];
		let mut inputs : Vec::<usize> = vec![];
		for line in data.linesi() {
			let op = Op::read(line);
			if let INP(_) = op {
				inputs.push(ret.len())
			}
			ret.push(op);
		}
		inputs.push(ret.len());
		Self(ret, inputs.as_slice().try_into().unwrap())
	}
	fn run_digit(&self, it : usize, val : i64) -> Mem {
		let mut mem = Mem::default();
		let st = self.1[it];
		let end = self.1[it+1];
		let r = (st)..end;
		if let INP(_) = self.0[st] {

		} else {
			panic!("{:?}", );
		}
		for i in r {
			self.0[i].apply(&mut mem, &mut ||val)
		}
		mem
	}
	fn run(&self, val : u64) -> Mem {
		let mut mem = Mem::default();
		let s = val.to_string();
		let mut chars = s.chars();
		let mut next = ||chars.next().unwrap().to_digit(10).unwrap() as i64;
		for ope in &self.0{
			ope.apply(&mut mem, &mut next);
		}
		mem
	}
}

lib::make_main!(2021, 1);
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"inp w
add z w
mod z 2
div w 2
add y w
mod y 2
div w 2
add x w
mod x 2
div w 2
mod w 2";
		lib::test_run1!(input, 12521);
	}
}
