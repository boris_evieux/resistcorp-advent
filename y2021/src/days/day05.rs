use core::cmp::{max};
use lib::make_main;
make_main!(2021, 5);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut a = (0,0);
	let mut grid = vec![vec![(0u16, 0u16);1024];1024];
	for line in lines {
		let mut coords = line.split(" -> ");
		let start = coords.next().map(|c| c.split(",").map(|c|c.parse::<usize>().unwrap()).collect::<Vec<usize>>()).unwrap();
		let end = coords.next().map(|c| c.split(",").map(|c|c.parse::<usize>().unwrap()).collect::<Vec<usize>>()).unwrap();
		let start_x = start[0];
		let start_y = start[1];
		let end_x = end[0];
		let end_y = end[1];
		let move_h = end_x as isize - start_x as isize;
		let move_v = end_y as isize - start_y as isize;
		let mag = max(move_h.abs(), move_v.abs());
		let dir_h = move_h.signum();
		let dir_v = move_v.signum();
		let is_h = dir_v == 0;
		let is_v = dir_h == 0;

		for i in 0..=mag {
			let x = (start_x as isize + dir_h * i) as usize;
			let y = (start_y as isize + dir_v * i) as usize;
			grid[y][x].1 += 1;
			if grid[y][x].1 == 2 {
				a.1 += 1;
			}
			if is_h || is_v {
				grid[y][x].0 += 1;
				if grid[y][x].0 == 2 {
					a.0 += 1;
				}
			}
		}
		// for l in &grid[0..10] {
		// 	println!("{}{}{}{}{}{}{}{}{}{}     {}{}{}{}{}{}{}{}{}{}",
		// 		l[0].0,l[1].0,l[2].0,l[3].0,l[4].0,l[5].0,l[6].0,l[7].0,l[8].0,l[9].0,
		// 		l[0].1,l[1].1,l[2].1,l[3].1,l[4].1,l[5].1,l[6].1,l[7].1,l[8].1,l[9].1
		// 		);

		// }

	}
	(a.0.to_string(), a.1.to_string())
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = "0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2";
		lib::test_run!(input, 5, 12);

	}
}
