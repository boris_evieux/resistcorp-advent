use crate::days::day25::Cucumber::*;
use std::iter::Iterator;

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0u64,0i64);
	let mut map : Vec<Vec<Cucumber>> = Vec::with_capacity(150);
	for line in data.linesi() {
		map.push(line.chars().map(make_cucumber).collect())
	}
	let w = map[0].len();
	let h = map.len();
	let mut next = map.to_vec();
	let mut turn = 0;
	print(&map);
	a.0 = loop {
		let mut moved = false;
		next[0] = map[0].to_vec();
		for y in 1..=h {
			let cy = y%h;
			let line = &map[cy];
			if y != h {
				next[cy] = line.to_vec();
			}
			for x in 1..=w {
				let cx = x%w;
				if line[cx] == NoCucumber {
					let left = line[x-1];
					if left == EastBound {
						next[cy][x-1] = NoCucumber;
						next[cy][cx] = EastBound;
						moved = true;
					}else{
						next[cy][cx] = NoCucumber;
					}
				}
			}
		}
		map[0] = next[0].to_vec();
		for y in 1..=h {
			let cy = y%h;
			let line = &next[cy];
			if y != h {
				map[cy] = line.to_vec();
			}
			for x in 0..w {
				if line[x] == NoCucumber {
					let up = next[y-1][x];
					if up == SouthBound {
						map[y-1][x] = NoCucumber;
						map[cy][x] = SouthBound;
						moved = true;
					}else{
						map[cy][x] = NoCucumber;
					}
				}
			}
		}

		turn +=1;
		// println!("\nAfter {} steps:", turn);
		// print(&map);
		if !moved{ break turn; }
	};
	(a.0.to_string(), a.1.to_string())
}
fn make_cucumber(c : char) -> Cucumber {
	match c {
		'.' => NoCucumber,
		'>' => EastBound,
		'v' => SouthBound,
		oth => panic!("not a known cucumber {:?}", oth),
	}
}
fn write_cucumber(c : &Cucumber) -> char {
	match c {
		NoCucumber => '.',
		EastBound => '>',
		SouthBound => 'v',
	}
}
#[derive(PartialEq, Eq, Debug, Copy, Clone)]
enum Cucumber {
	EastBound,
	SouthBound,
	NoCucumber,
}
fn print(map : &Vec<Vec<Cucumber>>) {
	for line in map {
		println!("{}", line.iter().map(write_cucumber).collect::<String>());
	}
}


lib::make_main!(2021, 1);
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"v...>>.vv>
.vv>>.vv..
>>.>v>...v
>>v>>.>.v.
v>v.vv.v..
>.>>..v...
.vv..>.>v.
v.v..>>v.v
....v..v.>";
		lib::test_run1!(input, 58);
	}
}
