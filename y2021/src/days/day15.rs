
use lib::make_main;
make_main!(2021, 1);

// struct Path{
// 	x : usize,
// 	y : usize,
// 	risk : usize
// };
#[derive(Copy, Clone, PartialEq, Eq, Debug)]
struct Pos{x:usize, y:usize}
impl Pos{
	fn new(x : usize, y : usize)->Self{
		Self{x,y}
	}
	fn neighbors(&self, side : usize)->Vec<Self>{
		let mut ret = Vec::with_capacity(8);
		if self.x > 0 {ret.push(Pos::new(self.x-1,self.y))}
		if self.x < side*5-1 {ret.push(Pos::new(self.x+1,self.y))}
		if self.y > 0 {ret.push(Pos::new(self.x,self.y-1))}
		if self.y < side*5-1 {ret.push(Pos::new(self.x,self.y+1))}
		ret
	}
	fn idx<'a>(&self, grid : &'a [usize], side : usize, max : usize) -> usize{
		let Pos{x, y} = *self;
		let mut val = &grid[(y%side) * side + (x%side)] + x/side + y/side;
		// if max == 9{
		// 	println!("{:?} : {}", self, val);
		// }
		while val > max{
			val -= max
		}
		val
	}
	fn idx_mut<'a>(&self, grid : &'a mut [usize], side : usize) -> &'a mut usize{
		&mut grid[self.y * (side*5) + self.x]
	}
}

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0,0);
	let grid : &[u8] = data.into();
	let grid : Vec<usize> = grid.iter().filter(|&b|b>=&48).map(|&b| (b-48) as usize).collect();

	let size = grid.len();
	let side = (size as f64).sqrt() as usize;
	let five_side = side * 5;
	let five_size = size * 25;

	let end1 = Pos::new(side-1, side-1);
	let end2 = Pos::new(five_side-1, five_side-1);
	let mut found = (false, false);
	let mut risks = vec![100_000;five_size];
	risks[0] = 0;
	let calc = |a : &Pos, b : &Pos, risks : &[usize]| a.idx(risks, five_side, 100_000) + b.idx(&grid, side, 9); 
	// println!("{:?}", grid);
	let mut test : Vec<Pos> = Vec::from([Pos::new(0,0)]);
	while test.len() > 0 && found != (true, true) {
		test.sort_by(|a, b|b.idx(&risks, five_side, 100_000).cmp(&a.idx(&risks, five_side, 100_000)));
		let t = test.pop().unwrap();
		// let risk = t.idx(&mut risks, side);
		// println!("t {:?} ({})", t, risk);
		for n in t.neighbors(side) {
			// println!("n {:?}", n);
			let r = calc(&t, &n, &risks);
			let prev = n.idx_mut(&mut risks, side);
			if r < *prev {
				// if n.x > side || n.y > side {
				// 	panic!("{:?}, {}->{}", n, *prev, r);
				// }
				// println!("{:?}, {}->{}", n, *prev, r);
				*prev = r;
				if n == end1 {
					found.0 = true;
				}
				if n == end2 {
					found.1 = true;
				}
				test.push(n);
			}
		}
		// println!("{:?}", risks);
	}
	a.0 = end1.idx(&risks, five_side, 100_000);
	a.1 = end2.idx(&risks, five_side, 100_000);
	(a.0.to_string(), a.1.to_string())
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"1163751742
1381373672
2136511328
3694931569
7463417111
1319128137
1359912421
3125421639
1293138521
2311944581";
		use lib::test_run;
		test_run!(input, 40, 315);
	}
}
