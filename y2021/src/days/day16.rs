use lib::make_main;
make_main!(2021, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	// println!("for input {:?}", <DayInput<'_> as Into<String>>::into(*data));
	let bits = to_bits(data.into());
	let mut it = bits.iter();
	let p = read_packet(&mut it, 0);
	(p.vsm.to_string(), p.val.to_string())
}

fn read_packet (it : &mut dyn Iterator<Item = &bool>, depth : usize) -> Packet {
	let indent = ".".repeat(depth);
	let ver = read_int(it, 3);
	let typ = read_int(it, 3);
	let len = 6;
	let vsm = ver;
	let val = 0;
	let mut packet = Packet{len, ver, vsm, val, typ};
	// println!("{}packet v{}, t{}", indent, ver, typ);
	if typ == 4{
		read_litteral(it, &mut packet)
	}else{
		let subs = if read(it) {
			packet.len += 12;
			let n = read_int(it, 11);
			// println!("{} {} children", indent, n);
			let mut v = vec![];
			for _ in 0..n{
				v.push(read_packet(it, depth+1));
			}
			v
		}else{
			// tye ID 0
			let l = read_int(it, 15);
			// println!("{} {} bytes", indent, l);
			let mut read = 0;
			let mut v = vec![];
			packet.len += 16;
			while read < l {
				let p = read_packet(it, depth+1);
				read += p.len;
				v.push(p);
			}
			v
		};
		let mut vals = vec![];
		for p in subs {
			packet.len += p.len;
			packet.vsm += p.vsm;
			vals.push(p.val);
		}
		packet.val = do_op(typ, &vals, depth);
	};
	// println!("{}<<{} ({}) - {}", indent, packet.val, packet.len, packet.vsm);
	packet
}

fn read_litteral (it : &mut dyn Iterator<Item = &bool>, p : &mut Packet) {
	loop{
		let cont = read(it);
		p.val <<= 4;
		p.val += read_int(it, 4);
		p.len +=5;
		if !cont { break; }
	}
}

fn do_op (typ : u64, vals : &[u64], depth : usize) -> u64 {
	let indent = ".".repeat(depth);
	let ret = match typ {
		0 => {
			// println!("{}sum : {:?} ", indent, vals);
			vals.iter().sum()
		},//sum
		1 => {
			// println!("{}product : {:?} ", indent, vals);
			vals.iter().product()
		},//product
		2 => {
			// println!("{}min : {:?} ", indent, vals);
			*vals.iter().min().unwrap()
		},//min
		3 => {
			// println!("{}max : {:?} ", indent, vals);
			*vals.iter().max().unwrap()
		},//max
		5 => {
			// println!("{}gt : {:?} ", indent, vals);
			if vals[0] > vals[1] { 1 } else { 0 }
		},//gt
		6 => {
			// println!("{}lt : {:?} ", indent, vals);
			if vals[0] < vals[1] { 1 } else { 0 }
		},//lt
		7 => {
			// println!("{}lt : {:?} ", indent, vals);
			if vals[0] == vals[1] { 1 } else { 0 }
		},//lt
		x => panic!("unknown op {:?}", x)
	};
	ret
}

fn read_int (it : &mut dyn Iterator<Item = &bool>, l : u8) -> u64 {
	let mut ret = 0;
	for exp in 0..l {
		if read(it) {
			ret += 1 << (l-1-exp);
		}
	}
	ret
}

fn read(it : &mut dyn Iterator<Item = &bool>) -> bool{
	*it.next().unwrap()
}

fn to_bits(input : &[u8]) -> Vec<bool> {
	input.iter().flat_map( hex2_bits ).collect()
}
fn hex2_bits(input : &u8) -> Vec<bool> {
	let bits = match *input {
		48 => [0,0,0,0],
		49 => [0,0,0,1],
		50 => [0,0,1,0],
		51 => [0,0,1,1],
		52 => [0,1,0,0],
		53 => [0,1,0,1],
		54 => [0,1,1,0],
		55 => [0,1,1,1],
		56 => [1,0,0,0],
		57 => [1,0,0,1],
		65 => [1,0,1,0],
		66 => [1,0,1,1],
		67 => [1,1,0,0],
		68 => [1,1,0,1],
		69 => [1,1,1,0],
		70 => [1,1,1,1],
		10|13 => [0,0,0,0],
		 _ => panic!("")
	};
	let ret = bits.iter().map(|&i| i != 0).collect::<Vec<_>>();
	ret
}
#[derive(Default)]
struct Packet{
	len : u64,
	ver : u64,
	typ : u64,
	vsm : u64,
	val : u64
}

#[cfg(test)]
mod tests{
	use super::*;
	#[test]
	pub fn example_lit(){
		let bits = to_bits("D2FE28".as_bytes());
		let mut it = bits.iter();
		let mut p = Packet::default();
		let ver = read_int(&mut it, 3);
		let typ = read_int(&mut it, 3);
		read_litteral(&mut it, &mut p);
		assert_eq!(6, ver);
		assert_eq!(4, typ);
		assert_eq!(2021, p.val);

	}
	use lib::test_run1;
	#[test]
	pub fn example_1(){
		let input = "EE00D40C823060";
		test_run1!(input, 14);
	}
	#[test]
	pub fn example_2(){
		let input = "8A004A801A8002F478";
		test_run1!(input, 16);
	}
	#[test]
	pub fn example_3(){
		let input = "620080001611562C8802118E34";
		test_run1!(input, 12);
	}
	#[test]
	pub fn example_4(){
		let input = "C0015000016115A2E0802F182340";
		test_run1!(input, 23);

	}
	#[test]
	pub fn example_5(){
		let input = "A0016C880162017C3686B18A3D4780";
		test_run1!(input, 31);

	}
	use lib::test_run2;
	#[test]
	pub fn part2(){
		test_run2!("C200B40A82", 3);
		test_run2!("04005AC33890", 54);
		test_run2!("880086C3E88112", 7);
		test_run2!("CE00C43D881120", 9);
		test_run2!("D8005AC2A8F0", 1);
		test_run2!("F600BC2D8F", 0);
		test_run2!("9C005AC2F8F0", 0);
		test_run2!("9C0141080250320F1802104A08", 1);
	}
}
