use lib::make_main;
make_main!(2021, 6);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0, 0);

	let mut state = [0u64; 9];
	for v in data.trim().split(","){
		let i :usize = v.parse().unwrap();
		state[i] += 1;
	}
	
	for i in 0..256{
		if i == 80 {
			a.0 = state.iter().sum();
		}
		let spawn = state[0];
		for i in 0..8{
			state[i] = state[i+1]
		}
		state[6] += spawn;
		state[8] = spawn;
	}
	a.1 = state.iter().sum();

	(a.0.to_string(), a.1.to_string())
}

#[cfg(test)]
mod tests{
	
	
	#[test]
	pub fn example(){
		let input = "3,4,3,1,2";
		lib::test_run!(input, 5934, 26984457539i64);
	}
}
