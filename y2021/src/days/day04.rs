use lib::make_main;
make_main!(2021, 4);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	run_old(data, _p)
}
pub fn run_new(data : &DayInput, _p : &Params)->(String, String){
	(0.to_string(),0.to_string())
}
pub fn run_old(data : &DayInput, _p : &Params)->(String, String){
	let lines = &data.linesv();
	let mut a = (0u64, 0u64);
	let numbers : Vec<u8>= lines[0].split(",").map(|s| s.trim().parse::<u8>()).map(Result::unwrap).collect();
	let boards_num : usize = (lines.len()-1) / 6;
	let mut boards : Vec<[[u8;5];5]> = vec![[[0;5];5];boards_num];
	let mut map : Vec<Vec<(usize, usize, usize)>> = Vec::with_capacity(256);
	for _ in 0..256 {
		map.push(vec![]);
	}
	for i in 0..boards_num {
		for y in 0..5 {
			let line = lines[2+i*6+y];
			for x in 0..5 {
				let num = line[x*3..x*3+2].trim().parse::<u8>().unwrap();
				boards[i][x][y] = num;
				map[num as usize].push((i,x,y));
			}
		}
	}

	for n in numbers {
		let i = n as usize;
		for &(b, x, y) in &map[i] {
			let board = &mut boards[b];
			if check(board){
				continue;
			}

			board[x][y] = 255;
			if check(board) {
				a.1 = score(board, n);
				if a.0 == 0 {
					a.0 = a.1;
				}
			}
		}
	}

	(a.0.to_string(), a.1.to_string())
}

fn check(b : &[[u8;5];5])->bool{
	for i in 0..5 {
		let mut col = 0;
		let mut row = 0;
		for j in 0..5 {
			col += b[i][j]as u32;
			row += b[j][i]as u32;
		}
		if col == (255 * 5) || row == (255 * 5) {
			return true;
		}
	}
	false
}
fn score(b : &[[u8;5];5], called : u8)->u64{
	let mut sum = 0;
	for row in b {
		for &num in row {
			sum += match num {
				255 => 0,
				v => v as u64
			}
		}
	}
	sum * called as u64
}

#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = "7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1

22 13 17 11  0
 8  2 23  4 24
21  9 14 16  7
 6 10  3 18  5
 1 12 20 15 19

 3 15  0  2 22
 9 18 13 17  5
19  8  7 25 23
20 11 10 24  4
14 21 16 12  6

14 21 17 24  4
10 16 15  9 19
18  8 23 26 20
22 11 13  6  5
 2  0 12  3  7";
		lib::test_run!(input, 4512, 1924);
	}
}
