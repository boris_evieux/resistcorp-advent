use std::collections::HashMap;
use lib::make_main;
make_main!(2021, 1);

pub fn run(data : &DayInput, _p : &Params)->(String, String){
	let mut a = (0,0);
	let mut die = 0;
	let mut lines = data.linesi();

	let mut scores = [0, 0];
	let start_pos = [
		lines.next().unwrap().replace("Player 1 starting position: ", "").parse::<u8>().unwrap()-1,
		lines.next().unwrap().replace("Player 2 starting position: ", "").parse::<u8>().unwrap()-1
	];
	let mut positions = [start_pos[0] as usize, start_pos[1] as usize];

	let mut turns = 0;
	a.0 = loop {
		let player = turns %2;
		turns += 1;

		let roll = roll(&mut die);
		positions[player] += roll;
		scores[player] += (positions[player]%10)+1;

		if scores[0] >= 1000 {
			break die * scores[1];
		}
		if scores[1] >= 1000 {
			break die * scores[0];
		}
	};

	let mut scores : HashMap::<[u8;4], u64> = HashMap::new();
	let mut next : HashMap::<[u8;4], u64> = HashMap::new();
	scores.insert([0, start_pos[0], 0, start_pos[1]], 1);
	let mut universes_won = [0u64, 0u64];
	let mut turn = 0;
	let mut rolls :Vec<(u8,u8,u8)> = vec![];
	for a in 1..=3 {
		for b in 1..=3 {
			for c in 1..=3 {
				rolls.push((a, b, c));
			}
		}
	}
	let rolls = rolls;//un-mut
	while cont(&scores) {
		next.clear();
		let mut won = (0,0);
		for ([sc1, pos1, sc2, pos2], num) in scores.iter() {
			for (a, b, c) in &rolls {
				let die = a+b+c;
				let [sc1, pos1, sc2, pos2] = if turn % 2 == 0 {
					let pos1 = (*pos1+die)%10;
					let sc1 = *sc1 + 1 + pos1;
					[sc1, pos1, *sc2, *pos2]
				}else{
					let pos2 = (*pos2+die)%10;
					let sc2 = *sc2 + 1 + pos2;
    			[*sc1, *pos1, sc2, pos2]
				};
				if sc1 >= 21 {
					won.0 += num;
				} else if sc2 >= 21 {
					won.1 += num;
				} else {
					let universes = next.entry([sc1, pos1, sc2, pos2]).or_insert(0);
					*universes += num;
				}
			}
		}
		// println!("pl 1 wins in {} + {:?} universes", universes_won[0], won.0);
		// println!("pl 2 wins in {} + {:?} universes", universes_won[0], won.1);
		turn +=1;
		// let total = next.values().sum::<u64>()+won.0+won.1+universes_won[0]+ universes_won[1];
		// assert_eq!(3u64.pow(turn), total, "after turn {}", turn);
		let tmp = scores;
		scores = next;
		next = tmp;
		universes_won[0] += won.0;
		universes_won[1] += won.1;
		// println!("{:?}", scores);
	}
	a.1 = if universes_won[0]>universes_won[1] {
		universes_won[0]
	}else{
		universes_won[1]
	};

	(a.0.to_string(), a.1.to_string())
}
fn cont(scores : &HashMap::<[u8;4], u64>) -> bool {
	scores.len() > 0
}
fn roll(die : &mut usize) -> usize {
	let val = *die;
	*die +=3;
	let ret1 = (val+1)%100;
	let ret2 = (val+2)%100;
	let ret3 = (val+3)%100;
	ret1+ret2+ret3
}
#[cfg(test)]
mod tests{
	
	#[test]
	pub fn example(){
		let input = 
"Player 1 starting position: 4
Player 2 starting position: 8";
		lib::test_run!(input, 739785, 444356092776315u64);

	}
}
