use lib::util::Params;
use std::fs;
use criterion::{criterion_group, criterion_main, Criterion};

fn day_00(c: &mut Criterion) {
}
fn day_01(c: &mut Criterion) {
    use ::y2021::days::day01::run;
    let p = Params::new(None, None);
    let input = fs::read("./input/day_01.txt").expect("file!!!");
    let input = input[..].into();
    c.bench_function("day 1 with vec ", |b| b.iter(|| run(&input, &p)));
}// criterion_group!(benches, all_days);


fn day_22(c: &mut Criterion) {
    let p = Params::new(None, None);
    let input = fs::read_to_string("./input/day_22.txt").expect("file!!!");
    let input = input[..].into();
    use ::y2021::days::day22::run;
    let mut group = c.benchmark_group("day_22");
    group.bench_function("day 22 normal ", |b| b.iter(|| run(&input, &p)));
    group.finish();
}// criterion_group!(benches, all_days);


fn day_04(c: &mut Criterion) {
    let p = Params::new(None, None);
    let input = fs::read_to_string("./input/day_04.txt").expect("file!!!");
    let input = input[..].into();
    use ::y2021::days::day04::run_old;
    use ::y2021::days::day04::run_new;
    let mut group = c.benchmark_group("day_04");
    group.bench_function("day 4 normal ", |b| b.iter(|| run_old(&input, &p)));
    group.bench_function("day 4 new ", |b| b.iter(|| run_new(&input, &p)));
    // group.bench_function("day 2 without vec ", |b| b.iter(|| run_old(&input, &p)));
    // group.bench_function("day 2 without vec and optim", |b| b.iter(|| run_clever(&input, &p)));
    group.finish();
}// criterion_group!(benches, all_days);


fn day_02(c: &mut Criterion) {
    let p = Params::new(None, None);
    let input = fs::read_to_string("./input/day_02.txt").expect("file!!!");
    let input = input[..].into();
    // use ::y2021::days::day02::run_clever;
    // use ::y2021::days::day02::run_old;
    use ::y2021::days::day02::run;
    let mut group = c.benchmark_group("day_02");
    group.bench_function("day 2 bytes ", |b| b.iter(|| run(&input, &p)));
    // group.bench_function("day 2 without vec ", |b| b.iter(|| run_old(&input, &p)));
    // group.bench_function("day 2 without vec and optim", |b| b.iter(|| run_clever(&input, &p)));
    group.finish();
}// criterion_group!(benches, all_days);

criterion_group!(benches, day_22);
criterion_main!(benches);

